$(function() {
    // ランキングカテゴリ読み込み
    var url = '/blog/common/get_category/';
    
    $.ajax({
        type: 'GET',
        url:  url,
        dataType: 'json',
        timeout: 1000000,
        success: function(ret, status, xhr) {
            $.each(ret, function(i, val) {
                $('.ranking_category li.sub ul').append('<li><a href="/page2/" data-id="' + i + '">' + val + '</a></li>');
            });
            
            $('.ranking_category li.sub ul li a').click(function() {
                var selected = $(this).text();
                if (selected.length > 7) {
                    selected = selected.substr(0, 6) + "…";
                }
                //$('.ranking_category li.sub span').text($(this).text());
                $('.ranking_category li.sub span').text(selected);
                getRankingData($(this).attr('data-id'));
                return false;
            });
        },
        error: function() {
        }
    });
    
    // ランキング取得
    getRankingData(0);
    
    $("#tab02 li").click(function() {
        $('.ranking_category li.sub span').text('全て');
        $("#tab02 li").removeClass('active');
        $(this).addClass('active');
        getRankingData(0);
    });
});

function getRankingData(category_id) {
    var type = 1;
    $('.ranking_btn ul li').each(function(index, element) {
        if ($(this).hasClass('active')) {
            type = index + 1;
        }
    });
    
    var url = '/blog/ranking/get_ranking_blog/?type=' + type + '&category_id=' + category_id;
    $.ajax({
        type: 'GET',
        url: url,
        dataType: 'json',
        timeout: 1000000,
        success: function(ret, status, xhr) {
            $('.ranking_category li.sub').removeClass('click');
            $('.ranking_block_01').remove();
            $('.ranking_block_02').remove();
            
            var fragment = $("<div/>");
            var top10    = $("<ul/>");
            var count    = 0;
            $.each(ret, function(i, val) {
                count++;
                if (i <= 3) {
                    $("<div/>").attr("class", "ranking_block_01")
                    .append(
                        $("<p/>").attr("class", "no" + i).append(
                            $("<a/>").attr("href", "/page2/" + val["id"]).text(val["name"])
                        )
                    ).append(
                        $("<div/>").attr("class", "image").append(
                            $("<a/>").attr("href", "/page2/" + val["id"]).append(
                                $("<img/>").attr({
                                    src:    val["image"],
                                    alt:    val["name"],
                                    width:  "87",
                                    height: "65"
                                })
                            )
                        )
                    ).appendTo(fragment);
                } else {
                    $("<li/>").attr("class", "no" + i).append(
                        $("<a/>").attr("href", "/page2/" + val["id"]).text(val["name"])
                    ).appendTo(top10);
                }
            });
            
            if (count === 0) {
                fragment.append($("<div/>").attr("class", "ranking_block_01").text("ランキングがありません"));
            }
            if (3 < count) {
                fragment.append($("<div/>").attr("class", "ranking_block_02").append(top10));
            }
            $('.content_wrap_02 .right_category_block').after(fragment);
            
            $('#category_overlay').remove();
        },
        error: function() {
            $('.ranking_category li.sub').removeClass('click');
            $('.ranking_block_01').remove();
            $('.ranking_block_02').remove();
        }
    });
}
