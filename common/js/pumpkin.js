/*
Pumpkin
*/
(function (win, doc, $) {

  'use strict';

  var devMode = false;

  var console = win.console || {
    log: function(){},
    degug: function(){},
    error: function(){}
  };

  function Pumpkin(x, y, v, r, rv, cvs){
    this.x = x || 0;
    this.y = y || 0;
    this.v = v || 1;
    this.r = r || 0;
    this.rv = rv || 1;
    this.cvs = cvs;

    this._width = 0;
    this._height = 0;

    this._img = new Image();

    var imgs;
    if(devMode === true){
      imgs = [
        'images/pumpkin.png',
        'images/coala.png',
      ];
    }else{
      imgs = [
        '/common/images/halloween/pumpkin.png',
        '/common/images/halloween/coala.png'
      ];
    }
    var img_src = imgs[~~(Math.random() * imgs.length)];
    /* console.log(img_src); */
    this._img.src = img_src;

    var self = this;
    this._img.onload = function(){
      self._width = this.width;
      self._height = this.height;
    };
  }

  var RADIAN = Math.PI / 180;
  Pumpkin.prototype = {
    up: function(){
      this.y -= this.v;
    },
    down: function(){
      this.y += this.v;
    },
    right: function(){
      this.x += 1;
    },
    left: function(){
      this.x -= 1;
    },
    rotate: function(){
      this.r -= 2 * this.rv;
    },
    update: function(){
      if(this.cvs.height < this.y - this._height){
        this.resetPos();
      }
      this.down();
      this.rotate();
    },
    resetPos: function(){
      this.x = Math.random() * this.cvs.width;
      this.y = - this._height;
    },
    draw: function(ctx){
      ctx.save();
      ctx.translate(this.x, this.y);
      ctx.rotate(this.r * RADIAN);
      ctx.drawImage(this._img, -(this._width / 2), -(this._height / 2) /*, (this._width / 2), (this._width / 2)*/);
      ctx.restore();
    },
    getHalf: function(){

    }
  };


  function PumpkinContainer(){
    this.pumpkins = [];
  }

  PumpkinContainer.prototype = {
    add: function(pumpkin){
      this.pumpkins.push(pumpkin);
    },
    each: function(configure){
      if(typeof configure === 'function'){
        for(var i=0, len = this.pumpkins.length; i < len; i++){
          configure(this.pumpkins[i]);
        }
      }
    }
  };


  var Animation = (function(){
    var raf = window.requestAnimationFrame ||
    window.webkitRequestAnimationFrame ||
    window.mozRequestAnimationFrame ||
    window.oRequestAnimationFrame ||
    function(callback) {
      setTimeout(callback, 15);
    };

    var winW;
    var winH;
    var targetW;
    var targetH;

    var cvs;
    var ctx;
    var pumpkin_container;

    function init(){
      winW = $(win).width();
      winH = $(win).height();
      targetW = $('#main').width();
      targetH = $('#main').height();
      cvs = $('<canvas>').attr({
        'id': 'pumpkin-container',
        'width': winW,
        'height': targetH
      }).css({
        'position': 'absolute',
        'top': 80,
        'left': 0,
        'overflow': 'hidden'
      });

      if(!cvs[0].getContext){
        return;
      }

      ctx = cvs[0].getContext('2d');
      pumpkin_container = new PumpkinContainer();
      var pumpkin_number = 10;
      var angles = [0, 90, 180];
      for(var i=0; i < pumpkin_number; i++){
        var px = Math.random() * winW;
        var py = Math.random() * targetH;
        var pv = ~~(Math.random() * 2 + 2);
        var pr = angles[~~(Math.random() * angles.length)];
        var prv = i % 2 == 0 ? -1 : 1;
        var pumpkin = new Pumpkin(px, py, pv, pr, prv, cvs[0]);
        pumpkin_container.add(pumpkin);
      }

      loop();
      $('body').prepend(cvs);
    }

    function loop(){
      ctx.clearRect(0, 0, winW, targetH);
      pumpkin_container.each(function(pumpkin){
        pumpkin.update();
        pumpkin.draw(ctx);
      });

      raf(loop);
    }

    function resizeEvent(){
      winW = $(win).width();
      winH = $(win).height();
      if(cvs) cvs[0].width = winW;
    }

    $(win).on('resize', function(e){
      resizeEvent();
    }).trigger('resize');

    $(win).on('load', function(e){
      $('#header').css({
        'position': 'relative'
      });
      $('#footer').css({
        'position': 'relative'
      });
      $('#main').css({
        'position': 'relative'
      });

      init();
    });
  }());

}(window, document, jQuery));