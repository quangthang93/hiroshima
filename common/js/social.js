// ソーシャルアクション用処理
var social = {
    /**
     * AJAX の結果に含まれる status_code のパターン一覧
     */
    status: {
        /**
         * 入力 OK
         */
        OK        : 0,
        
        /**
         * 必須パラメータが空欄であることを示すエラーコード
         */
        EMPTY     : 1,
        
        /**
         * 存在しない店舗コード・ブログID・記事IDが指定されたことを示すエラーコード
         */
        INVALID   : 2,
        
        /**
         * ログインしていないことを示すエラーコード
         */
        NOT_LOGIN : 3,
        
        /**
         * サーバーエラー
         */
        ERROR     : 99
    },
    
    /**
     * このユーザーがログイン済の場合のみ true
     */
    isLogin:      false,
    
    /**
     * このユーザーがログイン済で、かつ twitter 連携している場合のみ true
     */
    linksToTwitter:      false,
    
    /**
     * このユーザーがログイン済で、かつ facebook 連携している場合のみ true
     */
    linksToFacebook:     false,
    
    /**
     * このユーザーがログイン済で、かつ LIKE と facebook いいねを連携している場合のみ true
     */
    acceptsFacebookLike: false,
    
    // 店舗コード
    store_code: "",
    
    // ブログID
    blog_id: -1,
    
    // ブログパス名
    blog_path: "",
    
    // 特設ブログパス名変換用配列
    special_blog_paths: {},
    
    // 特設ブログID変換用配列
    special_blog_ids: {},
    
    // 記事ID
    post_id: -1,
    
    // お気に入り登録ボタンjQueryパス
    follow_btn_jquery_path: "",
    
    // お気に入り登録ボタン画像jQueryパス
    follow_btn_img_jquery_path: "",
    
    // お気に入り登録件数jQueryパス
    follow_number_jquery_path: "",
    
    // お気に入り登録ボタン画像パス
    follow_btn_img_path: "/shop/images/shop_btn_001",
    
    // お気に入り登録ボタンクリック時
    onClickShopFollow: function() {
        // ロック
        
        // お気に入り状態変更呼び出し
        $.ajax({
            type: "post",
            url:  "/social/follow/switch/",
            data: {
                "store_code": common.storeCode,
                "blog_id": social.blog_id
            },
            dateType: "json",
            success: function(data) {
                if (data.error_code == 0) {
                    // 現在のお気に入り件数の取得
                    var fav_number_text = $(social.follow_number_jquery_path).first().text().replace(/人/,'');
                    var fav_number = parseInt(fav_number_text);
                    
                    var img_path = social.follow_btn_img_path;
                    
                    // 変更後のお気に入りが登録状態であれば画像は登録状態、件数は+1
                    if (data.follow_id) {
                        fav_number++;
                        img_path += "_follow";
                    } else {
                        fav_number--;
                    }
                    
                    // 件数の変更
                    $(social.follow_number_jquery_path).text(fav_number + "人");
                    
                    // 画像の変更
                    $(social.follow_btn_jquery_path).css('background', "url(\""+img_path+"_on.png\")");
                    $(social.follow_btn_img_jquery_path).attr('src', img_path+".png");
                    $(social.follow_btn_img_jquery_path).attr('osrc', img_path+"_on.png");
                    $(social.follow_btn_img_jquery_path).attr('psrc', img_path+".png");
                } else if(data.error_code == social.status.NOT_LOGIN) {
                    // ログインしていない場合は、その場でログインモーダル表示
                    social.openLoginBox();
                }
            } ,
            error: function() {}
        });
    },
    openLoginBox: function() {
        $.fancybox.open({
            href: "/page2/social/login?store_code=" + common.storeCode + "&next=" + location.pathname,
            type: "iframe",
            fitToView: false,
            iframe: {
                scrolling: "no"
            }
        });
    },
    changeDisplay: function(button, parentClass, enabled, shift) {
        if (button[0] != null) {
            for (var i = 0; i < button.length; i ++) {
                social.changeDisplay(button[i], parentClass, enabled, shift);
            }
            return;
        }
        
        shift  = (shift == null) ? 0 : parseInt(shift, 10);
        
        var div       = $(button).closest("." + parentClass);
        var number    = $(".like_number", div);
        var value     = parseInt(number.text(), 10);
        var newValue  = isNaN(value) ? shift : value + shift;
        number.text(Math.max(0, newValue));
        
        var imgAttr = enabled ?
        {
            src:  "/common/images/common_post_icon_001_act.gif",
            psrc: "/common/images/common_post_icon_001_act.gif",
            osrc: "/common/images/common_post_icon_001_act_on.gif"
        }
        :
        {
            src:  "/common/images/common_post_icon_001.gif",
            psrc: "/common/images/common_post_icon_001.gif",
            osrc: "/common/images/common_post_icon_001_on.gif"
        };
        $("img", button).attr(imgAttr);
    },
    
    // LIKEボタンクリック時
    // @param className LIKEボタンの親クラス名(配下に LIKE 数を含む)
    onClickLike: function(blog_id, post_id, className, button, another) {
        $.ajax({
            type : "POST",
            url  : "/social/favorite/switch",
            data : {
                store_code: common.storeCode,
                blog_id:    blog_id,
                post_id:    post_id
            },
            complete : function(xhr) {
                var status = social.status;
                var json   = common.getJsonByXHR(xhr, {error_code: status.ERROR, error_status: "システムエラーが発生しました。"});
                switch (json.error_code) {
                    case status.OK:
                        // LIKE 解除の場合はお気に入り総数を -1, LIKE した場合はお気に入り総数を +1 します。
                        // count プロパティが null の場合は解除とみなします。
                        var enabled   = (json.favorite_id);
                        var increment = enabled ? 1 : -1;
                        
                        if (social.acceptsFacebookLike) {
                            // 記事のURL取得
                            var blog_path = blog_id;
                            if (social.special_blog_ids[blog_id] != undefined) {
                                blog_path = social.special_blog_ids[blog_id];
                            }
                            var url = "http://"+ location.hostname+ "/page2/"+ blog_path+ "/"+ post_id+ "/";
                            // FBいいね登録（解除はサーバ側で）
                            if (enabled) {
                                $.ajax({
                                    type: "POST",
                                    url: "https://graph.facebook.com/me/og.likes",
                                    data: {
                                        "object": url,
                                        "access_token": social.accessTokenFacebook
                                    },
                                    dataType: "json",
                                    success: function(data) {
                                        // 登録した記事お気に入り情報にLike IDを追加する
                                        $.ajax({
                                            type: "POST",
                                            url: "/social/favorite/setfblike/",
                                            data: {
                                                "favorite_id": json.favorite_id,
                                                "facebook_like_id": data.id
                                            },
                                            dataType: "json",
                                            success: function(data) {
                                            }
                                        });
                                    }
                                });
                            }
                        }
                        social.changeDisplay(button, className, enabled, increment);
                        if (another != null) {
                            social.changeDisplay(another, className, enabled, increment);
                        }
                        
                        if (enabled && $.cookie("no_more_help") == null) {
                            $.fancybox.open({
                                href: "/page2/social/favorite/done", 
                                type:"iframe"
                            });
                        }
                        return false;
                    case status.NOT_LOGIN:
                        social.openLoginBox();
                        return false;
                    default:
                        return false;
                }
            }
        });
    },
    
    // シェアボタンクリック時
    onClickShare: function(shop_id, post_id) {
        if (social.isLogin) {
            // ポータルサイトの場合は store_code をパラメータに付与
            var q = (location.hostname == common.storeDomain) ? "" : "?store_code=" + common.storeCode;
            $.fancybox.open({
                href: "/page2/social/share/" + shop_id + "/" + post_id + q, 
                type:"iframe"
            });
        } else {
            social.openLoginBox();
        }
    },
    // 友達に送るボタンクリック時
    onClickSend: function(shop_id, post_id) {
        if (social.isLogin) {
            // ポータルサイトの場合は store_code をパラメータに付与
            var q = (location.hostname == common.storeDomain) ? "" : "?store_code=" + common.storeCode;
            $.fancybox.open({
                href: "/page2/social/send/" + shop_id + "/" + post_id + q, 
                type:"iframe"
            });
        }else {
            social.openLoginBox();
        }
    },
    
    // 記事一覧用の初期表示 /social/favorite/get用のパラメータを指定
    initPostList: function(fav_param_posts){
        $.ajax({
            type: "post",
            url: "/social/favorite/get/",
            data: {"ids": fav_param_posts},
            dataType: "json",
            success: function(data) {
                if (data.error_code == 0) {
                    for (var i=0; i<data.favorites.length; i++) {
                        if (data.favorites[i] != null && data.favorites[i].favorite_id != "") {
                            $("div#post_"+ i+ " li.lefticon img").attr("src", "/common/images/common_post_icon_001_act.gif");
                            $("div#post_"+ i+ " li.lefticon img").attr("psrc", "/common/images/common_post_icon_001_act.gif");
                            $("div#post_"+ i+ " li.lefticon img").attr("osrc", "/common/images/common_post_icon_001_act_on.gif");
                        }
                    }
                }
            }
        });
    },
    
    // 初期化
    init: function(){
        // 店舗コード取得、ドメイン名から簡易的に取得
        var hostname = location.hostname;
        if(!hostname.match(/parco.jp$/)){
            hostname = "hiroshima.parco.jp";
        }
        
        // ブログID取得
        var matched = location.pathname.match(/^\/page2\/([^\/]+)\/?/);
        if (matched != null) {
            social.blog_id = matched[1];
            // 取得したブログIDが特設ブログのパス名だった場合はIDに変換
            if (social.special_blog_paths[social.blog_id] != undefined) {
                social.blog_path = social.blog_id;
                social.blog_id   = social.special_blog_paths[social.blog_id];
            }
        }
        
        // 記事ID取得
        var post_matched = location.pathname.match(/^\/page2\/[^\/]+\/([0-9]+)\/?/);
        if (post_matched != null) {
            social.post_id = post_matched[1];
        }
        
        // jQueryパスを決定
        social.follow_btn_jquery_path = 'div.shop_info_block ul.clear li.text_icon a';
        social.follow_btn_img_jquery_path = 'div.shop_info_block ul.clear li.text_icon a img';
        social.follow_number_jquery_path = 'div.shop_info_block ul.clear li.fav_number';
        
        // ショップ情報のページならお気に入り状態取得呼び出し
        if(social.blog_id > 0 && social.blog_id < 100000){
            // お気に入りブログ登録ボタンクリック時の処理割り当て
            $(social.follow_btn_jquery_path).click(social.onClickShopFollow);
        }
    }
};

$(document).ready(function(){
    social.init();
});
