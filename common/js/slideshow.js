(function($ , window){
	var print = window.console ? console.log : function(){} //alert;
	$.fn.ParcoTopBnr = function(options){
		var option = $.extend({}, $.fn.defaultOption, options);
		var mainBnrSelector;
		var thumbBnrSelector;
		var preIndex;
		var bnrIndex;
		var bnrMax;
		var timerId;
		var _main;
		var _thumb;
		var clickDelay;
		var mouseOnIndex;
		var animationFlag;

		/** 初期化**/
		function initialize(){
			mainBnrSelector  = option.mainSelector;
			thumbBnrSelector = option.thumbSelector;
			if($(mainBnrSelector).size() < 1 || $(thumbBnrSelector) < 1){
				return;
			}
			_main  = $(mainBnrSelector);
			_thumb = $(thumbBnrSelector);
			bnrMax        = _main.find("li").size();
			bnrIndex      = 0;
			preIndex      = bnrIndex;
			mouseOnIndex  = 0;
			animationFlag = false;
			mainSetting();
			thumbSetting();
			if(bnrMax == 1) return;
			setTimer();
		}

		/**メイン画像のセッティング**/
		function mainSetting(){
			_main.each( function(){
				$(this).find("li").each(function(){
					if($(this).index() == bnrIndex){
						$(this).css({"display": "block"})
					}else{
						$(this).css({"display": "none"})
					}
				});
			});
		}

		/** サムネイルのセッティング**/
		function thumbSetting(){
			changeThumbCurrent();
			if(bnrMax <= 1){
				$(".prevbtn").css({"display":"none"});
				$(".nextbtn").css({"display":"none"});
				return;
			}else if(bnrMax <= 3){
				$(".prevbtn").css({"display":"none"});
				$(".nextbtn").css({"display":"none"});
			}else{
				$(".prevbtn a").bind("click", upBtnClickEvent);
				$(".nextbtn a").bind("click", downBtnClickEvent);
			}

			_thumb.each( function(){
				$(this).find("li").bind("mouseover",thumbOverEvent);
				$(this).find("li").bind("mouseout" ,thumbOutEvent);
			})
		}

		/**メイン画像のフェード処理**/
		function mainSlide(){
//			print(preIndex);
//			print(bnrIndex);
			if(bnrMax <= 1) return;
			_main.find("li:eq("+ preIndex +")").stop(true, true).fadeOut(option.duration);
			_main.find("li:eq("+ bnrIndex +")").stop(true, true).fadeIn(option.duration);
		}

		/**サムネイルスライドイベント**/
		function thumbSlide(){
			thumbMoveDown();
		}

		/**サムネイル上移動**/
		function thumbMoveDown(){
			var _first = _thumb.find("li:first");
			var _last  = _thumb.find("li:last");
			animationFlag = true;
			_first.stop(true, true);
			_first.animate({
				"marginTop": -99
			},{ complete: function(){
				_main.find("li:eq("+ $(this).index() +")").appendTo(_main).hide();
				$(this).appendTo(_thumb).css("marginTop", 0);
				animationFlag = false;
			}})
		}

		/**サムネイル下移動**/
		function thumbMoveUp(){
			var _first = _thumb.find("li:first");
			var _last  = _thumb.find("li:last");
			animationFlag = true;
			_last.stop(true, true);
			_main.find("li:eq("+ _last.index() +")").insertBefore(_main.find("li:first"));
			_last.insertBefore(_first).css({"marginTop":-99})
			_thumb.find("li:first").animate({
				"marginTop": 0
			},{ complete: function(){
				animationFlag = false;
			}})
		}

		/**サムネイルのカレントクラスを変更**/
		function changeThumbCurrent(){
			_thumb.find("li").each(function(){
				if($(this).index() == bnrIndex){
					$(this).addClass("current");
				}else{
					$(this).removeClass("current");
				}
			})
		}

		/**サムネイルのマウスオーバー処理**/
		function thumbOverEvent(event){
			if(animationFlag === true) return;
			clearTimer();
			if(mouseOnIndex == $(this).index()) return;
			if(3 < bnrMax){
				preIndex = mouseOnIndex;
				bnrIndex = $(this).index();
			}else{ // 3枚以下の場合はこっち
				preIndex = bnrIndex;
				bnrIndex = $(this).index();
			}
			mainSlide();
			changeThumbCurrent();
			mouseOnIndex = $(this).index();
		}

		/**サムネイルのマウスアウト処理**/
		 function thumbOutEvent(event){
		 	clearTimer();
			setTimer();
		 }

		/**↑ボタンクリック処理**/
		function downBtnClickEvent(event){
			event.preventDefault();
			if(animationFlag === true) return;
			clearTimer();
			preIndex = bnrIndex;
			bnrIndex = mouseOnIndex ? mouseOnIndex + 1 : 1;
			thumbMoveDown();
			mainSlide();
			changeThumbCurrent();
			clearTimeout(clickDelay);
			clickDelay = setTimeout(function(){
				clearTimer();
				setTimer();
			},option.clickIntervalDelay);
		}

		/**↓ボタンクリック処理**/
		function upBtnClickEvent(event){
			event.preventDefault();
			if(animationFlag === true) return;
			clearTimer();
			preIndex = mouseOnIndex ? mouseOnIndex + 1 : 1;
			bnrIndex = mouseOnIndex ? mouseOnIndex : preIndex - 1;
			thumbMoveUp();
			mainSlide();
			changeThumbCurrent();
			clearTimeout(clickDelay);
			clickDelay = setTimeout(function(){
				clearTimer();
				setTimer();
			},option.clickIntervalDelay);
		}

		/**インターバルで実行**/
		function setTimer(){
			timerId = setInterval(function(){
				if(3 < bnrMax){
					preIndex   = mouseOnIndex ? mouseOnIndex : bnrIndex;
					bnrIndex   = mouseOnIndex ? mouseOnIndex + 1 : 1;
					thumbSlide();
				}else{ // 3枚以下の場合はこっち
					preIndex = bnrIndex;
					bnrIndex = bnrMax - 1 < bnrIndex + 1 ? 0 : bnrIndex + 1;
				}
				mainSlide();
				changeThumbCurrent();
			}, option.interval);
		}

		/**タイマーのクリア**/
		function clearTimer(){
			clearInterval(timerId);
		}

		initialize();
	}

	/****/
	$.fn.defaultOption = { "interval"          : 3000,
						   "duration"          : 300,
						   "clickIntervalDelay": 3000
	}
})(jQuery, window);

