//スマホ対応
$(function(){
  var agent = navigator.userAgent;
  if(agent.search(/iPhone/) != -1 || agent.search(/iPod/) != -1 || agent.search(/Android/) != -1){
    //aタグに「class="spLink"」を指定
    $('a.spLink').each(function(){
      var pcURL = $(this).attr('href');
      var spURL = pcURL.replace(/page/g,'spage');
      $(this).attr('href',spURL)
    });
  }
});
