
/*==========================================================================================*/
/* thumbnaillistviewer                                                                       */
/*==========================================================================================*/

  /* globalMenuBox
  -------------------------------------------------------- */
  $(function(){

    var maxViewItem = 3;
    var itemMgb = 15;
    var $main = $(".blog_main_image li");
    var $list = $(".blog_image_item li");
    var $btn = $(".blog_image_list .btn");
    var overList = 0;
    var overListheight =0;
    var itemSize = 110;
    var mainItemSize = 520;

    $(window).load(function () {

      $('.blog_main_image li').each(function(){

        var $this = $(this).find('img');
        var imgw = $this.width();
        var imgh = $this.height();

        if( imgw > imgh){
          $this.css('width',mainItemSize);
          var vh = Math.floor(imgh / (imgw/mainItemSize));
          $this.css('height',vh);
        }else if( imgh > imgw){
          $this.css('height',mainItemSize);
          var vw = Math.floor(imgw / (imgh/mainItemSize));
          $this.css('width',vw);
        }else{
           $this.css('width',mainItemSize);
           $this.css('height',mainItemSize);
        }

      });

    $(".blog_image_item li img").each(function(){

      var imgw = $(this).width();
      var imgh = $(this).height();

      if(imgw > imgh){
        $(this).height(itemSize);

        var vw = Math.floor(imgw / (imgh/itemSize));
        $(this).css('width',vw);

        var p = Math.floor(((vw-itemSize)/2)*-1);
        $(this).css('left',p);

      }else if(imgh > imgw){
        $(this).width(itemSize);

        var vh = Math.floor(imgh / (imgw/itemSize));
        $(this).css('height',vh);

        var p = Math.floor(((vh-itemSize)/2)*-1);
        $(this).css('top',p);

      }else{
        $(this).height(itemSize);
        $(this).width(itemSize);
      }
    });

    var h = itemMgb + $list.height();

    if($list.length > maxViewItem){
      var wrapH = h*maxViewItem;
      overList = $list.length - maxViewItem;
      overListheight = overList*h*-1;
      $(".blog_image_item_wrap").height(wrapH);
      $(".blog_image_list .btn.next").removeClass('disabled');

    }else{
      var wrapH = h*$list.length;
      $(".blog_image_item_wrap").height(wrapH);
    }


    $list.on('click', function(){
      var index = $list.index(this);
      $list.each(function(){ $(this).removeClass('active'); });
      $(this).addClass('active');
      $main.each(function(){ $(this).removeClass('active'); });
      $main.eq(index).addClass('active');
    });

    $btn.on('click', function(){
      var thispos = $(".blog_image_item").position().top;
      var $this = $(this);

      if($this.hasClass("move")){
        return false;
      }else{
        if($this.hasClass("next") && overListheight < thispos){
          $this.addClass('move');
          $(".blog_image_item").stop(true, true).animate({ top: '-='+ h },300,function(){
              thispos = $(".blog_image_item").position().top;
              if(overListheight > thispos){
                $(".blog_image_list .btn.next").addClass('disabled');
              }else if( overListheight == thispos ){
                $(".blog_image_list .btn.next").addClass('disabled');
                $(".blog_image_list .btn.prev").removeClass('disabled');
              }else{
                $(".blog_image_list .btn").removeClass('disabled');
              }
              $this.removeClass('move');
          });
        }else if($this.hasClass("prev") && thispos < 0){
          $this.addClass('move');
          $(".blog_image_item").stop(true, true).animate({ top: '+='+ h },300,function(){
              thispos = $(".blog_image_item").position().top;
              if(thispos > 0){
                $(".blog_image_list .btn.prev").addClass('disabled');
              }else if( thispos == 0 ){
                $(".blog_image_list .btn.prev").addClass('disabled');
                $(".blog_image_list .btn.next").removeClass('disabled');
              }else{
                $(".blog_image_list .btn").removeClass('disabled');
              }
              $this.removeClass('move');
          });
        }
      }
      return false;
    });

  });

});