var BlogList = (function() {
    var defaults = {
        floor_id      : "all",
        item_id       : "all",
        event_id      : "all",
        turn_flag     : 1,
        event_date    : "all",
        favorite_flag : 0,
        blog_id       : null,
        base          : null,
        wrapper       : "wrapper_class_name",
        keep          : 0,
        parts         : "shop-multi-columns-main",
        firstCount    : 4,
        increment     : 10,
        hot_blog_flag : false,
        onDisplayAll : function(container) { 
                    $(".more_post",container).remove();
        },
        onFetch       : function() { }
    };
    
    /**
     * コンストラクタ
     * @param   object options
     */
    var BlogList = function(options) {
        this.options = $.extend({}, defaults, options);
        if (typeof(this.options.base) !== "object" && typeof(console) !== "undefined") {
            console.log("必須オプション base が定義されていません");
        }
        this.onFetch        = this.options.onFetch;
        this.onHotblogFetch = this.options.onHotblogFetch;
        this.onDisplayAll   = this.options.onDisplayAll;
    };
    
    var p = BlogList.prototype;
    
    /**
     * AJAX アクセスで記事一覧を取得し、画面に表示します
     */
    p.fetch = function() {
        var options = this.options;
        var self    = this;
        $.ajax({
            url      : "/page2/ajax/getpart/" + options.parts,
            type     : "POST",
            dataType : "html",
            data     : {
                blog    : {
                    store_id      : common.storeCode,
                    floor_id      : options.floor_id,
                    item_id       : options.item_id,
                    event_id      : options.event_id,
                    turn_flag     : options.turn_flag,
                    event_date    : options.event_date,
                    favorite_flag : options.favorite_flag
                },
                blog_id : options.blog_id,
                keep    : options.keep
            },
            success  : function(data) {
                var boundary = "<!--POST_BOUNDARY-->";
                self.index = 0;
                if (-1 < data.indexOf(boundary)) {
                    self.posts = data.split(boundary);
                    self.posts.pop();
                } else {
                    self.posts = [data];
                }
                self.displayReset();
                self.displayMore(options.firstCount, options.hot_blog_flag);
                self.initMoreButton(options.hot_blog_flag);
                self.onFetch();
            }
        });
    };
    
    /**
     * ブログ記事一覧の表示領域を初期化します
     */
    p.displayReset = function() {
        var base = this.options.base;
        if (typeof(base) !== "object") {
            return;
        }
        
        base.html("");
        base.append($("<div/>").addClass(this.options.wrapper).addClass("clear"));
    };
    
    /**
     * ブログ記事一覧を格納するコンテナ要素を返します。
     * @returns jQuery
     */
    p.getContainer = function() {
        return $("." + this.options.wrapper, this.options.base);
    };
    
    /**
     * 指定された個数だけ、次の記事を表示させます
     * @param int count
     */
    p.displayMore  = function(count) {
        var base = this.options.base;
        if (typeof(base) !== "object") {
            return;
        }
        
        var self      = this;
        var container = this.getContainer();
        var next      = Math.min(this.index + count, this.posts.length);
        for (var i = this.index; i < next; i ++) {
            container.append(this.posts[i]);
        }
        if(typeof imgLiquid != "undefined"){
            $('.imgLiquidBox').imgLiquid();
        }
        if (this.options.hot_blog_flag === false ) {
            $(".each_post_block img", container).imagesLoaded(function() {
                var scroll = $("body").scrollTop();
                if ($(".masonry-brick", container).length > 0) {
                    container.masonry("destroy");
                }
                container.masonry({
                    itemSelector: ".each_post_block"
                });
                self.resize();
                $("body").scrollTop(scroll);
            });
        } else {
            $(".fixHeight").fixHeight();
        }
        this.index = next;
    };
    
    /**
     * 表示領域のリサイズを行います
     */
    p.resize = function() {
        var margin    = 20;
        var maxHeight = 0;
        var container = this.getContainer();
        $(".each_post_block", container).not(".content_wrap.disnon .each_post_block").each(function() {
            var top    = parseInt($(this).css("top"));
            var height = $(this).height() + top;
            
            if (maxHeight <= height) {
                maxHeight = height;
            }
        });
        
        maxHeight += margin;
        
        container.css("height", maxHeight);
    };
    
    p.initMoreButton = function() {
        var increment = this.options.increment;    // 増加量

        // 表示しているブログブロック数を取得します
        var blockNum = this.posts.length;
        var container = this.getContainer();
        // 表示件数がfirstCount件以上の場合処理を行います
        if (blockNum > this.options.firstCount) {
            $("<p/>").addClass("more_post").append(
                $("<a/>").attr("href", "javascript:void(0);").append(
                    $("<img/>").attr({
                        "src"   : "/common/images/common_moreposts_btn.png",
                        "alt"   : "もっと見る",
                        "class" : "rollover"
                    })
                )
            ).appendTo(container);
            var self = this;
            $(".more_post a", container).click(function() {
                // 表示件数を増加します
                self.displayMore(increment);

                // 表示件数が表示件数以上になった場合
                if (self.index >= blockNum) {
                     self.onDisplayAll(container);
                }
            });
        }
    };
    return BlogList;
})();