<?php
//基本情報の取得
//include($_SERVER['APPROOT_DIR']. '/api/classes/Config.class.php');

if (isset($_COOKIE["blog"])) {
    $floor = $_COOKIE["blog"]["floor_id"];
    $item  = $_COOKIE["blog"]["item_id"];
    $event = $_COOKIE["blog"]["event_id"];
    $turn  = (int) $_COOKIE["blog"]["turn_flag"];
    $date  = $_COOKIE["blog"]["event_date"];
    $fav   = (int) $_COOKIE["blog"]["favorite_flag"];
} else {
    $floor = "all";
    $item  = "all";
    $event = "all";
    $turn  = 2;
    $date  = "all";
    $fav   = 0;
}

header("Content-Type: text/javascript");
?>

//=====記事サーチSTART=====
function shop_show_Post(){
    //ページが読み込まれたら記事を表示
    $(function(){
        var floor    = '<?php echo $floor; ?>';
        var item     = '<?php echo $item;  ?>';
        var event    = '<?php echo $event; ?>';
        var turn     =  <?php echo $turn;  ?>;
        var date     = '<?php echo $date;  ?>';
        var favorite =  <?php echo $fav;   ?>;
        top_postSearch(floor, item, event, turn, date, favorite);
        // パフォーマンスチューニング
        new BlogList({
            wrapper    : "blog_list_block",
            firstCount : 8,
            base       : $(".content_wrap"),
            parts      : $("#post_parts_name").val(),
            floor_id      : floor,
            item_id       : item,
            event_id      : event,
            turn_flag     : turn,
            event_date    : date,
            favorite_flag : favorite,
            onFetch : function(container) { 
                $(".more_post",container).remove();
                displayListButton( $('.blog_list_block') );
            },
            onDisplayAll : function(container) { 
                $(".more_post",container).remove();
                displayListButton( container );
            }
        }).fetch();
    });

    // 記事を絞り込む
    function top_postSearch(floor, item, event, turn, date, favorite) {
    // アイテムカテゴリーで記事を絞り込む
        $('div.blog_search_window .search_words:first ul.clear li').click(function(){
            item = $(this).attr('id');
            addSearchLabel('item', $(this).children('a').text());
            new BlogList({
                wrapper    : "blog_list_block",
                firstCount : 4,
                base       : $(".content_wrap"),
                parts      : $("#post_parts_name").val(),
                floor_id      : floor,
                item_id       : item,
                event_id      : event,
                turn_flag     : turn,
                event_date    : date,
                favorite_flag : favorite,
                onDisplayAll : function(container) { 
                    $(".more_post",container).remove();
                    displayListButton( container );
                }
            }).fetch();
        });
    // イベント・セールで記事を絞り込む
        $('div.blog_search_window .search_words:last ul.clear li').click(function(){
            event = $(this).attr('id');
            addSearchLabel('event', $(this).children('a').text());
            new BlogList({
                wrapper    : "blog_list_block",
                firstCount : 4,
                base       : $(".content_wrap"),
                parts      : $("#post_parts_name").val(),
                floor_id      : floor,
                item_id       : item,
                event_id      : event,
                turn_flag     : turn,
                event_date    : date,
                favorite_flag : favorite,
                onDisplayAll : function(container) { 
                    $(".more_post",container).remove();
                    displayListButton( container );
                }
            }).fetch();
        });
    // 新着順・LIKE数順で絞り込む
        $('ul#tab.common_3tabbtn li.tab_btn').click(function(){
            var flag = $(this).attr('class');
            if (flag == 'tab_btn tab_002') {
                turn = 2;
                new BlogList({
                    wrapper    : "blog_list_block",
                    firstCount : 4,
                    base       : $(".content_wrap"),
                    parts      : $("#post_parts_name").val(),
                    floor_id      : floor,
                    item_id       : item,
                    event_id      : event,
                    turn_flag     : turn,
                    event_date    : date,
                    favorite_flag : favorite,
                    onDisplayAll : function(container) { 
                        $(".more_post",container).remove();
                        displayListButton( container );
                    }
                }).fetch();
            }else if(flag == 'tab_btn tab_003 last'){
                turn = 0;
                new BlogList({
                    wrapper    : "blog_list_block",
                    firstCount : 4,
                    base       : $(".content_wrap"),
                    parts      : $("#post_parts_name").val(),
                    floor_id      : floor,
                    item_id       : item,
                    event_id      : event,
                    turn_flag     : turn,
                    event_date    : date,
                    favorite_flag : favorite,
                    onDisplayAll : function(container) { 
                        $(".more_post",container).remove();
                        displayListButton( container );
                    }
                }).fetch();
            }
        });
    // カレンダーの日付で絞り込む
        $('#mycarousel li').click(function(){
            date = $(this).attr('id');
            addSearchLabel('date', $(this).children('a').text());
            new BlogList({
                wrapper    : "blog_list_block",
                firstCount : 4,
                base       : $(".content_wrap"),
                parts      : $("#post_parts_name").val(),
                floor_id      : floor,
                item_id       : item,
                event_id      : event,
                turn_flag     : turn,
                event_date    : date,
                favorite_flag : favorite,
                onDisplayAll : function(container) { 
                    $(".more_post",container).remove();
                    displayListButton( container );
                }
            }).fetch();
        });

        //お気に入りイベント
        $('div.bl_cb').click(function(){
            var favorite_check = $('#like01');
            if(favorite_check.prop("checked") != favorite){
                if(favorite_check.is(":checked")){
                    favorite = 1;
                        new BlogList({
                            wrapper    : "blog_list_block",
                            firstCount : 4,
                            base       : $(".content_wrap"),
                            parts      : $("#post_parts_name").val(),
                            floor_id      : floor,
                            item_id       : item,
                            event_id      : event,
                            turn_flag     : turn,
                            event_date    : date,
                            favorite_flag : favorite,
                            onDisplayAll : function(container) { 
                                $(".more_post",container).remove();
                                displayListButton( container );
                            }
                        }).fetch();
                }else{
                    favorite = 0;
                        new BlogList({
                            wrapper    : "blog_list_block",
                            firstCount : 4,
                            base       : $(".content_wrap"),
                            parts      : $("#post_parts_name").val(),
                            floor_id      : floor,
                            item_id       : item,
                            event_id      : event,
                            turn_flag     : turn,
                            event_date    : date,
                            favorite_flag : favorite,
                            onDisplayAll : function(container) { 
                                $(".more_post",container).remove();
                                displayListButton( container );
                            }
                        }).fetch();
                }
            }
        });
        
        // 絞り込み解除
        $(".ct_labelBox .peel").live("click", function(){
            var search_label = $(this).parent().attr("class");
            if(search_label == "label_item"){
                item = "all";
            }else if(search_label == "label_event"){
                event = "all";
            }else if(search_label == "label_date"){
                date = "all";
            }
            //ラベル非表示
            $(this).parent().addClass("hide");
            judgeDisplaySearchLabel();
            
            new BlogList({
                wrapper    : "blog_list_block",
                firstCount : 4,
                base       : $(".content_wrap"),
                parts      : $("#post_parts_name").val(),
                floor_id      : floor,
                item_id       : item,
                event_id      : event,
                turn_flag     : turn,
                event_date    : date,
                favorite_flag : favorite,
                onDisplayAll : function(container) { 
                    $(".more_post",container).remove();
                    displayListButton( container );
                }
            }).fetch();
        });
        
    }
    
    //絞り込み条件ラベル表示
    function addSearchLabel(type, value){
        var label_text = "";
        if(type == "item"){
            label_text = "カテゴリ：";
        }else if(type == "event"){
            label_text = "イベント：";
        }else if(type == "date"){
            label_text = "日付：";
        }
        label_text += value;
        label_name = "label_" + type;

        $(".ct_labelBox ." + label_name).children("span").text(label_text);
        $(".ct_labelBox ." + label_name).removeClass("hide");
        
        judgeDisplaySearchLabel();
    }
    
    //絞り込み条件ラベル枠全体の表示判定
    function judgeDisplaySearchLabel(){
        //表示するラベルがなければ枠ごと非表示
        if($(".ct_labelBox li.hide").length < 3){
            $(".ct_labelBox").show();
        }else{
            $(".ct_labelBox").hide();
        }
    }
    
    function displayListButton( target ){
       var ListButton = $("<p/>").addClass("more_post").append(
            $("<a/>").attr("href", "/page2/hotblog/").append(
                $("<img/>").attr({
                    "src"   : "/common/images/common_moreposts_list_btn.png",
                    "psrc"   : "/common/images/common_moreposts_list_btn.png",
                    "osrc"   : "/common/images/common_moreposts_list_btn_on.png",
                    "alt"   : "一覧",
                    "class" : "rollover"
                })
            )
        ).appendTo(target);
    }
    // 記事絞り込みに必要なデータをAJAXでPOSTする
    function top_Ajax(floor, item, event, turn, date, favorite){
        $.ajax({
            url: '/page2/ajax/getpart/' + $('#post_parts_name').val(),
            type: 'POST',
            dataType:'html',
            data: {
                blog: {'floor_id': floor,
                       'item_id': item,
                       'event_id': event,
                       'turn_flag': turn,
                       'event_date': date,
                       'favorite_flag': favorite,
                       },
            },
            success: function(data) {
                $($('#post_parts_class').val()).html(data);

                $('.each_post_block img').imagesLoaded(function () {
                    $('.blog_list_block').masonry({
                        itemSelector: '.each_post_block'
                    });
                    thumbnailBlockWrapResize();
                });
                $('.tooltip').tooltip({align: 'top'});
                more(turn);
            },
        });
    }

    // もっと見る用の処理を行います
    function more(turn) {
        var nowNum = 3;     // 現在の表示件数(0から数える)
        var addNum = 10;    // 増加量

        // 表示しているブログブロック数を取得します(新着順・LIKE順があるので半分にする)
        var blockNum = $('.content_wrap .blog_list_block .each_post_block').size() / 2;

        //like数順の場合、表示数を追加します。
        if(turn==0){
            nowNum += blockNum;
            var blockNum = $('.content_wrap .blog_list_block .each_post_block').size();
        }

        // 表示件数が5件以上の場合処理を行います
        if (blockNum >= 5) {
            $('.more_post').click(function () {

                // 表示件数を増加します
                nowNum += addNum;

                // 表示を切り替えます
                $('.blog_list_block .each_post_block').css('display', '');
                $('.blog_list_block .each_post_block:gt(' + (nowNum) + ')').css('display', 'none');
                
                // ブランク画像を本来の画像に差し替えます
                var visibleList = $('.blog_list_block .each_post_block:lt(' + (nowNum + 1) + ')');
                visibleList.each(function() {
                    var img = $(".post_image a img", this);
                    if (img.attr("src").match(/\/common\/images\/blank.gif$/)) {
                        img.attr("src", $("input", this).val());
                    }
                });
                
                // thumbnail_block_wrapのサイズ調整
                thumbnailBlockWrapResize();

                //1px移動して、戻すことでページTOPリンクボタンをスクロールさせる処理
                window.scrollBy(0,-1);
                window.scrollBy(0,1);

                // 表示件数が表示件数を超えた場合
                if (nowNum + 1 >= blockNum) {
                    // もっと見るを一覧へ変更
                    $('.more_post img').attr('src', '/common/images/common_moreposts_list_btn.png');
                    $('.more_post img').attr('psrc', '/common/images/common_moreposts_list_btn.png');
                    $('.more_post img').attr('osrc', '/common/images/common_moreposts_list_btn_on.png');
                    $('.more_post a').attr('href', '/page2/hotblog/');

                    // ボタンのクリックイベント削除
                    $('.more_post').unbind('click');

                    return false;
                }
            });
        }else{
            // もっと見るを一覧へ変更
            $('.more_post img').attr('src', '/common/images/common_moreposts_list_btn.png');
            $('.more_post img').attr('psrc', '/common/images/common_moreposts_list_btn.png');
            $('.more_post img').attr('osrc', '/common/images/common_moreposts_list_btn_on.png');
            $('.more_post a').attr('href', '/page2/hotblog/');
        }
    }

    // thumbnail_block_wrapのサイズ調整
    function thumbnailBlockWrapResize() {
        var margin    = 20;
        var maxHeight = 0;
        $('.blog_list_block .each_post_block').not('.content_wrap.disnon .each_post_block ').each(function(){
            if ($(this).css('display') == 'none') return false;
            var top    = parseInt($(this).css('top'));
            var height = $(this).height() + top;

            if (maxHeight <= height) {
                maxHeight = height;
            }
        });

        maxHeight += margin;

        $(".blog_list_block").css('height', maxHeight);
    }

}
