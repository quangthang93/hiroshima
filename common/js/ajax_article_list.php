<?php
//基本情報の取得
include('../../../wordpress/php/classes/Config.class.php');
?>

//日付を取得する関数です
function getDate(){
	var date = new Date();
	var year = date.getYear();
	var year4 = (year < 2000) ? year+1900 : year;
	var month = date.getMonth() + 1;
	var date = date.getDate();
	if (month < 10) {
		month = '0' + month;
	}
	if (date < 10) {
		date = '0' + date;
	}
	var strDate = year4 + '-' + month + '-' + date;
	return strDate;
}

//=====記事サーチSTART=====
function article_list_show_Post(){
	//ページが読み込まれたら記事を表示
	$(function(){
		var floor = 'all';
		var item = 'all';
		var event = 'all';
		var turn = 1;
		var date = getDate();
		article_list_postSearch(floor, item, event, turn, date);
		article_list_Ajax(floor, item, event, turn, date);
	});

	// 記事を絞り込む
	function article_list_postSearch(floor, item, event, turn, date) {
	// フロアーで記事を絞り込む
		$('li.blog_floor ul.menu li.sub li').click(function(){
			var floor_name = $('a', this).text();
			floor = $(this).attr('id');
			$('li.blog_floor ul.menu li.sub span').html(floor_name);
			article_list_Ajax(floor, item, event, turn, date);
		});
	// アイテムカテゴリーで記事を絞り込む
		$('li.blog_item_category ul.menu li.sub li').click(function(){
			var item_name = $('a', this).text();
			item = $(this).attr('id');
			$('li.blog_item_category ul.menu li.sub span').html(item_name);
			article_list_Ajax(floor, item, event, turn, date);
		});
	// イベント・セールで記事を絞り込む
		$('li.blog_event_sale ul.menu li.sub li').click(function(){
			var event_name = $('a', this).text();
			event = $(this).attr('id');
			$('li.blog_event_sale ul.menu li.sub span').html(event_name);
			article_list_Ajax(floor, item, event, turn, date);
		});
	}

	// 記事絞り込みに必要なデータをAJAXでPOSTする
	function article_list_Ajax(floor, item, event, turn, date){
		$.ajax({
			url: '<?php echo $PARTS_PATH; ?>favorite_post_list_main.php',
			type: 'POST',
			dataType:'html',
			data: {
				blog: {'floor_id': floor,
					   'item_id': item,
					   'event_id': event,
					   'turn_flag': turn,
					   'event_date': date,
					   },
			},
			success: function(data) {
				$('.content_wrap').html(data);
			},
		});
	}
}
