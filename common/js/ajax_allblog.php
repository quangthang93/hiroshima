<?php
/**
 * AJAXでJSONファイルを読み込むJavascriptです
 *
**/
//基本情報の取得
//include_once($_SERVER['APPROOT_DIR']. '/api/classes/Config.class.php');

header("Content-Type: text/javascript");
?>

//=====記事サーチSTART=====
/**
 * AJAXで記事を表示するJavascriptです
 * firstCount    hot_blogは初期表示行数
 * increment     hot_blogは増加行数
 * hot_blog_flag hot_blog整形時フラグ
**/
function shop_show_Post(firstCount, increment, hot_blog_flag) {
    if (typeof(firstCount) === 'undefined') {
        firstCount = 5;
    }
    if (typeof(increment) === 'undefined') {
        increment = 30;
    }
    if (typeof(hot_blog_flag) === 'undefined') {
        hot_blog_flag = false;
    }
    var blogListOption = {};
    blogListOption.wrapper       = "column5_blog_list_block_01";
    blogListOption.firstCount    = firstCount;
    blogListOption.increment     = increment;
    blogListOption.hot_blog_flag = hot_blog_flag;

    //ページが読み込まれたら記事を表示
    $(function() {
        blogListOption.parts = $("#post_parts_name").val();
        blogListOption.base  = $(".content_wrap");
        
        var cookies = {
                   item_id: $('#item_id').val(),
                   type: $('#type').val(),
                   store_code: $('#store_code').val(),
                   special_id: $("#special_id").val(),
                   start_at  : $("#start_at").val()
        };
        postSearch(cookies.type, cookies.item_id, cookies.store_code, cookies.special_id, cookies.start_at);
        new BlogList($.extend({}, blogListOption, cookies)).fetch();
    });

    // 暫定対応： ページ毎にプルダウン選択時の表示文字数を変更する
    var select_text_length = 10;
    var limit_length = select_text_length - 1;

    //記事絞り込みのプルダウンのイベントの関数です
    function postSearch(type, item_id, store_code, special_id, start_at) {
        var condition = {
            type      : type,
            item_id       : item_id,
            store_code     : store_code,
            special_id    : special_id,
            start_at         : start_at
        };
        
        //店舗選択イベント
        $('#blog_store').change(function(){
            var store_name = $('#blog_store option:selected').text();
            if(store_name.length > select_text_length){
                store_name = makeDisplayStrings(store_name, select_text_length);
            }
            $('#store_selected').html(store_name);
            
            condition.store_code = $('#blog_store option:selected').val();
            new BlogList($.extend({}, blogListOption, condition)).fetch();
        });

        //アイテムカテゴリー選択イベント
        $('#blog_item_category').change(function(){
            var item_name = $('#blog_item_category option:selected').text();
            if(item_name.length > select_text_length){
                item_name = makeDisplayStrings(item_name, select_text_length);
            }
            $('#item_selected').html(item_name);
            
            condition.item_id = $('#blog_item_category option:selected').val();
            new BlogList($.extend({}, blogListOption, condition)).fetch();
        });

        //特設ブログカテゴリー選択イベント
        $('#blog_spcial_category').change(function(){
            var spcial_category_name = $('#blog_spcial_category option:selected').text();
            if(spcial_category_name.length > 12){
                spcial_category_name = makeDisplayStrings(spcial_category_name, 12);
            }
            $('#special_selected').html(spcial_category_name);
            
            condition.special_id = $('#blog_spcial_category option:selected').val();
            new BlogList($.extend({}, blogListOption, condition)).fetch();
        });
    }

    // プルダウンで選択した文字列を指定長でカットして返却します
    function makeDisplayStrings(str, maxlength){
        return item_name = str.substr(0, maxlength) + "…";
    }
}
