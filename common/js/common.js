var common = {
    /**
     * @type String
     */
    storeCode:   "",

    /**
     * @type String
     */
    storeDomain: "",

    /**
     * @type String
     */
    portalDomain: "",

    /**
     *
     * @param XMLHttpResponse xhr
     * @param string          failedValue
     * @returns {common.getJsonByXHR.failedValue}
     */
    getJsonByXHR: function(xhr, failedValue) {
        if (failedValue == null) {
            failedValue = { result: "error" };
        }

        try {
            var json = $.parseJSON(xhr.responseText);
            return (json != null) ? json : failedValue;
        } catch (e) {
            return failedValue;
        }
    },

    /**
     *
     * @param    object json
     */
    loginByAjax: function(json) {
        var saveCookie = function(data) {
            if (data.value == null) {
                $.removeCookie(data.key);
            } else {
                $.cookie(data.key, data.value, { path: "/", domain: data.scope, expires: data.expires });
            }
        };
        saveCookie(json.session);
        saveCookie(json.auto_login);
        saveCookie({key: "member", value: 1, scope: json.session.scope, expires: null});
        var url      = "http://" + json.domain + "/page2/";
        location.replace(url);
    }
};
/*image on*/
var changeImageOn = function() {
	this.setAttribute("src", this.getAttribute("oSrc"));
};
/*image off*/
var changeImageOff = function() {
	this.setAttribute("src", this.getAttribute("pSrc"));
};
/*image load complete*/
var loadImageComp = function() {
	var src = this.src.split('/').pop().replace('_on','');
	var img = $('img.rollover[src$="' + src + '"]').get(0);
	img.addEventListener('mouseover', changeImageOn);
	img.addEventListener('mouseout', changeImageOff);
};
/*Rollover*/
function initRollOvers() {
    if (!document.getElementById){
        return;
    }
    var preLoads = new Array();
    var allImages = document.getElementsByTagName('img');
    for (var i = 0; i < allImages.length; i++) {
        if (allImages[i].className == 'rollover') {
            var src = allImages[i].getAttribute('src');
            var ftype = src.substring(src.lastIndexOf('.'), src.length);
            var oSrc = src.replace(ftype, '_on'+ftype);
            allImages[i].setAttribute('pSrc', src);
            allImages[i].setAttribute('oSrc', oSrc);
            preLoads[i] = new Image();
            if(preLoads[i].addEventListener){
                preLoads[i].addEventListener('load', loadImageComp);
                preLoads[i].src = oSrc;
            }else{
                preLoads[i].src = oSrc;
                var changeImage = function(attr) {
                    return function() {
                        this.setAttribute("src", this.getAttribute(attr));
                    }
                };
                allImages[i].onmouseover = changeImage("oSrc");
                allImages[i].onmouseout  = changeImage("pSrc");
                allImages[i].onmouseup   = changeImage("pSrc");
            }
        }
    }
    var preLoads = new Array();
    var allImages = document.getElementsByTagName('input');
    for (var i = 0; i < allImages.length; i++) {
        if (allImages[i].className == 'rollover') {
            var src = allImages[i].getAttribute('src');
            var ftype = src.substring(src.lastIndexOf('.'), src.length);
            var oSrc = src.replace(ftype, '_on'+ftype);
            allImages[i].setAttribute('pSrc', src);
            allImages[i].setAttribute('oSrc', oSrc);
            preLoads[i] = new Image();
            preLoads[i].src = oSrc;
            allImages[i].onmouseover = function() {
                this.setAttribute('src', this.getAttribute('oSrc'));
            }
            allImages[i].onmouseout = function() {
                this.setAttribute('src', this.getAttribute('pSrc'));
            }
            allImages[i].onmouseup = function() {
                this.setAttribute('src', this.getAttribute('pSrc'));
            }
        }
    }
}
function addOnload(func){
    if ( typeof window.addEventListener != "undefined" ){
        window.addEventListener( "load", func, false );
    }else if ( typeof window.attachEvent != "undefined" ) {
        window.attachEvent( "onload", func );
    }else{
        if ( window.onload != null ){
            var oldOnload = window.onload;
            window.onload = function ( e ) {
                oldOnload( e );
                window[func]();
            };
        }else
            window.onload = func;
    }
}
addOnload(initRollOvers);
/*Scroll*/
$(function () {
    var headH = 0;
    // page_top
    $('a[href^=#], area[href^=#]').not('a[href=#], area[href=#]').each(function () {
        // jquery.easing
        jQuery.easing.quart = function (x, t, b, c, d) {
            return -c * ((t = t / d - 1) * t * t * t - 1) + b;
        };
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname && this.hash.replace(/#/, '')) {
            var $targetId = $(this.hash),
            $targetAnchor = $('[name=' + this.hash.slice(1) + ']');
            var $target = $targetId.length ? $targetId : $targetAnchor.length ? $targetAnchor : false;
            if ($target) {
                var targetOffset = $target.offset().top - headH;
                $(this).click(function () {
                    $('html, body').animate({
                        scrollTop: targetOffset
                    }, 500, 'quart');
                    return false;
                });
            }
        }
    });
    if (location.hash && location.hash.indexOf("=") === -1) {
        var hash = location.hash;
        window.scroll(0, headH)
        $('a[href=' + hash + ']').click();
    }
});
/*Login Window*/
$(document).ready(function(){
    $(".header_login a.lgbtn").click(function () {
        $(".login_window").show();
        if(document.getElementById("login_overlay") === null){
            $('body').append($("<div/>").attr("id", "login_overlay"));
            $('#login_overlay').css({
                position:"absolute",
                left:'0px',
                top:'0px',
                width:$("body").width(),
                height:$("body").height()
                });
        }
        $('#login_overlay').click(function () {
            $(".login_window").hide();
            $('#login_overlay').remove();
        });
        return false;
    });
    $(".login_window p.close").click(function () {
        $(".login_window").hide();
    });
    $(".header_after_login a.lgbtn").click(function () {
        $(".after_login_window").toggle();
        if ($(".header_after_login a.lgbtn").hasClass('active')) {
            $(".header_after_login a.lgbtn").removeClass('active');
        } else {
            if(document.getElementById("after_login_overlay") === null){
                $('body').append($("<div/>").attr("id", "after_login_overlay"));
                $('#after_login_overlay').css({
                    position:"absolute",
                    left:'0px',
                    top:'0px',
                    width:$("body").width(),
                    height:$("body").height()
                    });
            }
            $('#after_login_overlay').click(function () {
                $(".after_login_window").toggle();
                $(".header_after_login a.lgbtn").removeClass('active');
                $('#after_login_overlay').remove();
            });
            $(".header_after_login a.lgbtn").addClass('active');
        }
        return false;
    });
});
/*Blog Search Window*/
$(document).ready(function(){
    $(".blog_search_block p.open_btn").click(function () {
        if ($(".blog_search_window").css("display")=="block") {
            $(".blog_search_window").hide();
            Glayer.fadeOut("glayer",{to:0,from:0.5});
        } else {
            $(".blog_search_window").show();
            Glayer.fadeIn("glayer",{to:0.8,from:0.2}).onclick = function(){Glayer.fadeOut();$(".blog_search_window").hide();};
        }
    });
    $(".blog_search_window p.close").click(function () {
        $(".blog_search_window").hide();
        Glayer.fadeOut("glayer",{to:0,from:0.5});
    });
    $(".blog_search_window .search_words ul").click(function () {
        $(".blog_search_window").hide();
        Glayer.fadeOut("glayer",{to:0,from:0.5});
    });
    $(".blog_search_window .search_words ul").click(function () {
        $(".blog_search_window").hide();
        Glayer.fadeOut("glayer",{to:0,from:0.5});
    });
    $("#mycarousel li").click(function () {
        if ($(".blog_search_window").size()>0){
            $(".blog_search_window").hide();
            Glayer.fadeOut("glayer",{to:0,from:0.5});
        }
    });
});
/*Form Check Box*/
$(document).ready(function(){
	var isIE = $.browser.msie && $.browser.version != "9.0" && $.browser.version != "10.0";
	if(isIE) {
	    $(".check_box_label").click(function(){
            if($(this).prev("input:checkbox").is(":checked")){
				$(this).prev("input:checkbox").prop("checked", false);
				$(this).removeClass("label_selected");
            }else{
				$(this).prev("input:checkbox").prop("checked", true);
				$(this).addClass("label_selected");
			}
		});
	} else {
	    $(".check_box").change(function(){
	        if($(this).is(":checked")){
	            $(this).next("label").addClass("label_selected");
	        }else{
	            $(this).next("label").removeClass("label_selected");
	        }
	    });
	}
});
/* Password Label */
$(document).ready(function() {
    if (document.createElement("input").placeholder === undefined) {
        $("[placeholder]").each(function() {
            var current = $(this);
            var id      = current.attr("id");
            var labelId = id + "_label";
            switch (this.nodeName.toUpperCase()) {
            case "INPUT":
                var label = $("<div/>").attr("id", labelId).attr("class", "overlabel").text(current.attr("placeholder")).insertBefore(current);
                if (!current.val()) {
                    current.hide();
                    label.show();
                } else {
                    label.hide();
                    current.show();
                };
                label.click(function() {
                    label.hide();
                    current.show().focus();
                });
                current.blur(function() {
                    if (!current.val()) {
                        label.show();
                        current.hide();
                    }
                });
                break;
            case "TEXTAREA":
                var defaultValue = current.attr("placeholder");
                current.val(defaultValue);
                current.focus(function() {
                    if (this.value == defaultValue) {
                        this.value = "";
                        this.style.color = "#333333";
                    }
                });
                current.blur(function() {
                    if (this.value == "") {
                        this.value = defaultValue;
                        this.style.color = "#666666";
                    }
                });
                break;
            }
        });
    }
});
/* Password Label 2 */
$(document).ready(function(){
    $('.passwd').hide();
    $('.passwd_label').show();
    $('.passwd_label').click(function(){
        $('.passwd_label').hide();
        $('.passwd').show();
        $('.passwd').focus();
    });
    $('.passwd').blur(function(){
        if(!$('.passwd').val()){
            $('.passwd_label').show().css({color:'#333333'});
            $('.passwd').hide();
        }
    });
});
/* Close Button */
$(document).ready(function() {
    var imgAttr;
    var anchor = $("#main #close a, #main .close a");
    var img    = $("#main #close img, #main .close img");
    var hasOpener = (window.opener != null);
    if (hasOpener) {
        anchor.click(function() { window.close(); return false; });
        imgAttr = {
            alt    : "閉じる"
        };
        img.attr(imgAttr);
    }
    else {
        var top = common.storeDomain.length ? "http://" + common.storeDomain + "/page2/" : "/";
        anchor.attr("href", top);
        imgAttr = {
            src    : "/common/images/top_btn_002.jpg",
            alt    : "店舗トップ",
            width  : "110",
            height : "32"
        };
        img.attr(imgAttr);
    }
});
/* Open External Link in New Window */
//自身のドメインと、ポータルのドメイン以外へのリンクを別ウィンドウで開く
$(document).ready( function () {
    if (location.hostname !== common.portalDomain) {
        $("a[href^='http']:not({[href*='" + location.hostname + "'],[href*='" + common.portalDomain + "']})").attr('target', '_blank');
    }
});
/* menu active image preload */
$(document).ready(function(){
	var menuPLoads = new Array();
	$('.grand_menu_block li img').each(function(){
		var src = $(this).attr('src');
		src = src.replace('_act','').replace('_on','');
		var ftype = src.substring(src.lastIndexOf('.'), src.length);
		var oSrc = src.replace(ftype, '_act'+ftype);
		var img = new Image();
		img.src = oSrc;
		menuPLoads.push(img);
	});
});
/* pumpkin.js dynamic load */
// $(document).ready(function(){
//     // check target page
//     var loc = location.href;
//     var re = new RegExp('^http:\/\/(sapporo|sendai|urawa|ikebukuro|shibuya|kichijoji|chofu|shintokorozawa|chiba|tsudanuma|matsumoto|shizuoka|nagoya|otsu|hiroshima|fukuoka|kumamoto)\.parco\.jp\/page2(\/|\/hotblog(\/|)|)$');
//     if(!re.test(loc)) {
//         return;
//     }
//     // load pumpkin.js
//     var script = document.createElement( 'script' );
//     script.type = 'text/javascript';
//     script.src = "/common/js/pumpkin.js";
//     var firstScript = document.getElementsByTagName( 'script' )[ 0 ];
//     firstScript.parentNode.insertBefore( script , firstScript );
// });
/* wear logo click open new window */
$(document).ready(function(){
    $('.entry_block #wear_profile_image').click(function() {
        var user_name = $(this).attr("data-wear-username");
        var snap_id = $(this).attr("data-wear-snapid");
        if(user_name === undefined || snap_id === undefined || user_name == '' || snap_id == '') return false;
        var wear_href = 'http://wear.jp/' + user_name + '/';
        window.open(wear_href, '_blank');
        return false;
    });
    $('.entry_block #wear_logo_image').click(function() {
        var user_name = $(this).attr("data-wear-username");
        var snap_id = $(this).attr("data-wear-snapid");
        if(user_name === undefined || snap_id === undefined || user_name == '' || snap_id == '') return false;
        var wear_href = 'http://wear.jp/' + user_name + '/coordinate/' + snap_id + '/';
        window.open(wear_href, '_blank');
        return false;
    });

    // shop header clip num
    $('.shop_info_block .icon_shop_clip_num').click(function() {
        $.fancybox.open({
            href: "/web/ppclip/",
            type: "iframe",
            width: "594"
        });
    });
});