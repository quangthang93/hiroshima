<?php
/**
 * AJAXでJSONファイルを読み込むJavascriptです
 *
 */
header("Content-Type: text/javascript");
?>
//=====記事サーチSTART=====
function shop_show_Post() {
    // ページが読み込まれたら記事を表示
    $(function() {
        var store = common.storeCode.length ? common.storeCode : "all";
        var floor = 'all';
        var item  = 'all';
        var event = 'all';
        var turn  = 1;
        postSearch(floor, item, event, turn, store);
        fav_Ajax(floor, item, event, turn, store);
    });
    
    // 暫定対応： ページ毎にプルダウン選択時の表示文字数を変更する
    var select_text_length = 8;
    if (location.href.slice(-8, -1) == "hotblog") {
        select_text_length = 6;
    }
    
    //記事絞り込みのプルダウンのイベントの関数です
    function postSearch(floor, item, event, turn, store) {
        //プルダウンの横幅リサイズ
        var anchors = $('.pull_down_block01 li.sub>ul>li>a');
        var maxAnchorlength = 12.3;
        var isIE = /*@cc_on!@*/false;
        for(i = 0; i < anchors.length; i++) {
        if (isIE){
            if (anchors[i].innerText.length>maxAnchorlength) {
                maxAnchorlength=anchors[i].innerText.length;
            }
        } else if (anchors[i].text.length>maxAnchorlength){
            maxAnchorlength=anchors[i].text.length;
        }
    }
    anchors.css("width", (maxAnchorlength*12)+"px");
    $('.pull_down_block01 li.sub>ul').css("width", (maxAnchorlength * 12 + 20) + "px");
        //店舗選択イベント
        $('div#blog_list_shop div.pull_down_block01 ul.menu ul li').click(function() {
            var store_name = $('a', this).text();
            var store = $(this).attr('id');
            if(store_name.length > 4) {
                $('div#blog_list_shop div.pull_down_block01 ul.menu li span').html(store_name.substring(0,4) + "…");
                $('div#blog_list_shop div.pull_down_block01 ul.menu li span').attr("id", store);
            } else {
                $('div#blog_list_shop div.pull_down_block01 ul.menu li span').html(store_name);
                $('div#blog_list_shop div.pull_down_block01 ul.menu li span').attr("id", store);
            }
        });
        
        // 店舗選択ボタンクリック時
        $('div#blog_list_shop div.select p a').click(function(){
            //データの初期化
            clearData();
            store = 'all';
            floor = 'all';
            item  = 'all';
            event = 'all';
            store = 'all';
            turn  = 1;
            
            var current_store_name = $('div#blog_list_shop div.pull_down_block01 ul.menu li span').text();
            if (current_store_name == '店舗名') {
                alert("店舗を選択してください");
                return false;
            }
            // TODO：店舗の情報で初期化
            store = $('div#blog_list_shop div.pull_down_block01 ul.menu li span').attr("id");
            $('ul#ul_category').css("display","block");
            fav_Ajax(floor, item, event, turn, store);
        });
        
        //フロアー選択イベント
        $('ul.clear .blog_floor .menu .sub li').click(function() {
            var floor_name = $('a', this).text();
            if (floor_name.length > 7) {
                floor_name = makeDisplayStrings(floor_name, 6);
            }
            floor = $(this).attr('id');
            $('.blog_floor .menu .sub span').html(floor_name);
            fav_Ajax(floor, item, event, turn, store);
        });

        //アイテムカテゴリー選択イベント
        $('ul.clear .blog_item_category .menu .sub li').click(function(){
            var item_name = $('a', this).text();
            if(item_name.length > select_text_length){
                item_name = makeDisplayStrings(item_name, select_text_length);
            }
            item = $(this).attr('id');
            $('.blog_item_category .menu .sub span').html(item_name);
            fav_Ajax(floor, item, event, turn, store);
        });
        
        //イベントカテゴリー選択イベント
        $('ul.clear .blog_event_sale .menu .sub li').click(function(){
            var event_name = $('a', this).text();
            if(event_name.length > select_text_length){
                event_name = makeDisplayStrings(event_name, select_text_length);
            }
            event = $(this).attr('id');
            $('.blog_event_sale .menu .sub span').html(event_name);
            fav_Ajax(floor, item, event, turn, store);
        });
    }
    
    //記事絞り込みに必要なデータをAJAXでPOST
    function fav_Ajax(floor, item, event, turn, store) {
        $.ajax({
            url: '/page2/ajax/getpart/' + $('#post_parts_name').val(),
            type: 'POST',
            dataType:'html',
            async: false,
            data: {
                blog: {
                    'store_id' : store,
                    'floor_id' : floor,
                    'item_id'  : item,
                    'event_id' : event,
                    'turn_flag': turn,
                }
            },
            success: function(data) {
                $($('#post_parts_class').val()).html(data);
                
                $('.each_post_block img').imagesLoaded(function () {
                    $('.column4_blog_list_block_01').masonry({
                        itemSelector: '.each_post_block'
                    });
                    thumbnailBlockWrapResize();
                });

                more(turn);
            }
        });
    }
    
    // もっと見る用の処理を行います
    function more(turn) {
        var nowNum = 3;     // 現在の表示件数(0から数える)
        var addNum = 10;    // 増加量
        
        // 表示しているブログブロック数を取得します
        var blockNum = $('.content_wrap .column4_blog_list_block_01 .each_post_block').size();
        
        //like数順の場合、表示数を追加します。
        if (turn == 0) {
            nowNum += blockNum;
            var blockNum = $('.content_wrap .column4_blog_list_block_01 .each_post_block').size();
        }
        
        // 表示件数が5件以上の場合処理を行います
        if (blockNum >= 5) {
            $('.more_post').click(function () {
                // 表示件数を増加します
                nowNum += addNum;

                // 表示を切り替えます
                $('.column4_blog_list_block_01 .each_post_block').css('display', '');
                $('.column4_blog_list_block_01 .each_post_block:gt(' + (nowNum) + ')').css('display', 'none');

                // thumbnail_block_wrapのサイズ調整
                thumbnailBlockWrapResize();

                //1px移動して、戻すことでページTOPリンクボタンをスクロールさせる処理
                window.scrollBy(0,-1);
                window.scrollBy(0,1);

                // 表示件数が表示件数を超えた場合
                if (nowNum + 1 >= blockNum) {
                    // ボタンのクリックイベント削除
                    $('.more_post').unbind('click');

                    //もっと見るボタンを削除
                    $('.more_post').remove();

                    return false;
                }
            });
        }else{
            $('.more_post').remove();
        }
    }
    
    // thumbnail_block_wrapのサイズ調整
    function thumbnailBlockWrapResize() {
        var margin    = 20;
        var maxHeight = 0;
        $('.column4_blog_list_block_01 .each_post_block').not('.content_wrap.disnon .each_post_block ').each(function(){
            if ($(this).css('display') == 'none') return false;
            var top    = parseInt($(this).css('top'));
            var height = $(this).height() + top;
            
            if (maxHeight <= height) {
                maxHeight = height;
            }
        });
        
        maxHeight += margin;
        
        $(".column4_blog_list_block_01").css('height', maxHeight);
    }
    // プルダウンで選択した文字列を指定長でカットして返却します
    function makeDisplayStrings(str, maxlength){
        return item_name = str.substr(0, maxlength) + "…";
    }
    //フロア、アイテム、イベントの初期化
    function clearData(){
        $('.blog_floor .menu .sub span').html('全て');
        $('.blog_item_category .menu .sub span').html('全て');
        $('.blog_event_sale .menu .sub span').html('全て');
    }
}