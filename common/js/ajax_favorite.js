//=====記事サーチSTART=====
function shop_show_Post() {
    // ページが読み込まれたら記事を表示
    $(function() {
        var store = common.storeCode.length ? common.storeCode : "all";
        var floor = 'all';
        var item  = 'all';
        var event = 'all';
        var turn  = 1;
        postSearch(floor, item, event, turn, store);
        favoriteAjax(floor, item, event, turn);
    });
    
    // 暫定対応： ページ毎にプルダウン選択時の表示文字数を変更する
    var select_text_length = 8;
    if (location.href.slice(-8, -1) == "hotblog") {
        select_text_length = 6;
    }
    
    //記事絞り込みのプルダウンのイベントの関数です
    function postSearch(floor, item, event, turn, store) {
        //プルダウンの横幅リサイズ
        var anchors = $('.pull_down_block01 li.sub>ul>li>a');
        var maxAnchorlength = 12.3;
        var isIE = /*@cc_on!@*/false;
        for(i = 0; i < anchors.length; i++) {
        if (isIE){
            if (anchors[i].innerText.length>maxAnchorlength) {
                maxAnchorlength=anchors[i].innerText.length;
            }
        } else if (anchors[i].text.length>maxAnchorlength){
            maxAnchorlength=anchors[i].text.length;
        }
    }
    anchors.css("width", (maxAnchorlength*12)+"px");
    $('.pull_down_block01 li.sub>ul').css("width", (maxAnchorlength * 12 + 20) + "px");
        //店舗選択イベント
        $('div#blog_list_shop div.pull_down_block01 ul.menu ul li').click(function() {
            var store_name = $('a', this).text();
            var store = $(this).attr('id');
            if(store_name.length > 3) {
                $('div#blog_list_shop div.pull_down_block01 ul.menu li span').html(store_name.substring(0,4) + "…");
                $('div#blog_list_shop div.pull_down_block01 ul.menu li span').attr("id", store);
            } else {
                $('div#blog_list_shop div.pull_down_block01 ul.menu li span').html(store_name);
                $('div#blog_list_shop div.pull_down_block01 ul.menu li span').attr("id", store);
            }
        });
        
        // 店舗選択ボタンクリック時
        $('div#blog_list_shop div.select p a').click(function(){
            //データの初期化
            clearData();
            store = 'all';
            floor = 'all';
            item  = 'all';
            event = 'all';
            store = 'all';
            turn  = 1;
            
            var current_store_name = $('div#blog_list_shop div.pull_down_block01 ul.menu li span').text();
            if (current_store_name == '店舗名') {
                alert("店舗を選択してください");
                return false;
            }
            
            $('ul#ul_category').css("display","block");
            favoriteAjax(floor, item, event, turn);
        });
        
        //フロアー選択イベント
        $('ul.clear .blog_floor .menu .sub li').click(function() {
            var floor_name = $('a', this).text();
            if (floor_name.length > 7) {
                floor_name = makeDisplayStrings(floor_name, 6);
            }
            floor = $(this).attr('id');
            $('.blog_floor .menu .sub span').html(floor_name);
            favoriteAjax(floor, item, event, turn);
        });

        //アイテムカテゴリー選択イベント
        $('ul.clear .blog_item_category .menu .sub li').click(function(){
            var item_name = $('a', this).text();
            if(item_name.length > select_text_length){
                item_name = makeDisplayStrings(item_name, select_text_length);
            }
            item = $(this).attr('id');
            $('.blog_item_category .menu .sub span').html(item_name);
            favoriteAjax(floor, item, event, turn);
        });
        
        //イベントカテゴリー選択イベント
        $('ul.clear .blog_event_sale .menu .sub li').click(function(){
            var event_name = $('a', this).text();
            if(event_name.length > select_text_length){
                event_name = makeDisplayStrings(event_name, select_text_length);
            }
            event = $(this).attr('id');
            $('.blog_event_sale .menu .sub span').html(event_name);
            favoriteAjax(floor, item, event, turn);
        });
    }
    
    function favoriteAjax(floor, item, event, turn) {
        var wrapper = "column4_blog_list_block_01";
        var param   = {
            wrapper    : wrapper,
            firstCount : 3,
            base       : $(".content_wrap_inner"),
            parts      : "favorite-post-list-main",
            floor_id   : floor,
            item_id    : item,
            event_id   : event,
            turn_flag  : turn
        };
        common.storeCode = $('div#blog_list_shop div.pull_down_block01 ul.menu li span').attr("id");
        new BlogList(param).fetch();
    }
    
    // プルダウンで選択した文字列を指定長でカットして返却します
    function makeDisplayStrings(str, maxlength){
        return item_name = str.substr(0, maxlength) + "…";
    }
    //フロア、アイテム、イベントの初期化
    function clearData(){
        $('.blog_floor .menu .sub span').html('全て');
        $('.blog_item_category .menu .sub span').html('全て');
        $('.blog_event_sale .menu .sub span').html('全て');
    }
}