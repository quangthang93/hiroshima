//=====記事サーチSTART=====
/**
 * AJAXで記事を表示するJavascriptです
 *
**/
function follow_Onload(){
    //初期データの設定
    var store = common.storeCode;

    //ページが読み込まれたらショップを表示
    $(function(){
        follow_Ajax(store);

        //店舗選択ドロップダウンのイベント
        $('.sub ul li').click(function(){
            var store_name = $('a', this).text();
            var store = $(this).attr('id');
            if(store_name.length > 4) {
                $('div#blog_list_shop div.pull_down_block01 ul.menu li span').html(store_name.substring(0,4)+"…");
                $('div#blog_list_shop div.pull_down_block01 ul.menu li span').attr("id", store);
            } else {
                $('div#blog_list_shop div.pull_down_block01 ul.menu li span').html(store_name);
                $('div#blog_list_shop div.pull_down_block01 ul.menu li span').attr("id", store);
            }
        });

        // 店舗選択ボタンクリック時
        $('div#blog_list_shop .select p a').click(function(){
            var current_store_name = $('div#blog_list_shop div.pull_down_block01 ul.menu li span').text();
            if(current_store_name == '店舗名'){
                alert("店舗を選択してください");
                return false;
            }
            // TODO：店舗の情報で初期化
            store = $('div#blog_list_shop div.pull_down_block01 ul.menu li span').attr("id");
            if(store === undefined){
                store = common.storeCode;
            }
            follow_Ajax(store);
        });
    });
}

//ショップ絞り込みに必要なデータをAJAXでPOST
function follow_Ajax(store){
    $.ajax({
        url: '/page2/ajax/getpart/' + $('#post_parts_name').val(),
        type: 'POST',
        dataType:'html',
        data: {
            //blog: {'store_code': store}
            blog: {'store_code': store, 'store_id': common.storeCode }
        },
        success: function(data) {
            $($('#post_parts_class').val()).html(data);
            more();
        }
    });
}

// もっと見る用の処理を行います
function more(turn) {
    var nowNum = 11;        // 現在の表示件数(0から数える)
    var addNum = 10;    // 増加量

    // 表示しているブログブロック数を取得します
    var blockNum = $('#blog_list_like li').size();

    // 表示件数が13件以上の場合処理を行います
    if (blockNum >= 13) {
        $('.more_post').click(function () {

            // 表示件数を増加します
            nowNum += addNum;

            // 表示を切り替えます
            $('#blog_list_like li').css('display', '');
            $('#blog_list_like li:gt(' + (nowNum) + ')').css('display', 'none');

                //1px移動して、戻すことでページTOPリンクボタンをスクロールさせる処理
                window.scrollBy(0,-1);
                window.scrollBy(0,1);

            // 表示件数が表示件数を超えた場合
            if (nowNum + 1 >= blockNum) {
                // ボタンのクリックイベント削除
                $('.more_post').unbind('click');

                //もっと見るボタンを削除
                $('.more_post').remove();

                return false;
            }
        });
    }else{
        $('.more_post').remove();
    }
}