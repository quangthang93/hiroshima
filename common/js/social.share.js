// ソーシャルアクション　シェア専用処理
var socialShare = {
  // ブログID
  blog_id: 0,

  // 記事ID
  post_id: 0,

  // シェアコメント文字列の最大文字長
  countMax: 87,

  // サブミット時の処理
  onSubmitShare: function(){

    // バリデーション処理
    var result = socialShare.validate();
    if(result.error_code != 0){
      alert(result.error_status);
      return false;
    }
    
    // 入力内容がプレースホルダーと同じ場合は、未入力(空文字)として扱います
    var textarea = $("div.share form textarea");
    if (textarea.val() == textarea.attr("placeholder")) {
        textarea.val("");
    }
    
    // シェア登録 ajax呼び出し
    $.ajax({
        type: "post",
        url: "/social/share/create/",
        data: {
            "store_code": parent.common.storeCode, 
            "blog_id": socialShare.blog_id,
            "post_id": socialShare.post_id,
            "comment": textarea.val(),
            "tw": $('div.share form div.clear ul.cooperating li.clear input#logw01.check_box').attr('checked') == "checked"? "1": "0",
            "fb": $('div.share form div.clear ul.cooperating li.clear input#logw02.check_box').attr('checked') == "checked"? "1": "0"
        },
        dateType: "json",
        success: function(data){},
        error: function() {},
        async: false
    });

    // fancyboxを閉じる
    parent.$.fancybox.close();

    return false;
  },  

  // フォームのバリデーション
  validate: function(){

    // どっちも連携状態にない場合はエラー
    var twitter_checked = $('div.share form div.clear ul.cooperating li.clear input#logw01.check_box').attr('checked') == "checked";
    var facebook_checked = $('div.share form div.clear ul.cooperating li.clear input#logw02.check_box').attr('checked') == "checked";
    if(!twitter_checked && !facebook_checked){
      return {error_code: 1, error_status: "TwitterかFacebookのうち最低1つはチェックしてください"};
    }

    // シェアコメントが87文字以上の場合はエラー
    var length = $('div.share form textarea').val().length;
    if(length > socialShare.countMax){
      return {error_code: 2, error_status: "コメントは87文字以内で入力してください"};
    }

    return {error_code: 0, error_status: ""};

  },

  // 初期化処理
  init: function(){

    // 店舗コード取得、ドメイン名から簡易的に取得
    var hostname = location.hostname;
    if(!hostname.match(/parco.jp$/)){
         hostname = "hiroshima.parco.jp";
    }
    // ブログID取得
    if(location.pathname.match(/^\/page2\/social\/share\/[0-9]*\/?/)){
        socialShare.blog_id = location.pathname.match(/^\/page2\/social\/share\/([0-9]*)\/?/)[1];
    }

    // 記事ID取得
    if(location.pathname.match(/^\/page2\/social\/share\/[^\/]*\/[0-9]*\/?/)){
        socialShare.post_id = location.pathname.match(/^\/page2\/social\/share\/[^\/]*\/([0-9]*)\/?/)[1];
    }

    // サブミット時の処理割り当て
    $('div.share form').submit(socialShare.onSubmitShare);

  },

};

$(document).ready(function(){
  socialShare.init();
});
