<?php
/**
 * AJAXでJSONファイルを読み込むJavascriptです
 *
**/
//基本情報の取得
//include_once($_SERVER['APPROOT_DIR']. '/api/classes/Config.class.php');

if (isset($_COOKIE["blog"])) {
    $floor = $_COOKIE["blog"]["floor_id"];
    $item  = $_COOKIE["blog"]["item_id"];
    $event = $_COOKIE["blog"]["event_id"];
    $turn  = (int) $_COOKIE["blog"]["turn_flag"];
    $date  = $_COOKIE["blog"]["event_date"];
    $fav   = (int) $_COOKIE["blog"]["favorite_flag"];
} else {
    $floor = "all";
    $item  = "all";
    $event = "all";
    $turn  = 1;
    $date  = "all";
    $fav   = 0;
}

header("Content-Type: text/javascript");
?>

//日付を取得する関数です
function getDate(){
    var date = new Date();
    var year = date.getYear();
    var year4 = (year < 2000) ? year+1900 : year;
    var month = date.getMonth() + 1;
    var date = date.getDate();
    if (month < 10) {
        month = '0' + month;
    }
    if (date < 10) {
        date = '0' + date;
    }
    var strDate = year4 + '-' + month + '-' + date;
    return strDate;
}

//=====ブロックカレンダーSTART=====
/**
 * ブロックカレンダーを表示する関数です
 *
**/
function show_Calendar(){
    //ページが読み込まれたらサイドカレンダーを表示
    $(function(){
        var tmp_date = getDate();
        var date = tmp_date.substr(0, 4) + tmp_date.substr(5, 2);
        calendar_Ajax(date);
    });

    //先月と来月ボタンを動作させる関数です
    function calendarEvent() {
        $('.month_block .prev').click(function(){
            var get_date = $('.month_block p').text();
            get_date = get_date.replace('年', '');
            get_date = get_date.replace('月', '');
            get_date = get_date.replace('/', '');
            var year = parseInt(get_date.substr(0, 4));
            var month = parseInt(get_date.substr(4, 2));
            if(month == 1){
                year -= 1;
                month = 12;
            }else{
                month -= 1;
            }
            var date = year + ('0' + month).slice(-2);
            calendar_Ajax(date);
        });

        $('.month_block .next').click(function(){
            var get_date = $('.month_block p').text();
            get_date = get_date.replace('年', '');
            get_date = get_date.replace('月', '');
            get_date = get_date.replace('/', '');
            var year = parseInt(get_date.substr(0, 4));
            var month = parseInt(get_date.substr(4, 2));
            if(month == 12){
                year += 1;
                month = 1;
            }else{
                month += 1;
            }
            var date = year + ('0' + month).slice(-2);
            calendar_Ajax(date);
        });
    }

    //カレンダー作成に必要なデータをAJAXでPOSTする関数です
    function calendar_Ajax(date){
        $.ajax({
            url: '/page2/ajax/getpart/' + $('#calender_parts_name').val(),
            type: 'POST',
            dataType:'html',
            data: {
                date: date,
            },
            success: function(data) {
                $($('#calender_parts_class').val()).html(data);

                calendarEvent();
            },
        });
    }
}
//=====ブロックカレンダーEND=====

//=====記事サーチSTART=====
/**
 * AJAXで記事を表示するJavascriptです
 * firstCount    hot_blogは初期表示行数
 * increment     hot_blogは増加行数
 * hot_blog_flag hot_blog整形時フラグ
**/
function shop_show_Post(firstCount, increment, hot_blog_flag) {
    if (typeof(firstCount) === 'undefined') {
        firstCount = 4;
    }
    if (typeof(increment) === 'undefined') {
        increment = 10;
    }
    if (typeof(hot_blog_flag) === 'undefined') {
        hot_blog_flag = false;
    }
    var blogListOption = {};
    blogListOption.wrapper       = "blog_list_block";
    blogListOption.firstCount    = firstCount;
    blogListOption.increment     = increment;
    blogListOption.hot_blog_flag = hot_blog_flag;

    //ページが読み込まれたら記事を表示
    $(function() {
        blogListOption.parts = $("#post_parts_name").val();
        blogListOption.base  = $(".content_wrap");
        
        var cookies = {
            floor_id      : '<?php echo $floor; ?>',
            item_id       : '<?php echo $item;  ?>',
            event_id      : '<?php echo $event; ?>',
            turn_flag     :  <?php echo $turn;  ?>,
            event_date    : '<?php echo $date;  ?>',
            favorite_flag :  <?php echo $fav . "\n"; ?>
        };
        postSearch(cookies.floor_id, cookies.item_id, cookies.event_id, cookies.turn_flag, cookies.event_date, cookies.favorite_flag);
        new BlogList($.extend({}, blogListOption, cookies)).fetch();
    });

    // 暫定対応： ページ毎にプルダウン選択時の表示文字数を変更する
    var select_text_length = 8;
    if(location.href.slice(-8, -1) == "hotblog"){
        select_text_length = 11;
    }
    var limit_length = select_text_length - 1;

    //記事絞り込みのプルダウンのイベントの関数です
    function postSearch(floor, item, event, turn, date, favorite) {
        var condition = {
            floor_id      : floor,
            item_id       : item,
            event_id      : event,
            turn_flag     : turn,
            event_date    : date,
            favorite_flag : favorite
        };
        
        //フロアー選択イベント
        $('ul.clear .blog_floor .menu .sub li').click(function(){
            var floor_name = $('a', this).text();
            if(floor_name.length > 11){
                floor_name = makeDisplayStrings(floor_name, 10);
            }
            $('.blog_floor .menu .sub span').html(floor_name);
            
            condition.floor_id = $(this).attr('id');
            new BlogList($.extend({}, blogListOption, condition)).fetch();
        });

        //アイテムカテゴリー選択イベント
        $('ul.clear .blog_item_category .menu .sub li').click(function(){
            var item_name = $('a', this).text();
            if(item_name.length > select_text_length){
                item_name = makeDisplayStrings(item_name, select_text_length);
            }
            $('.blog_item_category .menu .sub span').html(item_name);
            
            condition.item_id = $(this).attr('id');
            new BlogList($.extend({}, blogListOption, condition)).fetch();
        });

        //イベントカテゴリー選択イベント
        $('ul.clear .blog_event_sale .menu .sub li').click(function(){
            var event_name = $('a', this).text();
            if(event_name.length > select_text_length){
                event_name = makeDisplayStrings(event_name, select_text_length);
            }
            $('.blog_event_sale .menu .sub span').html(event_name);
            
            condition.event_id = $(this).attr('id');
            new BlogList($.extend({}, blogListOption, condition)).fetch();
        });

        //新着順、LIKE数順イベント
        $('ul#tab.clear li').click(function(){
            if(!$(this).hasClass('active')) {
                condition.turn_flag = ($('img', this).attr('alt') == '新着順') ? 1 : 0;
                new BlogList($.extend({}, blogListOption, condition)).fetch();
            }
        });

        //お気に入りイベント
        $('div.bl_cb').click(function() {
            var favorite_check = document.getElementById('like01');
            if(favorite_check.checked != favorite) {
                condition.favorite_flag = favorite_check.checked ? 1 : 0;
                new BlogList($.extend({}, blogListOption, condition)).fetch();
            }
        });

        //カレンダーイベント
        $('#mycarousel li').live("click",function() {
            condition.event_date = $(this).attr('id');
            new BlogList($.extend({}, blogListOption, condition)).fetch();
        });
    }

    // プルダウンで選択した文字列を指定長でカットして返却します
    function makeDisplayStrings(str, maxlength){
        return item_name = str.substr(0, maxlength) + "…";
    }
}
