<?php
//基本情報の取得
include('../../../wordpress/php/classes/Config.class.php');
?>

//日付を取得する関数です
function getDate(){
	var date = new Date();
	var year = date.getYear();
	var year4 = (year < 2000) ? year+1900 : year;
	var month = date.getMonth() + 1;
	var date = date.getDate();
	if (month < 10) {
		month = '0' + month;
	}
	if (date < 10) {
		date = '0' + date;
	}
	var strDate = year4 + '-' + month + '-' + date;
	return strDate;
}

//=====ブロックカレンダーSTART=====
function show_Calendar(){
	//ページが読み込まれたらサイドカレンダーを表示
	$('.right_block .side_calendar_block').ready(function(){;
		var tmp_date = getDate();
		var date = tmp_date.substr(0, 6);
		calendar_Ajax(date);
	});

	//先月と来月ボタンの動作
	function calendarEvent() {
		$('.month_block .prev').click(function(){
			var get_date = $('.month_block p').text();
			get_date = get_date.replace('年', '');
			get_date = get_date.replace('月', '');
			var year = parseInt(get_date.substr(0, 4));
			var month = parseInt(get_date.substr(4, 2));
			if(month == 1){
				year -= 1;
				month = 12;
			}else{
				month -= 1;
			}
			var date = year + ('0' + month).slice(-2);
			calendar_Ajax(date);
		});

		$('.month_block .next').click(function(){
			var get_date = $('.month_block p').text();
			get_date = get_date.replace('年', '');
			get_date = get_date.replace('月', '');
			var year = parseInt(get_date.substr(0, 4));
			var month = parseInt(get_date.substr(4, 2));
			if(month == 12){
				year += 1;
				month = 1;
			}else{
				month += 1;
			}
			var date = year + ('0' + month).slice(-2);
			calendar_Ajax(date);
		});
	}

	//カレンダー作成に必要なデータをAJAXでPOST
	function calendar_Ajax(date){
		$.ajax({
			url: '<?php echo $PARTS_PATH; ?>side_calendar_block.php',
			type: 'POST',
			dataType:'html',
			data: {
				date: date,
			},
			success: function(data) {
				$('.side_calendar_block').html(data);
				calendarEvent();
			},
		});
	}
}
//=====ブロックカレンダーEND=====

//=====記事サーチSTART=====
function shop_show_Post(){
	//ページが読み込まれたら記事を表示
	$('.content_wrap').ready(function(){
		var floor = 'all';
		var item = 'all';
		var event = 'all';
		var turn = 1;
		var date = getDate();
		shop_postSearch(floor, item, event, turn, date);
		shop_Ajax(floor, item, event, turn, date);
	});

// 記事を絞り込む
	function shop_postSearch(floor, item, event, turn, date) {
// アイテムカテゴリーで記事を絞り込む
		$('ul.clear .blog_item_category .menu .sub li').click(function(){
			var item_name = $('a', this).text();
			item = $(this).attr('id');
			$('.blog_item_category .menu .sub span').html(item_name);
			shop_Ajax(floor, item, event, turn, date);
		});
// イベント・セールで記事を絞り込む
		$('ul.clear .blog_event_sale .menu .sub li').click(function(){
			var event_name = $('a', this).text();
			event = $(this).attr('id');
			$('.blog_event_sale .menu .sub span').html(event_name);
			shop_Ajax(floor, item, event, turn, date);
		});
// 新着順・LIKE数順で絞り込む
		$('ul#tab.clear li').click(function(){
			var flag = $(this).attr('class');
			if(flag == 'tab_btn'){
				turn = 1;
			}else if(flag == 'tab_btn last'){
				turn = 0;
			}
			shop_Ajax(floor, item, event, turn, date);
		});
// カレンダーの日付で絞り込む
		$('#mycarousel li').click(function(){
			var tmp_date = $('a', this).text();
			var year = $(this).attr('id');
			tmp_date = tmp_date.replace('/', '');
			date = tmp_date.substr(0,2) + '-' + tmp_date.substr(2,4);
			date = year + '-' + date;
			shop_Ajax(floor, item, event, turn, date);
		});
	}

// 記事絞り込みに必要なデータをAJAXでPOSTする
	function shop_Ajax(floor, item, event, turn, date){
		$.ajax({
			url: '<?php echo $PARTS_PATH; ?>shop_content_wrap.php',
			type: 'POST',
			dataType:'html',
			data: {
				blog: {'floor_id': floor,
					   'item_id': item,
					   'event_id': event,
					   'turn_flag': turn,
					   'event_date': date,
					   },
			},
			success: function(data) {
				$('.content_wrap').html(data);
			},
		});
	}
}
