<?php
include_once($_SERVER['DOCUMENT_ROOT']. '/web/annex15th/inc/config.php');

$title = TITLE2.' | '.TITLE;
$description = DESCRIPTION2;
$keywords = KEYWORDS2;
define('PAGE_NAME', 'newshop');

function is_mobile () {
  $uamb = array(
    'DoCoMo',
    'KDDI',
    'SoftBank',
    'UP.Browser',
    'J-PHONE',
    'Vodafone'
  );
  $patternmb = '/'.implode('|', $uamb).'/i';
  return preg_match($patternmb, $_SERVER['HTTP_USER_AGENT']);
}
if (is_mobile()) {
  header("Location: /s/".DIRNAME."/".PAGE_NAME."/");
  exit;
}
?><!DOCTYPE html>
<html lang="ja">
<head>
<script type="text/javascript">
if ((navigator.userAgent.indexOf('iPhone') > 0 && navigator.userAgent.indexOf('iPad') == -1) || navigator.userAgent.indexOf('iPod') > 0 || navigator.userAgent.indexOf('Android') > 0) {
  location.href = '/s/<?=DIRNAME?>/<?=PAGE_NAME?>/';
}
</script>
<?php include_once($_SERVER['DOCUMENT_ROOT'].'/web/'.DIRNAME.'/inc/html-head.php'); ?>
</head>

<body class="newshop main">
<?php include_once($_SERVER['DOCUMENT_ROOT'] . '/load_script.php'); ?>

<?php include_once($_SERVER['DOCUMENT_ROOT']. '/web/'.DIRNAME.'/inc/header.php'); ?>

<div class="contents">

<h2 class="ta-c"><img src="/web/<?=DIRNAME?>/images/ttl-<?=PAGE_NAME?>.png" alt="NEW AND RENEWAL" /></h2>

<?php include_once($_SERVER['DOCUMENT_ROOT']. '/web/'.DIRNAME.'/inc/nav-nr.php'); ?>



<h2 class="ta-c mt60"><img src="../images/newshop/ttl-6f.png" alt="6F"></h2>

<div class="photo-wrap">
<?php
/**
 * PHOTO-START
 */
?>


<div class="photo-block item-all">
  <div class="photo-block-in"><img class="visual" src="../images/newshop/shop/1.jpg" width="490" height="490" alt="ユニケース">
    <div class="photo-hover"><span class="icon new"></span><span class="badge"></span>
      <div class="fadeInLeft animated">
        <h1 class="hover-ttl">本館 6F<br>9月16日（金）NEW OPEN!<br><span>ユニケース / スマートフォンケース</span></h1>
        <p class="hover-img"><img src="../images/newshop/logo/1.png" alt="ユニケース"></p>
        <p class="hover-text">ユニークで厳選されたアイテムを豊富に取り揃え、「面白い」「オリジナル」「可愛い」「トレンド」「便利」をコンセプトとした人気のアイテムを発信し続けるスマートフォンアクセサリー専門店です。</p>
        <div class="hover-box clearfix">
          <p></p>
          <!-- hover-box -->
        </div>
        <!-- fadeInLeft animated -->
      </div>
      <!-- photo-hover -->
    </div>
  </div>
</div>
<div class="photo-block item-all">
  <div class="photo-block-in"><img class="visual" src="../images/newshop/shop/2.jpg" width="490" height="490" alt="コムサコミューン">
    <div class="photo-hover"><span class="icon new"></span><span class="badge"></span>
      <div class="fadeInLeft animated">
        <h1 class="hover-ttl">本館 6F<br>9月16日（金）NEW OPEN!<br><span>コムサコミューン / メンズ</span></h1>
        <p class="hover-img"><img src="../images/newshop/logo/2.png" alt="コムサコミューン"></p>
        <p class="hover-text">"伝統的なトラッドをベースに現代の感性を通した服づくり。
          <br>知的で上品なコンテンポラリートラッドスタイルを提案します。 "
        </p>
        <div class="hover-box clearfix">
          <p></p>
          <!-- hover-box -->
        </div>
        <!-- fadeInLeft animated -->
      </div>
      <!-- photo-hover -->
    </div>
  </div>
</div>
<div class="photo-block item-all">
  <div class="photo-block-in"><img class="visual" src="../images/newshop/shop/3.jpg" width="490" height="490" alt="アルファショップ">
    <div class="photo-hover"><span class="icon new"></span><span class="badge"></span>
      <div class="fadeInLeft animated">
        <h1 class="hover-ttl">本館 6F<br>9月16日（金）NEW OPEN!<br><span>アルファショップ / メンズ・レディス</span></h1>
        <p class="hover-img"><img src="../images/newshop/logo/3.png" alt="アルファショップ"></p>
        <p class="hover-text">「ＫＩＮＧ　ＯＦ　ＭＩＬＩＴＡＲＹ　ＡＬＰＨＡ　ＩＮＤＵＳＴＲＩＥＳ」
          <br>1959年から現在まで全世界で愛され続けてきたミリタリーウェアーの代表的ブランド。
          <br>時代や、流行に左右されることなく、・本物・を作り続けている。</p>
        <div class="hover-box clearfix">
          <p>お買上げの方ノベルティ進呈（数量限定）</p>
          <!-- hover-box -->
        </div>
        <!-- fadeInLeft animated -->
      </div>
      <!-- photo-hover -->
    </div>
  </div>
</div>
<div class="photo-block item-all">
  <div class="photo-block-in"><img class="visual" src="../images/newshop/shop/4.jpg" width="490" height="490" alt="レイジブルー">
    <div class="photo-hover"><span class="icon renewal"></span>
      <div class="fadeInLeft animated">
        <h1 class="hover-ttl">本館 6F<br>9月16日（金）RENEWAL OPEN!<br><span>レイジブルー / メンズ</span></h1>
        <p class="hover-img"><img src="../images/newshop/logo/4.png" alt="レイジブルー"></p>
        <p class="hover-text">アメリカンカジュアルとヨーロピアントラディッショナルを融合させ、 素材とディティールにこだわり、 その時々のトレンド要素を取り入れ、 様々なスタイルをミックスしたコーディネートを提案します。</p>
        <div class="hover-box clearfix">
          <p>9/16～17 パルコカードご利用・新規ご入会で15%OFF
          <br>9/18～25 パルコカードご利用・新規ご入会で10％OFF
          <br>クーポン付きのノベルティ進呈（数量限定）</p>
          <!-- hover-box -->
        </div>
        <!-- fadeInLeft animated -->
      </div>
      <!-- photo-hover -->
    </div>
  </div>
</div>
<div class="photo-block item-all">
  <div class="photo-block-in"><img class="visual" src="../images/newshop/shop/5.jpg" width="490" height="490" alt="シャッポ">
    <div class="photo-hover"><span class="icon renewal"></span>
      <div class="fadeInLeft animated">
        <h1 class="hover-ttl">本館 6F<br>9月16日（金）RENEWAL OPEN!<br><span>シャッポ / 帽子</span></h1>
        <p class="hover-img"><img src="../images/newshop/logo/5.png" alt="シャッポ"></p>
        <p class="hover-text">私達は、帽子を洋服の延長線だと考えております。洋服を毎日着替えるように、帽子もファッションの一部として取り入れてみて下さい。 「CA4LA」を中心に、こだわりのある帽子を幅広く取り揃えています。自分に似合う一点を探しに、是非一度ご来店下さい。</p>
        <div class="hover-box clearfix">
          <p>9/16～17 全商品20%OFF
          <br>9/18～25 全商品10%OFF</p>
          <!-- hover-box -->
        </div>
        <!-- fadeInLeft animated -->
      </div>
      <!-- photo-hover -->
    </div>
  </div>
</div>
<?php /* ?>
<div class="photo-block item-all">
  <div class="photo-block-in"><img class="visual" src="../images/newshop/shop/10.jpg" width="490" height="490" alt="【EVENT】KAMEN RIDER 45th EXHIBITION SHOP">
    <div class="photo-hover"><span class="icon "></span>
      <div class="fadeInLeft animated">
        <h1 class="hover-ttl">本館 6F 特設会場<br>9月16日（金） OPEN!<br><span>【EVENT】KAMEN RIDER 45th EXHIBITION SHOP / </span></h1>
        <p class="hover-img"><img src="../images/newshop/logo/10.png" alt="【EVENT】KAMEN RIDER 45th EXHIBITION SHOP"></p>
        <p class="hover-text">仮面ライダーの45周年を記念したエキシビションショップが期間限定で広島パルコにオープン！
          <br>仮面ライダーの45年間の軌跡を展示とスペシャルアイテムで辿ります。
          <br>詳しくはこちら
          <br><a href="http://hiroshima.parco.jp/page2/event/9667/">http://hiroshima.parco.jp/page2/event/9667/</a></p>
        <div class="hover-box clearfix">
          <p></p>
          <!-- hover-box -->
        </div>
        <!-- fadeInLeft animated -->
      </div>
      <!-- photo-hover -->
    </div>
  </div>
</div>
<?php */ ?>


<?php
/**
 * PHOTO-END
 */
?>
</div>

<h2 class="ta-c mt60"><img src="../images/newshop/ttl-247f.png" alt="2F/4F/7F"></h2>


<div class="photo-wrap">
<?php
/**
 * PHOTO-START
 */
?>

<div class="photo-block item-all">
  <div class="photo-block-in"><img class="visual" src="../images/newshop/shop/6.jpg" width="490" height="490" alt="グランビュフル">
    <div class="photo-hover"><span class="icon new"></span>
      <div class="fadeInLeft animated">
        <h1 class="hover-ttl">本館 2F<br>9月16日（金）NEW OPEN!<br><span>グランビュフル / レディス</span></h1>
        <p class="hover-img"><img src="../images/newshop/logo/6.png" alt="グランビュフル"></p>
        <p class="hover-text">全ての女性に最高の輝きを・・・・・
          <br>いつまでも輝き続けたい・・・・・
          <br>知的な女性の為のラグジュアリー感のあるお呼ばれ服・アクセサリー・雑貨の提案。
          <br>
          <br>洗練されたフォルムとジュエリーのような華やかさ、大切な仲間との素敵な時間をGRAN BUFULLで・・・・・</p>
        <div class="hover-box clearfix">
          <p>全商品10%OFF
          <br>15,000円以上お買上げでノベルティ進呈（数量限定）</p>
          <!-- hover-box -->
        </div>
        <!-- fadeInLeft animated -->
      </div>
      <!-- photo-hover -->
    </div>
  </div>
</div>
<div class="photo-block item-all">
  <div class="photo-block-in"><img class="visual" src="../images/newshop/shop/7.jpg" width="490" height="490" alt="トミーヒルフィガー">
    <div class="photo-hover"><span class="icon new"></span>
      <div class="fadeInLeft animated">
        <h1 class="hover-ttl">本館 2F<br>9月16日（金）NEW OPEN!<br><span>トミーヒルフィガー / メンズ・レディス</span></h1>
        <p class="hover-img"><img src="../images/newshop/logo/7.png" alt="トミーヒルフィガー"></p>
        <p class="hover-text">1985年にアメリカで誕生したトミーヒルフィガーのショップがオープン！</p>
        <div class="hover-box clearfix">
          <p>9/16～25　PARCOカードご利用または新規ご入会で10％OFF</p>
          <!-- hover-box -->
        </div>
        <!-- fadeInLeft animated -->
      </div>
      <!-- photo-hover -->
    </div>
  </div>
</div>
<div class="photo-block item-all">
  <div class="photo-block-in"><img class="visual" src="../images/newshop/shop/8.jpg" width="490" height="490" alt="アイショップファイブ">
    <div class="photo-hover"><span class="icon new"></span>
      <div class="fadeInLeft animated">
        <h1 class="hover-ttl">本館 4F<br>9月16日（金）NEW OPEN!<br><span>アイショップファイブ / カラーコンタクトレンズ</span></h1>
        <p class="hover-img"><img src="../images/newshop/logo/8.png" alt="アイショップファイブ"></p>
        <p class="hover-text">カラーコンタクトレンズ（カラコン）専門店！ 話題のカラコンをいち早く度なしはもちろん、度ありも豊富に取り揃えております。 他店で度数がなかった方、種類が多くて何が良いのかわからない方、カラコンはカラコンアドバイザー在籍の専門店で。</p>
        <div class="hover-box clearfix">
          <p>〈PARCOカード〉ご利用で全商品5%OFF</p>
          <!-- hover-box -->
        </div>
        <!-- fadeInLeft animated -->
      </div>
      <!-- photo-hover -->
    </div>
  </div>
</div>
<div class="photo-block item-all">
  <div class="photo-block-in"><img class="visual" src="../images/newshop/shop/9.jpg" width="490" height="490" alt="アルゴンキン">
    <div class="photo-hover"><span class="icon new"></span>
      <div class="fadeInLeft animated">
        <h1 class="hover-ttl">本館 7F<br>9月16日（金）NEW OPEN!<br><span>アルゴンキン / レディス</span></h1>
        <p class="hover-img"><img src="../images/newshop/logo/9.png" alt="アルゴンキン"></p>
        <p class="hover-text">アルゴンキンはカテゴリーにとらわれない発想で、常に新鮮かつ遊び心のあるファッションをカジュアルに表現します。こだわりのシルエットを基本に、色々なテイストを取り入れたイマジネーションで、着る人が自由な組み合わせで新しい発見と刺激を感じられるようなアイテムを創造します。
        </p>
        <div class="hover-box clearfix">
          <p>10,000円以上お買上げでノベルティ進呈（数量限定）</p>
          <!-- hover-box -->
        </div>
        <!-- fadeInLeft animated -->
      </div>
      <!-- photo-hover -->
    </div>
  </div>
</div>

<?php
/**
 * PHOTO-END
 */
?>
</div>

<!-- /contents --></div>

<?php include_once($_SERVER['DOCUMENT_ROOT']. '/web/'.DIRNAME.'/inc/footer.php'); ?>
</body>
</html>