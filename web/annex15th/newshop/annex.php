<?php
include_once($_SERVER['DOCUMENT_ROOT']. '/web/annex15th/inc/config.php');

$title = TITLE2.' | '.TITLE;
$description = DESCRIPTION2;
$keywords = KEYWORDS2;
define('PAGE_NAME', 'newshop');

function is_mobile () {
  $uamb = array(
    'DoCoMo',
    'KDDI',
    'SoftBank',
    'UP.Browser',
    'J-PHONE',
    'Vodafone'
  );
  $patternmb = '/'.implode('|', $uamb).'/i';
  return preg_match($patternmb, $_SERVER['HTTP_USER_AGENT']);
}
if (is_mobile()) {
  header("Location: /s/".DIRNAME."/".PAGE_NAME."/");
  exit;
}
?><!DOCTYPE html>
<html lang="ja">
<head>
<script type="text/javascript">
if ((navigator.userAgent.indexOf('iPhone') > 0 && navigator.userAgent.indexOf('iPad') == -1) || navigator.userAgent.indexOf('iPod') > 0 || navigator.userAgent.indexOf('Android') > 0) {
  location.href = '/s/<?=DIRNAME?>/<?=PAGE_NAME?>/';
}
</script>
<?php include_once($_SERVER['DOCUMENT_ROOT'].'/web/'.DIRNAME.'/inc/html-head.php'); ?>
</head>

<body class="newshop annex">
<?php include_once($_SERVER['DOCUMENT_ROOT'] . '/load_script.php'); ?>

<?php include_once($_SERVER['DOCUMENT_ROOT']. '/web/'.DIRNAME.'/inc/header.php'); ?>

<div class="contents">

<h2 class="ta-c"><img src="/web/<?=DIRNAME?>/images/ttl-<?=PAGE_NAME?>.png" alt="NEW AND RENEWAL" /></h2>

<?php include_once($_SERVER['DOCUMENT_ROOT']. '/web/'.DIRNAME.'/inc/nav-nr.php'); ?>




<div class="photo-wrap">
<?php
/**
 * PHOTO-START
 */
?>


<div class="photo-block item-all">
  <div class="photo-block-in"><img class="visual" src="../images/newshop/shop/11.jpg" width="490" height="490" alt="アダム エ ロペ">
    <div class="photo-hover"><span class="icon renewal"></span>
      <div class="fadeInLeft animated">
        <h1 class="hover-ttl">新館 B1F メンズ / 新館 1F レディス<br>9月16日（金）RENEWAL OPEN!<br><span>アダム エ ロペ / 新館 B1F メンズ / 新館 1F レディス</span></h1>
        <p class="hover-img"><img src="../images/newshop/logo/11.png" alt="アダム エ ロペ"></p>
        <p class="hover-text">「ORDINARY　NEWNESS　ORDINARY　COMFORT」アップデートされていく自分を楽しむこと。ADAMETROPEは毎日に洗練された心地よさと新しさを提案します。</p>
        <div class="hover-box clearfix">
          <p>①9/16～25　〈PARCOカード〉ご利用・新規ご入会で全商品10％OFF
            <br>②9/16～22　Wポイントサービス
            <br>③9/16～25　＜ANITA　BILARDI（アニタ・ビラルディ）＞POP UP SHOP
            <br>④9/16～　売り切れ次第終了　＜ST・CAT（エスティキャット）＞POP UP SHOP</p>
          <!-- hover-box -->
        </div>
        <!-- fadeInLeft animated -->
      </div>
      <!-- photo-hover -->
    </div>
  </div>
</div>
<div class="photo-block item-all">
  <div class="photo-block-in"><img class="visual" src="../images/newshop/shop/12.jpg" width="490" height="490" alt="ムラサキスポーツ">
    <div class="photo-hover"><span class="icon renewal"></span>
      <div class="fadeInLeft animated">
        <h1 class="hover-ttl">新館 6F<br>9月16日（金）RENEWAL OPEN!<br><span>ムラサキスポーツ / スポーツ</span></h1>
        <p class="hover-img"><img src="../images/newshop/logo/12.png" alt="ムラサキスポーツ"></p>
        <p class="hover-text fz12">アクションスポーツの専門店ムラサキスポーツ。 サーフィン、スノーボード、スケートボードのギアをはじめ、ウェア、水着、シューズ、時計、サングラスなどのファッションアイテムが盛りだくさん。 カッコよく、カワイく、オシャレに着こなすならムラサキスポーツにお任せ！！</p>
        <div class="hover-box clearfix fz12">
          <p>9/16～25
          <br>①〈PARCOカード〉ご利用・新規ご入会で全商品5％OFF
          <br>②Wポイント進呈
          <br>③スノーボード関連商品アウトレットセール開催</p>
          <!-- hover-box -->
        </div>
        <!-- fadeInLeft animated -->
      </div>
      <!-- photo-hover -->
    </div>
  </div>
</div>
<div class="photo-block item-all">
  <div class="photo-block-in"><img class="visual" src="../images/newshop/shop/13.jpg" width="490" height="490" alt="ABCマート">
    <div class="photo-hover"><span class="icon renewal"></span>
      <div class="fadeInLeft animated">
        <h1 class="hover-ttl">新館 6F<br>9月16日（金）RENEWAL OPEN!<br><span>ABCマート / シューズ</span></h1>
        <p class="hover-img"><img src="../images/newshop/logo/13.png" alt="ABCマート"></p>
        <p class="hover-text">足元からライフスタイルを提案するシューズ専門店。メンズ・レディス・キッズのシューズを、リーズナブルな価格で幅広く取り揃えております。</p>
        <div class="hover-box clearfix">
          <p>9/16～25　Wポイント進呈</p>
          <!-- hover-box -->
        </div>
        <!-- fadeInLeft animated -->
      </div>
      <!-- photo-hover -->
    </div>
  </div>
</div>
<div class="photo-block item-all">
  <div class="photo-block-in"><img class="visual" src="../images/newshop/shop/14.jpg" width="490" height="490" alt="ガリャルダガランテ">
    <div class="photo-hover"><span class="icon renewal"></span>
      <div class="fadeInLeft animated">
        <h1 class="hover-ttl">新館 2F レディス<br>9月16日（金）RENEWAL OPEN!<br><span>ガリャルダガランテ / 新館 2F レディス</span></h1>
        <p class="hover-img"><img src="../images/newshop/logo/14.png" alt="ガリャルダガランテ"></p>
        <p class="hover-text fz12">新しいコンセプトのもと、リニューアルオープンしたショップは温かみあるインテリアが特徴。北欧ヴィンテージが好きな、センスのいい人のお宅に招かれたような気分になる、ゆったりとくつろげるおしゃれなホームインテリアをイメージ。内装、家具、ディスプレイされたアートなど、すべて本物志向にこだわっている。<br>①9/16～18　全商品10％OFF
            <br>②9/16限定　Wポイントサービス
            <br>③9/16限定　税込32,400円以上お買い上げの先着30名様に「オリジナルノベルティ」進呈</p>
        <div class="hover-box clearfix">
          <p></p>
          <!-- hover-box -->
        </div>
        <!-- fadeInLeft animated -->
      </div>
      <!-- photo-hover -->
    </div>
  </div>
</div>
<div class="photo-block item-all">
  <div class="photo-block-in"><img class="visual" src="../images/newshop/shop/15.jpg" width="490" height="490" alt="チューズライフ　バイ　キャサリンハムネット">
    <div class="photo-hover"><span class="icon renewal"></span>
      <div class="fadeInLeft animated">
        <h1 class="hover-ttl">新館 B1F<br>9月16日（金）RENEWAL OPEN!<br><span>チューズライフ　バイ　キャサリンハムネット / メンズ・レディス</span></h1>
        <p class="hover-img"><img src="../images/newshop/logo/15.png" alt="チューズライフ　バイ　キャサリンハムネット"></p>
        <p class="hover-text">アバンギャルドでセクシーだが、スマート。英国の伝統的なクラシックデザインのバランス感覚を独自の感性で表現。</p>
        <div class="hover-box clearfix">
          <p>自店アプリダウンロードの方全商品10%OFF</p>
          <!-- hover-box -->
        </div>
        <!-- fadeInLeft animated -->
      </div>
      <!-- photo-hover -->
    </div>
  </div>
</div>
<div class="photo-block item-all">
  <div class="photo-block-in"><img class="visual" src="../images/newshop/shop/16.jpg" width="490" height="490" alt="カミーユビスランダ">
    <div class="photo-hover"><span class="icon new"></span>
      <div class="fadeInLeft animated">
        <h1 class="hover-ttl">新館 3F<br>9月16日（金）NEW OPEN!<br><span>カミーユビスランダ / レディスシューズ</span></h1>
        <p class="hover-img"><img src="../images/newshop/logo/16.png" alt="カミーユビスランダ"></p>
        <p class="hover-text">「work simple play natural」
          <br>カジュアルをベースにシンプル・モードを自分らしく着こなす高感度な女性のためのシュークローゼット。</p>
        <div class="hover-box clearfix">
          <p>5,400円以上お買上げでノベルティ進呈（数量限定）</p>
          <!-- hover-box -->
        </div>
        <!-- fadeInLeft animated -->
      </div>
      <!-- photo-hover -->
    </div>
  </div>
</div>
<div class="photo-block item-all">
  <div class="photo-block-in"><img class="visual" src="../images/newshop/shop/17.jpg" width="490" height="490" alt="スマホ修理工房">
    <div class="photo-hover"><span class="icon new"></span>
      <div class="fadeInLeft animated">
        <h1 class="hover-ttl">新館 3F<br>8月26日（金）NEW OPEN!<br><span>スマホ修理工房 / スマートフォン即日修理</span></h1>
        <p class="hover-img"><img src="../images/newshop/logo/17.png" alt="スマホ修理工房"></p>
        <p class="hover-text">スマホ修理工房にスマートフォン・タブレットでお困りの事は何でもご相談ください。
          <br>ガラス割れ、液晶不良、バッテリー交換、水没修理など即日対応いたします。</p>
        <div class="hover-box clearfix">
          <p></p>
          <!-- hover-box -->
        </div>
        <!-- fadeInLeft animated -->
      </div>
      <!-- photo-hover -->
    </div>
  </div>
</div>
<div class="photo-block item-all">
  <div class="photo-block-in"><img class="visual" src="../images/newshop/shop/18.jpg" width="490" height="490" alt="サンキューマート">
    <div class="photo-hover"><span class="icon new"></span>
      <div class="fadeInLeft animated">
        <h1 class="hover-ttl">新館 6F<br>9月16日（金）NEW OPEN!<br><span>サンキューマート / 雑貨</span></h1>
        <p class="hover-img"><img src="../images/newshop/logo/18.png" alt="サンキューマート"></p>
        <p class="hover-text">いつ来ても楽しい、カワイイ、素敵なモノいっぱい！店内全品390円！</p>
        <div class="hover-box clearfix">
          <p></p>
          <!-- hover-box -->
        </div>
        <!-- fadeInLeft animated -->
      </div>
      <!-- photo-hover -->
    </div>
  </div>
</div>


<?php
/**
 * PHOTO-END
 */
?>
</div>



<!-- /contents --></div>

<?php include_once($_SERVER['DOCUMENT_ROOT']. '/web/'.DIRNAME.'/inc/footer.php'); ?>
</body>
</html>