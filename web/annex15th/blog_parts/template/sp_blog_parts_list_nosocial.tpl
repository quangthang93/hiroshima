{{#data.result.items}}
<div class="list_block divc">
    <div class="image">
        <div class="square_img">
            <a href="{{url_sp}}"{{#view.view_link_target}} target="{{view.view_link_target}}"{{/view.view_link_target}}><img src="{{image.M_url}}" alt="{{title}}" /></a>
        </div>
    </div>
    {{#view.view_store_name}}<p class="post_store">{{store_name}}PARCO</p>{{/view.view_store_name}}
    {{#storesjp_product_id}}
        {{^view.view_store_name}}<div class="kaeru_block"></div>{{/view.view_store_name}}
        {{#view.view_store_name}}<div class="kaeru_block_storename"></div>{{/view.view_store_name}}
    {{/storesjp_product_id}}
    <div class="text clear middle_block">
        <div class="text_content_middle">
            {{#view.view_shop_name}}<p class="shop_name">{{shop_name}}</p>{{/view.view_shop_name}}
            <p class="text_link"><a href="{{url_sp}}"{{#view.view_link_target}} target="{{view.view_link_target}}"{{/view.view_link_target}}>{{title}}</a></p>
        </div>
    </div>
    <p class="list_wrap_link">
        <a href="{{url_sp}}"> </a>
    </p>
</div>
{{/data.result.items}}