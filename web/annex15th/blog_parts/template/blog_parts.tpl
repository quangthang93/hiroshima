{{#data.result.items}}
<div class="each_post_block">
    <div class="inner">
        <div class="post_image{{^image.type}} square_img_table{{/image.type}}">
            <div{{^image.type}} class="square_img"{{/image.type}}>
                <a href="{{url_pc}}"{{#image.type}} class="imgLiquidBox"{{/image.type}}{{#view.view_link_target}} target="{{view.view_link_target}}"{{/view.view_link_target}}><img src="{{image.L_url}}" alt="{{title}}" /></a>
            </div>
            <div class="like_block">
                <p class="like_number">{{clipnum}}</p>
            </div>
            {{#storesjp_product_id}}
                {{^view.view_store_name}}<div class="kaeru_block"></div>{{/view.view_store_name}}
                {{#view.view_store_name}}<div class="kaeru_block_storename"></div>{{/view.view_store_name}}
            {{/storesjp_product_id}}
        </div>
        {{#view.view_store_name}}<p class="post_store">{{store_name}}PARCO</p>{{/view.view_store_name}}
        {{#view.view_shop_name}}<p class="post_shop">{{#view.view_floor_name}}<span class="label">{{floor.type}} {{floor.floor}}</span>{{/view.view_floor_name}}{{shop_name}}</p>{{/view.view_shop_name}}
        <p class="post_title">
        {{#event_date_isopen}}<span class="post_event_date"><img src="/common/images/common_event_icon_001.gif"></span>{{/event_date_isopen}}
        {{#event_date_string}}<span class="post_event_date">{{event_date_string}}</span>{{/event_date_string}}
        <a href="{{url_pc}}"{{#view.view_link_target}} target="{{view.view_link_target}}"{{/view.view_link_target}}>{{title}}</a>
        </p>
    </div>
</div>
{{/data.result.items}}