{{#data.result.items}}
<div class="list_block divc">
    <div class="image">
        <div class="square_img">
            <a href="{{url_sp}}"{{#view.view_link_target}} target="{{view.view_link_target}}"{{/view.view_link_target}}><img src="{{image.M_url}}" alt="{{title}}" /></a>
        </div>
        <div class="like_number">
            <p>{{clipnum}}</p>
        </div>
        {{#storesjp_product_id}}
            {{^view.view_store_name}}<div class="kaeru_block"></div>{{/view.view_store_name}}
            {{#view.view_store_name}}<div class="kaeru_block_storename"></div>{{/view.view_store_name}}
        {{/storesjp_product_id}}
    </div>
    {{#view.view_store_name}}<p class="post_store">{{store_name}}PARCO</p>{{/view.view_store_name}}
    <div class="text clear">
        <div class="text_content">
            {{#view.view_shop_name}}<p class="shop_name">{{shop_name}}</p>{{/view.view_shop_name}}
            {{#event_date_isopen}}<span class="post_event_date"><img src="/s/common/images/common_icon_033.png" width="32"></span>{{/event_date_isopen}}
            {{#event_date_string}}<p class="post_event_date">{{event_date_string}}</p>{{/event_date_string}}
            <p class="text_link"><a href="{{url_sp}}"{{#view.view_link_target}} target="{{view.view_link_target}}"{{/view.view_link_target}}>{{title}}</a></p>
        </div>
    </div>
</div>
{{/data.result.items}}