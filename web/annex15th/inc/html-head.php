<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=1060">
<title><?php echo $title ?></title>
<meta name="description" content="<?php echo $description ?>" />
<meta name="keywords" content="<?php echo $keywords ?>"/>
<?php include_once($_SERVER['DOCUMENT_ROOT'].'/web/'.DIRNAME.'/inc/ogp.php'); ?>
<link rel="alternate" media="only screen and (max-width: 640px)" href="/s/<?=DIRNAME?>" />
<link rel="stylesheet" href="/web/<?=DIRNAME?>/css/default.css" media="screen,print" />
<link rel="stylesheet" href="/web/<?=DIRNAME?>/blog_parts/css/blog_parts.css" media="all"/>
<link rel="stylesheet" href="/web/<?=DIRNAME?>/css/style.css" media="screen,print" />
<link rel="stylesheet" href="/web/<?=DIRNAME?>/css/<?=PAGE_NAME?>.css" />

<script src="/web/<?=DIRNAME?>/js/modernizr.js"></script>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<!-- <script>window.jQuery || document.write('<script src="/web/<?=DIRNAME?>/js/jquery.js"><\/script>')</script> -->
<!-- <script src="/common/js/jquery.js"></script> -->
<script src="/web/<?=DIRNAME?>/js/masonary.min.js"></script>
<!--<script src="/web/<?=DIRNAME?>/js/jquery.transit.min.js"></script>-->
<script src="/web/<?=DIRNAME?>/js/common.js"></script>
<script src="/common/js/social.js"></script>
<script src="/common/js/imgLiquid.js"></script>
<script src="/web/<?=DIRNAME?>/blog_parts/js/jquery.tile.js"></script>
<script src="/web/<?=DIRNAME?>/blog_parts/js/hogan-2.0.0.js"></script>
<script src="/web/<?=DIRNAME?>/blog_parts/js/jquery.blog_parts.js"></script>

