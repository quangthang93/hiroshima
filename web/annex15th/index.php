<?php
include_once($_SERVER['DOCUMENT_ROOT']. '/web/annex15th/inc/config.php');

$title = TITLE;
$description = DESCRIPTION1;
$keywords = KEYWORDS1;
define('PAGE_NAME', 'top');

function is_mobile () {
  $uamb = array(
    'DoCoMo',
    'KDDI',
    'SoftBank',
    'UP.Browser',
    'J-PHONE',
    'Vodafone'
  );
  $patternmb = '/'.implode('|', $uamb).'/i';
  return preg_match($patternmb, $_SERVER['HTTP_USER_AGENT']);
}
if (is_mobile()) {
  header("Location: /s/".DIRNAME);
  exit;
}
?><!DOCTYPE html>
<html lang="ja">
<head>
<script type="text/javascript">
if ((navigator.userAgent.indexOf('iPhone') > 0 && navigator.userAgent.indexOf('iPad') == -1) || navigator.userAgent.indexOf('iPod') > 0 || navigator.userAgent.indexOf('Android') > 0) {
  location.href = '/s/<?=DIRNAME?>';
}
</script>
<?php include_once($_SERVER['DOCUMENT_ROOT'].'/web/'.DIRNAME.'/inc/html-head.php'); ?>
</head>

<body class="top">
<?php include_once($_SERVER['DOCUMENT_ROOT'] . '/load_script.php'); ?>

<a name="top" id="top"></a>

<div class="top-contents">
<div class="top-contents__in">
<h1 class="top-title"><img src="images/title.png" alt="<?=TITLE?>" /></h1>
<p class="top-catch"><img src="images/catch.png" alt="毎日に、ドキドキを。広島に、ワクワクを。"></p>
<p class="top-date"><img src="images/date.png" alt="9/16（金） オープン" /></p>
<p class="top-text01"><img src="images/top-txt01.png" alt="本館・新館  大リニューアル!" /></p>

<div class="ta-c mt70"><iframe width="560" height="315" src="https://www.youtube.com/embed/GyqCe0ACuj8?rel=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe></div>

<ul class="top-btns">
<li class="top-btns-item"><a href="newshop/" class="top-btns-link type01">NEW AND RENEWAL</a></li><!--
 --><li class="top-btns-item"><a href="news/" class="top-btns-link type02">NEWS AND EVENT</a></li><!--
 --><li class="top-btns-item mr00 mt65"><a href="star/" class="top-btns-link type03">パルコスター</a></li><!--
 
 --><!--<li class="top-btns-item mr00 mt65"><a href="http://www.chushinren.jp/fashionweek/" target="_blank"><img src="images/bnr01.jpg" width="1000" height="180" alt="HIROSHIMA FASHION WEEK"></a></li>-->
</ul>

<!-- /top-contents__in --></div>
<!-- /top-contents --></div>

<?php include_once($_SERVER['DOCUMENT_ROOT']. '/web/'.DIRNAME.'/inc/footer.php'); ?>
</body>
</html>