(function($) {
  $.fn.randomize = function(childElem) {
    return this.each(function() {
        var $this = $(this);
        var elems = $this.children(childElem);
        elems.sort(function() { return (Math.round(Math.random())-0.5); });
        $this.remove(childElem);
        for(var i=0; i < elems.length; i++)
          $this.append(elems[i]);
    });
  }
})(jQuery);

/*================================================================

共通スクリプト

================================================================*/
$(function(){
	// クラスの追加
	$('body :first-child').addClass('firstChild'); //最後の要素
	$('body :last-child').addClass('lastChild'); //最初の要素


	// back to pagetop
	$('a[href^=#]').click(function () {
		$(this).blur();
			$('html,body').animate({ scrollTop: 0 }, 'slow');
		return false;
	});

  var modal = $('.modal');
  var modalWrap = $('.modal-wrap');
  var modalCloseBtn = $('.btn_modal_close');
  function modalClose() {
    modal.fadeOut('fast');
  };
	$('.js-modal').on('click', function(e) {
    e.preventDefault();
      modal.fadeIn('fast');
  });
  modalCloseBtn.on('click', function(e) {
    e.preventDefault();
    modalClose();
  });
  modalWrap.on('click', function(e) {
    e.preventDefault();
    modalClose();
  });

	$('.photo-link').each(function() {
  	$(this).hover(function(){
    	$(this).find('.photo-hover').fadeIn(200);
  	},function(){
    	$(this).find('.photo-hover').fadeOut(200);
  });

});

/* !categoryclass --------------------------------------------------- */

  $('.nav-in li a').each(function() {

    $(this).click(function(event){
      $this = $(this);
      $body = $('body');
      var selected = $(this).attr('class');
      event.preventDefault();
      $body.removeClass(function(index, className) {
      return (className.match(/\bcategory-\S+/g) || []).join(' ');
      });
      //alert(selected);
      $body.addClass('category-' + selected);

      $('#photo-wrap').masonry({
        itemSelector: '.photo-block',
        columnWidth: 580,
        gutter: 3,
        isFitWidth: true  //親要素の幅に合わせてカラム数を自動調整
      });
    });
  });

});
jQuery(function($){
  $('#photo-wrap').randomize('.photo-block').masonry({
    itemSelector: '.photo-block',
    columnWidth: 580,
    gutter: 3,
    isFitWidth: true  //親要素の幅に合わせてカラム数を自動調整
  });
});

