<?php
include_once($_SERVER['DOCUMENT_ROOT']. '/web/annex15th/inc/config.php');

$title = TITLE3.' | '.TITLE;
$description = DESCRIPTION3;
$keywords = KEYWORDS3;
define('PAGE_NAME', 'news');

function is_mobile () {
  $uamb = array(
    'DoCoMo',
    'KDDI',
    'SoftBank',
    'UP.Browser',
    'J-PHONE',
    'Vodafone'
  );
  $patternmb = '/'.implode('|', $uamb).'/i';
  return preg_match($patternmb, $_SERVER['HTTP_USER_AGENT']);
}
if (is_mobile()) {
  header("Location: /s/".DIRNAME."/".PAGE_NAME."/");
  exit;
}
?><!DOCTYPE html>
<html lang="ja">
<head>
<script type="text/javascript">
if ((navigator.userAgent.indexOf('iPhone') > 0 && navigator.userAgent.indexOf('iPad') == -1) || navigator.userAgent.indexOf('iPod') > 0 || navigator.userAgent.indexOf('Android') > 0) {
  location.href = '/s/<?=DIRNAME?>/<?=PAGE_NAME?>/';
}
</script>
<?php include_once($_SERVER['DOCUMENT_ROOT'].'/web/'.DIRNAME.'/inc/html-head.php'); ?>
</head>

<body class="news">
<?php include_once($_SERVER['DOCUMENT_ROOT'] . '/load_script.php'); ?>

<?php include_once($_SERVER['DOCUMENT_ROOT']. '/web/'.DIRNAME.'/inc/header.php'); ?>

<div class="contents clearfix">

<h2 class="ta-c"><img src="/web/<?=DIRNAME?>/images/ttl-<?=PAGE_NAME?>.png" alt="NEWS&amp;EVENT" /></h2>

<div class="line01"></div>

<div class="blog">
<div class="blog_parts_block"
  data-view-type="1"
  data-view-store-name="false"
  data-view-shop-name="true"
  data-ext-response-fields="storesjp_product_id"
  data-init-count="8"
  data-more-post="true"
  data-next-count="8"
  data-blog-type="3"
  data-category-id="<?=CATEGORY_ID1?>"
  data-category-id-or=""
  data-post-sdate="<?=POST_SDATE1?>"
  data-post-edate="<?=POST_EDATE1?>"
></div>
<!-- /blog --></div>

<!-- /contents --></div>

<?php include_once($_SERVER['DOCUMENT_ROOT']. '/web/'.DIRNAME.'/inc/footer.php'); ?>
</body>
</html>