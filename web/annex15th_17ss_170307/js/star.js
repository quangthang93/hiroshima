/**
 * staff.js
 **/

/* !stack ------------------------------------------------------------------- */
$(function() {
  staffNav();
});
$(window).load(function () {
  listAnimation();
});

/* ! Scr Animation
--------------------------------------------------- */
var listAnimation = (function(){
    var scrollCont = $('.scr'),
    delayHeight = 400;

    $('.list01').addClass('active');
    $('html,body').animate({scrollTop:0},1);
    $(window).on('load scroll resize',function(){
        scrollCont.each(function(){
            var setThis = $(this),
            elmTop = setThis.offset().top,
            elmHeight = setThis.height(),
            scrTop = $(window).scrollTop(),
            winHeight = $(window).height();
            if (scrTop > elmTop - winHeight + delayHeight && scrTop < elmTop + elmHeight){
                setThis.stop().addClass("active");
            }
        });
    });
});

/* ! staff nav slider
--------------------------------------------------- */
var staffNav = (function(){

    if($('body').hasClass('star-detail01')) {
      $('.staff-nav .slider').slick({
        dots:false,
        arrows:true,
        initialSlide: 0
      });
    }

    if($('body').hasClass('star-detail02')) {
      $('.staff-nav .slider').slick({
        dots:false,
        arrows:true,
        initialSlide: 1
      });
    }

    if($('body').hasClass('star-detail03')) {
      $('.staff-nav .slider').slick({
        dots:false,
        arrows:true,
        initialSlide: 2
      });
    }

    if($('body').hasClass('star-detail04')) {
      $('.staff-nav .slider').slick({
        dots:false,
        arrows:true,
        initialSlide: 3
      });
    }

    if($('body').hasClass('star-detail05')) {
      $('.staff-nav .slider').slick({
        dots:false,
        arrows:true,
        initialSlide: 4
      });
    }

    if($('body').hasClass('star-detail06')) {
      $('.staff-nav .slider').slick({
        dots:false,
        arrows:true,
        initialSlide: 5
      });
    }

    if($('body').hasClass('star-detail07')) {
      $('.staff-nav .slider').slick({
        dots:false,
        arrows:true,
        initialSlide: 6
      });
    }

    if($('body').hasClass('star-detail08')) {
      $('.staff-nav .slider').slick({
        dots:false,
        arrows:true,
        initialSlide: 7
      });
    }

    if($('body').hasClass('star-detail09')) {
      $('.staff-nav .slider').slick({
        dots:false,
        arrows:true,
        initialSlide: 8
      });
    }

    if($('body').hasClass('star-detail10')) {
      $('.staff-nav .slider').slick({
        dots:false,
        arrows:true,
        initialSlide: 9
      });
    }

    if($('body').hasClass('star-detail11')) {
      $('.staff-nav .slider').slick({
        dots:false,
        arrows:true,
        initialSlide: 10
      });
    }

    if($('body').hasClass('star-detail12')) {
      $('.staff-nav .slider').slick({
        dots:false,
        arrows:true,
        initialSlide: 11
      });
    }

    if($('body').hasClass('star-detail13')) {
      $('.staff-nav .slider').slick({
        dots:false,
        arrows:true,
        initialSlide: 12
      });
    }

    if($('body').hasClass('star-detail14')) {
      $('.staff-nav .slider').slick({
        dots:false,
        arrows:true,
        initialSlide: 13
      });
    }

    if($('body').hasClass('star-detail15')) {
      $('.staff-nav .slider').slick({
        dots:false,
        arrows:true,
        initialSlide: 14
      });
    }

    if($('body').hasClass('star-detail16')) {
      $('.staff-nav .slider').slick({
        dots:false,
        arrows:true,
        initialSlide: 15
      });
    }

});