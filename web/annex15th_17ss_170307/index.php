<?php
include_once 'inc/config.php';

$title = TITLE;
$description = DESCRIPTION2;
$keywords = KEYWORDS2;
define('PAGE_NAME', 'pickup');

function is_mobile () {
  $uamb = array(
    'DoCoMo',
    'KDDI',
    'SoftBank',
    'UP.Browser',
    'J-PHONE',
    'Vodafone'
  );
  $patternmb = '/'.implode('|', $uamb).'/i';
  return preg_match($patternmb, $_SERVER['HTTP_USER_AGENT']);
}
if (is_mobile()) {
  header("Location: /s/annex15th_17ss/");
  exit;
}
?><!DOCTYPE html>
<html lang="ja">
<head>
<script>
  var ua = navigator.userAgent;
  var redirectPass = '../../s/annex15th_17ss/';

  if ((ua.indexOf('iPhone') > 0 && ua.indexOf('iPad') == -1) || ua.indexOf('iPod') > 0 || (ua.indexOf('Android') > 0 && ua.indexOf('Mobile') > 0)){
      location.href = redirectPass;
  }
</script>
<!-- <script type="text/javascript">
if ((navigator.userAgent.indexOf('iPhone') > 0 && navigator.userAgent.indexOf('iPad') == -1) || navigator.userAgent.indexOf('iPod') > 0 || navigator.userAgent.indexOf('Android') > 0) {
  location.href = '/s/<?=DIRNAME?>/<?=PAGE_NAME?>/';
}
</script> -->
<?php include_once 'inc/html-head.php'; ?>
</head>

<body class="newshop pickup">
  <!-- Google Tag Manager -->
  <noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-5FDDGB" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
  <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src='//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);})(window,document,'script','dataLayer','GTM-5FDDGB');</script>
  <!-- End Google Tag Manager -->

<?php include_once 'inc/header.php'; ?>

<p class="lead">15周年のPARCO新館、<br>
この春ついにリニューアル完成！</p>


<article class="article">
  <div class="article-in">
    <header class="article-hd">
      <p class="article-num"><img src="images/pickup/txt-point1.png" alt="Point1"></p>
      <h2 class="article-title">新館B1F UNITED ARROWSメンズ３月17日リニューアル</h2>
      <p class="article-note">1/16(月)～3/16（木）改装の為一時休業<br>1/18（水）～3/12（日）新館B1F・本館6Fにて仮営業</p>
    </header>
    <div class="article-body">
      <div class="article-colblock">
        <div class="article-colblock-item">
          <p class="article-imgbox"><img src="images/pickup/img01.png" class="article-imgbox-img" alt="UNITED ARROWS B1F MEN'S FLOOR RE-OPENING 2017.3.17 FRI"></p>
        </div>
        <div class="article-colblock-item">
          <p class="article-imgbox"><img src="images/pickup/img02.png" class="article-imgbox-img" alt="UNITED ARROWS B1F MEN'S FLOORの外観"></p>
        </div>
      </div>
      <div class="open mt40">
        <div class="open-item open-item1 pl10">
          <img src="images/pickup/img03.png" alt="オープン記念限定商品 バッグ(Felisi) 72,360円、広島東洋カープ コラボバッグ 5,400円">
        </div>
        <div class="open-item open-item2 pl60">
          <p class="pt15">開店以来となる大幅なリニューアル。<br>これまでもより快適な空間でお買い物を楽しんでいただけるよう、内装を一新。<br>特徴としては木を基調とした落ち着きのある空間を商品カテゴリーごとに天井や床、壁面の素材の変化をつけて作りこむことで全体の統一感を持たせながらもシーンごとの移り変わりを感じられる店舗に仕上げています。昨年秋に新館1Fレディスフロアがリニューアル、今回のメンズでユナイテッドアローズの全面リニューアルが完成となります。</p>
        </div>
      </div>
    <!-- article-body --></div>
  </div>
</article>

<article class="article">
  <div class="article-in">
    <header class="article-hd">
      <p class="article-num"><img src="images/pickup/txt-point2.png" alt="Point2"></p>
      <h2 class="article-title">上質なライフスタイルをもつ<br>オトナ層に支持される新ブランドが初出店！</h2>
      <!-- <p class="article-note">1/16(月)～3/16（木）改装の為一時休業<br>1/18（水）～3/12（日）新館B1F・本館6Fにて仮営業</p> -->
    </header>
    <div class="article-body">
      <div class="article-colblock">
        <div class="article-colblock-item">
          <p class="article-imgbox"><img src="images/pickup/img04.png" class="article-imgbox-img" alt="プチバトー"></p>
          <p class="article-txt01">新館4F プチバトー<br>３月17日 NEW OPEN!<br>フランス発着心地のよいディス・キッズ</p>
          <p class="article-txt02">フランスの国民的ブランド「プチバトー」はやわらかな肌ざわりとシンプルながら遊び心あふれるデザインで世界中のあらゆる世代の人に親しまれています。中国地方初出店となるショップでは、ベビー、キッズはもちろん、レディース＆メンズアイテムまでフルラインを取り揃えて皆様をお迎え致します。</p>
        </div>
        <div class="article-colblock-item">
          <p class="article-imgbox"><img src="images/pickup/img05.png" class="article-imgbox-img" alt="マーコート"></p>
          <p class="article-txt01">新館3F マーコート<br>4月2日NEW OPEN!<br>オトナの女性に贈る上質ファッション</p>
          <p class="article-txt02">穏やかな空気の中で、大切な時間をゆっくり・じっくり、愉しみながら過してください。<br>ＭＡＲｃｏｕｒｔはオリジナルブランドを中心に国内外のアイテムをミックスしたセレクトショップです。</p>
        </div>
      </div>
    <!-- article-body --></div>
  </div>
</article>

<article class="article">
  <div class="article-in">
    <header class="article-hd">
      <p class="article-num"><img src="images/pickup/txt-point3.png" alt="Point3"></p>
      <h2 class="article-title">シューズのセレクトショップ・エースシューズが中四国初登場！</h2>
      <!-- <p class="article-note">1/16(月)～3/16（木）改装の為一時休業<br>1/18（水）～3/12（日）新館B1F・本館6Fにて仮営業</p> -->
    </header>
    <div class="article-body">
      <div class="article-colblock center">
        <div class="article-colblock-item">
          <p class="article-imgbox"><img src="images/pickup/img06.png" class="article-imgbox-img" alt="エースシューズ"></p>
          <p class="article-txt01">エースシューズ</p>
          <p class="article-txt02">女性目線で捉えたスニーカーのトレンドアイテムを中心に「カワイイ」を体感できる店。ショップスタッフのスニーカー女子がエースシューズのバイヤーとして商品のセレクトに携わり、現場のショップスタッフがお客様に近い距離で情報発信、商品提案をしていきます。</p>
        </div>
      </div>
    <!-- article-body --></div>
  </div>
</article>

<article class="article">
  <div class="article-in">
    <header class="article-hd">
      <p class="article-num"><img src="images/pickup/txt-point4.png" alt="Point4"></p>
      <h2 class="article-title">新しくて魅力的なショップが続々登場する<br>実験的スペースが本館１Fにオープン！</h2>
      <!-- <p class="article-note">1/16(月)～3/16（木）改装の為一時休業<br>1/18（水）～3/12（日）新館B1F・本館6Fにて仮営業</p> -->
    </header>
    <div class="article-body">
      <p class="">「新しい・面白い・限定」をテーマに旬のブランド・ショップを期間限定でご紹介していくPOPUPスペースが広島パルコ本館1Fに登場！期間限定で入れ替わる、様々なショップやブランドのアイテムをお楽しみください。</p>
      <div class="article-colblock mt35">
        <div class="article-colblock-item">
          <p class="article-imgbox"><img src="images/pickup/img07.png" class="article-imgbox-img" alt="ビジュマム"><img src="images/pickup/img09.png" class="article-imgbox-logo" alt="ビジュマムのロゴ"></p>
          <p class="article-txt01 type02 lh13">ビジュマム<br><span class="fz14">（３月7日～3月15日）</span></p>
          <p class="article-txt02">オトナが使えるデコラティブ!!をコンセプトに掲げるBiju mam。世界中のデザインをセレクト&現地職人によるオリジナル製作しております。太陽の国ブラジルで熟練職人によるハンドメイドのBiju mamサンダルやイスタンブールのカラフルな天然石を使用した1点ものアクセサリーなど。デザインを見るたびにワクワクする、ずっとお気に入りで側に置いておきたくなるアイテムをご紹介！</p>
        </div>
        <div class="article-colblock-item">
          <p class="article-imgbox"><img src="images/pickup/img08.png" class="article-imgbox-img" alt="ラヴィドリュクス"><img src="images/pickup/img10.png" class="article-imgbox-logo" alt="ラヴィドリュクスのロゴ"></p>
          <p class="article-txt01 type02 lh13">ラヴィドリュクス<br><span class="fz14">（３月17日～3月29日）</span></p>
          <p class="article-txt02">大人気ハンドメイド作家80ブランド強が一堂に会するラヴィドリュクス期間限定ショップVol.13が広島に初登場！お洋服からアクセサリーまで燦然と展開されます！</p>
        </div>
      </div>
    <!-- article-body --></div>
  </div>
</article>


<?php include_once'inc/footer.php'; ?>
</body>
</html>
