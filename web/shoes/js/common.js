/*================================================================

共通スクリプト

================================================================*/
$(function(){
	// フェード処理 （img要素に.fadeを追加して使用）
	$("img.fade").fadeTo(0,1.0);
	$("img.fade").hover(function(){
			$(this).fadeTo(200,0);
		},
		function(){
			$(this).fadeTo(300,1.0);
		}
	);

	// クラスの追加
	$('body :first-child').addClass('firstChild'); //最後の要素
	$('body :last-child').addClass('lastChild'); //最初の要素

	// ロールオーバー （img要素に.overを追加して使用）


	$('img.over').mouseover(function(){
		$(this).attr('src',$(this).attr('src').replace(/^(.+)(\.[a-z]+)$/, '$1_on$2'));
	}).mouseout(function(){
		$(this).attr('src',$(this).attr('src').replace(/^(.+)_on(\.[a-z]+)$/, '$1$2'));
	}).each(function(){
		$('<img>').attr('src',$(this).attr('src').replace(/^(.+)(\.[a-z]+)$/, '$1_on$2'));
	});

	// smooth scroll
   $('a[href^=#]').click(function() {
      // スクロールの速度
      var speed = 400; // ミリ秒
      // アンカーの値取得
      var href= $(this).attr("href");
      // 移動先を取得
      var target = $(href == "#" || href == "" ? 'html' : href);
      // 移動先を数値で取得
      var position = target.offset().top;
      // スムーススクロール
      $('body,html').animate({scrollTop:position}, speed, 'swing');
      return false;
   });

	// クリックエリア拡大
	$(".blockHover").click(function(){
			var target = $(this).find("a").attr("target");
			var href = $(this).find("a").attr("href");
			if(target){
					window.open(href);
			}else{
					window.location=href;
			}
			return false;
	});
	$(".blockHover").hover(function() {
			$(this).css("cursor","pointer").addClass("hover");
	},function() {
			$(this).css("cursor","auto").removeClass("hover");
	});

});


