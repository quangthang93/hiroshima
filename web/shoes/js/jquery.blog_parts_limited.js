;(function($) {

	$.fn.blog_parts = function(options) {

		var base = $(this);
		var init_count = options.init_count;
		var next_count = options.next_count;
		var more_post = options.more_post;

		var view_type = options.view_type;
		var view_data = {
				'view_store_name' : false,
				'view_shop_name' : true,
				'view_social' : true,
				'view_link_target' : '',
				'view_event_date' : false,
				'view_floor_name' : false
		};
		if(options.view_store_name === true || options.view_store_name === 1){
			view_data.view_store_name = true;
		}
		if(options.view_shop_name === false || options.view_shop_name === 0){
			view_data.view_shop_name = false;
		}
		if(options.view_social === false || options.view_social === 0){
			view_data.view_social = false;
		}
		if(options.view_link_target !== undefined && options.view_link_target.replace(/\s+$/, "") != ""){
			view_data.view_link_target = options.view_link_target;
		}
		if(options.view_event_date === true || options.view_event_date === 1){
			view_data.view_event_date = true;
		}
		if(options.view_floor_name === true || options.view_floor_name === 1){
			view_data.view_floor_name = true;
		}

		var wrapper = ''
		var template_name = '';
		switch(view_type){
			case 1:
				template_name = 'blog_parts.tpl';
				wrapper = 'column5_blog_list_block_01';
				break;
			case 2:
				if(view_data.view_social){
					template_name = 'sp_blog_parts_list.tpl';
					wrapper = 'list_block_wrap';
				}else{
					template_name = 'sp_blog_parts_list_nosocial.tpl';
					wrapper = 'list_block_wrap_ns';
				}
				break;
			case 3:
				template_name = 'sp_blog_parts.tpl';
				wrapper = 'thumbnail_block_wrap';
				break;
			default:
				return false;
		}

		var param = {};
		param['t'] = 1;
		var item = [
				 'blog_type'
				,'scode'
				,'sid'
				,'sid_or'
				,'cid'
				,'cid_or'
				,'post_sdate'
				,'post_edate'
				,'ext_response_fields'
			];
		for (var i=0; i < item.length; i++){
			if(options[item[i]]){
				param[item[i]] = options[item[i]];
			}
		}
		// クリップ数パラメタは強制セット
		if('ext_response_fields' in param) {
			var tmp = param['ext_response_fields'].split(",");
			if($.inArray("clipnum", tmp) == -1) {
				tmp.push("clipnum");
				param['ext_response_fields'] = tmp.join(",");
			}
		} else {
			param['ext_response_fields'] = 'clipnum';
		}
		// フロア名表示時はAPIにフロア情報取得パラメタは強制セット
		if(view_data.view_floor_name) {
			if('ext_response_fields' in param) {
				var tmp = param['ext_response_fields'].split(",");
				if($.inArray("floor", tmp) == -1) {
					tmp.push("floor");
					param['ext_response_fields'] = tmp.join(",");
				}
			}
		}

		base.addClass("blog_parts_view_type_" + view_type);
		base.html("");
		base.append($("<div/>").addClass(wrapper).addClass("clearfix"));
		var container = $("." + wrapper, base);

		$.get('/web/blog_parts/template/' + template_name + '?timestamp=' + new Date().getTime(), function(template) {
			var tmpl = Hogan.compile(template);
			writeContents(param, tmpl, base, container, more_post, 1, init_count, next_count, view_type, view_data);
		});

	};
	
	function writeContents(param, tmpl, base, container, more_post, first, last, next_count, view_type, view_data) {
		
		param['first'] = first;
		param['last'] = last;
		
		var w = ["日","月","火","水","木","金","土"];

		$.ajax({
			url: '/api/search_post/',
			type: 'post',
			cache: false,
			data: param,
			dataType: 'json',
			success: function(data){

				if(data.header.code != '0'){
					return;
				}
				
				//socialボタン動作用
				social.isLogin = data.result.user.is_login;
				
				if(!data.header.count){
					$("<p/>").addClass("no_result").text("該当する記事はありません").appendTo(base);
				}else{
				
					var tmpl_data = {};
					tmpl_data.data = data;
					tmpl_data.view = view_data;
					
					$.each(tmpl_data.data.result.items, function () {
						if(this.store_code == 'WWW'){
							this.store_name = '';
						}
						if(param['blog_type'] == 3 && view_data.view_event_date && (this.event_sdate != null || this.event_edate != null)) {
							var event_sdate;
							var event_edate;
							if(this.event_sdate != null) {
								event_sdate = new Date(this.event_sdate.replace(/\-/g, '/'));
								this.event_date_string = ('0' + (event_sdate.getMonth()+1)).slice(-2) + '/' + ('0' + event_sdate.getDate()).slice(-2) + '(' + w[event_sdate.getDay()] + ')-';
							} else {
								event_sdate = new Date('2000/01/01 00:00:00');
								this.event_date_string = '-';
							}
							if(this.event_edate != null) {
								event_edate = new Date(this.event_edate.replace(/\-/g, '/') + " 23:59:59");
								// 開始日、終了日が同じだった場合
								if(this.event_sdate != null && this.event_sdate == this.event_edate) {
									this.event_date_string = this.event_date_string.slice(0, -1);
								} else {
									this.event_date_string = this.event_date_string + ('0' + (event_edate.getMonth()+1)).slice(-2) + '/' + ('0' + event_edate.getDate()).slice(-2) + '(' + w[event_edate.getDay()] + ')';
								}
							} else {
								event_edate = new Date('2037/12/31 23:59:59');
							}
							//
							var now = new Date();
							if(now.getTime() >= event_sdate.getTime() && now.getTime() <= event_edate.getTime()) {
								this.event_date_isopen = true;
							}
						}
//							this.event_edate2 = w[(new Date(this.event_edate)).getDay()];
					});
					
					container.append(tmpl.render(tmpl_data));
					$('.imgLiquidBox', container).imgLiquid();
					switch(view_type){
						case 1:
							$(".post_shop", container).tile(3);
							$(".post_title", container).tile(3);
							$(".post_icon_block", container).css( 'position', 'relative'); //IE8以前用に再指定（上詰めになるのを防ぐ）
							break;
						case 3:
							$(".shop_name", container).tile(2);
							$(".text_link", container).tile(2);
							break;
					}
				}

				if(more_post && next_count) {
					if(data.header.last < data.header.count){
						//初回のみ「もっと見る」作成
						if(!$(".more_post", base).size()) {
							switch(view_type){
								case 1:
									$("<div/>").addClass("more_post").append(
										$("<a/>").attr("href", "javascript:void(0);").append(
											$("<img/>").attr({
												"src"   : "/web/blog_parts/images/common_moreposts_btn.png",
												"alt"   : "もっと見る"
											})
										)
									).appendTo(base);
									break;
								case 2:
								case 3:
									$("<div/>").addClass("btn_block_07").addClass("more_post").append(
										$("<a/>").attr("href", "javascript:void(0);").text("もっと見る")
									).appendTo(base);
									break;
							}
						}
						
						first = last + 1;
						last = last + next_count;
						$(".more_post a", base).unbind("click");
						$(".more_post a", base).click(function() {
							writeContents(param, tmpl, base, container, more_post, first, last, next_count, view_type, view_data);
						});
					}else{
						$(".more_post", base).remove();
					}
				}
			}
		});
	}

}) (jQuery);


$(function(){
	$(".blog_parts_block").each(function(){
		$(this).blog_parts({
			view_type: $(this).data("viewType"),
			view_store_name: $(this).data("viewStoreName"),
			view_shop_name: $(this).data("viewShopName"),
			view_social: $(this).data("viewSocial"),
			view_link_target: $(this).data("viewLinkTarget"),
			view_event_date: $(this).data("viewEventDate"),
			view_floor_name: $(this).data("viewFloorName"),
			init_count: $(this).data("initCount"),
			more_post: $(this).data("morePost"),
			next_count: $(this).data("nextCount"),
			blog_type: $(this).data("blogType"),
			scode: $(this).data("storeCode"),
			sid: $(this).data("shopId"),
			sid_or: $(this).data("shopIdOr"),
			cid: $(this).data("categoryId"),
			cid_or: $(this).data("categoryIdOr"),
			post_sdate: $(this).data("postSdate"),
			post_edate: $(this).data("postEdate"),
			ext_response_fields: $(this).data("extResponseFields")
		});
	});
});
