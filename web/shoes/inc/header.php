<a name="top" id="top"></a>

<div class="parco-header">
<div class="parco-header__in">
<p class="parco-header__logo"><a href="/"><img src="/images/check/header-logo-parco.png" alt="PARCO"></a></p>
<ul class="social parco-header__social clearfix">
<li class="social__item">
<?php
  $urlencode = urlencode($shareurl);
  $titleencode = urlencode($title);
  $urlhost =str_replace('http%3A%2F%2F','',$urlencode);
?>
<a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo $urlencode; ?>" target="_blank"><img src="/images/check/snsbtn-facebook.png" alt="facebookでシェアする"></a>
</li>
<li class="social__item">
<a href="https://twitter.com/share?url=<?php echo $urlencode; ?>&text=<?php echo $titleencode; ?>" target="_blank"><img src="/images/check/snsbtn-twitter.png" alt="ツイートする"></a>
</li>
<li class="social__item">
<a href="https://plus.google.com/share?url=<?php echo $urlhost; ?>" target="_blank"><img src="/images/check/snsbtn-googleplus.png" alt="ツイートする"></a>
</li>
</ul>
</div>
<!-- /parco-header --></div>

<div class="header">
<div class="header__in">
<h1 class="main"><img src="/static/<?=DIRNAME?>/images/main.png" alt="<?php echo $title; ?>" /></h1>
<div class="btn01"><a href="#shops"><img src="/static/<?=DIRNAME?>/images/btn_shops.png" alt="SHOPS"></a></div>
<div class="btn02"><a href="#blog"><img src="/static/<?=DIRNAME?>/images/btn_blog.png" alt="BLOG"></a></div>
<?php /*<p class="bnr-area"><a href="#" target="_blank"><img src="/static/<?=DIRNAME?>/images/bnr01.jpg" alt="福袋大市2017"></a></p>*/ ?>

<!-- /header__in --></div>
<!-- /header --></div>
