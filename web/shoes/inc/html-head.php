<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=1010">
<title><?php echo $title; ?></title>
<meta name="description" content="<?php echo $description; ?>" />
<meta name="keywords" content="<?php echo $keywords; ?>"/>

<link rel="alternate" media="only screen and (max-width: 640px)" href="<?php echo $otherdevice; ?>" />
<link rel="stylesheet" href="/static/<?=DIRNAME?>/css/default.css" media="screen,print" />
<link rel="stylesheet" href="/web/blog_parts/css/blog_parts.css" media="all"/>
<link rel="stylesheet" href="/static/<?=DIRNAME?>/css/style.css" media="screen,print" />

<script src="/static/<?=DIRNAME?>/js/modernizr.js"></script>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<!-- <script>window.jQuery || document.write('<script src="/static/<?=DIRNAME?>/js/jquery.js"><\/script>')</script> -->
<!-- <script src="/common/js/jquery.js"></script> -->
<script src="/static/<?=DIRNAME?>/js/common.js"></script>
<script src="/common/js/social.js"></script>
<script src="/common/js/imgLiquid.js"></script>
<script src="/web/blog_parts/js/jquery.tile.js"></script>
<script src="/web/blog_parts/js/hogan-2.0.0.js"></script>
<script src="/web/blog_parts/js/jquery.blog_parts.js"></script>

<!-- OGP -->
<meta property="fb:admins" content="100005423256030">
<meta property="og:locale" content="ja_JP" />
<meta property="og:type" content="website" />
<meta property="og:title" content="<?php echo $title; ?>" />
<meta property="og:url" content="<?php echo $shareurl; ?>" />
<meta property="og:site_name" content="<?=STORE_NAME_JA?>PARCO" />
<meta property="og:description" content="<?php echo $description; ?>" />
<meta property="og:image" content="http://<?=STORE_NAME?>.parco.jp/static/<?=DIRNAME?>/images/ogp.jpg" />
<!-- /OGP -->
