<?php
include_once($_SERVER['DOCUMENT_ROOT']. '/static/shoes/inc/config.php');

$title = 'Shoes Shops｜'. STORE_NAME_JA .'PARCO';
$description = STORE_NAME_JA .'パルコのShoes Shops。'. STORE_NAME_JA .'パルコおすすめシューズが勢ぞろい！注目のヒールやスニーカーをご紹介いたします。';
$keywords = 'シューズ,スニーカー,ヒール';
$shareurl = 'http://'. STORE_NAME.'.parco.jp/web/'.DIRNAME.'/';
$otherdevice = '/s/'.DIRNAME.'/';

function is_mobile () {
  $uamb = array(
    'DoCoMo',
    'KDDI',
    'SoftBank',
    'UP.Browser',
    'J-PHONE',
    'Vodafone'
  );
  $patternmb = '/'.implode('|', $uamb).'/i';
  return preg_match($patternmb, $_SERVER['HTTP_USER_AGENT']);
}
if (is_mobile()) {
  header("Location: /mpage/check/floor/?view=page".CHECK_NUM);
  exit;
}
?><!DOCTYPE html>
<html lang="ja">
<head>
<script type="text/javascript">
if ((navigator.userAgent.indexOf('iPhone') > 0 && navigator.userAgent.indexOf('iPad') == -1) || navigator.userAgent.indexOf('iPod') > 0 || navigator.userAgent.indexOf('Android') > 0) {
  location.href = '<?php echo $otherdevice; ?>';
}
</script>
<?php include_once($_SERVER['DOCUMENT_ROOT'].'/static/'.DIRNAME.'/inc/html-head.php'); ?>
</head>

<body class="blog">
<?php include_once($_SERVER['DOCUMENT_ROOT'] . '/load_script.php'); ?>

<?php include_once($_SERVER['DOCUMENT_ROOT']. '/static/'.DIRNAME.'/inc/header.php'); ?>

<div class="contents-wrap b_radius clearfix">
<div class="contents-wrap__in clearfix">

<?php /*<?php include_once($_SERVER['DOCUMENT_ROOT']. '/static/'.DIRNAME.'/inc/localnav.php'); ?>*/ ?>


<div class="contents clearfix">

<h2 class="contents-title02" id="shops"><img src="/static/<?=DIRNAME?>/images/txt_shops.png" alt="SHOPS"></h2>

<ul class="list-shops">
  <li class="item-list-shops">
    <a href="http://hiroshima.parco.jp/page2/968/">
      <div class="floor">本館<br>1F</div>
      <div class="image"><img src="/static/<?=DIRNAME?>/images/img_shops01.jpg" alt=""></div>
      <div class="brand">
        <div class="logo"><img src="/static/<?=DIRNAME?>/images/logo_shops01.png" alt=""></div>
        <p class="name">ダイアナ</p>
      </div>
    </a>
  </li>
  <li class="item-list-shops">
    <a href="http://hiroshima.parco.jp/page2/976/">
      <div class="floor">本館<br>1F</div>
      <div class="image"><img src="/static/<?=DIRNAME?>/images/img_shops02.jpg" alt=""></div>
      <div class="brand">
        <div class="logo"><img src="/static/<?=DIRNAME?>/images/logo_shops02.png" alt=""></div>
        <p class="name">オゥバニスター</p>
      </div>
    </a>
  </li>
  <li class="item-list-shops">
    <a href="http://hiroshima.parco.jp/page2/1100/">
      <div class="floor">本館<br>4F</div>
      <div class="image"><img src="/static/<?=DIRNAME?>/images/img_shops03.jpg" alt=""></div>
      <div class="brand">
        <div class="logo"><img src="/static/<?=DIRNAME?>/images/logo_shops03.png" alt=""></div>
        <p class="name">エスペランサ</p>
      </div>
    </a>
  </li>
  <li class="item-list-shops">
    <a href="http://hiroshima.parco.jp/page2/10823/">
      <div class="floor">本館<br>6F</div>
      <div class="image"><img src="/static/<?=DIRNAME?>/images/img_shops04.jpg" alt=""></div>
      <div class="brand">
        <div class="logo"><img src="/static/<?=DIRNAME?>/images/logo_shops04.png" alt=""></div>
        <p class="name">エースシューズ</p>
      </div>
    </a>
  </li>
  <li class="item-list-shops">
    <a href="http://hiroshima.parco.jp/page2/1242/">
      <div class="floor">本館<br>7F</div>
      <div class="image"><img src="/static/<?=DIRNAME?>/images/img_shops05.jpg" alt=""></div>
      <div class="brand">
        <div class="logo"><img src="/static/<?=DIRNAME?>/images/logo_shops05.png" alt=""></div>
        <p class="name">オリエンタル<br>トラフィック</p>
      </div>
    </a>
  </li>
  <li class="item-list-shops">
    <a href="http://hiroshima.parco.jp/page2/10854/">
      <div class="floor">新館<br>3F</div>
      <div class="image"><img src="/static/<?=DIRNAME?>/images/img_shops06.jpg" alt=""></div>
      <div class="brand">
        <div class="logo"><img src="/static/<?=DIRNAME?>/images/logo_shops06.png" alt=""></div>
        <p class="name">シューズラウンジ</p>
      </div>
    </a>
  </li>
  <li class="item-list-shops">
    <a href="http://hiroshima.parco.jp/page2/1073/">
      <div class="floor">新館<br>6F</div>
      <div class="image"><img src="/static/<?=DIRNAME?>/images/img_shops07.jpg" alt=""></div>
      <div class="brand">
        <div class="logo"><img src="/static/<?=DIRNAME?>/images/logo_shops07.png" alt=""></div>
        <p class="name">ABCマート</p>
      </div>
    </a>
  </li>
</ul>

<?php /*<h2 class="contents-title02"><img src="/images/check/txt_news_event.png" alt="NEWS&amp;EVENT"></h2>*/ ?>

<?php /* カスタム用 ?>
<h2 class="contents-title03"><img src="images/shop_event.png" alt="NEWS&amp;EVENT" /></h2>
<?php */ ?>
<?php /*<div class="blog">
<div class="blog_parts_block"
  data-view-type="1"
  data-view-store-name="false"
  data-view-shop-name="true"
  data-ext-response-fields="kaeru2_product_id"
  data-init-count="5"
  data-more-post="true"
  data-next-count="10"
  data-blog-type="3"
  data-category-id="170"
  data-category-id-or=""
  data-post-sdate="20161212000000"
  data-post-edate="20170118000000"
></div>
<!-- /blog --></div>*/ ?>


<h2 class="contents-title02" id="blog"><img src="/static/<?=DIRNAME?>/images/txt_shop_blog.png" alt="BLOG"></h2>

<?php /* カスタム用 ?>
<h2 class="contents-title03"><img src="images/shop_blog.png" alt="SHOP BLOG" /></h2>
<?php */ ?>
<div class="blog">
<div class="blog_parts_block"
  data-view-type="1"
  data-view-store-name="false"
  data-view-shop-name="true"
  data-ext-response-fields="kaeru2_product_id"
  data-init-count="10"
  data-more-post="true"
  data-next-count="10"
  data-blog-type="1"
  data-category-id="20"
  data-category-id-or=""
  data-post-sdate="20170508000000"
  data-post-edate=""
></div>
<!-- /blog --></div>

<!-- /contents --></div>
<!-- /contents-wrap__in --></div>


<?php include_once($_SERVER['DOCUMENT_ROOT']. '/static/'.DIRNAME.'/inc/footer.php'); ?>
<!-- /contents-wrap --></div>
</body>
</html>