<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=1060">
<title><?php echo $title ?></title>
<meta name="description" content="<?php echo $description ?>" />
<meta name="keywords" content="<?php echo $keywords ?>"/>
<?php include_once($_SERVER['DOCUMENT_ROOT'].'/web/'.DIRNAME.'/inc/ogp.php'); ?>
<link rel="stylesheet" href="/web/<?=DIRNAME?>/css/default.css" media="screen,print" />
<link rel="stylesheet" href="/web/<?=DIRNAME?>/css/style.css" media="screen,print" />
<link rel="stylesheet" href="/web/<?=DIRNAME?>/css/<?=PAGE_NAME?>.css" />
<link rel="alternate" media="only screen and (max-width: 640px)" href="http://hiroshima.parco.jp/s/autumun_newshop/" />

<script src="/web/<?=DIRNAME?>/js/modernizr.js"></script>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script src="/web/<?=DIRNAME?>/js/masonary.min.js"></script>
<script src="/web/<?=DIRNAME?>/js/common.js"></script>
<script src="/common/js/social.js"></script>
<script src="/common/js/imgLiquid.js"></script>

