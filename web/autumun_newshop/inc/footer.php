<p class="btn-pagetop"><a href="#" class="page-scroll"><img src="/web/<?=DIRNAME?>/images/btn_pagetop.png" alt="ページトップ"></a></p>
<div class="footer">
<div class="footer__in">
<div class="parco"><a href="/"><img src="/web/<?=DIRNAME?>/images/btn_sitetop.png" alt="SITE TOP" /></a></div>
<div class="copyright"><img src="/web/<?=DIRNAME?>/images/txt_copyright.png" alt="COPYRIGHT (C) PARCO CO., LTD. ALL RIGHTS RESERVED." /></div>

<ul class="snsnlist">
  <li><a href="https://www.facebook.com/sharer/sharer.php?u=http%3A%2F%2F<?=STORE_NAME?>.parco.jp%2Fweb%2F<?=DIRNAME?>%2F" onclick="window.open(this.href, 'window', 'width=650, height=450, menubar=no, toolbar=no, scrollbars=yes'); return false;"
><img src="/web/<?=DIRNAME?>/images/ico-facebook.png" alt="Facebook"></a></li>
  <li><a href="https://twitter.com/share?url=http%3A%2F%2F<?=STORE_NAME?>.parco.jp%2Fweb%2F<?=DIRNAME?>%2F&amp;text=<?php echo $sns_title_encode; ?>" onclick="window.open(this.href, 'window', 'width=650, height=450, menubar=no, toolbar=no, scrollbars=yes'); return false;"
><img src="/web/<?=DIRNAME?>/images/ico-twitter.png" alt="Twitter"></a></li>
  <li><a href="https://plus.google.com/share?url=<?=STORE_NAME?>.parco.jp/web/<?=DIRNAME?>/" onclick="window.open(this.href, 'window', 'width=400, height=630, menubar=no, toolbar=no, scrollbars=yes'); return false;"
><img src="/web/<?=DIRNAME?>/images/ico-google.png" alt="Google +"></a></li>

</ul>

</div>
</div>

<?php // include_once($_SERVER['DOCUMENT_ROOT']. '/web/cardoff/inc/modal.php'); ?>
