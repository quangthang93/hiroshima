<!-- OGP -->
<meta property="fb:admins" content="100005423256030">
<meta property="og:locale" content="ja_JP" />
<meta property="og:type" content="website" />
<meta property="og:title" content="<?=TITLE?>" />
<meta property="og:url" content="http://<?=STORE_NAME?>.parco.jp/web/<?=DIRNAME?>/" />
<meta property="og:site_name" content="<?=TITLE?>" />
<meta property="og:description" content="<?=DESCRIPTION1?>" />
<meta property="og:image" content="http://<?=STORE_NAME?>.parco.jp/web/<?=DIRNAME?>/images/ogp.png" />
<!-- /OGP -->
