<?php
define('STORE_NAME', 'hiroshima');
define('STORE_NAME_JA', '広島PARCO');
define('DIRNAME', 'autumun_newshop');
define('TITLE', 'AUTUMN NEW SHOP｜広島PARCO');
define('DESCRIPTION1', '広島PARCOのAUTUMN NEW SHOPのブランドコンセプトや取扱いアイテムなどをご紹介！2017年秋、広島PARCOに注目のショップが続々オープン！');
define('KEYWORDS1', 'NEW,RENEWAL,リニューアル,秋改装,広島パルコ,PARCO');
$sns_url = 'http://hiroshima.parco.jp/web/autumun_newshop/';
$sns_url_sp = 'http://hiroshima.parco.jp/s/autumun_newshop/';
$sns_title_encode = urlencode(TITLE);
?>