<?php
include_once( $_SERVER[ 'DOCUMENT_ROOT' ] . '/web/autumun_newshop/inc/config.php' );

$title = TITLE;
$description = DESCRIPTION1;
$keywords = KEYWORDS1;
define( 'PAGE_NAME', 'newshop' );

function is_mobile() {
	$uamb = array(
		'DoCoMo',
		'KDDI',
		'SoftBank',
		'UP.Browser',
		'J-PHONE',
		'Vodafone'
	);
	$patternmb = '/' . implode( '|', $uamb ) . '/i';
	return preg_match( $patternmb, $_SERVER[ 'HTTP_USER_AGENT' ] );
}
if ( is_mobile() ) {
	header( "Location: /s/" . DIRNAME . "/" . PAGE_NAME . "/" );
	exit;
}
?>
<!DOCTYPE html>
<html lang="ja">
<head>
<script>
var ua = navigator.userAgent;
var redirectPass = '../../s/autumun_newshop/';

if ((ua.indexOf('iPhone') > 0 && ua.indexOf('iPad') == -1) || ua.indexOf('iPod') > 0 || (ua.indexOf('Android') > 0 && ua.indexOf('Mobile') > 0)){
    location.href = redirectPass;
}
</script>
	<?php include_once($_SERVER['DOCUMENT_ROOT'].'/web/'.DIRNAME.'/inc/html-head.php'); ?>
</head>

<body>
	<?php include_once($_SERVER['DOCUMENT_ROOT'] . '/load_script.php'); ?>
	<h2 class="ttl"><img src="/web/<?=DIRNAME?>/images/ttl-<?=PAGE_NAME?>.png" alt="AUTUMN NEW SHOP" /></h2>
	<div class="contents">
		<div class="photo-wrap">
			<?php
			/**
			 * PHOTO-START
			 */
			?>


			<div class="photo-block item-all">
				<div class="photo-block-in">
					<div class="top-visual"><img class="visual" src="images/newshop/shop/01.jpg" width="490" height="490" alt="キャスキッドソン">
						<div class="visual-bottom">
							<p class="top-floor">新館3F</p>
							<p class="top-shop">キャスキッドソン&nbsp;&nbsp;<span class="top-category">レディス・雑貨</span>
							</p>
							<p class="top-date">9/22(金)&nbsp;NEW OPEN!</p>
						</div>
					</div>
					<div class="photo-hover"><span class="icon new"></span><span class="badge"></span>
						<div class="fadeInLeft animated">
							<h1 class="hover-ttl">新館3F<br>9/22(金)NEW OPEN!<br><span>レディス・雑貨</span></h1>
							<p class="hover-img"><img src="images/newshop/logo/lo-01.png" alt="キャスキッドソン">
							</p>
							<p class="hover-text">「モダンヴィンテージ」をテーマに、多彩なプリントデザインで世界中の女性に愛される英国発のライフスタイルブランド。</p>
							<div class="hover-box clearfix">
								<!-- hover-box -->
							</div>
							<!-- fadeInLeft animated -->
						</div>
						<!-- photo-hover -->
					</div>
				</div>
			</div>
			<div class="photo-block item-all">
				<div class="photo-block-in">
					<div class="top-visual"><img class="visual" src="images/newshop/shop/02.jpg" width="490" height="490" alt="ジェイダ">
						<div class="visual-bottom">
							<p class="top-floor">本館4F</p>
							<p class="top-shop">ジェイダ&nbsp;&nbsp;<span class="top-category">レディス</span>
							</p>
							<p class="top-date">9/2(土)&nbsp;NEW OPEN!</p>
						</div>
					</div>
					<div class="photo-hover"><span class="icon new"></span><span class="badge-ch"></span>
						<div class="fadeInLeft animated">
							<h1 class="hover-ttl">本館4F<br>9/2(土)NEW OPEN!<br><span>レディス</span></h1>
							<p class="hover-img"><img src="images/newshop/logo/lo-02.png" alt="ジェイダ">
							</p>
							<p class="hover-text">西海岸LAの空気感をイメージした、カジュアルで色気のあるオンナの子のスタイルを提案。</p>
							<div class="hover-box clearfix">
								<p class="hover-plan">〈PARCOカード〉新規入会またはご利用で10％OFF
									<br>★プレミアムデイ＜PARCOカード＞新規入会またはご利用で15％OFF</p>
								<!-- hover-box -->
							</div>
							<!-- fadeInLeft animated -->
						</div>
						<!-- photo-hover -->
					</div>
				</div>
			</div>
			<div class="photo-block item-all">
				<div class="photo-block-in">
					<div class="top-visual"><img class="visual" src="images/newshop/shop/03.jpg" width="490" height="490" alt="アンビー＆エクラン">
						<div class="visual-bottom">
							<p class="top-floor">本館3F</p>
							<p class="top-shop">アンビー＆エクラン&nbsp;&nbsp;<span class="top-category">レディス</span>
							</p>
							<p class="top-date">8/26(土)&nbsp;NEW OPEN!</p>
						</div>
					</div>
					<div class="photo-hover"><span class="icon new"></span><span class="badge-ch"></span>
						<div class="fadeInLeft animated">
							<h1 class="hover-ttl">本館3F<br>8/26(土)NEW OPEN!<br><span>レディス</span></h1>
							<p class="hover-img"><img src="images/newshop/logo/lo-03.png" alt="アンビー＆エクラン">
							</p>
							<p class="hover-text">（アンビー）シンプルなCOORDINATEの中に、エッジのあるITEMを取り入れた現代的な女性のSTYLE。<br> （エクラン）フェミニティとクリアなセンシュアルモードで女性の心をくすぐるリアルクローズを提案。
							</p>
							<div class="hover-box clearfix">
								<p class="hover-plan">〈PARCOカード〉新規入会またはご利用で10％OFF</p>
								<!-- hover-box -->
							</div>
							<!-- fadeInLeft animated -->
						</div>
						<!-- photo-hover -->
					</div>
				</div>
			</div>
			<div class="photo-block item-all">
				<div class="photo-block-in">
					<div class="top-visual"><img class="visual" src="images/newshop/shop/04.jpg" width="490" height="490" alt="オズモーシス">
						<div class="visual-bottom">
							<p class="top-floor">本館3F</p>
							<p class="top-shop">オズモーシス&nbsp;&nbsp;<span class="top-category">レディス</span>
							</p>
							<p class="top-date">8/5(土)&nbsp;NEW OPEN!</p>
						</div>
					</div>
					<div class="photo-hover"><span class="icon new"></span>
						<div class="fadeInLeft animated">
							<h1 class="hover-ttl">本館3F<br>8/5(土)NEW OPEN!<br><span>レディス</span></h1>
							<p class="hover-img"><img src="images/newshop/logo/lo-04.png" alt="オズモーシス">
							</p>
							<p class="hover-text">時代に左右されないタイムレスなデザインベースにトレンド性を盛り込み、常に新鮮なスタイルを提案</p>
							<div class="hover-box clearfix">
								<p class="hover-plan">〈PARCOカード〉新規入会またはご利用で10％OFF【9/15～18】</p>
								<!-- hover-box -->
							</div>
							<!-- fadeInLeft animated -->
						</div>
						<!-- photo-hover -->
					</div>
				</div>
			</div>
			<div class="photo-block item-all">
				<div class="photo-block-in">
					<div class="top-visual"><img class="visual" src="images/newshop/shop/05.jpg" width="490" height="490" alt="トランジション">
						<div class="visual-bottom">
							<p class="top-floor">本館5F</p>
							<p class="top-shop">トランジション&nbsp;&nbsp;<span class="top-category">メンズ</span>
							</p>
							<p class="top-date">9/1(金)&nbsp;RENEWAL OPEN!</p>
						</div>
					</div>
					<div class="photo-hover"><span class="icon renew"></span><span class="badge-ch"></span>
						<div class="fadeInLeft animated">
							<h1 class="hover-ttl">本館5F<br>9/1(金)RENEWAL OPEN!<br><span>メンズ</span></h1>
							<p class="hover-img"><img src="images/newshop/logo/lo-05.png" alt="トランジション">
							</p>
							<p class="hover-text">「本物・リアルクローズ」。最旬のドメスティックブランド、IMPORTラインや雑貨を取り入れ、洗練されたコーディネートを提案。</p>
							<div class="hover-box clearfix">
								<p class="hover-plan">〈PARCOカード〉新規入会またはご利用で10％OFF</p>
								<!-- hover-box -->
							</div>
							<!-- fadeInLeft animated -->
						</div>
						<!-- photo-hover -->
					</div>
				</div>
			</div>
			<div class="photo-block item-all">
				<div class="photo-block-in">
					<div class="top-visual"><img class="visual" src="images/newshop/shop/06.jpg" width="490" height="490" alt="ポルコロッソ">
						<div class="visual-bottom">
							<p class="top-floor">新館3F</p>
							<p class="top-shop">ポルコロッソ&nbsp;&nbsp;<span class="top-category">バッグ・雑貨</span>
							</p>
							<p class="top-date">9/7(木)&nbsp;NEW OPEN!</p>
						</div>
					</div>
					<div class="photo-hover"><span class="icon new"></span>
						<div class="fadeInLeft animated">
							<h1 class="hover-ttl">新館3F<br>9/7(木)NEW OPEN!<br><span>バッグ・雑貨</span></h1>
							<p class="hover-img"><img src="images/newshop/logo/lo-06.png" alt="ポルコロッソ">
							</p>
							<p class="hover-text">愛着を持って長く使えるモノをコンセプトに、手のぬくもりが感じられる革のカバンやハンドメイド小物が多数。</p>
							<div class="hover-box clearfix">
								<p class="hover-plan">〈PARCOカード〉新規入会またはご利用でバッグ15％ＯＦＦ</p>
								<!-- hover-box -->
							</div>
							<!-- fadeInLeft animated -->
						</div>
						<!-- photo-hover -->
					</div>
				</div>
			</div>
			<div class="photo-block item-all">
				<div class="photo-block-in">
					<div class="top-visual"><img class="visual" src="images/newshop/shop/07.jpg" width="490" height="490" alt="グランビュフル">
						<div class="visual-bottom">
							<p class="top-floor">本館2F</p>
							<p class="top-shop">グランビュフル&nbsp;&nbsp;<span class="top-category">レディス</span>
							</p>
							<p class="top-date">8/25(金)&nbsp;NEW OPEN!</p>
						</div>
					</div>
					<div class="photo-hover"><span class="icon new"></span>
						<div class="fadeInLeft animated">
							<h1 class="hover-ttl">本館2F<br>8/25(金)NEW OPEN!<br><span>レディス</span></h1>
							<p class="hover-img"><img src="images/newshop/logo/lo-07.png" alt="グランビュフル">
							</p>
							<p class="hover-text"></p>
							<div class="hover-box clearfix">
								<!-- hover-box -->
							</div>
							<!-- fadeInLeft animated -->
						</div>
						<!-- photo-hover -->
					</div>
				</div>
			</div>
			<div class="photo-block item-all">
				<div class="photo-block-in">
					<div class="top-visual"><img class="visual" src="images/newshop/shop/08.jpg" width="490" height="490" alt="アルファショップ">
						<div class="visual-bottom">
							<p class="top-floor">本館6F</p>
							<p class="top-shop">アルファショップ&nbsp;&nbsp;<span class="top-category">メンズ・レディス</span>
							</p>
							<p class="top-date">8/25(金)&nbsp;NEW OPEN!</p>
						</div>
					</div>
					<div class="photo-hover"><span class="icon new"></span>
						<div class="fadeInLeft animated">
							<h1 class="hover-ttl">本館6F<br>8/25(金)NEW OPEN!<br><span>メンズ・レディス</span></h1>
							<p class="hover-img"><img src="images/newshop/logo/lo-08.png" alt="アルファショップ">
							</p>
							<p class="hover-text">1959年から現在まで全世界で愛され続けてきたミリタリーウェアーの代表的ブランド。</p>
							<div class="hover-box clearfix">
								<!-- hover-box -->
							</div>
							<!-- fadeInLeft animated -->
						</div>
						<!-- photo-hover -->
					</div>
				</div>
			</div>
			<div class="photo-block item-all">
				<div class="photo-block-in">
					<div class="top-visual"><img class="visual" src="images/newshop/shop/09.jpg" width="490" height="490" alt="コレットアンドバイイング">
						<div class="visual-bottom">
							<p class="top-floor">本館7F</p>
							<p class="top-shop">コレットアンドバイイング&nbsp;&nbsp;<span class="top-category">アクセサリー</span>
							</p>
							<p class="top-date">9/15(金)&nbsp;NEW OPEN!</p>
						</div>
					</div>
					<div class="photo-hover"><span class="icon new"></span>
						<div class="fadeInLeft animated">
							<h1 class="hover-ttl">本館7F<br>9/15(金)NEW OPEN!<br><span>アクセサリー</span></h1>
							<p class="hover-img"><img src="images/newshop/logo/lo-09.png" alt="コレットアンドバイイング">
							</p>
							<p class="hover-text">"手に取りたくなるモノがいっぱい""大量買いしたくなる店"をコンセプトとしたアクセサリーショップ</p>
							<div class="hover-box clearfix">
								<p class="hover-plan">2,160円以上お買い上げでノベルティプレゼント【9/15～24】</p>
								<!-- hover-box -->
							</div>
							<!-- fadeInLeft animated -->
						</div>
						<!-- photo-hover -->
					</div>
				</div>
			</div>
			<div class="photo-block item-all">
				<div class="photo-block-in">
					<div class="top-visual"><img class="visual" src="images/newshop/shop/10.jpg" width="490" height="490" alt="ハナアフバイバルコスデザイン">
						<div class="visual-bottom">
							<p class="top-floor">新館3F</p>
							<p class="top-shop">ハナアフバイバルコスデザイン&nbsp;&nbsp;<span class="top-category">バッグ</span>
							</p>
							<p class="top-date">9/1(金)&nbsp;NEW OPEN!</p>
						</div>
					</div>
					<div class="photo-hover"><span class="icon new"></span><span class="badge-li"></span>
						<div class="fadeInLeft animated">
							<h1 class="hover-ttl">新館3F<br>9/1(金)NEW OPEN!<br><span>バッグ</span></h1>
							<p class="hover-img"><img src="images/newshop/logo/lo-10.png" alt="ハナアフバイバルコスデザイン">
							</p>
							<p class="hover-text">東洋らしいフォルムと西洋のエレガンスが融合するスタイルが新しいジャパニーズ・モダンを表現。</p>
							<div class="hover-box clearfix">
								<!-- hover-box -->
							</div>
							<!-- fadeInLeft animated -->
						</div>
						<!-- photo-hover -->
					</div>
				</div>
			</div>
			<div class="photo-block item-all">
				<div class="photo-block-in">
					<div class="top-visual"><img class="visual" src="images/newshop/shop/11.jpg" width="490" height="490" alt="soa">
						<div class="visual-bottom">
							<p class="top-floor">新館4F</p>
							<p class="top-shop">soa&nbsp;&nbsp;<span class="top-category">アクセサリー</span>
							</p>
							<p class="top-date">9/15(金)&nbsp;NEW OPEN!</p>
						</div>
					</div>
					<div class="photo-hover"><span class="icon new"></span><span class="badge-li"></span>
						<div class="fadeInLeft animated">
							<h1 class="hover-ttl">新館4F<br>9/15(金)NEW OPEN!<br><span>アクセサリー</span></h1>
							<p class="hover-img"><img src="images/newshop/logo/lo-11.png" alt="soa">
							</p>
							<p class="hover-text">足を運んでいただく度に違った装いをみせるアクセサリーのラインナップは一期一会を表す世界。自分へのご褒美をもっと気軽に。</p>
							<div class="hover-box clearfix">
								<p class="hover-plan">3,780円以上ご購入で3ピースポーチプレゼント（先着100名様）</p>
								<!-- hover-box -->
							</div>
							<!-- fadeInLeft animated -->
						</div>
						<!-- photo-hover -->
					</div>
				</div>
			</div>


			<?php
			/**
			 * PHOTO-END
			 */
			?>
		</div>

		<!-- /contents -->
	</div>

	<?php include_once($_SERVER['DOCUMENT_ROOT']. '/web/'.DIRNAME.'/inc/footer.php'); ?>
</body>

</html>