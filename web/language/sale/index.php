<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width,initial-scale=1">
<meta name="description" content="HIROSHIMA PARCO　Winter Sale！A/W clothes and lifestyle goods are off price.">
<meta name="keyword" content="広島パルコ,PARCO,HIROSHIMAParco,SALE,セール,EVENT,イベント">
<title>HIROSHIMA PARCO｜WINTER SALE</title>
<?php /* OGP */ ?>
<meta property="og:site_name" content="PARCO">
<meta property="og:type" content="article">
<meta property="og:url" content="http://hiroshima.parco.jp/web/language/sale/">
<meta property="og:title" content="HIROSHIMA PARCO｜WINTER SALE">
<meta property="og:description" content="HIROSHIMA PARCO　Winter Sale！A/W clothes and lifestyle goods are off price.">
<meta property="og:image" content="http://hiroshima.parco.jp/web/language/sale/images/ogp.png">
<meta property="og:locale" content="en_US">
<meta property="fb:admins" content="100005423256030">
<?php /* stylesheet */ ?>
<link rel="stylesheet" href="css/reset.css">
<link rel="stylesheet" href="css/flickity.css">
<link rel="stylesheet" href="css/layout.css">
<?php /* scripts */ ?>
<script src="js/modernizr.js"></script>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="js/jquery.js"><\/script>')</script>
<script src="js/jquery.tile.js"></script>
<script src="js/common.js"></script>
<script src="js/flickity.pkgd.js"></script>

</head>
<body id="Top">
  <?php include_once('../../../load_script.php'); ?>
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.0";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<header class="hd_page">

  <div class="up_to">
    <img src="images/up_to_50.png" alt="UP TO 50% OFF!" />
  </div>

  <div class="hd_area">
    <h1 class="hd_ttl">
      <img src="images/main_ttl.png" width="100%" alt="HIROSHIMA PARCO WINTER SALE 1/2(Tue)~1/17(Wed)">
    </h1>
    <div class="logo">
      <img src="images/hd_logo.png" width="100%" alt="logo">
    </div>
    <div class="discription">
      <img src="images/sub_ttl.png" width="100%" alt="Winter Sale!">
    </div>
  </div>

  <ul class="btn_list clearfix">
    <li class="btn btn01">
      <a href="http://hiroshima.parco.jp/page/storeinformation/index?type=floor1&lang=eng" target="blank">
        <img src="images/btn01.png" width="100%" alt="TAX FREE SHOPS">
      </a>
    </li>
  </ul>
</header>


<section class="block block-sale" id="SALE">
  <header class="blockTitle">
    <div class="ttl">
      <img src="images/sale_ttl.png" width="100%" alt="SALE">
      <div class="sankaku"></div>
    </div>

     <div class="shop-list-box">
       <div class="ico-box">
         <table>
         <tr>
           <th><img src="images/ico_taxfree.png" width="22" alt="TAX FREE"></th>
           <td class="tax">
             <img src="images/tax_txt.png" width="100%" alt="Tax Free transactions available for purchases over 5,000 yen.">
           </td>
         </tr>
         <tr>
           <th><img src="images/ico_unionpay.png" width="22" alt="UNIONPAY"></th>
           <td class="union">
             <img src="images/union_txt.png" width="100%" alt="China Union Pay is accepted at all shops.">
           </td>
         </tr>
         </table>
       </div>
      <!-- /.shop-list-box --></div>
  </header>


<?php /* ?> Ladies' <?php */ ?>
  <div class="conditions-box ladies">
  <img src="images/ladies_ttl.png" alt="Ladies'">
  <!-- /.conditions-box --></div>

  <ul class="shopList">

    <li class="shopList_item first">
      <div class="list-img">
        <img src="images/logo03.png" width="100%" alt="ROSE BUD">
      </div>
      <div class="list-icon">
        <ul>
          <li class="first">
            <img src="images/ico_taxfree.png" width="22" alt="TAXFREE">
          </li>
          <li class="last">
            <img src="images/ico_unionpay.png" width="22" alt="UNIONPAY">
          </li>
        </ul>
      </div>
      <div class="list-text">
        <div class="floor">MAIN WING 1F</div>
        <p class="shopName">ROSE BUD</p>
        <p class="shopCategory">Ladies'</p>
      <!-- /.list-text --></div>
    </li>

    <li class="shopList_item first">
      <div class="list-img">
        <img src="images/logo01.png" width="100%" alt="snidel">
      </div>
      <div class="list-icon">
        <ul>
          <li class="last">
            <img src="images/ico_unionpay.png" width="22" alt="UNIONPAY">
          </li>
        </ul>
      </div>
      <div class="list-text">
        <div class="floor">MAIN WING 2F</div>
        <p class="shopName">snidel</p>
        <p class="shopCategory">Ladies'</p>
        <!-- /.list-text --></div>
    </li>

    <li class="shopList_item first">
      <div class="list-img">
        <img src="images/logo02.png" width="100%" alt="Lily Brown">
      </div>
      <div class="list-icon">
        <ul>
          <li class="last">
            <img src="images/ico_unionpay.png" width="22" alt="UNIONPAY">
          </li>
        </ul>
      </div>
      <div class="list-text">
        <div class="floor">MAIN WING 2F</div>
        <p class="shopName">Lily Brown</p>
        <p class="shopCategory">Ladies'</p>
      <!-- /.list-text --></div>
    </li>

    <li class="shopList_item first">
      <div class="list-img">
        <img src="images/logo04.png" width="100%" alt="GRACE CONTINENTAL">
      </div>
      <div class="list-icon">
        <ul>
          <li class="first">
            <img src="images/ico_taxfree.png" width="22" alt="TAXFREE">
          </li>
          <li class="last">
            <img src="images/ico_unionpay.png" width="22" alt="UNIONPAY">
          </li>
        </ul>
      </div>
      <div class="list-text">
        <div class="floor">MAIN WING 2F</div>
        <p class="shopName">GRACE CONTINENTAL</p>
        <p class="shopCategory">Ladies'</p>
      <!-- /.list-text --></div>
    </li>

    <li class="shopList_item first">
      <div class="list-img">
        <img src="images/logo05.png" width="100%" alt="Mila Owen">
      </div>
      <div class="list-icon">
        <ul>
          <li class="first">
            <img src="images/ico_taxfree.png" width="22" alt="TAXFREE">
          </li>
          <li class="last">
            <img src="images/ico_unionpay.png" width="22" alt="UNIONPAY">
          </li>
        </ul>
      </div>
      <div class="list-text">
        <div class="floor">MAIN WING 2F</div>
        <p class="shopName">Mila Owen</p>
        <p class="shopCategory">Ladies'</p>
      <!-- /.list-text --></div>
    </li>

    <li class="shopList_item first">
      <div class="list-img">
        <img src="images/logo06.png" width="100%" alt="LEST ROSE">
      </div>
      <div class="list-icon">
        <ul>
          <li class="first">
            <img src="images/ico_taxfree.png" width="22" alt="TAXFREE">
          </li>
          <li class="last">
            <img src="images/ico_unionpay.png" width="22" alt="UNIONPAY">
          </li>
        </ul>
      </div>
      <div class="list-text">
        <div class="floor">MAIN WING 3F</div>
        <p class="shopName">LEST ROSE</p>
        <p class="shopCategory">Ladies'</p>
      <!-- /.list-text --></div>
    </li>

    <li class="shopList_item first">
      <div class="list-img">
        <img src="images/logo07.png" width="100%" alt="MERCURYDUO">
      </div>
      <div class="list-icon">
        <ul>
          <li class="first">
            <img src="images/ico_taxfree.png" width="22" alt="TAXFREE">
          </li>
          <li class="last">
            <img src="images/ico_unionpay.png" width="22" alt="UNIONPAY">
          </li>
        </ul>
      </div>
      <div class="list-text">
        <div class="floor">MAIN WING 3F</div>
        <p class="shopName">MERCURYDUO</p>
        <p class="shopCategory">Ladies'</p>
      <!-- /.list-text --></div>
    </li>

    <li class="shopList_item first">
      <div class="list-img">
        <img src="images/logo08.png" width="100%" alt="MURUA">
      </div>
      <div class="list-icon">
        <ul>
          <li class="first">
            <img src="images/ico_taxfree.png" width="22" alt="TAXFREE">
          </li>
          <li class="last">
            <img src="images/ico_unionpay.png" width="22" alt="UNIONPAY">
          </li>
        </ul>
      </div>
      <div class="list-text">
        <div class="floor">MAIN WING 3F</div>
        <p class="shopName">MURUA</p>
        <p class="shopCategory">Ladies'</p>
      <!-- /.list-text --></div>
    </li>

    <li class="shopList_item first">
      <div class="list-img">
        <img src="images/logo09.png" width="100%" alt="PEACH JOHN">
      </div>
      <div class="list-icon">
        <ul>
          <li class="first">
            <img src="images/ico_taxfree.png" width="22" alt="TAXFREE">
          </li>
          <li class="last">
            <img src="images/ico_unionpay.png" width="22" alt="UNIONPAY">
          </li>
        </ul>
      </div>
      <div class="list-text">
        <div class="floor">MAIN WING 3F</div>
        <p class="shopName">PEACH JOHN</p>
        <p class="shopCategory">Ladies'</p>
      <!-- /.list-text --></div>
    </li>

    <li class="shopList_item first">
      <div class="list-img">
        <img src="images/logo11.png" width="100%" alt="EMODA">
      </div>
      <div class="list-icon">
        <ul>
          <li class="first">
            <img src="images/ico_taxfree.png" width="22" alt="TAXFREE">
          </li>
          <li class="last">
            <img src="images/ico_unionpay.png" width="22" alt="UNIONPAY">
          </li>
        </ul>
      </div>
      <div class="list-text">
        <div class="floor">MAIN WING 4F</div>
        <p class="shopName">EMODA</p>
        <p class="shopCategory">Ladies'</p>
      <!-- /.list-text --></div>
    </li>

    <li class="shopList_item first">
      <div class="list-img">
        <img src="images/logo12.png" width="100%" alt="KBF">
      </div>
      <div class="list-icon">
        <ul>
          <li class="first">
            <img src="images/ico_taxfree.png" width="22" alt="TAXFREE">
          </li>
          <li class="last">
            <img src="images/ico_unionpay.png" width="22" alt="UNIONPAY">
          </li>
        </ul>
      </div>
      <div class="list-text">
        <div class="floor">MAIN WING 7F</div>
        <p class="shopName">KBF</p>
        <p class="shopCategory">Ladies'</p>
      <!-- /.list-text --></div>
    </li>

    <li class="shopList_item first">
      <div class="list-img">
        <img src="images/logo13.png" width="100%" alt="Heather">
      </div>
      <div class="list-icon">
        <ul>
          <li class="first">
            <img src="images/ico_taxfree.png" width="22" alt="TAXFREE">
          </li>
          <li class="last">
            <img src="images/ico_unionpay.png" width="22" alt="UNIONPAY">
          </li>
        </ul>
      </div>
      <div class="list-text">
        <div class="floor">MAIN WING 7F</div>
        <p class="shopName">Heather</p>
        <p class="shopCategory">Ladies'</p>
      <!-- /.list-text --></div>
    </li>

    <li class="shopList_item first">
      <div class="list-img">
        <img src="images/logo14.png" width="100%" alt="JEANASiS">
      </div>
      <div class="list-icon">
        <ul>
          <li class="first">
            <img src="images/ico_taxfree.png" width="22" alt="TAXFREE">
          </li>
          <li class="last">
            <img src="images/ico_unionpay.png" width="22" alt="UNIONPAY">
          </li>
        </ul>
      </div>
      <div class="list-text">
        <div class="floor">MAIN WING 7F</div>
        <p class="shopName">JEANASiS</p>
        <p class="shopCategory">Ladies'</p>
      <!-- /.list-text --></div>
    </li>

    <li class="shopList_item first">
      <div class="list-img">
        <img src="images/logo15.png" width="100%" alt="LOWRYS FARM">
      </div>
      <div class="list-icon">
        <ul>
          <li class="first">
            <img src="images/ico_taxfree.png" width="22" alt="TAXFREE">
          </li>
          <li class="last">
            <img src="images/ico_unionpay.png" width="22" alt="UNIONPAY">
          </li>
        </ul>
      </div>
      <div class="list-text">
        <div class="floor">MAIN WING 8F</div>
        <p class="shopName">LOWRYS FARM</p>
        <p class="shopCategory">Ladies'</p>
      <!-- /.list-text --></div>
    </li>

    <li class="shopList_item first">
      <div class="list-img">
        <img src="images/logo16.png" width="100%" alt="RNA MEDIA">
      </div>
      <div class="list-icon">
        <ul>
          <li class="first">
            <img src="images/ico_taxfree.png" width="22" alt="TAXFREE">
          </li>
          <li class="last">
            <img src="images/ico_unionpay.png" width="22" alt="UNIONPAY">
          </li>
        </ul>
      </div>
      <div class="list-text">
        <div class="floor">MAIN WING 8F</div>
        <p class="shopName">RNA MEDIA</p>
        <p class="shopCategory">Ladies'</p>
      <!-- /.list-text --></div>
    </li>

  </ul>
<?php /* ?> Ladies' END<?php */ ?>

<?php /* ?> Men's <?php */ ?>
  <div class="conditions-box mens">
  <img src="images/mens_ttl.png" alt="Men's">
  <!-- /.conditions-box --></div>

  <ul class="shopList">

    <li class="shopList_item first">
      <div class="list-img">
        <img src="images/logo18.png" width="100%" alt="MONSIEUR NICOLE">
      </div>
      <div class="list-icon">
        <ul>
          <li class="first">
            <img src="images/ico_taxfree.png" width="22" alt="TAXFREE">
          </li>
          <li class="last">
            <img src="images/ico_unionpay.png" width="22" alt="UNIONPAY">
          </li>
        </ul>
      </div>
      <div class="list-text">
        <div class="floor">MAIN WING 5F</div>
        <p class="shopName">MONSIEUR NICOLE</p>
        <p class="shopCategory">Men's</p>
      <!-- /.list-text --></div>
    </li>

    <li class="shopList_item first">
      <div class="list-img">
        <img src="images/logo19.png" width="100%" alt="JunRed">
      </div>
      <div class="list-icon">
        <ul>
          <li class="first">
            <img src="images/ico_taxfree.png" width="22" alt="TAXFREE">
          </li>
          <li class="last">
            <img src="images/ico_unionpay.png" width="22" alt="UNIONPAY">
          </li>
        </ul>
      </div>
      <div class="list-text">
        <div class="floor">MAIN WING 5F</div>
        <p class="shopName">JunRed</p>
        <p class="shopCategory">Men's</p>
      <!-- /.list-text --></div>
    </li>

    <li class="shopList_item first">
      <div class="list-img">
        <img src="images/logo21.png" width="100%" alt="Buffalo Bobs">
      </div>
      <div class="list-icon">
        <ul>
          <li class="first">
            <img src="images/ico_taxfree.png" width="22" alt="TAXFREE">
          </li>
          <li class="last">
            <img src="images/ico_unionpay.png" width="22" alt="UNIONPAY">
          </li>
        </ul>
      </div>
      <div class="list-text">
        <div class="floor">MAIN WING 5F</div>
        <p class="shopName">Buffalo Bobs</p>
        <p class="shopCategory">Men's</p>
      <!-- /.list-text --></div>
    </li>

    <li class="shopList_item first">
      <div class="list-img">
        <img src="images/logo17.png" width="100%" alt="RAGE BLUE">
      </div>
      <div class="list-icon">
        <ul>
          <li class="first">
            <img src="images/ico_taxfree.png" width="22" alt="TAXFREE">
          </li>
          <li class="last">
            <img src="images/ico_unionpay.png" width="22" alt="UNIONPAY">
          </li>
        </ul>
      </div>
      <div class="list-text">
        <div class="floor">MAIN WING 6F</div>
        <p class="shopName">RAGE BLUE</p>
        <p class="shopCategory">Men's</p>
      <!-- /.list-text --></div>
    </li>

  </ul>
<?php /* ?> Men's END<?php */ ?>

<?php /* ?> Ladies'&amp;Men's <?php */ ?>
  <div class="conditions-box ladies_mens">
  <img src="images/ladiesmens_ttl.png" alt="Ladies'&amp;Men's">
  <!-- /.conditions-box --></div>

  <ul class="shopList">

    <li class="shopList_item first">
      <div class="list-img">
        <img src="images/logo27.png" width="100%" alt="HARE">
      </div>
      <div class="list-icon">
        <ul>
          <li class="first">
            <img src="images/ico_taxfree.png" width="22" alt="TAXFREE">
          </li>
          <li class="last">
            <img src="images/ico_unionpay.png" width="22" alt="UNIONPAY">
          </li>
        </ul>
      </div>
      <div class="list-text">
        <div class="floor">MAIN WING 6F</div>
        <p class="shopName">HARE</p>
        <p class="shopCategory">Ladies'&amp;Men's</p>
      <!-- /.list-text --></div>
    </li>

    <li class="shopList_item first">
      <div class="list-img">
        <img src="images/logo28.png" width="100%" alt="XLARGE/X-girl">
      </div>
      <div class="list-icon">
        <ul>
          <li class="first">
            <img src="images/ico_taxfree.png" width="22" alt="TAXFREE">
          </li>
          <li class="last">
            <img src="images/ico_unionpay.png" width="22" alt="UNIONPAY">
          </li>
        </ul>
      </div>
      <div class="list-text">
        <div class="floor">MAIN WING 6F</div>
        <p class="shopName">XLARGE/X-girl</p>
        <p class="shopCategory">Ladies'&amp;Men's</p>
      <!-- /.list-text --></div>
    </li>

    <li class="shopList_item first">
      <div class="list-img">
        <img src="images/logo24.png" width="100%" alt="URBAN RESEARCH">
      </div>
      <div class="list-icon">
        <ul>
          <li class="first">
            <img src="images/ico_taxfree.png" width="22" alt="TAXFREE">
          </li>
          <li class="last">
            <img src="images/ico_unionpay.png" width="22" alt="UNIONPAY">
          </li>
        </ul>
      </div>
      <div class="list-text">
        <div class="floor">MAIN WING 2F</div>
        <p class="shopName">URBAN RESEARCH</p>
        <p class="shopCategory">Ladies'&amp;Men's</p>
      <!-- /.list-text --></div>
    </li>

    <li class="shopList_item first">
      <div class="list-img">
        <img src="images/logo22.png" width="100%" alt="UNITED ARROWS">
      </div>
      <div class="list-icon">
        <ul>
          <li class="first">
            <img src="images/ico_taxfree.png" width="22" alt="TAXFREE">
          </li>
          <li class="last">
            <img src="images/ico_unionpay.png" width="22" alt="UNIONPAY">
          </li>
        </ul>
      </div>
      <div class="list-text">
        <div class="floor">NEW WING B1・1F</div>
        <p class="shopName">UNITED ARROWS</p>
        <p class="shopCategory">Ladies'&amp;Men's</p>
      <!-- /.list-text --></div>
    </li>

    <li class="shopList_item first">
      <div class="list-img">
        <img src="images/logo29.png" width="100%" alt="CABANE de ZUCCa">
      </div>
      <div class="list-icon">
        <ul>
          <li class="first">
            <img src="images/ico_taxfree.png" width="22" alt="TAXFREE">
          </li>
          <li class="last">
            <img src="images/ico_unionpay.png" width="22" alt="UNIONPAY">
          </li>
        </ul>
      </div>
      <div class="list-text">
        <div class="floor">NEW WING 2F</div>
        <p class="shopName">CABANE de ZUCCa</p>
        <p class="shopCategory">Ladies'&amp;Men's</p>
      <!-- /.list-text --></div>
    </li>

    <li class="shopList_item first">
      <div class="list-img">
        <img src="images/logo23.png" width="100%" alt="UNITED ARROWS green label relaxing">
      </div>
      <div class="list-icon">
        <ul>
          <li class="first">
            <img src="images/ico_taxfree.png" width="22" alt="TAXFREE">
          </li>
          <li class="last">
            <img src="images/ico_unionpay.png" width="22" alt="UNIONPAY">
          </li>
        </ul>
      </div>
      <div class="list-text">
        <div class="floor">NEW WING 4F</div>
        <p class="shopName">UNITED ARROWS green label relaxing</p>
        <p class="shopCategory">Ladies'&amp;Men's</p>
      <!-- /.list-text --></div>
    </li>

    <li class="shopList_item first">
      <div class="list-img">
        <img src="images/logo25.png" width="100%" alt="URBAN RESEARCH DOORS">
      </div>
      <div class="list-icon">
        <ul>
          <li class="first">
            <img src="images/ico_taxfree.png" width="22" alt="TAXFREE">
          </li>
          <li class="last">
            <img src="images/ico_unionpay.png" width="22" alt="UNIONPAY">
          </li>
        </ul>
      </div>
      <div class="list-text">
        <div class="floor">NEW WING 4F</div>
        <p class="shopName">URBAN RESEARCH DOORS</p>
        <p class="shopCategory">Ladies',Men's,Goods</p>
      <!-- /.list-text --></div>
    </li>

    <li class="shopList_item first">
      <div class="list-img">
        <img src="images/logo30.png" width="100%" alt="MHL.">
      </div>
      <div class="list-icon">
        <ul>
          <li class="first">
            <img src="images/ico_taxfree.png" width="22" alt="TAXFREE">
          </li>
          <li class="last">
            <img src="images/ico_unionpay.png" width="22" alt="UNIONPAY">
          </li>
        </ul>
      </div>
      <div class="list-text">
        <div class="floor">NEW WING 4F</div>
        <p class="shopName">MHL.</p>
        <p class="shopCategory">Ladies'&amp;Men's</p>
      <!-- /.list-text --></div>
    </li>

    <li class="shopList_item first">
      <div class="list-img">
        <img src="images/logo31.png" width="100%" alt="HUMOR MARKET">
      </div>
      <div class="list-icon">
        <ul>
          <li class="first">
            <img src="images/ico_taxfree.png" width="22" alt="TAXFREE">
          </li>
          <li class="last">
            <img src="images/ico_unionpay.png" width="22" alt="UNIONPAY">
          </li>
        </ul>
      </div>
      <div class="list-text">
        <div class="floor">NEW WING 4F</div>
        <p class="shopName">HUMOR MARKET</p>
        <p class="shopCategory">Ladies',Men's,Kids</p>
      <!-- /.list-text --></div>
    </li>

    <li class="shopList_item first">
      <div class="list-img">
        <img src="images/logo32.png" width="100%" alt="MURASAKI SPORTS">
      </div>
      <div class="list-icon">
        <ul>
          <li class="first">
            <img src="images/ico_taxfree.png" width="22" alt="TAXFREE">
          </li>
          <li class="last">
            <img src="images/ico_unionpay.png" width="22" alt="UNIONPAY">
          </li>
        </ul>
      </div>
      <div class="list-text">
        <div class="floor">NEW WING 6F</div>
        <p class="shopName">MURASAKI SPORTS</p>
        <p class="shopCategory">Ladies',Men's,Sport</p>
      <!-- /.list-text --></div>
    </li>

    <li class="shopList_item first">
      <div class="list-img">
        <img src="images/logo33.png" width="100%" alt="HYSTERIC MINI">
      </div>
      <div class="list-icon">
        <ul>
          <li class="first">
            <img src="images/ico_taxfree.png" width="22" alt="TAXFREE">
          </li>
          <li class="last">
            <img src="images/ico_unionpay.png" width="22" alt="UNIONPAY">
          </li>
        </ul>
      </div>
      <div class="list-text">
        <div class="floor">NEW WING 6F</div>
        <p class="shopName">HYSTERIC MINI</p>
        <p class="shopCategory">Kids</p>
      <!-- /.list-text --></div>
    </li>

    <li class="shopList_item first">
      <div class="list-img">
        <img src="images/logo26.png" width="100%" alt="MUJIRUSHI RYOHIN">
      </div>
      <div class="list-icon">
        <ul>
          <li class="first">
            <img src="images/ico_taxfree.png" width="22" alt="TAXFREE">
          </li>
          <li class="last">
            <img src="images/ico_unionpay.png" width="22" alt="UNIONPAY">
          </li>
        </ul>
      </div>
      <div class="list-text">
        <div class="floor">NEW WING 7・8F</div>
        <p class="shopName">MUJIRUSHI RYOHIN</p>
        <p class="shopCategory">Ladies',Men's,Kids,Goods</p>
      <!-- /.list-text --></div>
    </li>


  </ul>
<?php /* ?> Ladies'&amp;Men's END<?php */ ?>


<?php /* ?> Fashion goods &amp; Accesory <?php */ ?>
  <div class="conditions-box fashion">
  <img src="images/fashion_ttl.png" alt="Fashion goods &amp; Accesory">
  <!-- /.conditions-box --></div>

  <ul class="shopList">

    <li class="shopList_item first">
      <div class="list-img">
        <img src="images/logo34.png" width="100%" alt="Samantha Vega">
      </div>
      <div class="list-icon">
        <ul>
          <li class="last">
            <img src="images/ico_unionpay.png" width="22" alt="UNIONPAY">
          </li>
        </ul>
      </div>
      <div class="list-text">
        <div class="floor">MAIN WING 1F</div>
        <p class="shopName">Samantha Vega</p>
        <p class="shopCategory">Bags,Goods</p>
      <!-- /.list-text --></div>
    </li>

    <li class="shopList_item first">
      <div class="list-img">
        <img src="images/logo35.png" width="100%" alt="TiC TAC">
      </div>
      <div class="list-icon">
        <ul>
          <li class="first">
            <img src="images/ico_taxfree.png" width="22" alt="TAXFREE">
          </li>
          <li class="last">
            <img src="images/ico_unionpay.png" width="22" alt="UNIONPAY">
          </li>
        </ul>
      </div>
      <div class="list-text">
        <div class="floor">MAIN WING 1F</div>
        <p class="shopName">TiC TAC</p>
        <p class="shopCategory">Watch</p>
      <!-- /.list-text --></div>
    </li>

    <li class="shopList_item first">
      <div class="list-img">
        <img src="images/logo36.png" width="100%" alt="DIANA">
      </div>
      <div class="list-icon">
        <ul>
          <li class="first">
            <img src="images/ico_taxfree.png" width="22" alt="TAXFREE">
          </li>
          <li class="last">
            <img src="images/ico_unionpay.png" width="22" alt="UNIONPAY">
          </li>
        </ul>
      </div>
      <div class="list-text">
        <div class="floor">MAIN WING 1F</div>
        <p class="shopName">DIANA</p>
        <p class="shopCategory">Shoes,Bags</p>
      <!-- /.list-text --></div>
    </li>

    <li class="shopList_item first">
      <div class="list-img">
        <img src="images/logo40.png" width="100%" alt="SIENA">
      </div>
      <div class="list-icon">
        <ul>
          <li class="first">
            <img src="images/ico_taxfree.png" width="22" alt="TAXFREE">
          </li>
          <li class="last">
            <img src="images/ico_unionpay.png" width="22" alt="UNIONPAY">
          </li>
        </ul>
      </div>
      <div class="list-text">
        <div class="floor">MAIN WING 1F</div>
        <p class="shopName">SIENA</p>
        <p class="shopCategory">Jewelry</p>
      <!-- /.list-text --></div>
    </li>

    <li class="shopList_item first">
      <div class="list-img">
        <img src="images/logo37.png" width="100%" alt="gelato pique">
      </div>
      <div class="list-icon">
        <ul>
          <li class="last">
            <img src="images/ico_unionpay.png" width="22" alt="UNIONPAY">
          </li>
        </ul>
      </div>
      <div class="list-text">
        <div class="floor">MAIN WING 2F</div>
        <p class="shopName">gelato pique</p>
        <p class="shopCategory">Roomwear,Goods</p>
      <!-- /.list-text --></div>
    </li>

    <li class="shopList_item first">
      <div class="list-img">
        <img src="images/logo39.png" width="100%" alt="DIESEL">
      </div>
      <div class="list-icon">
        <ul>
          <li class="first">
            <img src="images/ico_taxfree.png" width="22" alt="TAXFREE">
          </li>
          <li class="last">
            <img src="images/ico_unionpay.png" width="22" alt="UNIONPAY">
          </li>
        </ul>
      </div>
      <div class="list-text">
        <div class="floor">MAIN WING 2F</div>
        <p class="shopName">DIESEL</p>
        <p class="shopCategory">Shoes,Bags,Accessories</p>
      <!-- /.list-text --></div>
    </li>

    <li class="shopList_item first">
      <div class="list-img">
        <img src="images/logo41.png" width="100%" alt="MCPC PRODUCT SORT">
      </div>
      <div class="list-icon">
        <ul>
          <li class="first">
            <img src="images/ico_taxfree.png" width="22" alt="TAXFREE">
          </li>
          <li class="last">
            <img src="images/ico_unionpay.png" width="22" alt="UNIONPAY">
          </li>
        </ul>
      </div>
      <div class="list-text">
        <div class="floor">NEW WING B1F</div>
        <p class="shopName">MCPC PRODUCT SORT</p>
        <p class="shopCategory">Bags</p>
      <!-- /.list-text --></div>
    </li>

    <li class="shopList_item first">
      <div class="list-img">
        <img src="images/logo42.png" width="100%" alt="COLLECTORS">
      </div>
      <div class="list-icon">
        <ul>
          <li class="first">
            <img src="images/ico_taxfree.png" width="22" alt="TAXFREE">
          </li>
          <li class="last">
            <img src="images/ico_unionpay.png" width="22" alt="UNIONPAY">
          </li>
        </ul>
      </div>
      <div class="list-text">
        <div class="floor">NEW WING 4F</div>
        <p class="shopName">COLLECTORS</p>
        <p class="shopCategory">Bags,Men's,Goods</p>
      <!-- /.list-text --></div>
    </li>

    <li class="shopList_item first">
      <div class="list-img">
        <img src="images/logo38.png" width="100%" alt="Franc franc">
      </div>
      <div class="list-icon">
        <ul>
          <li class="last">
            <img src="images/ico_unionpay.png" width="22" alt="UNIONPAY">
          </li>
        </ul>
      </div>
      <div class="list-text">
        <div class="floor">NEW WING 5F</div>
        <p class="shopName">Franc franc</p>
        <p class="shopCategory">Goods</p>
      <!-- /.list-text --></div>
    </li>

  </ul>
<?php /* ?> Fashion goods &amp; Accesory END<?php */ ?>

<?php /* ?> Cosmetics <?php */ ?>
  <div class="conditions-box cosme">
  <img src="images/cosme_ttl.png" alt="Cosmetics">
  <!-- /.conditions-box --></div>

  <ul class="shopList">


    <li class="shopList_item first">
      <div class="list-img">
        <img src="images/logo43.png" width="100%" alt="LALINE">
      </div>
      <div class="list-icon">
        <ul>
          <li class="first">
            <img src="images/ico_taxfree.png" width="22" alt="TAXFREE">
          </li>
          <li class="last">
            <img src="images/ico_unionpay.png" width="22" alt="UNIONPAY">
          </li>
        </ul>
      </div>
      <div class="list-text">
        <div class="floor">MAIN WING B1F</div>
        <p class="shopName">LALINE</p>
        <p class="shopCategory">Cosmetics</p>
      <!-- /.list-text --></div>
    </li>

    <li class="shopList_item first">
      <div class="list-img">
        <img src="images/logo44.png" width="100%" alt="ROSEMARY">
      </div>
      <div class="list-icon">
        <ul>
          <li class="first">
            <img src="images/ico_taxfree.png" width="22" alt="TAXFREE">
          </li>
          <li class="last">
            <img src="images/ico_unionpay.png" width="22" alt="UNIONPAY">
          </li>
        </ul>
      </div>
      <div class="list-text">
        <div class="floor">MAIN WING B1F</div>
        <p class="shopName">ROSEMARY</p>
        <p class="shopCategory">Cosmetics</p>
      <!-- /.list-text --></div>
    </li>

    <li class="shopList_item first">
      <div class="list-img">
        <img src="images/logo45.png" width="100%" alt="SKINFOOD">
      </div>
      <div class="list-icon">
        <ul>
          <li class="first">
            <img src="images/ico_taxfree.png" width="22" alt="TAXFREE">
          </li>
          <li class="last">
            <img src="images/ico_unionpay.png" width="22" alt="UNIONPAY">
          </li>
        </ul>
      </div>
      <div class="list-text">
        <div class="floor">MAIN WING B1F</div>
        <p class="shopName">SKINFOOD</p>
        <p class="shopCategory">Cosmetics</p>
      <!-- /.list-text --></div>
    </li>

  </ul>
<?php /* ?> Cosmetics END<?php */ ?>


<div class="conditions-caution-ttl"><img src="images/and_more.png" width="118" alt="and more...!"></div>
<div class="conditions-caution">
*The service is available only during the valid period. *The service and provided cannot be used with other services. *Sales and discounts may not apply to some items in each shop. *Sale details to be checked at each shop. *Some shops are not having a sale. shop.
</div>
</section>

<div class="pageTop">
  <a href="#Top"><img src="images/btn_pagetop.png" width="45" alt="ページTOP"></a>
<!-- /.pageTop --></div>

<footer class="page-footer">

<div class="snavi">
    <ul class="clearfix">
      <div class="list"><div class="fb-like" data-href="http://hiroshima.parco.jp/web/language/sale/" data-layout="button" data-action="like" data-show-faces="true" data-share="false"></div></div>
        <div class="list"><a class="twitter-share-button" href="https://twitter.com/share" data-dnt="true" data-lang="en">Tweet</a>
        <script>window.twttr=(function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],t=window.twttr||{};if(d.getElementById(id))return;js=d.createElement(s);js.id=id;js.src="https://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);t._e=[];t.ready=function(f){t._e.push(f);};return t;}(document,"script","twitter-wjs"));</script></div>
        <div class="list line"><span>
        <script type="text/javascript" src="//media.line.me/js/line-button.js?v=20140411" ></script>
        <script type="text/javascript">
        new media_line_me.LineButton({"pc":false,"lang":"ja","type":"b"});
        </script>
        </div></span>

        <div class="list"><script src="https://apis.google.com/js/platform.js" async defer></script><div class="g-plusone" data-size="medium" data-annotation="none" data-href="http://hiroshima.parco.jp/web/language/sale/"></div></div>
        <div class="list"><script src="http://tjs.sjs.sinajs.cn/open/api/js/wb.js" type="text/javascript" charset="utf-8"></script><wb:share-button addition="simple" type="button" pic="http%3a%2f%2fhiroshima%2eparco%2ejp%2fweb%2flanguage%2fsale%2f"></wb:share-button></div>
    </ul>

  </div>
<div class="footer_bottom">
  <p class="btn_siteTop"><a href="http://hiroshima.parco.jp/page/storeinformation/index?type=recommend&lang=eng" target="blank"><img src="images/btn_sitetop.png" width="121" alt="SITE TOP"></a></p>
  <p class="copyright"><img src="images/copyright.png" width="175" alt="CopyRight"></p>
</div>
</footer>

<script>


  $('#flickity').flickity({
  cellAlign:'left',
  contain:true
  });

</script>

</body>
</html>
