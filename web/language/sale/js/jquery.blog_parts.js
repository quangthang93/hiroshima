;(function($) {

	$.fn.blog_parts = function(options) {
		console.log(options);
		var base = $(this);
		var init_count = options.init_count;
		var next_count = options.next_count;
		var more_post = options.more_post;

		var view_type = options.view_type;
		var view_data = {
				'view_store_name' : false,
				'view_shop_name' : true,
				'view_social' : true,
				'view_link_target' : ''
		};
		if(options.view_store_name === true || options.view_store_name === 1){
			view_data.view_store_name = true;
		}
		if(options.view_shop_name === false || options.view_shop_name === 0){
			view_data.view_shop_name = false;
		}
		if(options.view_social === false || options.view_social === 0){
			view_data.view_social = false;
		}
		if(options.view_link_target !== undefined && options.view_link_target.replace(/\s+$/, "") != ""){
			view_data.view_link_target = options.view_link_target;
		}

		var wrapper = ''
		var template_name = '';
		switch(view_type){
			case 1:
				template_name = 'blog_parts.tpl';
				wrapper = 'column5_blog_list_block_01';
				break;
			case 2:
				if(view_data.view_social){
					template_name = 'sp_blog_parts_list.tpl';
					wrapper = 'list_block_wrap';
				}else{
					template_name = 'sp_blog_parts_list_nosocial.tpl';
					wrapper = 'list_block_wrap_ns';
				}
				break;
			case 3:
				template_name = 'sp_blog_parts.tpl';
				wrapper = 'thumbnail_block_wrap';
				break;
			default:
				return false;
		}

		var param = {};
		param['t'] = 1;
		var item = [
				 'blog_type'
				,'scode'
				,'sid'
				,'cid'
				,'cid_or'
				,'post_sdate'
				,'post_edate'
				,'ext_response_fields'
			];
		for (var i=0; i < item.length; i++){
			if(options[item[i]]){
				param[item[i]] = options[item[i]];
			}
		}

		base.addClass("blog_parts_view_type_" + view_type);
		base.html("");
		base.append($("<div/>").addClass(wrapper).addClass("clearfix"));
		var container = $("." + wrapper, base);

		$.get('http://shibuya.parco.jp/web/storeinfomation/blog_parts/template/' + template_name + '?timestamp=' + new Date().getTime(), function(template) {
			var tmpl = Hogan.compile(template);
			writeContents(param, tmpl, base, container, more_post, 1, init_count, next_count, view_type, view_data);
		});
	};
	function pad(n){return n<10 ? '0'+n : n}
	function writeContents(param, tmpl, base, container, more_post, first, last, next_count, view_type, view_data) {

		param['first'] = first;
		param['last'] = last;

		$.ajax({
			url: '/api/search_post/',
			type: 'post',
			cache: false,
			data: param,
			dataType: 'json',
			success: function(data){
				console.log(data);

				if(data.header.code != '0'){
					return;
				}

				//socialボタン動作用
				social.isLogin = data.result.user.is_login;

				if(!data.header.count){
					base.removeClass('loading');
					$("<p/>").addClass("no_result").text("該当する記事はありません").appendTo(base);
				}else{
					var tmpl_data = {};
					tmpl_data.data = data;
					tmpl_data.view = view_data;

					var $def = 	$('.default');
					$blogContents = $('.column5_blog_list_block_01');
					var wNames = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu.', 'Fri', 'Sat'];
					$.each(tmpl_data.data.result.items, function () {
						if(this.store_code == 'WWW'){
							this.store_name = '';
						}
						if(this.event_sdate){
								var startDate = new Date(this.event_sdate);
								this.event_sdate =pad(startDate.getMonth() + 1)+'/'+ pad(startDate.getDate())+'/'+ '('+wNames[startDate.getDay()]+')';
							}
						if(this.event_edate){
							var endDate = new Date(this.event_edate);
							this.event_edate =pad(endDate.getMonth() + 1)+'/'+ pad(endDate.getDate())+'/'+ '('+wNames[endDate.getDay()]+')';
						}
					});

					container.html(tmpl.render(tmpl_data));
					base.removeClass('loading');
					$('.imgLiquidBox', container).imgLiquid();
					switch(view_type){
						case 1:
							$(".event-date", container).tile(3);
							$(".post_shop", container).tile(3);
							$(".post_title", container).tile(3);
							$(".post_icon_block", container).css( 'position', 'relative'); //IE8以前用に再指定（上詰めになるのを防ぐ）
							break;
						case 3:
							$(".shop_name", container).tile(2);
							$(".text_link", container).tile(2);
							break;
					}
					$('.js-blog-dec', base).click(function() {
						$def.hide();
						$(".more_post", base).remove();
						var postID = $(this).attr('data-post-id');
						$.each(tmpl_data.data.result.items, function() {
							if(this.post_id == postID) {
								//$blogContents.html(this.contents);
								var postDate = new Date(this.post_date);
								$blogContents.html('').append($('<div class="blog-detail"></div>').append('<p class="blog-title">'+this.title+'</p>'+'<p class="post-date">'+postDate.getFullYear()+'/'+pad(postDate.getMonth() + 1)+'/'+ pad(postDate.getDate())+'</p>'+this.contents));
								$blogContents.append('<div class="blog-back-btn"><a id="back-btn" href="javascript:void(0)"><img src="http://shibuya.parco.jp/web/storeinfomation/blog_parts/images/back_btn.png" /></a></div>');
								$('#back-btn').click(function() {
									$blogContents.html('');
									base.addClass('loading');
									writeContents(param, tmpl, base, container, more_post, 1, data.header.count, next_count, view_type, view_data);
								});
							}
						});
					});
				}
				//アンカ処理
				$('a[href^=#]').click(function(e) {
					$('html,body').stop(true, true);
					var topPos = $(window).scrollTop();
					var hash = $(this.hash);
					var hashOffset = $(hash).offset().top;
					var speed = Math.abs(($(hash).offset().top - topPos));
					if(true) {
						$def.show();
						$blogContents.html('');
						base.addClass('loading');
						writeContents(param, tmpl, base, container, more_post, 1, 3, next_count, view_type, view_data);
					}
					$('html,body').stop(true, true).animate({
						scrollTop: hashOffset
					}, 300);
					e.preventDefault();
				});
				if(more_post && next_count) {
					if(data.header.last < data.header.count){
						//初回のみ「もっと見る」作成
						if(!$(".more_post", base).size()) {
							switch(view_type){
								case 1:
									$("<div/>").addClass("more_post").append(
										$("<a/>").attr("href", "javascript:void(0);")
									).appendTo(base);
									break;
								case 2:
								case 3:
									$("<div/>").addClass("btn_block_07").append(
										$("<a/>").attr("href", "/spage/storeinformation/news_list.php").append('<img src="http://shibuya.parco.jp/s/storeinfomation/blog_parts/images/btn_more.png" width="31" height="9" />')
									).appendTo(base);
									// $("<div/>").addClass("btn_block_07").addClass("more_post").append(
									// 	$("<a/>").attr("href", "javascript:void(0);").text("もっと見る")
									// ).appendTo(base);
									break;
							}
						}

						first = last + 1;
						last = last + next_count;
						$(".more_post a", base).unbind("click");
						$(".more_post a", base).click(function() {
							$def.hide();
							$(".more_post", base).remove();
							$blogContents.html('');
							base.addClass('loading');
							writeContents(param, tmpl, base, container, more_post, 1, data.header.count, next_count, view_type, view_data);
						});
					}else{
						$(".more_post", base).remove();
					}
				}
			}
		});
	}

}) (jQuery);

$(function(){
	$(".blog_parts_block").each(function(){
		$(this).blog_parts({
			view_type: $(this).data("viewType"),
			view_store_name: $(this).data("viewStoreName"),
			view_shop_name: $(this).data("viewShopName"),
			view_social: $(this).data("viewSocial"),
			view_link_target: $(this).data("viewLinkTarget"),
			init_count: $(this).data("initCount"),
			more_post: $(this).data("morePost"),
			next_count: $(this).data("nextCount"),
			blog_type: $(this).data("blogType"),
			scode: $(this).data("storeCode"),
			sid: $(this).data("shopId"),
			cid: $(this).data("categoryId"),
			cid_or: $(this).data("categoryIdOr"),
			post_sdate: $(this).data("postSdate"),
			post_edate: $(this).data("postEdate"),
			ext_response_fields: $(this).data("extResponseFields")
		});
	});
});
