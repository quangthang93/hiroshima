/**
 * common.js
 *
 *  version --- 3.7
 *  updated --- 2012/10/12
 */


/* !stack ------------------------------------------------------------------- */
jQuery(document).ready(function($) {
});

/*  Loaded
------------------------------------------------------------*/
$(window).on('load', function() {
  // modalControl();
  $('.itemList li').tile(3);
});

/*  modalControl
------------------------------------------------------------------------------------------------------------------------*/
// var modalControl = function(){
//   $(".modal").colorbox({
//         inline: true,
//         opacity: 0.8,
//         returnFocus: false,
//         width: 890,
//         initialWidth: 200
//     });
// };

/* !selectMenu -------------------------------------------------------------------- */
var selectMenu = function(){
  $('.selectMenu').each(function() {
    $(this).bind('change', function() {
      var linkurl = $(this).val();
      if (linkurl) {
        location.href = linkurl;
      };
    });
  });
};

/* !isUA -------------------------------------------------------------------- */
var isUA = (function(){
  var ua = navigator.userAgent.toLowerCase();
  indexOfKey = function(key){ return (ua.indexOf(key) != -1)? true: false;}
  var o = {};
  o.ie      = function(){ return indexOfKey("msie"); }
  o.fx      = function(){ return indexOfKey("firefox"); }
  o.chrome  = function(){ return indexOfKey("chrome"); }
  o.opera   = function(){ return indexOfKey("opera"); }
  o.android = function(){ return indexOfKey("android"); }
  o.ipad    = function(){ return indexOfKey("ipad"); }
  o.ipod    = function(){ return indexOfKey("ipod"); }
  o.iphone  = function(){ return indexOfKey("iphone"); }
  return o;
})();

/* !pageScroll -------------------------------------------------------------- */
var pageScroll = function(){
  jQuery.easing.easeInOutCubic = function (x, t, b, c, d) {
    if ((t/=d/2) < 1) return c/2*t*t*t + b;
    return c/2*((t-=2)*t*t + 2) + b;
  };
  $('a.scroll, .scroll a, .pageTop a').each(function(){
    $(this).bind("click keypress",function(e){
      e.preventDefault();
      var target  = $(this).attr('href');
      var targetY = $(target).offset().top;
      var parent  = ( isUA.opera() )? (document.compatMode == 'BackCompat') ? 'body': 'html' : 'html,body';
      $(parent).animate(
        {scrollTop: targetY-68 },
        400,
        'easeInOutCubic'
      );
      return false;
    });
  });
}
