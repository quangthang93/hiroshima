/************************************************************
 * Confirm Modal Plugin V1.0
 * https://github.com/klutche/confirm_modal
 * Released under the MIT license
 ************************************************************/

$(function(){
	var modal = $(".movie-modal");//モーダルウインドウのクラス
	var opacity = 0.9;//モーダル背景の透明度
	var button = $(".close_movie-modal");//モーダル解除ボタンのクラス
	var limit = 0;//Cookieの有効期限(分)
	var cookie = $.cookie("modal");
	if(cookie !== "off"){
		var overlay = $("<div></div>");
		overlay.css({
			"position":"fixed",
			"z-index":100,
			"top":0,
			"left":0,
			"height":100+"%",
			"width":100+"%",
			"background":"#dd6b47",
			"opacity":opacity
		});
		$("body").append(overlay);
		$("body").addClass('test');
		modal.css("display", "block");
	}
	button.click(function(){
		$(overlay).fadeOut("slow");
		$(modal).hide();
		$("body").removeClass('test');
		var clearTime = new Date();
		clearTime.setTime(clearTime.getTime()+(limit*60*1000));
		$.cookie("modal", "off", {expires:clearTime, path:"/"});
	});
	$(".remove_cookie").click(function(){
		$.removeCookie("modal", {expires:-1, path:"/"});
		location.reload();
	});
});


$(function(){
  $(".close_movie-modal").on("click", function(){
    videoControl("stopVideo");
  });
  function videoControl(action){ 
    var $playerWindow = $('#popup-YouTube-player')[0].contentWindow;
    $playerWindow.postMessage('{"event":"command","func":"'+action+'","args":""}', '*');
  }
});


