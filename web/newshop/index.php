<!DOCTYPE html>
<html class="no-js" lang="ja">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<title>NEW＆RENEWAL｜広島PARCO</title>
<meta name="description" content="広島PARCOのNEW＆RENEWAL。SHOPのブランドコンセプトや取扱いアイテムなどをご紹介しています。2018年春、広島PARCOに注目のショップが続々オープン！">
<meta name="keyword" content="広島パルコ,PARCO,NEW,RENEWAL,リニューアル">

<meta property="og:site_name" content="広島PARCO">
<meta property="og:type" content="article">
<meta property="og:url" content="http://hiroshima.parco.jp/web/newshop/">
<meta property="og:title" content="NEW＆RENEWAL｜広島PARCO">
<meta property="og:description" content="広島PARCOのNEW＆RENEWAL。SHOPのブランドコンセプトや取扱いアイテムなどをご紹介しています。2018年春、広島PARCOに注目のショップが続々オープン！">
<meta property="og:image" content="http://hiroshima.parco.jp/web/newshop/images/ogp.png">

<meta name="viewport" content="width=1500">
<link rel="alternate" media="only screen and (max-width: 640px)" href="http://hiroshima.parco.jp/s/newshop/" />
<link rel="stylesheet" href="css/reset.css">
<!-- <link rel="stylesheet" href="css/colorbox.css"> -->
<link rel="stylesheet" href="css/remodal.css">
<link rel="stylesheet" href="css/remodal-default-theme.css">
<link rel="stylesheet" href="css/layout.css">


<script src="js/modernizr.js"></script>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="js/jquery.js"><\/script>')</script>
<script src="js/jquery.tile.js"></script>
<script src="js/remodal.min.js"></script>
<script src="js/smoothScroll.js"></script>
<script src="js/common.js"></script>


<script>
  var ua = navigator.userAgent;
  var redirectPass = '../../s/newshop/';

  if ((ua.indexOf('iPhone') > 0 && ua.indexOf('iPad') == -1) || ua.indexOf('iPod') > 0 || (ua.indexOf('Android') > 0 && ua.indexOf('Mobile') > 0)){
    location.href = redirectPass;
  }
</script>
</head>

<body id="top">
<?php include ('../../load_script.php'); ?>

<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/ja_JP/sdk.js#xfbml=1&version=v2.0";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<div class="parco-header">
  <div class="parco-header_in">
    <p class="parco-header_logo"><a href="/"><img src="images/parco_logo.png" alt="広島PARCO"></a></p>
    <ul class="social parco-header_social clearfix">
      <li class="social_item"><a href="http://www.facebook.com/hiroshimaparco" target="_blank"><img src="images/icon_facebook.png" alt="PARCO Facebook"></a></li>
      <li class="social_item"><a href="https://twitter.com/parco_hiroshima" target="_blank"><img src="images/icon_twitter.png" alt="PARCO Twitter"></a></li>
      <li class="social_item"><a href="https://page.line.me/hiroshimaparco" target="_blank"><img src="images/icon_line.png" alt="PARCO Line"></a></li>
      <li class="social_item"><a href="https://www.instagram.com/parco_hiroshima_official/" target="_blank"><img src="images/icon_insta.png" alt="PARCO Instagram"></a></li>
    </ul>
  </div>
</div>

<div class="contents">
  <h1 class="mainLogo"><img class="mainLogoImg" src="images/main.png" alt="全24ショップNEW&RENEWAL OPEN"></h1>

  <section class="contentsBlock contentsBlock-type01 js-tile">
    <h2 class="sec-ttl">第一弾</h2>
	  <div class="sec-in">
      <?php include ('inc/shop-list.php'); ?>
	  </div>
  </section>

  <p class="more_introduce">and more！ 続々NEW SHOPが登場！</p>
  <ul class="bnrs">
    <li class="bnr"><a href="http://hiroshima.parco.jp/web/parco_card/"><img src="images/img_bnr01.png" alt=""></a></li>
    <li class="bnr"><a href="http://hiroshima.parco.jp/page2/event/14605/"><img src="images/img_bnr02.jpg" alt=""></a></li>
  </ul>

  <div class="sns_list">
    <div class="sns_item facebook">
      <div class="fb-like" data-href="http://hiroshima.parco.jp/web/newshop/" data-layout="button_count" data-action="like" data-size="small" data-show-faces="false" data-share="false"></div>
    </div>
    <div class="sns_item">
      <a href="https://twitter.com/share" class="twitter-share-button">Tweet</a>
      <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
    </div>
    <div class="sns_item">
      <div class="g-plusone" data-size="medium" data-href="'+e+'"></div>
      <script type="text/javascript">
        window.___gcfg = {lang: 'ja'};
        (function() {
        var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
        po.src = 'https://apis.google.com/js/platform.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
        })();
      </script>
    </div>
  </div>

</div>

<div class="footer">
  <p class="pageTop"><a href="#top"><img src="images/pagetop.png" alt="page top"></a></p>
  <p class="footer_logo"><img src="images/logo_footer.png" alt="広島PARCO"></p>
  <p class="copyright">COPYRIGHT © PARCO CO.,LTD ALL RIGHTS RESERVED.</p>
</div>

</body>
</html>
