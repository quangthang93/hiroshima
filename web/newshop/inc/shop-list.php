<h2 class="floor-ttl mt00"></h2>
<ul class="itemList">
    <li style="height: 420px;">
      <a data-remodal-target="item1">
          <p class="listImg"><img src="images/item/img_8.jpg" width="310" height="310" alt="エクストララージ"></p>
          <div class="listTxt_box">
              <div class="date">
                  <p class="day">2.16</p>
                  <p class="week">FRI</p>
                  <p class="new">RENEWAL</p>
              </div>
              <div class="logo">
                  <p class="logo-img"><img src="images/logo/logo_8.png" alt="エクストララージ"></p>
              </div>
          </div>
      </a>
      <div class="modal-area">
          <div class="remodal modal__in" data-remodal-id="item1">
              <div class="clearfix">
                  <figure class="modal-img">
                      <img src="images/item/img_8.jpg" alt="エクストララージ" width="310" height="310">
                  </figure>
                  <div class="modal-txt">
                      <p class="modal-date">2.16 FRI RENEWAL!</p>
                      <h2 class="modal-shop"><a href="javascript:void(0)">本館&nbsp;6F&nbsp;エクストララージ</a><div class="modal_category">メンズ</div></h2>
                      <p>1991年にLAにて設立。メンズストリートウェアブランドのパイオニア。<br>
                        ブランドロゴの「OGゴリラ」は、アイコン的存在として世界中に認知されている。</p>
                      <p class="modal-plan">￥15,000(税込)以上お買い上げで、アウトドアミニチェアをプレゼント【2/16～】</p>
                      <button data-remodal-action="close" class="remodal-close"></button>
                      <!-- /.modal-txt -->
                  </div>
                  <!-- /.clearfix -->
              </div>
              <!-- /.modal__in -->
          </div>
      </div>
    </li>
    <li style="height: 420px;">
        <a data-remodal-target="item2">
            <p class="listImg"><img src="images/item/img_10.jpg" width="310" height="310" alt="エックスガール"></p>
            <div class="listTxt_box">
                <div class="date">
                    <p class="day">2.16</p>
                    <p class="week">FRI</p>
                    <p class="new">RENEWAL</p>
                </div>
                <div class="logo">
                    <p class="logo-img"><img src="images/logo/logo_10.png" alt="エックスガール"></p>
                </div>
            </div>
        </a>
        <div class="modal-area">
            <div class="remodal modal__in" data-remodal-id="item2">
                <div class="clearfix">
                    <figure class="modal-img">
                        <img src="images/item/img_10.jpg" alt="エックスガール" width="310" height="310">
                    </figure>
                    <div class="modal-txt">
                        <p class="modal-date">2.16 FRI RENEWAL!</p>
                        <h2 class="modal-shop"><a href="javascript:void(0)">本館&nbsp;8F&nbsp;エックスガール</a><div class="modal_category">レディス</div></h2>
                        <p>音楽・カルチャー・スポーツ等のストリートシーンから、様々な要素を取り込み、時代の変化に合わせて生み出されていく“REAL GIRL’S CLOTHING”をテーマに掲げ、リアルな女の子のストリートスタイルを提案し続けている。</p>
                        <p class="modal-plan">￥5,000(税抜)以上お買い上げで、ポーチセットを￥15,000(税抜)以上お買い上げで、アウトドアミニチェアをプレゼント【2/16～】</p>
                        <button data-remodal-action="close" class="remodal-close"></button>
                        <!-- /.modal-txt -->
                    </div>
                    <!-- /.clearfix -->
                </div>
                <!-- /.modal__in -->
            </div>
        </div>
    </li>
    <li style="height: 420px;">
        <a data-remodal-target="item3">
          <p class="ribbon"><img src="images/ribbon_first.png" alt="中四国初" width="67" height="90"></p>
            <p class="listImg"><img src="images/item/img_7.jpg" width="310" height="310" alt="トップオブザヒル"></p>
            <div class="listTxt_box">
                <div class="date">
                    <p class="day">3.16</p>
                    <p class="week">FRI</p>
                    <p class="new">NEW</p>
                </div>
                <div class="logo">
                    <p class="logo-img"><img src="images/logo/logo_7.png" alt="トップオブザヒル"></p>
                </div>
            </div>
        </a>
        <div class="modal-area">
            <div class="remodal modal__in" data-remodal-id="item3">
                <div class="clearfix">
                    <figure class="modal-img">
                        <p class="ribbon"><img src="images/ribbon_first.png" alt="中四国初" width="67" height="90"></p>
                        <img src="images/item/img_7.jpg" alt="トップオブザヒル" width="310" height="310">
                    </figure>
                    <div class="modal-txt">
                        <p class="modal-date">3.16 FRI NEW!</p>
                        <h2 class="modal-shop"><a href="javascript:void(0)">本館&nbsp;6F&nbsp;トップオブザヒル</a><div class="modal_category">メンズ・レディス</div></h2>
                        <p>メンズ・レディースの60年代～80年代のEURO・USA古着・オリジナルウェアが１ランク上のセレクトで並びます。<br>
                          着回しの効く定番アイテム、トレンドアイテム、珍しいアイテムなどバラエティに富んだ物が充実。<br>
                          インポートウェア、バッグ、帽子、靴、アクセサリーなども豊富で古着×新品のMIXスタイルも提案しています。<br>
                          プレゼントに最適なアクセサリーや時計などの小物やユニセックスアイテムも数多く取り扱っています。</p>
                        <p class="modal-plan" style="margin-top:12px;">3,000円以上お買い上げのお客様にオリジナルトートバッグプレゼント（先着400枚限定）</p>
                        <button data-remodal-action="close" class="remodal-close"></button>
                        <!-- /.modal-txt -->
                    </div>
                    <!-- /.clearfix -->
                </div>
                <!-- /.modal__in -->
            </div>
        </div>
    </li>
    <li style="height: 420px;">
        <a data-remodal-target="item5">
            <p class="listImg"><img src="images/item/img_14.jpg" width="310" height="310" alt="パルコファクトリー"></p>
            <div class="listTxt_box">
                <div class="date">
                    <p class="day">3.16</p>
                    <p class="week">FRI</p>
                    <p class="new">NEW</p>
                </div>
                <div class="logo">
                    <p class="logo-img"><img src="images/logo/logo_14.png" alt="パルコファクトリー"></p>
                </div>
            </div>
        </a>
        <div class="modal-area">
            <div class="remodal modal__in" data-remodal-id="item5">
                <div class="clearfix">
                    <figure class="modal-img">
                        <img src="images/item/img_14.jpg" alt="パルコファクトリー" width="310" height="310">
                    </figure>
                    <div class="modal-txt">
                        <p class="modal-date">3.16 FRI NEW!</p>
                        <h2 class="modal-shop"><a href="javascript:void(0)">本館&nbsp;6F&nbsp;パルコファクトリー</a><div class="modal_category"></div></h2>
                        <p>情報発信の新拠点誕生！<br>
                          カルチャー・ファッションからアート・デザインまで多彩なイベントを展開するスペース「PARCO FACTORY」がオープン！</p>
                        <button data-remodal-action="close" class="remodal-close"></button>
                        <!-- /.modal-txt -->
                    </div>
                    <!-- /.clearfix -->
                </div>
                <!-- /.modal__in -->
            </div>
        </div>
    </li>
    <li style="height: 420px;">
      <a data-remodal-target="item6">
          <p class="listImg"><img src="images/item/img_2.jpg" width="310" height="310" alt="マーキュリーデュオ"></p>
          <div class="listTxt_box">
              <div class="date">
                  <p class="day">3.16</p>
                  <p class="week">FRI</p>
                  <p class="new">RENEWAL</p>
              </div>
              <div class="logo">
                  <p class="logo-img"><img src="images/logo/logo_2.png" alt="マーキュリーデュオ"></p>
              </div>
          </div>
      </a>
      <div class="modal-area">
          <div class="remodal modal__in" data-remodal-id="item6">
              <div class="clearfix">
                  <figure class="modal-img">
                      <img src="images/item/img_2.jpg" alt="マーキュリーデュオ" width="310" height="310">
                  </figure>
                  <div class="modal-txt">
                      <p class="modal-date">3.16 FRI RENEWAL!</p>
                      <h2 class="modal-shop"><a href="javascript:void(0)">本館&nbsp;3F&nbsp;マーキュリーデュオ</a><div class="modal_category">レディス</div></h2>
                      <p>華やかに咲き誇る薔薇のような可憐で気品のある彼女<br>
                        どこにいても、なにをしてても、華やかで印象的<br>
                        気品と知性を兼ね備えた女性になれる色気のあるスタイルを提案</p>
                      <p class="modal-plan">カラー限定トップススカート販売<br>
                        限定ワンピース販売<br>
                        ノベルティフェア</p>
                      <button data-remodal-action="close" class="remodal-close"></button>
                      <!-- /.modal-txt -->
                  </div>
                  <!-- /.clearfix -->
              </div>
              <!-- /.modal__in -->
          </div>
      </div>
    </li>
    <li style="height: 420px;">
        <a data-remodal-target="item7">
          <p class="ribbon"><img src="images/ribbon_first.png" alt="中四国初" width="67" height="90"></p>
            <p class="listImg"><img src="images/item/img_3.jpg" width="310" height="310" alt="ロイヤルパーティー"></p>
            <div class="listTxt_box">
                <div class="date">
                    <p class="day">3.30</p>
                    <p class="week">FRI</p>
                    <p class="new">NEW</p>
                </div>
                <div class="logo">
                    <p class="logo-img"><img src="images/logo/logo_3.png" alt="ロイヤルパーティー"></p>
                </div>
            </div>
        </a>
        <div class="modal-area">
            <div class="remodal modal__in" data-remodal-id="item7">
                <div class="clearfix">
                    <figure class="modal-img">
                        <p class="ribbon"><img src="images/ribbon_first.png" alt="中四国初" width="67" height="90"></p>
                        <img src="images/item/img_3.jpg" alt="ロイヤルパーティー" width="310" height="310">
                    </figure>
                    <div class="modal-txt">
                        <p class="modal-date">3.30 FRI NEW!</p>
                        <h2 class="modal-shop"><a href="javascript:void(0)">本館&nbsp;3F&nbsp;ロイヤルパーティー</a><div class="modal_category">レディス</div></h2>
                        <p>シンプルでグラマラス、ベーシックでグロッシー あらゆるシーンにおいて いつもの服をドラマティックに演出する ワンランク上のSEXYをリアルに表現</p>
                        <p class="modal-plan">税込15,000円以上お買上げでオリジナルミニウォレットプレゼント【3/30～】</p>
                        <button data-remodal-action="close" class="remodal-close"></button>
                        <!-- /.modal-txt -->
                    </div>
                    <!-- /.clearfix -->
                </div>
                <!-- /.modal__in -->
            </div>
        </div>
    </li>
    <li style="height: 420px;">
        <a data-remodal-target="item8">
            <p class="listImg"><img src="images/item/img_12.jpg" width="310" height="310" alt="スマホ修理工房"></p>
            <div class="listTxt_box">
                <div class="date">
                    <p class="day">2.28</p>
                    <p class="week">WED</p>
                    <p class="new">RENEWAL</p>
                </div>
                <div class="logo">
                    <p class="logo-img"><img src="images/logo/logo_12.png" alt="スマホ修理工房"></p>
                </div>
            </div>
        </a>
        <div class="modal-area">
            <div class="remodal modal__in" data-remodal-id="item8">
                <div class="clearfix">
                    <figure class="modal-img">
                        <img src="images/item/img_12.jpg" alt="スマホ修理工房" width="310" height="310">
                    </figure>
                    <div class="modal-txt">
                        <p class="modal-date">2.28 WED RENEWAL!</p>
                        <h2 class="modal-shop"><a href="javascript:void(0)">新館&nbsp;3F&nbsp;スマホ修理工房</a><div class="modal_category">スマートフォン修理</div></h2>
                        <p>スマートフォン・タブレットでお困りの事は何でもご相談下さい。ガラス割れ、液晶不良、バッテリー交換、水没修理など即日対応致します。</p>
                        <p class="modal-plan">スマホ修理された方に強化ガラス（2,000円相当）プレゼント【2/28～3/11】※機種に限定があります。</p>
                        <button data-remodal-action="close" class="remodal-close"></button>
                        <!-- /.modal-txt -->
                    </div>
                    <!-- /.clearfix -->
                </div>
                <!-- /.modal__in -->
            </div>
        </div>
    </li>
    <li style="height: 420px;">
      <a data-remodal-target="item9">
          <p class="listImg"><img src="images/item/img_1.jpg" width="310" height="310" alt="オズモーシス"></p>
          <div class="listTxt_box">
              <div class="date">
                  <p class="day">3.16</p>
                  <p class="week">FRI</p>
                  <p class="new">RENEWAL</p>
              </div>
              <div class="logo">
                  <p class="logo-img"><img src="images/logo/logo_1.png" alt="オズモーシス"></p>
              </div>
          </div>
      </a>
      <div class="modal-area">
          <div class="remodal modal__in" data-remodal-id="item9">
              <div class="clearfix">
                  <figure class="modal-img">
                      <img src="images/item/img_1.jpg" alt="オズモーシス" width="310" height="310">
                  </figure>
                  <div class="modal-txt">
                      <p class="modal-date">3.16 FRI RENEWAL!</p>
                      <h2 class="modal-shop"><a href="javascript:void(0)">本館&nbsp;2F&nbsp;オズモーシス</a><div class="modal_category">レディス</div></h2>
                      <p>時代に左右されないタイムレスなデザインベースにトレンド性を盛り込み常に新鮮なスタイルを提案<br>
                        タイムレスなデザインベースにトレンド感をプラス<br>
                        新鮮なスタイルを楽しめるデイリーカジュアル</p>
                      <p class="modal-plan">PARCOカードご利用または新規ご入会で10％OFF<br>【3/16～25】</p>
                      <button data-remodal-action="close" class="remodal-close"></button>
                      <!-- /.modal-txt -->
                  </div>
                  <!-- /.clearfix -->
              </div>
              <!-- /.modal__in -->
          </div>
      </div>
    </li>
    <li style="height: 420px;">
        <a data-remodal-target="item11">
            <p class="listImg"><img src="images/item/img_6.jpg" width="310" height="310" alt="ユニケース"></p>
            <div class="listTxt_box">
                <div class="date">
                    <p class="day">3.7</p>
                    <p class="week">WED</p>
                    <p class="new">RENEWAL</p>
                </div>
                <div class="logo">
                    <p class="logo-img"><img src="images/logo/logo_6.png" alt="ユニケース"></p>
                </div>
            </div>
        </a>
        <div class="modal-area">
            <div class="remodal modal__in" data-remodal-id="item11">
                <div class="clearfix">
                    <figure class="modal-img">
                        <img src="images/item/img_6.jpg" alt="ユニケース" width="310" height="310">
                    </figure>
                    <div class="modal-txt">
                        <p class="modal-date">3.7 WED RENEWAL!</p>
                        <h2 class="modal-shop"><a href="javascript:void(0)">本館&nbsp;6F&nbsp;ユニケース</a><div class="modal_category">スマートフォンアクセサリー</div></h2>
                        <p>ユニークで厳選されたアイテムを豊富に取り揃え、「面白い」「オリジナル」「可愛い」「トレンド」「便利」をコンセプトとした人気のアイテムを発信し続けるスマートフォンアクセサリー専門店です。</p>
                        <p class="modal-plan">一部商品最大60％OFF【～3/31】</p>
                        <button data-remodal-action="close" class="remodal-close"></button>
                        <!-- /.modal-txt -->
                    </div>
                    <!-- /.clearfix -->
                </div>
                <!-- /.modal__in -->
            </div>
        </div>
    </li>
    <li style="height: 420px;">
        <a data-remodal-target="item12">
            <p class="listImg"><img src="images/item/img_23.jpg" width="310" height="310" alt="エドマッチョ"></p>
            <div class="listTxt_box">
                <div class="date">
                    <p class="day">3.3</p>
                    <p class="week">SAT</p>
                    <p class="new">RENEWAL</p>
                </div>
                <div class="logo">
                    <p class="logo-img"><img src="images/logo/logo_23.png" alt="エドマッチョ"></p>
                </div>
            </div>
        </a>
        <div class="modal-area">
            <div class="remodal modal__in" data-remodal-id="item12">
                <div class="clearfix">
                    <figure class="modal-img">
                        <img src="images/item/img_23.jpg" alt="エドマッチョ" width="310" height="310">
                    </figure>
                    <div class="modal-txt">
                        <p class="modal-date">3.3 SAT RENEWAL!</p>
                        <h2 class="modal-shop"><a href="javascript:void(0)">本館&nbsp;4F&nbsp;エドマッチョ</a><div class="modal_category">アート作品・アクセサリー</div></h2>
                        <p>ARTで世界中の人々をHAPPYに!<br>
                          あなたの心を豊かにする世界一かわいいエドマッチョをお届け♪<br>
                          エドマッチョはARTを通して世界中の人々をハッピーにするお店です。九州で活躍するアーティストのできたてほっかほかの作品を販売しています。一緒に盛り上げてくれる広島のアーティストも大募集中です!
                        </p>
                        <button data-remodal-action="close" class="remodal-close"></button>
                        <!-- /.modal-txt -->
                    </div>
                    <!-- /.clearfix -->
                </div>
                <!-- /.modal__in -->
            </div>
        </div>
    </li>
    <li style="height: 420px;">
        <a data-remodal-target="item13">
            <p class="listImg"><img src="images/item/img_9.jpg" width="310" height="310" alt="ザッカイーキュー（コレット跡）"></p>
            <div class="listTxt_box">
                <div class="date">
                    <p class="day">3.9</p>
                    <p class="week">FRI</p>
                    <p class="new">RENEWAL</p>
                </div>
                <div class="logo">
                    <p class="logo-img"><img src="images/logo/logo_9.png" alt="ザッカイーキュー（コレット跡）"></p>
                </div>
            </div>
        </a>
        <div class="modal-area">
            <div class="remodal modal__in" data-remodal-id="item13">
                <div class="clearfix">
                    <figure class="modal-img">
                        <img src="images/item/img_9.jpg" alt="ザッカイーキュー（コレット跡）" width="310" height="310">
                    </figure>
                    <div class="modal-txt">
                        <p class="modal-date">3.9 FRI RENEWAL!</p>
                        <h2 class="modal-shop"><a href="javascript:void(0)">本館&nbsp;7F&nbsp;ザッカイーキュー（コレット跡）</a><div class="modal_category"></div></h2>
                        <p>「心の豊かさ」をテーマにワクワクドキドキ楽しい雑貨で埋め尽くします。<br>
                          プレゼントに、日用使いに、自分へのご褒美に、ワクワクしたいときに使える雑貨・お店・スタッフと楽しんでください♪</p>
                        <button data-remodal-action="close" class="remodal-close"></button>
                        <!-- /.modal-txt -->
                    </div>
                    <!-- /.clearfix -->
                </div>
                <!-- /.modal__in -->
            </div>
        </div>
    </li>
    <li style="height: 420px;">
        <a data-remodal-target="item14">
            <p class="listImg"><img src="images/item/img_11.jpg" width="310" height="310" alt="リーカ"></p>
            <div class="listTxt_box">
                <div class="date">
                    <p class="day">3.1</p>
                    <p class="week">THU</p>
                    <p class="new">RENEWAL</p>
                </div>
                <div class="logo">
                    <p class="logo-img"><img src="images/logo/logo_11.png" alt="リーカ"></p>
                </div>
            </div>
        </a>
        <div class="modal-area">
            <div class="remodal modal__in" data-remodal-id="item14">
                <div class="clearfix">
                    <figure class="modal-img">
                        <img src="images/item/img_11.jpg" alt="リーカ" width="310" height="310">
                    </figure>
                    <div class="modal-txt">
                        <p class="modal-date">3.1 THU RENEWAL!</p>
                        <h2 class="modal-shop"><a href="javascript:void(0)">新館&nbsp;3F&nbsp;リーカ</a><div class="modal_category">レディスシューズ</div></h2>
                        <p>“ 毎日服を変えるように、 毎日靴も変えてオシャレしたい。”<br>
                          トレンドを取り入れながら履き心地にもこだわった、どこか遊び心のある大人カジュアルな靴を取り揃えています。<br>
                          2018SSシーズンコンセプト『Curiosity in a chill-out moment』<br>
                          好奇心とリラックス<br>
                          リラックスアイテムがベース。でもそれだけじゃ物足りない女子のための、意表をついたアイテムたち。思わず心がときめくような、色使いやディティールをお楽しみください。</p>
                        <button data-remodal-action="close" class="remodal-close"></button>
                        <!-- /.modal-txt -->
                    </div>
                    <!-- /.clearfix -->
                </div>
                <!-- /.modal__in -->
            </div>
        </div>
    </li>
    <li style="height: 420px;">
        <a data-remodal-target="item15">
            <p class="listImg"><img src="images/item/img_4.jpg" width="310" height="310" alt="G＆H"></p>
            <div class="listTxt_box">
                <div class="date">
                    <p class="day">3.16</p>
                    <p class="week">FRI</p>
                    <p class="new">LIMITED</p>
                </div>
                <div class="logo">
                    <p class="logo-img"><img src="images/logo/logo_4.png" alt="G＆H"></p>
                </div>
            </div>
        </a>
        <div class="modal-area">
            <div class="remodal modal__in" data-remodal-id="item15">
                <div class="clearfix">
                    <figure class="modal-img">
                        <img src="images/item/img_4.jpg" alt="G＆H" width="310" height="310">
                    </figure>
                    <div class="modal-txt">
                        <p class="modal-date">3.16 FRI LIMITED!</p>
                        <h2 class="modal-shop"><a href="javascript:void(0)">本館&nbsp;4F&nbsp;G＆H</a><div class="modal_category">アクセサリー</div></h2>
                        <p>2016年から日本で本格的に展開をはじめたアクセサリーブランド G&amp;H ( ジーアンドエイチ ) は、東南アジア主要都市に点在する工場(こうば)を拠点とするアトリエ G-JAK による繊細でありながらも芯の強い世界観を豊かに表現したミスレニアスコレクションです。</p>
                        <button data-remodal-action="close" class="remodal-close"></button>
                        <!-- /.modal-txt -->
                    </div>
                    <!-- /.clearfix -->
                </div>
                <!-- /.modal__in -->
            </div>
        </div>
    </li>
    <li style="height: 420px;">
        <a data-remodal-target="item16">
            <p class="listImg"><img src="images/item/img_13.jpg" width="310" height="310" alt="三愛水着楽園"></p>
            <div class="listTxt_box">
                <div class="date">
                    <p class="day">2.7</p>
                    <p class="week">WED</p>
                    <p class="new">RENEWAL</p>
                </div>
                <div class="logo">
                    <p class="logo-img"><img src="images/logo/logo_13.png" alt="三愛水着楽園"></p>
                </div>
            </div>
        </a>
        <div class="modal-area">
            <div class="remodal modal__in" data-remodal-id="item16">
                <div class="clearfix">
                    <figure class="modal-img">
                        <img src="images/item/img_13.jpg" alt="三愛水着楽園" width="310" height="310">
                    </figure>
                    <div class="modal-txt">
                        <p class="modal-date">2.7 WED RENEWAL!</p>
                        <h2 class="modal-shop"><a href="javascript:void(0)">新館&nbsp;6F&nbsp;三愛水着楽園</a><div class="modal_category">水着</div></h2>
                        <p>2018年最新のファッション水着をはじめ、旅行にも最適なリゾートウェアやグッズ、インスタ映えする小物等、ビーチを楽しむアイテムを豊富に取り揃えています。今年のトレンドは「リバーシブルブラ」と「ノンワイヤーブラ」。ぜひお店で着心地をお試しください。</p>
                        <button data-remodal-action="close" class="remodal-close"></button>
                        <!-- /.modal-txt -->
                    </div>
                    <!-- /.clearfix -->
                </div>
                <!-- /.modal__in -->
            </div>
        </div>
    </li>
</ul>
<h2 class="sec-ttl">第二弾</h2>
<ul class="itemList">
    <li style="height: 420px;">
        <a data-remodal-target="item17">
          <p class="ribbon"><img src="images/ribbon_first.png" alt="中四国初" width="67" height="90"></p>
            <p class="listImg"><img src="images/item/img_15.jpg" width="310" height="310" alt="ベーシックアンドアクセント"></p>
            <div class="listTxt_box">
                <div class="date">
                    <p class="day">4月</p>
                    <p class="week">下旬</p>
                    <p class="new">NEW</p>
                </div>
                <div class="logo">
                    <p class="logo-img"><img src="images/logo/logo_15.png" alt="ベーシックアンドアクセント"></p>
                </div>
            </div>
        </a>
        <div class="modal-area">
            <div class="remodal modal__in" data-remodal-id="item17">
                <div class="clearfix">
                    <figure class="modal-img">
                        <p class="ribbon"><img src="images/ribbon_first.png" alt="中四国初" width="67" height="90"></p>
                        <img src="images/item/img_15.jpg" alt="ベーシックアンドアクセント" width="310" height="310">
                    </figure>
                    <div class="modal-txt">
                        <p class="modal-date">4月下旬 NEW!</p>
                        <h2 class="modal-shop"><a href="javascript:void(0)">新館&nbsp;2F&nbsp;ベーシックアンドアクセント</a><div class="modal_category">生活雑貨</div></h2>
                        <p>クオリティの高いベーシックなものを愛用し、ユーモアやニュアンスのあるものを日常生活のスパイスに取り入れる。そんな媚びない大人のかっこよさの中に、繊細さを持つ人たちへ。心地よく、ほんの少し刺激的な、いとおしい暮らしを贈るブランド。</p>
                        <button data-remodal-action="close" class="remodal-close"></button>
                        <!-- /.modal-txt -->
                    </div>
                    <!-- /.clearfix -->
                </div>
                <!-- /.modal__in -->
            </div>
        </div>
    </li>
    <li style="height: 420px;">
        <a data-remodal-target="item18">
          <p class="ribbon"><img src="images/ribbon_first.png" alt="中四国初" width="67" height="90"></p>
            <p class="listImg"><img src="images/item/img_16.jpg" width="310" height="310" alt="ラヴィジュール"></p>
            <div class="listTxt_box">
                <div class="date">
                    <p class="day">4.20</p>
                    <p class="week">FRI</p>
                    <p class="new">NEW</p>
                </div>
                <div class="logo">
                    <p class="logo-img"><img src="images/logo/logo_16.png" alt="ラヴィジュール"></p>
                </div>
            </div>
        </a>
        <div class="modal-area">
            <div class="remodal modal__in" data-remodal-id="item18">
                <div class="clearfix">
                    <figure class="modal-img">
                        <p class="ribbon"><img src="images/ribbon_first.png" alt="中四国初" width="67" height="90"></p>
                        <img src="images/item/img_16.jpg" alt="ラヴィジュール" width="310" height="310">
                    </figure>
                    <div class="modal-txt">
                        <p class="modal-date">4.20 FRI NEW!</p>
                        <h2 class="modal-shop"><a href="javascript:void(0)">本館&nbsp;3F&nbsp;ラヴィジュール</a><div class="modal_category">下着</div></h2>
                        <p>「セクシーに生きる-Just be your self-」をコンセプトに、女性の新しいセクシーさを追求し、ワンランク上のアイテムを展開すランジェリーブランド。</p>
                        <button data-remodal-action="close" class="remodal-close"></button>
                        <!-- /.modal-txt -->
                    </div>
                    <!-- /.clearfix -->
                </div>
                <!-- /.modal__in -->
            </div>
        </div>
    </li>
    <li style="height: 420px;">
        <a data-remodal-target="item19">
            <p class="listImg"><img src="images/item/img_17.jpg" width="310" height="310" alt="ミッシュマッシュ"></p>
            <div class="listTxt_box">
                <div class="date">
                    <p class="day">4.20</p>
                    <p class="week">FRI</p>
                    <p class="new">RENEWAL</p>
                </div>
                <div class="logo">
                    <p class="logo-img"><img src="images/logo/logo_17.png" alt="ミッシュマッシュ"></p>
                </div>
            </div>
        </a>
        <div class="modal-area">
            <div class="remodal modal__in" data-remodal-id="item19">
                <div class="clearfix">
                    <figure class="modal-img">
                        <img src="images/item/img_17.jpg" alt="ミッシュマッシュ" width="310" height="310">
                    </figure>
                    <div class="modal-txt">
                        <p class="modal-date">4.20 FRI RENEWAL!</p>
                        <h2 class="modal-shop"><a href="javascript:void(0)">本館&nbsp;3F&nbsp;ミッシュマッシュ</a><div class="modal_category">レディス</div></h2>
                        <p>大人っぽくて可愛いレディスアイテムをたっぷりとｍｉｘ！<br>
                          女の子の大好きが詰まった、わくわく感あるＳＨＯＰをイメージ。<br>
                          大人可愛いエレガンスカジュアル　ライフスタイルをそれそれの価値観でエンジョイする思考へと変化している今日、ＭＩＳＣＨ　ＭＡＳＣＨとすごす特別な時間を配信していく。<br>
                          美人度120％！恋するＯＮ/ＯＦＦ服！</p>
                        <button data-remodal-action="close" class="remodal-close"></button>
                        <!-- /.modal-txt -->
                    </div>
                    <!-- /.clearfix -->
                </div>
                <!-- /.modal__in -->
            </div>
        </div>
    </li>
    <li style="height: 420px;">
        <a data-remodal-target="item20">
            <p class="listImg"><img src="images/item/img_18.jpg" width="310" height="310" alt="アンレリッシュ"></p>
            <div class="listTxt_box">
                <div class="date">
                    <p class="day">5.11</p>
                    <p class="week">FRI</p>
                    <p class="new">RENEWAL</p>
                </div>
                <div class="logo">
                    <p class="logo-img"><img src="images/logo/logo_18.png" alt="アンレリッシュ"></p>
                </div>
            </div>
        </a>
        <div class="modal-area">
            <div class="remodal modal__in" data-remodal-id="item20">
                <div class="clearfix">
                    <figure class="modal-img">
                        <img src="images/item/img_18.jpg" alt="アンレリッシュ" width="310" height="310">
                    </figure>
                    <div class="modal-txt">
                        <p class="modal-date">5.11 FRI RENEWAL!</p>
                        <h2 class="modal-shop"><a href="javascript:void(0)">本館&nbsp;3F&nbsp;アンレリッシュ</a><div class="modal_category">レディス</div></h2>
                        <p>”Enjoy life every day"<br>
                          「TREND」「BASIC」「CASUAL」をキーワードに”いま”を生きる女性に向けた大人のワードローブを提案します。いま買うべきトレンドも自分らしさを後押しする要素として楽しんでいる。スタイルある凛とした女性をイメージ。</p>
                        <p class="modal-plan">10,000円以上お買い上げでOPEN限定ノベルティ（ミラー）をプレゼント【5/11～】</p>
                        <button data-remodal-action="close" class="remodal-close"></button>
                        <!-- /.modal-txt -->
                    </div>
                    <!-- /.clearfix -->
                </div>
                <!-- /.modal__in -->
            </div>
        </div>
    </li>
</ul>

<h2 class="sec-ttl">第三弾</h2>
<ul class="itemList">
    <li style="height: 420px;">
        <a data-remodal-target="item21">
          <p class="ribbon"><img src="images/ribbon_first.png" alt="中四国初" width="67" height="90"></p>
            <p class="listImg"><img src="images/item/img_20.jpg" width="310" height="310" alt="エチュードハウス"></p>
            <div class="listTxt_box">
                <div class="date">
                    <p class="day">6.8</p>
                    <p class="week">FRI</p>
                    <p class="new">NEW</p>
                </div>
                <div class="logo">
                    <p class="logo-img"><img src="images/logo/logo_20.png" alt="エチュードハウス"></p>
                </div>
            </div>
        </a>
        <div class="modal-area">
            <div class="remodal modal__in" data-remodal-id="item21">
                <div class="clearfix">
                    <figure class="modal-img">
                        <p class="ribbon"><img src="images/ribbon_first.png" alt="中四国初" width="67" height="90"></p>
                        <img src="images/item/img_20.jpg" alt="エチュードハウス" width="310" height="310">
                    </figure>
                    <div class="modal-txt">
                        <p class="modal-date">6.8 FRI NEW!</p>
                        <h2 class="modal-shop"><a href="javascript:void(0)">本館&nbsp;3F&nbsp;エチュードハウス</a><div class="modal_category">化粧品</div></h2>
                        <p>メイクアップは決まりきった習慣ではなく、ワクワクするPLAY（遊び）として楽しむもの“Makeup Play!”をコンセプトに、スキンケアからメイクアップアイテムまでユニークでスイートな商品ラインナップをご提供いたします。<br>
                          エチュードハウスなら、あなただけの特別な「かわいい」が叶います。<br>
                          今日をもっとスイートに！<br>
                          Life is Sweet</p>
                        <button data-remodal-action="close" class="remodal-close"></button>
                        <!-- /.modal-txt -->
                    </div>
                    <!-- /.clearfix -->
                </div>
                <!-- /.modal__in -->
            </div>
        </div>
    </li>
    <li style="height: 420px;">
        <a data-remodal-target="item22">
            <p class="listImg"><img src="images/item/img_21.jpg" width="310" height="310" alt="アトレナ"></p>
            <div class="listTxt_box">
                <div class="date">
                    <p class="day">6.8</p>
                    <p class="week">FRI</p>
                    <p class="new">NEW</p>
                </div>
                <div class="logo">
                    <p class="logo-img"><img src="images/logo/logo_21.png" alt="アトレナ"></p>
                </div>
            </div>
        </a>
        <div class="modal-area">
            <div class="remodal modal__in" data-remodal-id="item22">
                <div class="clearfix">
                    <figure class="modal-img">
                        <img src="images/item/img_21.jpg" alt="アトレナ" width="310" height="310">
                    </figure>
                    <div class="modal-txt">
                        <p class="modal-date">6.8 FRI NEW!</p>
                        <h2 class="modal-shop"><a href="javascript:void(0)">新館&nbsp;3F&nbsp;アトレナ</a><div class="modal_category">帽子</div></h2>
                        <p>コンセプトは"新しい自分と出会える"トレンド+機能性+自分らしさを演出できる、カスタマイズ機能を盛り込んだ商品の展開。丁寧な接客と確かな商品知識、また永らくご愛用いただける堅実な品質の商品を提供いたします。</p>
                        <button data-remodal-action="close" class="remodal-close"></button>
                        <!-- /.modal-txt -->
                    </div>
                    <!-- /.clearfix -->
                </div>
                <!-- /.modal__in -->
            </div>
        </div>
    </li>
    <li style="height: 420px;">
        <a data-remodal-target="item23">
            <p class="listImg"><img src="images/item/img_22.jpg" width="310" height="310" alt="マリークヮント"></p>
            <div class="listTxt_box">
                <div class="date">
                    <p class="day">5.25</p>
                    <p class="week">FRI</p>
                    <p class="new">RENEWAL</p>
                </div>
                <div class="logo">
                    <p class="logo-img"><img src="images/logo/logo_22.png" alt="マリークヮント"></p>
                </div>
            </div>
        </a>
        <div class="modal-area">
            <div class="remodal modal__in" data-remodal-id="item23">
                <div class="clearfix">
                    <figure class="modal-img">
                        <img src="images/item/img_22.jpg" alt="マリークヮント" width="310" height="310">
                    </figure>
                    <div class="modal-txt">
                        <p class="modal-date">5.25 FRI RENEWAL!</p>
                        <h2 class="modal-shop"><a href="javascript:void(0)">本館&nbsp;3F&nbsp;マリークヮント</a><div class="modal_category">化粧品・雑貨</div></h2>
                        <p>年齢問わず自分を持っているお客様に、コスメ、ファッションの幅広いラインナップで自分らしくなるお手伝いをさせて頂きます。<br>
                          「自由に、自分らしく」。<br>
                          全ての商品は、「ルック」を完成させるパーツを考え、自由な組み合わせで女性の美しさを彩ります。</p>
                        <button data-remodal-action="close" class="remodal-close"></button>
                        <!-- /.modal-txt -->
                    </div>
                    <!-- /.clearfix -->
                </div>
                <!-- /.modal__in -->
            </div>
        </div>
    </li>
</ul>
