<?php
//ユーザーエージェント
$ua = $_SERVER['HTTP_USER_AGENT'];
if(isSmartPhone($ua)) {
  header('Location: http://fukuoka.parco.jp/s/newautumn/');
}
function isSmartPhone($ua) {
  if((strpos($ua, 'iPhone') !== false) || (strpos($ua, 'iPod') !== false) ||(strpos($ua, 'Android') !== false)) return true;
  return false;
}
?>
<?php include_once 'inc/object.php'; ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ja" lang="ja">
<head>
<link rel="alternate" media="only screen and (max-width: 640px)" href="http://fukuoka.parco.jp/s/newautumn/">
<?php include_once 'inc/meta-title-ogp.php'; ?>
<?php /* CSS */ ?>
<link rel="stylesheet" href="css/style.css">
<?php /* js */ ?>
<script src="js/jquery-1.8.3.min.js"></script>
<script src="js/masonary.min.js"></script>
<script src="js/common.js"></script>
</head>
<body>
<?php include_once 'inc/fb.php'; ?>
<header class="header">
   <h1 class="header-ttl"><img src="images/main.png" alt="NEW SHOP OPEN" width="616" height="208" /></h1>
</header>
<div class="band">
  <p>2016年秋、福岡PARCOに注目のショップが続々オープン</p>
</div>
<main class="main">

<div class="main-in">

<section id="photo-wrap">
<?php
/**
 * PHOTO-START
 */
?>
<section class="photo-block">
    <a href="javascript:void(0)" class="photo-link"><img class="visual" src="images/item/item01.jpg" width="580" height="580" alt="治一郎">
        <div class="photo-hover">
            <div class="fadeInLeft animated"><div class="fade-wrap">
                <p class="ribbon"><img src="images/icon/ribbon.png" alt="九州初登場" /></p>
                <h1 class="hover-ttl">9/16(金) OPEN!<br>本館B1F 治一郎<br>バウムクーヘン/ラスク</span></h1>
                <p class="hover-img"><img src="images/logo/logo01.png" alt="治一郎" width="60" height="122"></p>
                <p class="hover-text">しっとりみずみずしい美味しさで評判の「治一郎のバウムクーヘン」専門店がついに登場。 インターネットお取り寄せＮo.1に輝いたことのあるバウムクーヘンやラスクを 是非お試し下さい。</p>
            </div>
            </div>
        </div>
    </a>
</section>
<section class="photo-block">
    <a href="javascript:void(0)" class="photo-link"><img class="visual" src="images/item/item02.jpg" width="580" height="580" alt="エービーシー・マート">
        <div class="photo-hover">
            <div class="fadeInLeft animated"><div class="fade-wrap">
                <h1 class="hover-ttl">9/16(金) OPEN!<br>本館7F エービーシー・マート<br>シューズ</span></h1>
                <p class="hover-img"><img src="images/logo/logo02.png" alt="エービーシー・マート"></p>
                <p class="hover-text">ライフスタイル&amp;トレンドの2つの視点でセレクトしたシューズを多数ご用意しております。<br>取扱ブランド：NIKE、adidas、New Balance、VANS等。</p>
            </div>
            </div>
        </div>
    </a>
</section>
<section class="photo-block">
    <a href="javascript:void(0)" class="photo-link"><img class="visual" src="images/item/item03.jpg" width="580" height="580" alt="チュチュ アンナ／ティー エー バイ チュチュ アンナ">
        <div class="photo-hover">
            <div class="fadeInLeft animated"><div class="fade-wrap">
                <p class="ribbon"><img src="images/icon/ribbon_debut.png" alt="デビューブランド" width="280" height="44" /></p>
                <h1 class="hover-ttl">9/16(金) OPEN!<br>本館6F チュチュ アンナ／ティー エー バイ チュチュ アンナ<br>ソックス＆インナー</span></h1>
                <p class="hover-img"><img src="images/logo/logo03.png" alt="チュチュ アンナ／ティー エー バイ チュチュ アンナ"></p>
                <p class="hover-text">Enjoy your life!<br>毎日をおしゃれに楽しく装うためのファッション感度の高いソックス・インナー・ウェアをシーズンごとにタイムリーに提案します。</p>
            </div>
            </div>
        </div>
    </a>
</section>
<section class="photo-block">
    <a href="javascript:void(0)" class="photo-link"><img class="visual" src="images/item/item04.jpg" width="580" height="580" alt="オーガニック マルシェ ナチュラル ナチュラル">
        <div class="photo-hover">
            <div class="fadeInLeft animated"><div class="fade-wrap">
                <p class="ribbon"><img src="images/icon/ribbon_debut.png" alt="デビューブランド" width="280" height="44" /></p>
                <h1 class="hover-ttl">9/16(金) OPEN!<br>新館B2F オーガニック マルシェ ナチュラル ナチュラル<br>食品・雑貨・カフェ</span></h1>
                <p class="hover-img"><img src="images/logo/logo04.png" alt="オーガニック マルシェ ナチュラル ナチュラル"></p>
                <p class="hover-text">自然食や九州産直食材を販売。健康な食生活と日本の農業＆食べ物を大切にする事を目的としています。イートインコーナーやキッズルームも併設しているのでゆっくりお買物をお楽しみ頂けます。</p>
            </div>
            </div>
        </div>
    </a>
</section>
<section class="photo-block">
    <a href="javascript:void(0)" class="photo-link"><img class="visual" src="images/item/item05.jpg" width="580" height="580" alt="ポジャギ">
        <div class="photo-hover"><span class="new-icon"><img src="images/icon/icon_all_onlyone.png" alt="" width="77" height="98" /></span>
            <div class="fadeInLeft animated"><div class="fade-wrap">
                <h1 class="hover-ttl">7/28(木) OPEN!<br>新館B2F ポジャギ<br>韓国料理</span></h1>
                <p class="hover-img"><img src="images/logo/logo05.png" alt="ポジャギ"></p>
                <p class="hover-text">本格韓国料理やマッコリを、定食やバルスタイルでお楽しみいただける韓国料理専門店。カウンター席もあるのでお一人様も気軽にお立ち寄りいただけます。</p>
            </div>
            </div>
        </div>
    </a>
</section>
<section class="photo-block">
    <a href="javascript:void(0)" class="photo-link"><img class="visual" src="images/item/item20.jpg" width="580" height="580" alt="うみの食堂">
        <div class="photo-hover">
            <div class="fadeInLeft animated"><div class="fade-wrap">
                <p class="ribbon"><img src="images/icon/ribbon_debut.png" alt="デビューブランド" width="280" height="44" /></p>
                <h1 class="hover-ttl">9/2(金) OPEN!<br>本館B1F うみの食堂<br>定食</span></h1>
                <p class="hover-img"><img src="images/logo/logo20.png" alt="うみの食堂"></p>
                <p class="hover-text">今時のようで昔懐かしい心も身体もホッとする食堂。天然魚を使ったぷりぷりのお刺身と海鮮丼、旨みがしみた魚の煮つけ、脂ジュワ～の焼魚、旨い魚と食べたいものを好きなように組み合わせてお召し上がり下さい。</p>
            </div>
            </div>
        </div>
    </a>
</section>
<section class="photo-block">
    <a href="javascript:void(0)" class="photo-link"><img class="visual" src="images/item/item23.jpg" width="580" height="580" alt="魚助食堂">
        <div class="photo-hover">
            <div class="fadeInLeft animated"><div class="fade-wrap">
                <p class="ribbon"><img src="images/icon/ribbon_debut.png" alt="デビューブランド" width="280" height="44" /></p>
                <h1 class="hover-ttl">9/16(金) OPEN!<br>新館 B2F 魚助食堂<br>海鮮定食</span></h1>
                <p class="hover-img"><img src="images/logo/logo23.png" alt="魚助食堂"></p>
                <p class="hover-text">長浜鮮魚卸会社直営のボリュームたっぷりのお魚定食。毎朝水揚げされた新鮮鮮魚を自社で買付け豪快に定食で提供。最高の鮮度とコストパフォーマンスのいい魚助食堂。旬の鮮魚を毎日いい状態で提供致します。</p>
            </div>
            </div>
        </div>
    </a>
</section>
<section class="photo-block">
    <a href="javascript:void(0)" class="photo-link"><img class="visual" src="images/item/item06.jpg" width="580" height="580" alt="博多 坦々麺 とり田">
        <div class="photo-hover">
            <div class="fadeInLeft animated"><div class="fade-wrap">
                <p class="ribbon"><img src="images/icon/ribbon_tenjin.png" alt="天神エリア初" width="280" height="44" /></p>
                <h1 class="hover-ttl">11月中旬 OPEN!<br>本館B1F 博多 坦々麺 とり田<br>坦々麺</span></h1>
                <p class="hover-img"><img src="images/logo/logo06.png" alt="博多 坦々麺 とり田"></p>
                <p class="hover-text">博多の郷土料理「水炊き」の専門店とり田が、こだわりのスープで作った至極の坦々麺専門店。</p>
            </div>
            </div>
        </div>
    </a>
</section>
<section class="photo-block">
    <a href="javascript:void(0)" class="photo-link"><img class="visual" src="images/item/item07.jpg" width="580" height="580" alt="メルシーボーク―、">
        <div class="photo-hover"><span class="new-icon"><img src="images/icon/icon_kyushu_onlyone.png" alt="" width="77" height="98" /></span>
            <div class="fadeInLeft animated"><div class="fade-wrap">
                <h1 class="hover-ttl">8/26(金) OPEN!<br>新館3F メルシーボーク―、<br>メンズ・レディス</span></h1>
                <p class="hover-img"><img src="images/logo/logo07.png" alt="メルシーボーク―、"></p>
                <p class="hover-text">清く・楽しく・美しく。メルシーボーク―。きちんとしてるけど、ちょっと笑える。主張はあるけど、気どっていない。そんな庶民的で遊び感のある、ちょっときれいな服を作りました。</p>
            </div>
            </div>
        </div>
    </a>
</section>
<section class="photo-block">
    <a href="javascript:void(0)" class="photo-link"><img class="visual" src="images/item/item08.jpg" width="580" height="580" alt="ライズフェロー プログレス">
        <div class="photo-hover">
            <div class="fadeInLeft animated"><div class="fade-wrap">
                <h1 class="hover-ttl">9/16(金) OPEN!<br>新館3F ライズフェロー プログレス<br>メンズ・レディス</span></h1>
                <p class="hover-img"><img src="images/logo/logo08.png" alt="ライズフェロー プログレス"></p>
                <p class="hover-text">“デキる男と女”のセレクトショップ。カジュアルをベースに、ヨーロッパ等の海外ブランドをはじめ、国内ブランドもセレクトしあなたの魅力をさらに引き出します。</p>
            </div>
            </div>
        </div>
    </a>
</section>
<section class="photo-block">
    <a href="javascript:void(0)" class="photo-link"><img class="visual" src="images/item/item09.jpg" width="580" height="580" alt="オジャガデザイン">
        <div class="photo-hover"><span class="new-icon"><img src="images/icon/icon_kyushu_onlyone.png" alt="" width="77" height="98" /></span>
            <div class="fadeInLeft animated"><div class="fade-wrap">
                <h1 class="hover-ttl">8/31(水) OPEN!<br>本館5F オジャガデザイン<br>革小物</span></h1>
                <p class="hover-img"><img src="images/logo/logo09.png" alt="オジャガデザイン"></p>
                <p class="hover-text">オールハンドメイド、MADE IN JAPANにこだわり続けるレザーブランド。東京都立川市にある工房で、革の染色、裁断、手縫いの縫製までを一貫して行っています。</p>
            </div>
            </div>
        </div>
    </a>
</section>
<section class="photo-block">
    <a href="javascript:void(0)" class="photo-link"><img class="visual" src="images/item/item10.jpg" width="580" height="580" alt="北斎グラフィック">
        <div class="photo-hover">
            <div class="fadeInLeft animated"><div class="fade-wrap">
                <p class="ribbon"><img src="images/icon/ribbon.png" alt="九州初登場" /></p>
                <h1 class="hover-ttl">9/3(土) OPEN!<br>本館5F 北斎グラフィック<br>和傘</span></h1>
                <p class="hover-img"><img src="images/logo/logo10.png" alt="北斎グラフィック"></p>
                <p class="hover-text">オリジナルのモダンな和柄を傘前面に施した和傘のブランド。普段使いの粋な傘から、カエルなど人気柄の透明傘、変身和装をくすぐる伝統的な工芸傘まで取り揃えています。</p>
            </div>
            </div>
        </div>
    </a>
</section>
<section class="photo-block">
    <a href="javascript:void(0)" class="photo-link"><img class="visual" src="images/item/item11.jpg" width="580" height="580" alt="アルファショップ">
        <div class="photo-hover"><span class="new-icon"><img src="images/icon/icon_fukuoka_onlyone.png" alt="" width="77" height="98" /></span>
            <div class="fadeInLeft animated"><div class="fade-wrap">
                <h1 class="hover-ttl">9/2(金) OPEN!<br>本館7F アルファショップ<br>メンズ・レディス</span></h1>
                <p class="hover-img"><img src="images/logo/logo11.png" alt="アルファショップ"></p>
                <p class="hover-text">「ＫＩＮＧ　ＯＦ　ＭＩＬＩＴＡＲＹ　ＡＬＰＨＡ　ＩＮＤＵＳＴＲＩＥＳ」<br>1959年から現在まで全世界で愛され続けてきたミリタリーウェアーの代表的ブランド。時代や流行に左右されることなく、本物を作り続けています。</p>
            </div>
            </div>
        </div>
    </a>
</section>
<section class="photo-block">
    <a href="javascript:void(0)" class="photo-link"><img class="visual" src="images/item/item12.jpg" width="580" height="580" alt="ビーラディエンス">
        <div class="photo-hover"><span class="new-icon"><img src="images/icon/icon_kyushu_onlyone.png" alt="" width="77" height="98" /></span>
            <div class="fadeInLeft animated"><div class="fade-wrap">
                <h1 class="hover-ttl">9/16(金) OPEN!<br>本館4F ビーラディエンス<br>レディス</span></h1>
                <p class="hover-img"><img src="images/logo/logo12.png" alt="ビーラディエンス"></p>
                <p class="hover-text">「REALCLOTHES」 ONもOFFもいつだって自分らしくおしゃれを楽しむ女性に向けた、時代とともに変化する“リアルクローズ”を提案します。</p>
            </div>
            </div>
        </div>
    </a>
</section>
<section class="photo-block">
    <a href="javascript:void(0)" class="photo-link"><img class="visual" src="images/item/item13.jpg" width="580" height="580" alt="ユニケース">
        <div class="photo-hover">
            <div class="fadeInLeft animated"><div class="fade-wrap">
                <h1 class="hover-ttl">9/13(火) OPEN!<br>本館3F ユニケース<br>スマートフォンアクセサリー専門店</span></h1>
                <p class="hover-img"><img src="images/logo/logo13.png" alt="ユニケース"></p>
                <p class="hover-text">「面白い・オリジナル・可愛い・トレンド・便利」をコンセプトとし、ユニークで厳選されたこだわりの関連アイテムやオリジナル商品を豊富に取り揃えるスマートフォンアクセサリー専門店です。</p>
            </div>
            </div>
        </div>
    </a>
</section>
<section class="photo-block">
    <a href="javascript:void(0)" class="photo-link"><img class="visual" src="images/item/item14.jpg" width="580" height="580" alt="ランダ">
        <div class="photo-hover">
            <div class="fadeInLeft animated"><div class="fade-wrap">
                <h1 class="hover-ttl">9/16(金) OPEN!<br>本館4F ランダ<br>レディスシューズ</span></h1>
                <p class="hover-img"><img src="images/logo/logo14.png" alt="ランダ"></p>
                <p class="hover-text">自身のムードやTPOに合わせて自由にスタイルを楽しむことが出来る女性に向けて、常に今を意識した遊び心あるデザインと高機能な履き心地でアーバナイズしたシューズを提案します。</p>
            </div>
            </div>
        </div>
    </a>
</section>
<section class="photo-block">
    <a href="javascript:void(0)" class="photo-link"><img class="visual" src="images/item/item22.jpg" width="580" height="580" alt="カナル4&#8451;">
        <div class="photo-hover">
            <div class="fadeInLeft animated"><div class="fade-wrap">
                <h1 class="hover-ttl">9月中旬 OPEN!<br>本館3F カナル4&#8451;<br>アクセサリー</span></h1>
                <p class="hover-img"><img src="images/logo/logo22.png" alt="カナル4&#8451;"></p>
                <p class="hover-text">すべての女性を可愛らしくするジュエリーを届けたいそんな想いから誕生したカナル4&#8451;。<br>「カナル」それは、運河のこと。今までにない、自由で、楽しい、そしてかわいいジュエリーを世界中からあつめてお届けします。</p>
            </div>
            </div>
        </div>
    </a>
</section>
<section class="photo-block">
    <a href="javascript:void(0)" class="photo-link"><img class="visual" src="images/item/item15.jpg" width="580" height="580" alt="ポルコロッソ">
        <div class="photo-hover">
            <div class="fadeInLeft animated"><div class="fade-wrap">
                <h1 class="hover-ttl">8/24(水) OPEN!<br>新館B1F ポルコロッソ<br>鞄・財布・雑貨</span></h1>
                <p class="hover-img"><img src="images/logo/logo15.png" alt="ポルコロッソ"></p>
                <p class="hover-text">「20年後、息子に譲るモノ」をコンセプトに、一点一点職人によるハンドメイドの革小物です。手のぬくもりが感じられる革の鞄や小物、雑貨を多く取り揃えております。</p>
            </div>
            </div>
        </div>
    </a>
</section>
<section class="photo-block">
    <a href="javascript:void(0)" class="photo-link"><img class="visual" src="images/item/item16.jpg" width="580" height="580" alt="ザッカ eQ">
        <div class="photo-hover">
            <div class="fadeInLeft animated"><div class="fade-wrap">
                <h1 class="hover-ttl">9/9(金) OPEN!<br>本館6F ザッカ eQ<br>雑貨</span></h1>
                <p class="hover-img"><img src="images/logo/logo16.png" alt="ザッカ eQ"></p>
                <p class="hover-text">日常の背活を楽しく彩るキャラクター雑貨を中心に、アクセサリー・ファッション雑貨も取り扱っております。</p>
            </div>
            </div>
        </div>
    </a>
</section>
<section class="photo-block">
    <a href="javascript:void(0)" class="photo-link"><img class="visual" src="images/item/item21.jpg" width="580" height="580" alt="エドマッチョ">
        <div class="photo-hover"><span class="new-icon"><img src="images/icon/icon_kyushu_onlyone.png" alt="" width="77" height="98" /></span>
            <div class="fadeInLeft animated"><div class="fade-wrap">
                <h1 class="hover-ttl">8/24(水) OPEN!<br>本館5F エドマッチョ<br>ARTケーキ、作家アクセサリー、雑貨</span></h1>
                <p class="hover-img"><img src="images/logo/logo21.png" alt="エドマッチョ"></p>
                <p class="hover-text">九州に縁のあるアーティストが作る新感覚のアートショップがリニューアル。待望の新人アーティスト8名がデビューします。<br>商品の受注販売やアーティストの来店イベントも！</p>
            </div>
            </div>
        </div>
    </a>
</section>
<section class="photo-block">
    <a href="javascript:void(0)" class="photo-link"><img class="visual" src="images/item/item17.jpg" width="580" height="580" alt="ヒマラヤスポーツ">
        <div class="photo-hover">
            <div class="fadeInLeft animated"><div class="fade-wrap">
                <h1 class="hover-ttl">9/9(金)〜10/23(日) 期間限定OPEN!<br>本館7F ヒマラヤスポーツ<br>スポーツ用品</span></h1>
                <p class="hover-img"><img src="images/logo/logo17.png" alt="ヒマラヤスポーツ"></p>
                <p class="hover-text">福岡県内に6店舗を展開中のスポーツショップです。売上げの一部を熊本地震災害義援金として日本赤十字社に寄付させて頂きます。</p>
            </div>
            </div>
        </div>
    </a>
</section>
<section class="photo-block">
    <a href="javascript:void(0)" class="photo-link"><img class="visual" src="images/item/item18.jpg" width="580" height="580" alt="ノーザンナイン">
        <div class="photo-hover">
            <div class="fadeInLeft animated"><div class="fade-wrap">
                <h1 class="hover-ttl">8/20(土) OPEN!<br>本館7F ノーザンナイン<br>メンズ</span></h1>
                <p class="hover-img"><img src="images/logo/logo18.png" alt="ノーザンナイン"></p>
                <p class="hover-text">お客様に夢と感動を与え、オシャレを楽しんで頂けるメンズセレクトショップ。</p>
            </div>
            </div>
        </div>
    </a>
</section>
<section class="photo-block">
    <a href="javascript:void(0)" class="photo-link"><img class="visual" src="images/item/item19.jpg" width="580" height="580" alt="ナムコキャラポップストア">
        <div class="photo-hover">
            <div class="fadeInLeft animated"><div class="fade-wrap">
                <h1 class="hover-ttl">7/16(土) OPEN!<br>本館8F ナムコキャラポップストア<br>イベント・アトラクション</span></h1>
                <p class="hover-img"><img src="images/logo/logo19.png" alt="ナムコキャラポップストア"></p>
                <p class="hover-text">人気アニメやゲームの世界観を楽しめる「キャラポップストア」がOPEN！ここでしか楽しめない限定グッズやミニゲームが盛りだくさん。クレーンゲームもご用意しており、どなたでも楽しめる空間です。</p>
            </div>
            </div>
        </div>
    </a>
</section>

<?php
/**
 * PHOTO-END
 */
?>
</section>
<!-- main-in --></div>
</main>
<footer class="footer">
  <div class="footer-link">
    <ul class="footer-link-in">
      <li><a href="/"><img src="images/parcotop.png" width="145" height="19" alt="PARCO TOP" /></a></li>
      <li><ul class="footer-sns-in">
      <li><fb:like href="<?php echo rawurldecode($pageUrl); ?>" send="false" layout="button_count" width="100" show_faces="false"></fb:like></li>
      <li><a href="http://twitter.com/share" class="twitter-share-button" data-url="<?php echo $pageUrl; ?>" data-text="<?php echo $pageTtl; ?>" data-hashtags="<?php echo $pageHash; ?>" data-count="horizontal" data-lang="ja">ツイート</a>
<script type="text/javascript" src="http://platform.twitter.com/widgets.js" charset="utf-8"></script></li>
    </ul></li>
      <li><a href="#"><img src="images/pagetop.png" width="145" height="20" alt="PAGE TOP" /></a></li>
    </ul>
  </div>
  <div class="footer-copyright">
  <p><img src="images/parco.png" height="53" width="175" alt="PARCO" /></p>
  <img src="images/copyright.png" width="313" height="11" alt="COPYRIGHT(C)PARCO.CO.LTD ALL RIGHTS RESERVED." /></div>
</footer>
</body>
</html>