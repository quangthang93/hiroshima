<?php /* meta */ ?>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="keywords" content="<?php echo htmlspecialchars($Keywords, ENT_QUOTES | ENT_HTML5, 'UTF-8'); ?>" />
<meta name="description" content="<?php echo htmlspecialchars($Description, ENT_QUOTES | ENT_HTML5, 'UTF-8'); ?>" />
<?php /* title */ ?>
<title><?php echo htmlspecialchars($pageTtl, ENT_QUOTES | ENT_HTML5, 'UTF-8'); ?></title>
<?php /* OGP */ ?>
<meta property="og:site_name" content="福岡パルコ">
<meta property="og:type" content="article">
<meta property="og:url" content="<?php echo htmlspecialchars($pageUrl, ENT_QUOTES | ENT_HTML5, 'UTF-8'); ?>">
<meta property="og:title" content="<?php echo htmlspecialchars($pageTtl, ENT_QUOTES | ENT_HTML5, 'UTF-8'); ?>">
<meta property="og:description" content="<?php echo htmlspecialchars($Description, ENT_QUOTES | ENT_HTML5, 'UTF-8'); ?>">
<meta property="og:image" content="<?php echo htmlspecialchars($pageUrl, ENT_QUOTES | ENT_HTML5, 'UTF-8'); ?>images/ogp.jpg">
<meta property="og:locale" content="ja_JP">