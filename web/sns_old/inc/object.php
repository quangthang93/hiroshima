<?php
$pageUrl = 'http://fukuoka.parco.jp/web/newspring/';
$pageTtl = '＼パルコの上、変わります／春のRENEWAL｜福岡パルコ';
$Keywords = '福岡パルコ,PARCO,NEWSHOP,RENEWAL SHOP';
$Description = '福岡PARCO、春のNEWSHOPやRENEWAL SHOPのブランドコンセプトや取扱いアイテムなどをご紹介しています。2017年春、福岡PARCOにこの春注目のショップが続々オープン！';
?>