<?php
//ユーザーエージェント
$ua = $_SERVER['HTTP_USER_AGENT'];
if(isSmartPhone($ua)) {
  header('Location: http://fukuoka.parco.jp/s/newspring/');
}
function isSmartPhone($ua) {
  if((strpos($ua, 'iPhone') !== false) || (strpos($ua, 'iPod') !== false) ||(strpos($ua, 'Android') !== false)) return true;
  return false;
}
?>
<?php include_once 'inc/object.php'; ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ja" lang="ja">
<head>
<link rel="alternate" media="only screen and (max-width: 640px)" href="http://fukuoka.parco.jp/s/newspring/">
<?php include_once 'inc/meta-title-ogp.php'; ?>
<?php /* CSS */ ?>
<link rel="stylesheet" href="css/style.css">
<?php /* js */ ?>
<script src="js/jquery-1.8.3.min.js"></script>
<script src="js/masonary.min.js"></script>
<script src="js/common.js"></script>
</head>
<body>
<?php include_once 'inc/fb.php'; ?>
<header class="header">
   <h1 class="header-ttl"><img src="images/txt_main.png" alt="NEW SHOP OPEN" width="616" height="208" /></h1>
</header>
<div class="band">
  <p>2017年春、福岡PARCOに注目のショップが続々オープン</p>
</div>
<main class="main">

<div class="main-in">

<section id="photo-wrap">
<?php
/**
 * PHOTO-START
 */
?>
<div class="conditions-box womens">
<img src="images/womens_ttl.png" alt="WOMEN'S">
<!-- /.conditions-box --></div>

<ul class="shopList">

  <li class="shopList_item first">
    <div class="list-img">
      <img src="images/logo06.png" width="100%" alt="EMODA">
    </div>
    <div class="list-icon">
      <ul>
        <li class="first">
          <img src="images/ico_taxfree.png" width="22" alt="TAXFREE">
        </li>
        <li class="last">
          <img src="images/ico_unionpay.png" width="22" alt="UNIONPAY">
        </li>
      </ul>
    </div>
    <div class="list-text">
      <div class="floor">Main Building B2F</div>
      <p class="shopName">EMODA</p>
      <p class="shopCategory">WOMEN'S</p>
    <!-- /.list-text --></div>
  </li>

  <li class="shopList_item first">
    <div class="list-img">
      <img src="images/logo07.png" width="100%" alt="MURUA">
    </div>
    <div class="list-icon">
      <ul>
        <li class="first">
          <img src="images/ico_taxfree.png" width="22" alt="TAXFREE">
        </li>
        <li class="last">
          <img src="images/ico_unionpay.png" width="22" alt="UNIONPAY">
        </li>
      </ul>
    </div>
    <div class="list-text">
      <div class="floor">Main Building B2F</div>
      <p class="shopName">MURUA</p>
      <p class="shopCategory">WOMEN'S</p>
    <!-- /.list-text --></div>
  </li>

  <li class="shopList_item first">
    <div class="list-img">
      <img src="images/logo08.png" width="100%" alt="YUMMY MART from PEACH JHON">
    </div>
    <div class="list-icon">
      <ul>
        <li class="first">
          <img src="images/ico_taxfree.png" width="22" alt="TAXFREE">
        </li>
        <li class="last">
          <img src="images/ico_unionpay.png" width="22" alt="UNIONPAY">
        </li>
      </ul>
    </div>
    <div class="list-text">
      <div class="floor">Main Building B2F</div>
      <p class="shopName">YUMMY MART from PEACH JHON</p>
      <p class="shopCategory">WOMEN'S</p>
    <!-- /.list-text --></div>
  </li>

  <li class="shopList_item first">
    <div class="list-img">
      <img src="images/logo09.png" width="100%" alt="MERCURYDUO">
    </div>
    <div class="list-icon">
      <ul>
        <li class="first">
          <img src="images/ico_taxfree.png" width="22" alt="TAXFREE">
        </li>
        <li class="last">
          <img src="images/ico_unionpay.png" width="22" alt="UNIONPAY">
        </li>
      </ul>
    </div>
    <div class="list-text">
      <div class="floor">Main Building B1F</div>
      <p class="shopName">MERCURYDUO</p>
      <p class="shopCategory">WOMEN'S</p>
    <!-- /.list-text --></div>
  </li>

  <li class="shopList_item first">
    <div class="list-img">
      <img src="images/logo10.png" width="100%" alt="Lily Brown boutique">
    </div>
    <div class="list-icon">
      <ul>
        <li class="first">
          <img src="images/ico_taxfree.png" width="22" alt="TAXFREE">
        </li>
        <li class="last">
          <img src="images/ico_unionpay.png" width="22" alt="UNIONPAY">
        </li>
      </ul>
    </div>
    <div class="list-text">
      <div class="floor">Main Building B1F</div>
      <p class="shopName">Lily Brown boutique</p>
      <p class="shopCategory">WOMEN'S</p>
    <!-- /.list-text --></div>
  </li>

  <li class="shopList_item first">
    <div class="list-img">
      <img src="images/logo11.png" width="100%" alt="COCO DEAL">
    </div>
    <div class="list-icon">
      <ul>
        <li class="first">
          <img src="images/ico_taxfree.png" width="22" alt="TAXFREE">
        </li>
        <li class="last">
          <img src="images/ico_unionpay.png" width="22" alt="UNIONPAY">
        </li>
      </ul>
    </div>
    <div class="list-text">
      <div class="floor">Main Building B1F</div>
      <p class="shopName">COCO DEAL</p>
      <p class="shopCategory">WOMEN'S</p>
    <!-- /.list-text --></div>
  </li>

  <li class="shopList_item first">
    <div class="list-img">
      <img src="images/logo12.png" width="100%" alt="JILL by JILLSTUART">
    </div>
    <div class="list-icon">
      <ul>
        <li class="first">
          <img src="images/ico_taxfree.png" width="22" alt="TAXFREE">
        </li>
        <li class="last">
          <img src="images/ico_unionpay.png" width="22" alt="UNIONPAY">
        </li>
      </ul>
    </div>
    <div class="list-text">
      <div class="floor">Main Building B1F</div>
      <p class="shopName">JILL by JILLSTUART</p>
      <p class="shopCategory">WOMEN'S</p>
    <!-- /.list-text --></div>
  </li>

  <li class="shopList_item first">
    <div class="list-img">
      <img src="images/logo13.png" width="100%" alt="Samantha Vega">
    </div>
    <div class="list-icon">
      <ul>
        <li class="first">
          <img src="images/ico_taxfree.png" width="22" alt="TAXFREE">
        </li>
        <li class="last">
          <img src="images/ico_unionpay.png" width="22" alt="UNIONPAY">
        </li>
      </ul>
    </div>
    <div class="list-text">
      <div class="floor">Main Building B1F</div>
      <p class="shopName">Samantha Vega</p>
      <p class="shopCategory">WOMEN'S</p>
    <!-- /.list-text --></div>
  </li>

  <li class="shopList_item first">
    <div class="list-img">
      <img src="images/logo14.png" width="100%" alt="GRACE CONTINENTAL">
    </div>
    <div class="list-icon">
      <ul>
        <li class="first">
          <img src="images/ico_taxfree.png" width="22" alt="TAXFREE">
        </li>
        <li class="last">
          <img src="images/ico_unionpay.png" width="22" alt="UNIONPAY">
        </li>
      </ul>
    </div>
    <div class="list-text">
      <div class="floor">Main Building 1F</div>
      <p class="shopName">GRACE CONTINENTAL</p>
      <p class="shopCategory">WOMEN'S</p>
    <!-- /.list-text --></div>
  </li>

  <li class="shopList_item first">
    <div class="list-img">
      <img src="images/logo15.png" width="100%" alt="Samantha Thavasa">
    </div>
    <div class="list-icon">
      <ul>
        <li class="first">
          <img src="images/ico_taxfree.png" width="22" alt="TAXFREE">
        </li>
        <li class="last">
          <img src="images/ico_unionpay.png" width="22" alt="UNIONPAY">
        </li>
      </ul>
    </div>
    <div class="list-text">
      <div class="floor">Main Building 1F</div>
      <p class="shopName">Samantha Thavasa</p>
      <p class="shopCategory">WOMEN'S</p>
    <!-- /.list-text --></div>
  </li>

  <li class="shopList_item first">
    <div class="list-img">
      <img src="images/logo16.png" width="100%" alt="Samantha Tiara">
    </div>
    <div class="list-icon">
      <ul>
        <li class="last">
          <img src="images/ico_unionpay.png" width="22" alt="UNIONPAY">
        </li>
      </ul>
    </div>
    <div class="list-text">
      <div class="floor">Main Building 1F</div>
      <p class="shopName">Samantha Tiara</p>
      <p class="shopCategory">WOMEN'S</p>
    <!-- /.list-text --></div>
  </li>

  <li class="shopList_item first">
    <div class="list-img">
      <img src="images/logo17.png" width="100%" alt="Another Edition">
    </div>
    <div class="list-icon">
      <ul>
        <li class="first">
          <img src="images/ico_taxfree.png" width="22" alt="TAXFREE">
        </li>
        <li class="last">
          <img src="images/ico_unionpay.png" width="22" alt="UNIONPAY">
        </li>
      </ul>
    </div>
    <div class="list-text">
      <div class="floor">Main Building 2F</div>
      <p class="shopName">Another Edition</p>
      <p class="shopCategory">WOMEN'S</p>
    <!-- /.list-text --></div>
  </li>

  <li class="shopList_item first">
    <div class="list-img">
      <img src="images/logo18.png" width="100%" alt="USAGI ONLINE STORE">
    </div>
    <div class="list-icon">
      <ul>
        <li class="first">
          <img src="images/ico_taxfree.png" width="22" alt="TAXFREE">
        </li>
        <li class="last">
          <img src="images/ico_unionpay.png" width="22" alt="UNIONPAY">
        </li>
      </ul>
    </div>
    <div class="list-text">
      <div class="floor">Main Building 2F</div>
      <p class="shopName">USAGI ONLINE STORE</p>
      <p class="shopCategory">WOMEN'S</p>
    <!-- /.list-text --></div>
  </li>

  <li class="shopList_item first">
    <div class="list-img">
      <img src="images/logo19.png" width="100%" alt="snidel">
    </div>
    <div class="list-icon">
      <ul>
        <li class="first">
          <img src="images/ico_taxfree.png" width="22" alt="TAXFREE">
        </li>
        <li class="last">
          <img src="images/ico_unionpay.png" width="22" alt="UNIONPAY">
        </li>
      </ul>
    </div>
    <div class="list-text">
      <div class="floor">Main Building 2F</div>
      <p class="shopName">snidel</p>
      <p class="shopCategory">WOMEN'S</p>
    <!-- /.list-text --></div>
  </li>

  <li class="shopList_item first">
    <div class="list-img">
      <img src="images/logo20.png" width="100%" alt="Diagram GRACE CONTINENTAL">
    </div>
    <div class="list-icon">
      <ul>
        <li class="first">
          <img src="images/ico_taxfree.png" width="22" alt="TAXFREE">
        </li>
        <li class="last">
          <img src="images/ico_unionpay.png" width="22" alt="UNIONPAY">
        </li>
      </ul>
    </div>
    <div class="list-text">
      <div class="floor">Main Building 2F</div>
      <p class="shopName">Diagram GRACE CONTINENTAL</p>
      <p class="shopCategory">WOMEN'S</p>
    <!-- /.list-text --></div>
  </li>

  <li class="shopList_item first">
    <div class="list-img">
      <img src="images/logo21.png" width="100%" alt="UNITED TOKYO">
    </div>
    <div class="list-icon">
      <ul>
        <li class="first">
          <img src="images/ico_taxfree.png" width="22" alt="TAXFREE">
        </li>
        <li class="last">
          <img src="images/ico_unionpay.png" width="22" alt="UNIONPAY">
        </li>
      </ul>
    </div>
    <div class="list-text">
      <div class="floor">Main Building 2F</div>
      <p class="shopName">UNITED TOKYO</p>
      <p class="shopCategory">WOMEN'S</p>
    <!-- /.list-text --></div>
  </li>

  <li class="shopList_item first">
    <div class="list-img">
      <img src="images/logo22.png" width="100%" alt="Ray BEAMS">
    </div>
    <div class="list-icon">
      <ul>
        <li class="first">
          <img src="images/ico_taxfree.png" width="22" alt="TAXFREE">
        </li>
        <li class="last">
          <img src="images/ico_unionpay.png" width="22" alt="UNIONPAY">
        </li>
      </ul>
    </div>
    <div class="list-text">
      <div class="floor">Main Building 2F</div>
      <p class="shopName">Ray BEAMS</p>
      <p class="shopCategory">WOMEN'S</p>
    <!-- /.list-text --></div>
  </li>

  <li class="shopList_item first">
    <div class="list-img">
      <img src="images/logo23.png" width="100%" alt="X-girl">
    </div>
    <div class="list-icon">
      <ul>
        <li class="first">
          <img src="images/ico_taxfree.png" width="22" alt="TAXFREE">
        </li>
        <li class="last">
          <img src="images/ico_unionpay.png" width="22" alt="UNIONPAY">
        </li>
      </ul>
    </div>
    <div class="list-text">
      <div class="floor">P'PARCO 1F</div>
      <p class="shopName">X-girl</p>
      <p class="shopCategory">WOMEN'S</p>
    <!-- /.list-text --></div>
  </li>

  <!--<li class="shopList_item first">
    <div class="list-img">
      <img src="images/logo24.png" width="100%" alt="STUSSY WOMEN">
    </div>
    <div class="list-icon">
      <ul>
        <li class="first">
          <img src="images/ico_taxfree.png" width="22" alt="TAXFREE">
        </li>
        <li class="last">
          <img src="images/ico_unionpay.png" width="22" alt="UNIONPAY">
        </li>
      </ul>
    </div>
    <div class="list-text">
      <div class="floor">P'PARCO 1F</div>
      <p class="shopName">STUSSY WOMEN</p>
      <p class="shopCategory">WOMEN'S</p>
    <!-- /.list-text --></div>
  </li>

  <li class="shopList_item first">
    <div class="list-img">
      <img src="images/logo25.png" width="100%" alt="Candy Stripper">
    </div>
    <div class="list-icon">
      <ul>
        <li class="last">
          <img src="images/ico_unionpay.png" width="22" alt="UNIONPAY">
        </li>
      </ul>
    </div>
    <div class="list-text">
      <div class="floor">P'PARCO 1F</div>
      <p class="shopName">Candy Stripper</p>
      <p class="shopCategory">WOMEN'S</p>
    <!-- /.list-text --></div>
  </li>

  <li class="shopList_item first">
    <div class="list-img">
      <img src="images/logo26.png" width="100%" alt="Swankiss">
    </div>
    <div class="list-icon">
      <ul>
        <li class="first">
          <img src="images/ico_taxfree.png" width="22" alt="TAXFREE">
        </li>
        <li class="last">
          <img src="images/ico_unionpay.png" width="22" alt="UNIONPAY">
        </li>
      </ul>
    </div>
    <div class="list-text">
      <div class="floor">P'PARCO 2F</div>
      <p class="shopName">Swankiss</p>
      <p class="shopCategory">WOMEN'S</p>
    <!-- /.list-text --></div>
  </li>

  <li class="shopList_item first">
    <div class="list-img">
      <img src="images/logo27.png" width="100%" alt="BABY,THE STARS SHINE BRIGHT">
    </div>
    <div class="list-icon">
      <ul>
        <li class="last">
          <img src="images/ico_unionpay.png" width="22" alt="UNIONPAY">
        </li>
      </ul>
    </div>
    <div class="list-text">
      <div class="floor">P'PARCO 2F</div>
      <p class="shopName">BABY,THE STARS SHINE BRIGHT</p>
      <p class="shopCategory">WOMEN'S</p>
    <!-- /.list-text --></div>
  </li>

</ul>
<?php /* ?> WOMEN'S END<?php */ ?>

<?php /* ?> MEN'S/WOMEN'S <?php */ ?>
<div class="conditions-box mens_womens">
<img src="images/menwomen_ttl.png" alt="MEN'S/WOMEN'S">
<!-- /.conditions-box --></div>

<ul class="shopList">

  <li class="shopList_item first">
    <div class="list-img">
      <img src="images/logo28.png" width="100%" alt="Onitsuka Tiger">
    </div>
    <div class="list-icon">
      <ul>
        <li class="first">
          <img src="images/ico_taxfree.png" width="22" alt="TAXFREE">
        </li>
        <li class="last">
          <img src="images/ico_unionpay.png" width="22" alt="UNIONPAY">
        </li>
      </ul>
    </div>
    <div class="list-text">
      <div class="floor">Main Building 2F</div>
      <p class="shopName">Onitsuka Tiger</p>
      <p class="shopCategory">MEN'S/WOMEN'S</p>
    <!-- /.list-text --></div>
  </li>

  <li class="shopList_item first">
    <div class="list-img">
      <img src="images/logo29.png" width="100%" alt="CABANE de ZUCCa">
    </div>
    <div class="list-icon">
      <ul>
        <li class="first">
          <img src="images/ico_taxfree.png" width="22" alt="TAXFREE">
        </li>
        <li class="last">
          <img src="images/ico_unionpay.png" width="22" alt="UNIONPAY">
        </li>
      </ul>
    </div>
    <div class="list-text">
      <div class="floor">Main Building 3F</div>
      <p class="shopName">CABANE de ZUCCa</p>
      <p class="shopCategory">MEN'S/WOMEN'S</p>
    <!-- /.list-text --></div>
  </li>

  <li class="shopList_item first">
    <div class="list-img">
      <img src="images/logo30.png" width="100%" alt="JOURNAL STANDARD">
    </div>
    <div class="list-icon">
      <ul>
        <li class="first">
          <img src="images/ico_taxfree.png" width="22" alt="TAXFREE">
        </li>
        <li class="last">
          <img src="images/ico_unionpay.png" width="22" alt="UNIONPAY">
        </li>
      </ul>
    </div>
    <div class="list-text">
      <div class="floor">Main Building 3F</div>
      <p class="shopName">JOURNAL STANDARD</p>
      <p class="shopCategory">MEN'S/WOMEN'S</p>
    <!-- /.list-text --></div>
  </li>

  <li class="shopList_item first">
    <div class="list-img">
      <img src="images/logo31.png" width="100%" alt="FRAPBOIS">
    </div>
    <div class="list-icon">
      <ul>
        <li class="first">
          <img src="images/ico_taxfree.png" width="22" alt="TAXFREE">
        </li>
        <li class="last">
          <img src="images/ico_unionpay.png" width="22" alt="UNIONPAY">
        </li>
      </ul>
    </div>
    <div class="list-text">
      <div class="floor">Main Building 5F</div>
      <p class="shopName">FRAPBOIS</p>
      <p class="shopCategory">MEN'S/WOMEN'S</p>
    <!-- /.list-text --></div>
  </li>

<?php /* ?>
  <li class="shopList_item first">
    <div class="list-img">
      <img src="images/logo32.png" width="100%" alt="EVANGELION STORE">
    </div>
    <div class="list-icon">
      <ul>
        <li class="first">
          <img src="images/ico_taxfree.png" width="22" alt="TAXFREE">
        </li>
        <li class="last">
          <img src="images/ico_unionpay.png" width="22" alt="UNIONPAY">
        </li>
      </ul>
    </div>
    <div class="list-text">
      <div class="floor">P'PARCO 2F</div>
      <p class="shopName">EVANGELION STORE</p>
      <p class="shopCategory">MEN'S/WOMEN'S</p>
    <!-- /.list-text --></div>
  </li>
<?php */ ?>
</ul>
<?php /* ?> MEN'S/WOMEN'S END<?php */ ?>


<?php /* ?> SHOES <?php */ ?>
<div class="conditions-box shoes">
<img src="images/shoes_ttl.png" alt="SHOES">
<!-- /.conditions-box --></div>

<ul class="shopList">

  <li class="shopList_item first">
    <div class="list-img">
      <img src="images/logo_1.png" width="100%" alt="スズカフェ">
    </div>
    <div class="list-icon">
      <ul>
        <li class="first">
          <img src="images/ico_taxfree.png" width="22" alt="TAXFREE">
        </li>
        <li class="last">
          <img src="images/ico_unionpay.png" width="22" alt="UNIONPAY">
        </li>
      </ul>
    </div>
    <div class="list-text">
      <div class="floor">Main Building 4F</div>
      <p class="shopName">スズカフェ</p>
      <p class="shopCategory">SHOES</p>
    <!-- /.list-text --></div>
  </li>

</ul>


<?php
/**
 * PHOTO-END
 */
?>
</section>
<!-- main-in --></div>
</main>
<footer class="footer">
  <div class="footer-link">
    <ul class="footer-link-in">
      <li><a href="/"><img src="images/parcotop.png" width="145" height="19" alt="PARCO TOP" /></a></li>
      <li><ul class="footer-sns-in">
      <li><fb:like href="<?php echo rawurldecode($pageUrl); ?>" send="false" layout="button_count" width="100" show_faces="false"></fb:like></li>
      <li><a href="http://twitter.com/share" class="twitter-share-button" data-url="<?php echo $pageUrl; ?>" data-text="<?php echo $pageTtl; ?>" data-hashtags="<?php echo $pageHash; ?>" data-count="horizontal" data-lang="ja">ツイート</a>
<script type="text/javascript" src="http://platform.twitter.com/widgets.js" charset="utf-8"></script></li>
    </ul></li>
      <li><a href="#"><img src="images/pagetop.png" width="145" height="20" alt="PAGE TOP" /></a></li>
    </ul>
  </div>
  <div class="footer-copyright">
  <p><img src="images/parco.png" height="53" width="175" alt="PARCO" /></p>
  <img src="images/copyright.png" width="313" height="11" alt="COPYRIGHT(C)PARCO.CO.LTD ALL RIGHTS RESERVED." /></div>
</footer>
</body>
</html>
