/*---------

!common.js

------------ */

/* !smoothscroll --------------------------------------------------- */
$(function() {
  $('a[href^=#]').click(function(){
    var speed = 500;
    var href= $(this).attr('href');
    var target = $(href == '#' || href == "" ? 'html' : href);
    var position = target.offset().top;
    $('html, body').animate({scrollTop:position}, speed, 'swing');
    return false;
  });

$('.photo-link').each(function() {
  $(this).hover(function(){
    $(this).find('.photo-hover').fadeIn(200);
  },function(){
    $(this).find('.photo-hover').fadeOut(200);
  });
});
/* !categoryclass --------------------------------------------------- */
  // $('.nav-in li a').each(function() {
  //   $(this).click(function(event){
  //     $this = $(this);
  //     $body = $('body');
  //     var selected = $(this).attr('class');
  //     event.preventDefault();
  //     $body.removeClass(function(index, className) {
  //     return (className.match(/\bcategory-\S+/g) || []).join(' ');
  //     });
  //     //alert(selected);
  //     $body.addClass('category-' + selected);
  //     $('#photo-wrap').masonry({
  //       itemSelector: '.photo-block',
  //       columnWidth: 580,
  //       gutter: 3,
  //       isFitWidth: true  //親要素の幅に合わせてカラム数を自動調整
  //     });
  //   });
  // });

});
jQuery(function($){
  $('#photo-wrap').masonry({
    itemSelector: '.photo-block',
    columnWidth: 580,
    gutter: 3,
    isFitWidth: true  //親要素の幅に合わせてカラム数を自動調整
  });
});



