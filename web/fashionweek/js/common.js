/*  Stack
------------------------------------------------------------*/

$(function() {
  globalNavi();
  logoHover();
});

$(window).load(function(){
  largeText();
});

$(window).scroll(function(){
  fixPagetop();
  scrollFix();
  parallaxBg();
});

/* ! scrollFix
--------------------------------------------------- */
var scrollFix = function(){

  var fixList = $(this).scrollTop();
  var start_m = 10;

  $(function () {
    if( fixList > start_m ) {
      $('.header-block01').addClass('fixed');
      $('.cmn_header').addClass('fixed');
      $('.main_contents').addClass('fixed');
    }else{
      $('.header-block01').removeClass('fixed');
      $('.cmn_header').removeClass('fixed');
      $('.main_contents').removeClass('fixed');
    }
  });
};

/* ! logoHover
--------------------------------------------------- */
var logoHover = function(){
  $('.cmn_header').hover(
    function(e) {
      $('.cmn_header').removeClass('fixed');
      $('.header-block01').removeClass('hover');
      $('.main_contents').removeClass('fixed');
    },
    function(e) {
      $('.cmn_header').addClass('fixed');
      $('.header-block01').addClass('hover');
      $('.main_contents').addClass('fixed');
    }
  );
};

/* ! largeText
--------------------------------------------------- */
var largeText = function(){
  $('.size_select').click(function(){
    $('.size_select').addClass('current')
    $('.size_select_large').removeClass('current')
    $('body').removeClass('fz-l')
    $('body').addClass('fz-m')
    if($('.fix_pagetop').length){

    };
  });
  $('.size_select_large').click(function(){
    $('.size_select_large').addClass('current')
    $('.size_select').removeClass('current')
    $('body').removeClass('fz-m')
    $('body').addClass('fz-l')
  });
};

/* ! globalNavi
--------------------------------------------------- */
var globalNavi = function(){
  $('.global_nav_contents').hide();
  $('.btn_global_nav').click(function(){
      $('.global_nav_contents').slideDown("slow");
      // $('.menu-open').toggleClass("active");
  });
  $('.menu_close').click(function(){
      $('.global_nav_contents').slideUp("slow");
      // $('.menu-open').toggleClass("active");
  });
};


/* !fixPagetop
--------------------------------------------------- */
var fixPagetop = function(){

  if($('.fix_pagetop').length){
    var fixBtn = $(this).scrollTop();
    var start_m = 200;

    $(function () {
      if( fixBtn > start_m ) {
        $('.fix_pagetop').fadeIn('slow');
      }else{
        $('.fix_pagetop').fadeOut('slow');
      }
    });
  };
};

/* !parallaxBg
--------------------------------------------------- */
var parallaxBg = function(){
      //メイン画像の高さ合わせ
      var imgHeight  = $('.bg_area_parallax').height();
      //スクロールにあわせて透過
      var ScrollPoint = $(window).scrollTop(); //スクロール量
      var windowHeight = $(window).height();//ウィンドウの高さ
      var percent = imgHeight / 100; //背景画像の高さ /100
      var scrollResult =  (ScrollPoint / percent / 100);  //背景画像に対するスクロール量の割合
      var bgPercent = scrollResult * 100; //背景画像の移動量
      $('.bg_area_parallax').css("top","-" + bgPercent + "px");
};












