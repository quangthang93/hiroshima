<?php
$userAgent = null;
if(array_key_exists('HTTP_USER_AGENT', $_SERVER)) {
    $userAgent = $_SERVER['HTTP_USER_AGENT'];
}
if(!empty($userAgent)) {
    if ((strpos($userAgent, 'iPhone') !== false && strpos($userAgent, 'iPad') === false) || strpos($userAgent, 'iPod') !== false || strpos($userAgent, 'Android') !== false) {
        $dispMode = $_REQUEST['mode'];
        if($dispMode != "pc"){
            header("Location: /s/fashionweek/");
            exit;
        }
    }
}
?>
<!DOCTYPE html>
<!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7 le6"><![endif]-->
<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8 ie7"><![endif]-->
<!--[if IE 8]><html class="no-js lt-ie9 ie8"><![endif]-->
<!--[if gt IE 8]><!--><html class="no-js"><!--<![endif]-->

<head>
<meta charset="Shift_JIS">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=950,initial-scale=1">
<title>2016 広島ファッションウィーク</title>
<meta name="description" content="ファッションで広島のマチを盛り上げる！ナイトランウェイをはじめ、ライブや豪華ゲストもぞくぞく登場。ここでしか体感できないイベントが盛りだくさん。">
<meta name="keyword" content="広島,ファッション,ウィーク,イベント,ショー,ライブ,アリスガーデン,パルコ">

<link rel="alternate" media="only screen and (max-width: 640px)" href="/s/fashionweek/" />

<?php /* OGP */ ?>
<meta property="og:locale" content="ja_JP" />
<meta property="og:type" content="website" />
<meta property="og:title" content="2016 広島ファッションウィーク" />
<meta property="og:site_name" content="" />
<meta property="og:description" content="ファッションで広島のマチを盛り上げる！ナイトランウェイをはじめ、ライブや豪華ゲストもぞくぞく登場。ここでしか体感できないイベントが盛りだくさん。" />
<meta property="og:image" content="http://www.chushinren.jp/fashionweek/images/common/ogp.jpg" />

<?php /* stylesheet */ ?>
<link rel="stylesheet" href="css/animate.css">
<link rel="stylesheet" href="css/common.css">
<link rel="stylesheet" href="css/home.css">

<?php /* scripts */ ?>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="js/jquery.js"><\/script>')</script>
<script src="js/_src/modernizr.js"></script>
<script src="js/_src/scrollsmoothly.js"></script>
<script src="js/_src/wow.min.js"></script>
<script src="js/common.js"></script>
<script src="js/home.js"></script>

<?php /* wow初期化 */ ?>
<script>
new WOW().init();
</script>
</head>

<body id="Top" class="home">

<?php /* SNS */ ?>
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/ja_JP/sdk.js#xfbml=1&version=v2.7";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>

<script src="https://apis.google.com/js/platform.js" async defer>
  {lang: 'ja'}
</script>

<div class="wrapper">

  <div class="border_box">
    <i class="border top"></i>
    <i class="border right"></i>
    <i class="border bottom"></i>
    <i class="border left"></i>
  <!-- border_box --></div>

  <header class="hd_cmn clearfix">
    <div class="hd__left"><a href="index.php"><img src="images/common/hd_left.png" width="374" height="49" alt="HIROSHIMA FASHION WEEK"></a></div>
    <div class="hd__right">
      <div class="ttl type01"><a href="runway.php"><img src="images/common/hd_ttl01.png" width="110" height="17" alt="NIGHT RUNWAY"></a></div><!--
      --><div class="ttl type02"><a href="http://www.chushinren.jp/fashionweek/event/"><img src="images/common/hd_ttl02.png" width="44" height="17" alt="EVENT"></a></div><!--
      --><div class="ttl type03"><a href="https://parcophotos.shuttlerock.com/content/hiroshimafashionweek16" target="blank"><img src="images/common/hd_ttl03.png" width="89" height="17" alt="SOCIAL FEED"></a></div><!--
      --><div class="sns"><a href="https://www.facebook.com/hiroshimafashionweek/" target="blank"><img src="images/common/sns01.png" width="60" height="60" alt=""></a></div><!--
      --><div class="sns"><a href="https://twitter.com/hiroshima_fw" target="blank"><img src="images/common/sns02.png" width="61" height="60" alt=""></a></div><!--
      --><div class="sns"><a href="https://www.instagram.com/hiroshima_fw/" target="blank"><img src="images/common/sns03.png" width="60" height="60" alt=""></a></div><!--
    --></div>
  </header>

  <div class="contents">
    <div class="main_wrapper">
      <div class="main"><img src="images/home/main.png" width="702" height="948" alt="HIROSHIMA FASHION WEEK"></div>
      <ul class="bnr_list wow fadeInUp"><!--
        --><li><a href="runway.php"><img src="images/home/list01.png" width="390" height="390" alt="Night Runway"></a></li><!--
        --><li><a href="http://www.chushinren.jp/fashionweek/event/"><img src="images/home/list02.png" width="390" height="390" alt="Night Runway"></a></li><!--
        --><li><a href="https://parcophotos.shuttlerock.com/content/hiroshimafashionweek16" target="blank"><img src="images/home/list03.png" width="390" height="390" alt="Night Runway"></a></li><!--
      --></ul>
    <!-- main_wrapper --></div>

    <div class="txt_area wow fadeInUp" data-wow-duration="1s">
      ドレスコードは「HIROSHIMA RED」！！<br>
      期間中テーマカラーである「赤」の服や小物などを<br>
      身に着けている方にはステキな特典があるかも♪
    <!-- txt_area --></div>

    <div class="contents01 wow fadeInUp" data-wow-duration="1s"><img src="images/home/contents01.png" width="1171" height="2479" alt="オシャレでチャンス"></div>


    <div class="sec02_contents wow fadeInUp" data-wow-duration="1s">
      <div class="txt"><img src="images/home/sec02_txt.png" width="473" height="119" alt="FOLLOW US ON"></div>
      <ul class="sns_list"><!--
        --><li><a href="https://www.facebook.com/hiroshimafashionweek/" target="blank"><img src="images/home/sns01.png" width="90" height="90" alt="Facebook"></a></li><!--
        --><li><a href="https://twitter.com/hiroshima_fw" target="blank"><img src="images/home/sns02.png" width="90" height="90" alt="Twitter"></a></li><!--
        --><li><a href="https://www.instagram.com/hiroshima_fw/" target="blank"><img src="images/home/sns03.png" width="90" height="90" alt="Instagram"></a></li><!--
      --></ul>
    <!-- sec02_contents --></div>

    <div class="txt-block01 wow fadeInUp" data-wow-duration="1s">
      <table class="table_list01">
      <tr>
        <th>主催</th>
        <td>
          <div class="td_in">
            <span>広島ファッションウィーク実行委員会</span><br>
            広島市　広島商工会議所　中国新聞社　 広島市中央部商店街振興組合連合会<br>
            パルコ広島店　福屋　そごう広島店　アクア広島センター街　基町クレド・パセーラテナント会
          </div>
        </td>
      </tr>
      <tr>
        <th>後援</th>
        <td>
          <div class="td_in">
            中国新聞社・NHK広島放送局・中国放送・広島テレビ・テレビ新広島・広島ホームテレビ・広島エフエム放送・FMちゅーピー76.6MHZ
          </div>
        </td>
      </tr>
      <tr>
        <th>協賛</th>
        <td>
          <div class="td_in">(株)クレディセゾン</div>
        </td>
      </tr>
      </table>
    <!-- txt-block01 --></div>

    <div class="sns-block">
      <ul class="sns_list">
        <li>
          <div class="fb-like" data-href="http://www.chushinren.jp/fashionweek/" data-layout="button" data-action="like" data-size="small" data-show-faces="true" data-share="true"></div>
        </li>
        <li>
          <a href="https://twitter.com/share" class="twitter-share-button">Tweet</a>
        </li>
        <li>
          <div class="g-plusone" data-annotation="none"></div>
        </li>
      </ul>
    <!-- sns-block --></div>
    <p class="copyright">&copy; HIROSHIMA FASHION WEEK ORGANIZATION 2016 , All Rights Reserved.</p>
  <!-- contents --></div>

  <div class="btn_pagetop">
    <a href="#Top">
      <img src="images/common/btn_pagetop.png" width="62" height="62" alt="page top">
    </a>
  </div>

<!-- wrapper --></div>
</body>
</html>
