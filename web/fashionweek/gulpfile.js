
var gulp = require("gulp");

//Sassのコンパイル
var sass = require("gulp-sass");

//ベンダープリフィックス付与
var autoprefixer = require("gulp-autoprefixer");

//Watchを中断させない
var plumber = require('gulp-plumber');

//デスクトップにエラー通知
var notify  = require('gulp-notify');

//jsの圧縮
var uglify = require("gulp-uglify");

//ブラウザの自動リロード
var connect = require("gulp-connect");


//Sass
gulp.task("sass", function() {
    gulp.src("scss/*.scss")
        .pipe(sass())
        .pipe(gulp.dest("css"));
});

// Watch
gulp.task("default", function() {
    gulp.watch("scss/**/*.scss",["sass"]);
});

// autoprefixer
gulp.task("sass", function() {
    gulp.src("scss/**/*.scss")
        .pipe(sass())
        .pipe(autoprefixer())
        .pipe(gulp.dest("css"));
});

//Error
gulp.task('sass', function(){
  gulp.src("scss/**/*.scss")
    .pipe(plumber({
      errorHandler: notify.onError("Error: <%= error.message %>") //<-
    }))
    .pipe(sass())
    .pipe(gulp.dest("css"))
    .pipe(connect.reload());
});

// uglify
gulp.task("js", function() {
    gulp.src(["js/**/*.js","!js/min/**/*.js"])
        .pipe(uglify())
        .pipe(gulp.dest("./js/min"));
});



