<?php
$userAgent = null;
if(array_key_exists('HTTP_USER_AGENT', $_SERVER)) {
    $userAgent = $_SERVER['HTTP_USER_AGENT'];
}
if(!empty($userAgent)) {
    if ((strpos($userAgent, 'iPhone') !== false && strpos($userAgent, 'iPad') === false) || strpos($userAgent, 'iPod') !== false || strpos($userAgent, 'Android') !== false) {
        $dispMode = $_REQUEST['mode'];
        if($dispMode != "pc"){
            header("Location: /s/fashionweek/runway.php");
            exit;
        }
    }
}
?>
<!DOCTYPE html>
<!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7 le6"><![endif]-->
<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8 ie7"><![endif]-->
<!--[if IE 8]><html class="no-js lt-ie9 ie8"><![endif]-->
<!--[if gt IE 8]><!--><html class="no-js"><!--<![endif]-->

<head>
<meta charset="Shift_JIS">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=950,initial-scale=1">
<title>ナイトランウェイ｜2016 HIROSHIMA FASHION</title>
<meta name="description" content="10/8（土）開催のナイトランウェイ！最旬ファッションが夜のランウェイを駆け抜ける！">
<meta name="keyword" content="ナイトランウェイ,広島,ファッション,ウィーク,イベント,ショー,ライブ,アリスガーデン,パルコ">

<link rel="alternate" media="only screen and (max-width: 640px)" href="/s/fashionweek/runway.php" />

<?php /* OGP */ ?>
<meta property="og:locale" content="ja_JP" />
<meta property="og:type" content="website" />
<meta property="og:title" content="ナイトランウェイ｜2016 HIROSHIMA FASHION" />
<meta property="og:site_name" content="" />
<meta property="og:description" content="10/8（土）開催のナイトランウェイ！最旬ファッションが夜のランウェイを駆け抜ける！" />
<meta property="og:image" content="http://www.chushinren.jp/fashionweek/images/common/ogp.jpg" />

<?php /* stylesheet */ ?>
<link rel="stylesheet" href="css/animate.css">
<link rel="stylesheet" href="css/common.css">
<link rel="stylesheet" href="css/runway.css">

<?php /* scripts */ ?>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="js/jquery.js"><\/script>')</script>
<script src="js/_src/modernizr.js"></script>
<script src="js/_src/scrollsmoothly.js"></script>
<script src="js/_src/wow.min.js"></script>
<script src="js/common.js"></script>


<?php /* wow初期化 */ ?>
<script>
new WOW().init();
</script>
</head>

<body id="Top" class="runway">

<?php /* SNS */ ?>
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/ja_JP/sdk.js#xfbml=1&version=v2.7";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>

<script src="https://apis.google.com/js/platform.js" async defer>
  {lang: 'ja'}
</script>

<div class="wrapper">

  <div class="border_box">
    <i class="border top"></i>
    <i class="border right"></i>
    <i class="border bottom"></i>
    <i class="border left"></i>
  <!-- border_box --></div>

  <header class="hd_cmn clearfix">
    <div class="hd__left"><a href="index.php"><img src="images/common/hd_left.png" width="374" height="49" alt="HIROSHIMA FASHION WEEK"></a></div>
    <div class="hd__right">
      <div class="ttl type01"><a href="runway.php"><img src="images/common/hd_ttl01.png" width="110" height="17" alt="NIGHT RUNWAY"></a></div><!--
      --><div class="ttl type02"><a href="http://www.chushinren.jp/fashionweek/event/"><img src="images/common/hd_ttl02.png" width="44" height="17" alt="EVENT"></a></div><!--
      --><div class="ttl type03"><a href="https://parcophotos.shuttlerock.com/content/hiroshimafashionweek16" target="blank"><img src="images/common/hd_ttl03.png" width="89" height="17" alt="SOCIAL FEED"></a></div><!--
      --><div class="sns"><a href="https://www.facebook.com/hiroshimafashionweek/" target="blank"><img src="images/common/sns01.png" width="60" height="60" alt=""></a></div><!--
      --><div class="sns"><a href="https://twitter.com/hiroshima_fw" target="blank"><img src="images/common/sns02.png" width="61" height="60" alt=""></a></div><!--
      --><div class="sns"><a href="https://www.instagram.com/hiroshima_fw/" target="blank"><img src="images/common/sns03.png" width="60" height="60" alt=""></a></div><!--
    --></div>
  </header>

  <div class="contents">
    <div class="main_wrapper">
      <div class="main"><img src="images/runway/main.png" width="667" height="384" alt="Night Runway"></div>
      <div class="ttl01 wow fadeInUp" data-wow-duration="1s">
        <div class="ttl01__in">
          <img src="images/runway/title01.png" width="360" height="133" alt="SPECIAL GUEST">
        </div>
      </div>
    <!-- main_wrapper --></div>

    <div class="contents-block01">
      <div class="list type01 wow fadeInUp" data-wow-duration="1s">
        <div class="photo"><img src="images/runway/guest01.jpg" width="500" height="240" alt="西山 茉希"></div>
        <div class="ttl">西山 茉希</div>
        <p class="txt">
          1985年11月16日生まれ、新潟県出身。O型。<br>
          2005年より雑誌「CanCam」の専属モデルを務め、主に同世代の女性から支持を集め人気を博す。現在はバラエティ番組にも多く出演し、活動の幅を広げている。
        </p>
      <!-- list --></div>
      <div class="list type02 wow fadeInUp" data-wow-duration="1s">
        <div class="photo"><img src="images/runway/guest02.jpg" width="500" height="240" alt="DJ DC BRAND'S"></div>
        <div class="ttl">DJ DC BRAND'S</div>
        <p class="txt">
          DC BRAND’S（ディーシー・ブランズ）-SINCE2016-<br>
          DJ SAILORS by 掟ポルシェ ＋ DJ K-FACTORY by ミッツィー申し訳 ＋ DJ Karl Helmut by 藤井隆を中心に、DJ LUNA MATTINO by アキオクマガイ、DJ PERSON’S、DJ IXI:Zなど擁する、80~90年代のELECTRO DANCE MUSIC「Hi-NRG」に特化したDJ TEAM。<br>
          高価で綺羅びやかなDCブランドファッションを身に纏い、ANALOG,CDJ,SAMPLERを駆使したDJ PLAYに加え、DANCE,LIVEなど予想もつかないアンチファストなGIG STYLEはサプールの進化形と評される。<br>
          NO BUBBLE!, NO IMPORT BRAND!, MORE PWL!を基調にEDM界へ殴りこみをかけるFASHION ICON型DJ TEAM。
        </p>
      <!-- list --></div>
      <div class="txt_caution wow fadeInUp" data-wow-duration="1s"><img src="images/runway/txt_more.png" width="190" height="39" alt="AND MORE!"></div>
    <!-- contents-block01 --></div>

    <div class="contents-block02 wow fadeInUp" data-wow-duration="1s">
      <div class="contents-block02__in">
        <img src="images/runway/bnr01_txt.png" width="740" height="189" alt="FASHION SHOW">
      </div>
    <!-- contents-block02 --></div>

    <div class="contents-block02 type02 wow fadeInUp" data-wow-duration="1s">
      <div class="contents-block02__in">
        <img src="images/runway/bnr02_txt.png" width="740" height="189" alt="学生ファッションショー">
      </div>
      <div class="caution"><img src="images/runway/bnr02_sub_txt.png" width="111" height="12" alt="2015年開催の様子"></div>
    <!-- contents-block02 --></div>

    <div class="txt_coming wow fadeInUp" data-wow-duration="1s"><img src="images/runway/txt_time_schedule.png" width="999" height="618" alt="TIME SCHEDULE"></div>

    <div class="sns-block">
      <ul class="sns_list">
        <li>
          <div class="fb-like" data-href="http://www.chushinren.jp/fashionweek/runway.php" data-layout="button" data-action="like" data-size="small" data-show-faces="true" data-share="true"></div>
        </li>
        <li>
          <a href="https://twitter.com/share" class="twitter-share-button">Tweet</a>
        </li>
        <li>
          <div class="g-plusone" data-annotation="none"></div>
        </li>
      </ul>
    <!-- sns-block --></div>
    <p class="copyright">&copy; HIROSHIMA FASHION WEEK ORGANIZATION 2016 , All Rights Reserved.</p>
  <!-- contents --></div>

  <div class="btn_pagetop">
    <a href="#Top">
      <img src="images/common/btn_pagetop.png" width="62" height="62" alt="page top">
    </a>
  </div>


<!-- wrapper --></div>
</body>
</html>
