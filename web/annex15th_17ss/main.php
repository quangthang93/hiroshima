<?php
include_once 'inc/config.php';

$title = TITLE;
$description = DESCRIPTION2;
$keywords = KEYWORDS2;
define('PAGE_NAME', 'newshop');

function is_mobile () {
  $uamb = array(
    'DoCoMo',
    'KDDI',
    'SoftBank',
    'UP.Browser',
    'J-PHONE',
    'Vodafone'
  );
  $patternmb = '/'.implode('|', $uamb).'/i';
  return preg_match($patternmb, $_SERVER['HTTP_USER_AGENT']);
}
if (is_mobile()) {
  header("Location: /s/annex15th_17ss/main.php");
  exit;
}
?><!DOCTYPE html>
<html lang="ja">
<head>
<script>
  var ua = navigator.userAgent;
  var redirectPass = '../../s/annex15th_17ss/main.php';

  if ((ua.indexOf('iPhone') > 0 && ua.indexOf('iPad') == -1) || ua.indexOf('iPod') > 0 || (ua.indexOf('Android') > 0 && ua.indexOf('Mobile') > 0)){
      location.href = redirectPass;
  }
</script>
<!-- <script type="text/javascript">
if ((navigator.userAgent.indexOf('iPhone') > 0 && navigator.userAgent.indexOf('iPad') == -1) || navigator.userAgent.indexOf('iPod') > 0 || navigator.userAgent.indexOf('Android') > 0) {
  location.href = '/s/<?=DIRNAME?>/<?=PAGE_NAME?>/';
}
</script> -->
<?php include_once 'inc/html-head.php'; ?>
</head>

<body class="newshop main">
  <!-- Google Tag Manager -->
  <noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-5FDDGB" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
  <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src='//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);})(window,document,'script','dataLayer','GTM-5FDDGB');</script>
  <!-- End Google Tag Manager -->

<?php include_once 'inc/header.php'; ?>

<div class="contents">

<h2 class="ta-c"><img src="images/ttl-<?=PAGE_NAME?>.png" alt="NEW AND RENEWAL" /></h2>

<?php include_once 'inc/nav-nr.php'; ?>




<div class="photo-wrap">
<?php
/**
 * PHOTO-START
 */
?>


<div class="photo-block item-all">
  <div class="photo-block-in"><img class="visual" src="images/shop/image01.jpg" width="490" height="490" alt="POPUP SPACE">
    <div class="photo-hover"><span class="icon limited"></span>
      <div class="fadeInLeft animated">
        <h1 class="hover-ttl">本館 1F<br>3月7日 LIMITED OPEN！<br><span>POPUP SPACE</span></h1>
        <p class="hover-img"><img src="images/logo/logo01.png" alt="POPUP SPACE"></p>
        <p class="hover-text fz12">「新しい・面白い・限定」をテーマに旬のブランド・ショップを期間限定でご紹介していくPOPUPスペースが広島パルコ本館1Fに登場！期間限定で入れ替わる、様々なショップやブランドのアイテムをお楽しみください。<br><br>ビジュマム【3/7(火)～15(水)】<br>ラヴィドリュクス【3/17(金)～29(水)】</p>
        <div class="hover-box clearfix fz12">
          <!-- <p>大人気ハンドメイド作家80ブランド強が一堂に会するラヴィドリュクス期間限定ショップVol.13が広島に初登場！お洋服からアクセサリーまで燦然と展開されます！【3/17(金)～29(水)】</p> -->
          <!-- hover-box -->
        </div>
        <!-- fadeInLeft animated -->
      </div>
      <!-- photo-hover -->
    </div>
  </div>
</div>
<div class="photo-block item-all">
  <div class="photo-block-in"><img class="visual" src="images/shop/image02.jpg" width="490" height="490" alt="エースシューズ">
    <div class="photo-hover"><span class="icon new"></span><span class="badge"></span>
      <div class="fadeInLeft animated">
        <h1 class="hover-ttl">本館 6F<br>4月7日 NEW OPEN!<br><span>エースシューズ / シューズ</span></h1>
        <p class="hover-img"><img src="images/logo/logo02.png" alt="エースシューズ"></p>
        <p class="hover-text">"世界中のトレンドが集まる５つ星のシューズセレクトSHOP。<br>
        お客様に光り輝くファッションを提案するSHOP。
        </p>
        <div class="hover-box clearfix">
          <p></p>
          <!-- hover-box -->
        </div>
        <!-- fadeInLeft animated -->
      </div>
      <!-- photo-hover -->
    </div>
  </div>
</div>
<div class="photo-block item-all">
  <div class="photo-block-in"><img class="visual" src="images/shop/image03.jpg" width="490" height="490" alt="ローズバッド">
    <div class="photo-hover"><span class="icon renewal"></span>
      <div class="fadeInLeft animated">
        <h1 class="hover-ttl">本館 1F<br>2月25日 RENEWAL OPEN!<br><span>ローズバッド / レディス</span></h1>
        <p class="hover-img"><img src="images/logo/logo03.png" alt="ローズバッド"></p>
        <p class="hover-text">オリジナルブランドROSEBUDと世界各国から集めたインポートアイテムを取り扱うセレクトショップ！リニューアルに伴い、メンズの取り扱いもスタート。カップルでも楽しめるSHOPです。</p>
        <div class="hover-box clearfix">
          <p>10,000円（税込）以上お買い上げでROSEBUDオリジナル日傘プレゼント（数量限定）</p>
          <!-- hover-box -->
        </div>
        <!-- fadeInLeft animated -->
      </div>
      <!-- photo-hover -->
    </div>
  </div>
</div>
<div class="photo-block item-all">
  <div class="photo-block-in"><img class="visual" src="images/shop/image04.jpg" width="490" height="490" alt="ステークス">
    <div class="photo-hover"><span class="icon new"></span>
      <div class="fadeInLeft animated">
        <h1 class="hover-ttl">本館 6F<br>3月17日 NEW OPEN!<br><span>ステークス / レディス・メンズ</span></h1>
        <p class="hover-img"><img src="images/logo/logo04.png" alt="ステークス"></p>
        <p class="hover-text">ベーシックなファッションの要素を持ちながら、現代的な新しい感覚。斬新な要素を取り入れたセレクトSHOP。</p>
<?php /* ?>
        <div class="hover-box clearfix">
          <p>一部セール【3/17～31】仮</p>
          <!-- hover-box -->
        </div>
        <!-- fadeInLeft animated -->
<?php */ ?>
      </div>
      <!-- photo-hover -->
    </div>
  </div>
</div>
<div class="photo-block item-all">
  <div class="photo-block-in"><img class="visual" src="images/shop/image05.jpg" width="490" height="490" alt="マジェスティックレゴン">
    <div class="photo-hover"><span class="icon renewal"></span>
      <div class="fadeInLeft animated">
        <h1 class="hover-ttl">本館 7F<br>3月10日 RENEWAL OPEN!<br><span>マジェスティックレゴン / レディス</span></h1>
        <p class="hover-img"><img src="images/logo/logo05.png" alt="マジェスティックレゴン"></p>
        <p class="hover-text">シーンを選ばず着られる、ちょっと綺麗でちょっと可愛い、気軽で高品質な女性のカジュアルスタイリングを提案。</p>
        <div class="hover-box clearfix">
          <p>11,000円（税込）以上お買い上げでスープジャープレゼント（先着50名）<br>ノベルティークリアファイルプレゼント（先着500名）【3/10～無くなり次第終了】</p>
          <!-- hover-box -->
        </div>
        <!-- fadeInLeft animated -->
      </div>
      <!-- photo-hover -->
    </div>
  </div>
</div>
<div class="photo-block item-all">
  <div class="photo-block-in"><img class="visual" src="images/shop/image06.jpg" width="490" height="490" alt="ヘザー">
    <div class="photo-hover"><span class="icon renewal"></span>
      <div class="fadeInLeft animated">
        <h1 class="hover-ttl">本館 7F<br>3月3日 RENEWAL OPEN!<br><span>ヘザー / レディス</span></h1>
        <p class="hover-img"><img src="images/logo/logo06.png" alt="ヘザー"></p>
        <p class="hover-text">いつまでも遊び心を忘れない“前向きな女の子”のためのブランド。 ガーリッシュで程よくエッジのきいたアイテムをベースにトレンドをミックスし、等身大のスタイルを提案します。</p>
        <div class="hover-box clearfix">
          <p>税込￥8,000以上お買い上げでヘザーオリジナル　写ルンですインスタントカメラプレゼント【3/3～無くなり次第終了】</p>
          <!-- hover-box -->
        </div>
        <!-- fadeInLeft animated -->
      </div>
      <!-- photo-hover -->
    </div>
  </div>
</div>
<div class="photo-block item-all">
  <div class="photo-block-in"><img class="visual" src="images/shop/image07.jpg" width="490" height="490" alt="島村楽器">
    <div class="photo-hover"><span class="icon renewal"></span>
      <div class="fadeInLeft animated">
        <h1 class="hover-ttl">本館 9F<br>3月4日 RENEWAL OPEN!<br><span>島村楽器 / 楽器・スタジオ・音楽教室</span></h1>
        <p class="hover-img"><img src="images/logo/logo07.png" alt="島村楽器"></p>
        <p class="hover-text fz10">管楽器コーナーリニューアルオープン！展示スペースを拡張、リペアブース・試奏室を完備し、国内外の定番機種をはじめ、これまで店頭に なかった限定品番・派生楽器など総本数100本を常時展示します！</p>
        <div class="hover-box clearfix fz10">
          <p>「リペアマンによる管楽器点検会」<br>
          ■日程:3/4、3/5■料金(税込):会員￥1,080、一般￥1,404<br><br>
         「世界的SAXプレイヤー、福井健太マウスピースセミナー」<br>
          ■日時:3/4 START:13:00■料金(税込):無料/来場特典付■定員35名
          </p>
          <!-- hover-box -->
        </div>
        <!-- fadeInLeft animated -->
      </div>
      <!-- photo-hover -->
    </div>
  </div>
</div>



<?php
/**
 * PHOTO-END
 */
?>
</div>


<!-- /contents --></div>

<?php include_once'inc/footer.php'; ?>
</body>
</html>
