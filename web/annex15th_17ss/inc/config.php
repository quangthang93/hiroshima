<?php
define('STORE_NAME', 'hiroshima');
define('STORE_NAME_JA', '広島PARCO');
// define('CHECK_NUM', '2254');
define('DIRNAME', 'annex15th_17ss');
define('TITLE', 'SPRING NEW＆RENEWAL｜広島PARCO');
define('DESCRIPTION1', '広島PARCOのNEW SHOP＆RENEWAL SHOPのブランドコンセプトや取扱いアイテムなどをご紹介！2017年春、広島PARCOに注目のショップが続々オープン！');
define('KEYWORDS1', 'NEW,RENEWAL,リニューアル,広島パルコ,PARCO');
define('DESCRIPTION2', '広島PARCOのNEW SHOP＆RENEWAL SHOPのブランドコンセプトや取扱いアイテムなどをご紹介！2017年春、広島PARCOに注目のショップが続々オープン！');
define('KEYWORDS2', 'NEW,RENEWAL,リニューアル,広島パルコ,PARCO');
define('DESCRIPTION3', '2016年秋、広島パルコ新館15周年！広島パルコの新館15周年を記念して様々なイベント開催します！');
define('KEYWORDS3', 'NEW,RENEWAL,リニューアル,広島パルコ,PARCO');
define('DESCRIPTION4', '接客のスペシャリストが集結！広島パルコで働く個性輝くスタッフです。');
define('KEYWORDS4', 'NEW,RENEWAL,リニューアル,広島パルコ,PARCO');
define('POST_SDATE1', '20160810000000');
define('POST_EDATE1', '20170810000000');
//define('POSTS_SDATE2', '20160401000000'); //ブログごとに違う場合
//define('POSTS_EDATE2', '20170401000000'); //ブログごとに違う場合
define('CATEGORY_ID1', '205');
// define('CATEGORY_ID2', '32');
$sns_url = 'http://hiroshima.parco.jp/web/annex15th_17ss/main.php';
$sns_url_sp = 'http://hiroshima.parco.jp/s/annex15th_17ss/main.php';
$sns_title_encode = urlencode(TITLE);
?>
