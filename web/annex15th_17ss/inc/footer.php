<p class="ta-c mt60"><a href="/web/recruit/" target="_blank"><img src="/web/recruit/images/pc_recruitpage_bnr_hiroshima.jpg" alt="あなたもパルコで働こう！スタッフ募集 | 広島PARCO"></a></p>

<p class="btn-pagetop"><a href="#" class="page-scroll"><img src="images/btn_pagetop.png" alt="ページトップ"></a></p>
<div class="footer">
<div class="footer__in">
<div class="parco"><a href="http://hiroshima.parco.jp/page2/"><img src="images/btn_sitetop.png" alt="SITE TOP" /></a></div>
<div class="copyright"><img src="images/txt_copyright.png" alt="COPYRIGHT (C) PARCO CO., LTD. ALL RIGHTS RESERVED." /></div>

<ul class="snsnlist">
  <li><a href="https://www.facebook.com/sharer/sharer.php?u=http%3A%2F%2F<?=STORE_NAME?>.parco.jp%2Fweb%2F<?=DIRNAME?>%2Fmain.php"><img src="images/ico-facebook.png" alt="Facebook"></a></li>
  <li><a href="https://twitter.com/share?url=http%3A%2F%2F<?=STORE_NAME?>.parco.jp%2Fweb%2F<?=DIRNAME?>%2Fmain.php&amp;text=<?php echo $sns_title_encode; ?>"><img src="images/ico-twitter.png" alt="Twitter"></a></li>
  <li><a href="https://plus.google.com/share?url=<?=STORE_NAME?>.parco.jp/web/<?=DIRNAME?>/main.php"><img src="images/ico-google.png" alt="Google +"></a></li>
</ul>

</div>
</div>

<?php // include_once($_SERVER['DOCUMENT_ROOT']. '/web/cardoff/inc/modal.php'); ?>
