<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=1060">
<title><?php echo $title ?></title>
<meta name="description" content="<?php echo $description ?>" />
<meta name="keywords" content="<?php echo $keywords ?>"/>
<?php include_once'inc/ogp.php'; ?>
<link rel="alternate" media="only screen and (max-width: 640px)" href="/s/<?=DIRNAME?>/main.php" />
<link rel="stylesheet" href="css/default.css" media="screen,print" />
<link rel="stylesheet" href="css/style.css" media="screen,print" />
<link rel="stylesheet" href="css/<?=PAGE_NAME?>.css" />

<script src="js/modernizr.js"></script>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<!-- <script>window.jQuery || document.write('<script src="js/jquery.js"><\/script>')</script> -->
<!-- <script src="/common/js/jquery.js"></script> -->
<script src="js/masonary.min.js"></script>
<!--<script src="js/jquery.transit.min.js"></script>-->
<script src="js/common.js"></script>
<script src="../../common/js/social.js"></script>
<script src="../../common/js/imgLiquid.js"></script>
