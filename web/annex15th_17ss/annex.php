<?php
include_once 'inc/config.php';

$title = TITLE;
$description = DESCRIPTION2;
$keywords = KEYWORDS2;
define('PAGE_NAME', 'newshop');

function is_mobile () {
  $uamb = array(
    'DoCoMo',
    'KDDI',
    'SoftBank',
    'UP.Browser',
    'J-PHONE',
    'Vodafone'
  );
  $patternmb = '/'.implode('|', $uamb).'/i';
  return preg_match($patternmb, $_SERVER['HTTP_USER_AGENT']);
}
if (is_mobile()) {
  header("Location: /s/annex15th_17ss/annex.php");
  exit;
}
?><!DOCTYPE html>
<html lang="ja">
<head>
<script>
var ua = navigator.userAgent;
var redirectPass = '../../s/annex15th_17ss/annex.php';

if ((ua.indexOf('iPhone') > 0 && ua.indexOf('iPad') == -1) || ua.indexOf('iPod') > 0 || (ua.indexOf('Android') > 0 && ua.indexOf('Mobile') > 0)){
    location.href = redirectPass;
}
</script>
<!-- <script type="text/javascript">
if ((navigator.userAgent.indexOf('iPhone') > 0 && navigator.userAgent.indexOf('iPad') == -1) || navigator.userAgent.indexOf('iPod') > 0 || navigator.userAgent.indexOf('Android') > 0) {
  location.href = '/s/<?=DIRNAME?>/<?=PAGE_NAME?>/';
}
</script> -->
<?php include_once 'inc/html-head.php'; ?>
</head>

<body class="newshop annex">
  <!-- Google Tag Manager -->
  <noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-5FDDGB" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
  <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src='//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);})(window,document,'script','dataLayer','GTM-5FDDGB');</script>
  <!-- End Google Tag Manager -->
<?php include_once 'inc/header.php'; ?>

<div class="contents">

<h2 class="ta-c"><img src="images/ttl-<?=PAGE_NAME?>.png" alt="NEW AND RENEWAL" /></h2>

<?php include_once 'inc/nav-nr.php'; ?>




<div class="photo-wrap">
<?php
/**
 * PHOTO-START
 */
?>


<div class="photo-block item-all">
  <div class="photo-block-in"><img class="visual" src="images/shop/image08.jpg" width="490" height="490" alt="ユナイテッドアローズ（メンズ）">
    <div class="photo-hover"><span class="icon renewal"></span>
      <div class="fadeInLeft animated">
        <h1 class="hover-ttl">新館 B1F<br>3月17日 RENEWAL OPEN!<br><span>ユナイテッドアローズ（メンズ） / メンズ</span></h1>
        <p class="hover-img"><img src="images/logo/logo08.png" alt="ユナイテッドアローズ（メンズ）"></p>
        <p class="hover-text fz12">開店以来となる大幅なリニューアル。これまでもより快適な空間でお買い物を楽しんでいただけるよう、内装を一新。<br>
          特徴としては木を基調とした落ち着きのある空間を商品カテゴリーごとに天井や床、壁面の素材の変化をつけて作りこむことで全体の統一感を持たせながらもシーンごとの移り変わりを感じられる店舗に仕上げています。</p>
        <div class="hover-box clearfix fz12">
          <p>￥15,000以上お買い上げの方にリングベルトを進呈（数量限定）【3/17～】<br>FELISIのBAG、CARUSOのスーツ販売</p>
          <!-- hover-box -->
        </div>
        <!-- fadeInLeft animated -->
      </div>
      <!-- photo-hover -->
    </div>
  </div>
</div>
<div class="photo-block item-all">
  <div class="photo-block-in"><img class="visual" src="images/shop/image09.jpg" width="490" height="490" alt="プチバトー">
    <div class="photo-hover"><span class="icon new"></span><span class="badge"></span>
      <div class="fadeInLeft animated">
        <h1 class="hover-ttl">新館 4F<br>3月17日 NEW OPEN!<br><span>プチバトー / レディス・メンズ</span></h1>
        <p class="hover-img"><img src="images/logo/logo09.png" alt="プチバトー"></p>
        <p class="hover-text fz11">フランスの国民的ブランド「プチバトー」はやわらかな肌ざわりとシンプルながら遊び心あふれるデザインで世界中のあらゆる世代の人に親しまれています。中国地方初出店となるショップでは、ベビー、キッズはもちろん、レディース＆メンズアイテムまでフルラインを取り揃えて皆様をお迎え致します。</p>
        <div class="hover-box clearfix fz11">
          <p>Nouvelle Collection ポイントUP＆クーポンキャンペーン
          <br>プチバトーメンバーズプログラムの会員の方へポイントUPをご提供。
          <br>会員以外の方へは、10,000円以上購入頂いた際に2000円分のクーポンをプレゼント。(クーポンは次回の購入から４月30日まで利用可能)【3/17～30】</p>
          <!-- hover-box -->
        </div>
        <!-- fadeInLeft animated -->
      </div>
      <!-- photo-hover -->
    </div>
  </div>
</div>
<div class="photo-block item-all">
  <div class="photo-block-in"><img class="visual" src="images/shop/image10.jpg" width="490" height="490" alt="マーコート">
    <div class="photo-hover"><span class="icon new"></span><span class="badge"></span>
      <div class="fadeInLeft animated">
        <h1 class="hover-ttl">新館 3F<br>4月20日 NEW OPEN!<br><span>マーコート / レディス</span></h1>
        <p class="hover-img"><img src="images/logo/logo10.png" alt="マーコート"></p>
        <p class="hover-text">穏やかな空気の中で、大切な時間をゆっくり・じっくり、愉しみながら過してください。<br>
        ＭＡＲｃｏｕｒｔはオリジナルブランドを中心に国内外のアイテムをミックスしたセレクトショップです。</p>
        <div class="hover-box clearfix">
          <p>￥10,000以上お買い上げでオリジナルノベルティーをプレゼント【3/17～】</p>
          <!-- hover-box -->
        </div>
        <!-- fadeInLeft animated -->
      </div>
      <!-- photo-hover -->
    </div>
  </div>
</div>
<div class="photo-block item-all">
  <div class="photo-block-in"><img class="visual" src="images/shop/image11.jpg" width="490" height="490" alt="ネストローブ">
    <div class="photo-hover"><span class="icon renewal"></span>
      <div class="fadeInLeft animated">
        <h1 class="hover-ttl">新館 3F<br>2月18日 RENEWAL OPEN!<br><span>ネストローブ / レディス・メンズ・ファッション雑貨</span></h1>
        <p class="hover-img"><img src="images/logo/logo11.png" alt="ネストローブ"></p>
        <p class="hover-text">厳選した素材で作る、日常になじむ服。表情豊かに育ち、自然と愛着のわく服。自然体でリラックスしたい・・・そう思っている人のために、ベーシックながらも独自のセンスを取り入れたラインナップを心地良い空間で提案するお店です。</p>
        <div class="hover-box clearfix">
          <p></p>
          <!-- hover-box -->
        </div>
        <!-- fadeInLeft animated -->
      </div>
      <!-- photo-hover -->
    </div>
  </div>
</div>
<div class="photo-block item-all">
  <div class="photo-block-in"><img class="visual" src="images/shop/image12.jpg" width="490" height="490" alt="チューズライフ　バイ　キャサリンハムネット">
    <div class="photo-hover"><span class="icon renewal"></span>
      <div class="fadeInLeft animated">
        <h1 class="hover-ttl">新館 B1F<br>3月17日 RENEWAL OPEN!<br><span>チューズライフ　バイ　キャサリンハムネット / メンズ・レディス・雑貨</span></h1>
        <p class="hover-img"><img src="images/logo/logo12.png" alt="チューズライフ　バイ　キャサリンハムネット"></p>
        <p class="hover-text">アバンギャルドでセクシーだが、スマート。英国の伝統的なクラシックデザインのバランス感覚を独自の感性で表現。</p>
        <div class="hover-box clearfix">
          <p></p>
          <!-- hover-box -->
        </div>
        <!-- fadeInLeft animated -->
      </div>
      <!-- photo-hover -->
    </div>
  </div>
</div>


<?php
/**
 * PHOTO-END
 */
?>
</div>



<!-- /contents --></div>

<?php include_once 'inc/footer.php'; ?>
</body>
</html>
