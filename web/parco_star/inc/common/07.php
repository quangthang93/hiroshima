    <section class="heroArea bg--colored">
        <div class="heroImgArea">
                <img src="assets/images/hero07.jpg" alt="">
        </div>
        <div class="widthBinder">
            <div class="heroInfoArea">
                <div class="badge--star">
                     07                        
                </div>
                <div class="hero__info">
                    <span>
                        新館 / B1F                                                
                    </span>
                    <span>
                    ユナイテッドアローズ                        
                    </span>
                </div>
                <a href="/page2/1053/" class="btn--line">
                    <svg>
                        <use xmlns:xlink="http://www.w3.org/2000/svg" xlink:href="#char_shopinfo"/>
                    </svg>
                     <span class="arrow"></span>
                </a>
                <p class="hero__copy">
                    Mr.UA
                </p>
                <p class="hero__name">
                    村上<span class="hero__name--small" >さん</span>
                </p>
                <div class="hero__sns">
                    
                                        
                </div>
            </div>
        </div>
    </section>
    <section class="qaListArea">
        <div class="widthBinder">
             <ul class="qaList">
                <li class="qaItem">
                    <p class="qa_question">
                        Q.ニックネームをお教え下さい。   
                    </p>
                    <p class="qa_answer">
                            特にございません
                    </p>
                </li>
                <li class="qaItem">
                    <p class="qa_question">
                        Q.こんな接客を心がけています！   
                    </p>
                    <p class="qa_answer">
                            経験が長い分、フォーマル・ビジネス・トレンドカジュアルと幅広い対応が出来るのが強みです。お客様のご来店背景をよく知る為、必ずヒアリングを十分にさせれ頂いてから、ご納得・ご満足していただける提案をさせて頂きます。
                            
                    </p>
                </li>
                <li class="qaItem">
                    <p class="qa_question">
                            Q.特技   
                    </p>
                    <p class="qa_answer">
                            特にございません
                    </p>
                </li>
                <li class="qaItem">
                    <p class="qa_question">
                            Q.趣味・休日の過ごし方   
                    </p>
                    <p class="qa_answer">
                            ・週に1回ジムでトレーニング
 ・海外のテレビドラマシリーズが好きなので、BS放送を録画して溜まった分を休日に一気に観ています。    
                    </p>
                </li>
                <li class="qaItem">
                    <p class="qa_question">
                            Q.目標・野望   
                    </p>
                    <p class="qa_answer">
                            2001年オープン以来16年間在籍していますが、親子でご利用頂いているお客様もいらっしゃいます。二世代といわず三世代でご利用頂けるような顧客様との関係を築いていきたいです。                                
                    </p>
                </li>
                <li class="qaItem">
                    <p class="qa_question">
                            Q.あなたにとってのスターとは？
                    </p>
                    <p class="qa_answer">
                            ファッション誌のスナップ特集をよく見るのですが、海外のファッション関係の方のスナップはいつも格好良いなあと思って見ています。                                
                            
                    </p>
                </li>
                <li class="qaItem">
                    <p class="qa_question">
                            Q.自店以外で、好きな(お気に入りの)パルコのショップは？  
                    </p>
                    <p class="qa_answer">
                            ジョンマスターオーガニックセレクト                                
                            
                    </p>
                </li>
                <li class="qaItem">
                    <p class="qa_question">
                            Q.広島のおススメスポットは？  
                    </p>
                    <p class="qa_answer">
                            市内の美術館（広島市立美術館・広島県立美術館・広島現代美術館）で特別展が開催される時は良く見に行きます。周辺に自然があり、街中でも静かな空間なのでリラックス出来ます。                                
                            
                    </p>
                </li>
                <li class="qaItem">
                    <p class="qa_question">
                            Q.好きな音楽・歌手は？   
                    </p>
                    <p class="qa_answer">
                            70年代ソウル・90年代オルタナロック・ダブミュージック。「ラプリオ」というバンド                                
                            
                    </p>
                </li>
                <li class="qaItem">
                    <p class="qa_question">
                            Q.お客様への一言メッセージ  
                    </p>
                    <p class="qa_answer">
                            2001年にオープンして以来、15年越しにリニューアルして新鮮な売り場へと変貌しています。まだお越し頂いていないお客様も是非一度お立ち寄り下さい。                                
                            
                    </p>
                </li>
                
            </ul>
        </div>
    </div>
    </section>
    <section class="staffListSlider">
        <div class="widthBinder">
            <p class="contentsList__ttl"><img src="assets/images/char_stflist_w.svg" alt="STAFF LIST"></p>
            <ul class="contentsLists">
                
                <li class="listItem"  id="01">
                    <div class="listItem__imgArea">
                        <a href="01.php">
                            <div class="badge--fill">
                                <span>01</span>
                            </div>
                            <img src="assets/images/01.jpg" alt="">
                        </a>
                    </div>
                </li>
                
                <li class="listItem"  id="02">
                    <div class="listItem__imgArea">
                        <a href="02.php">
                            <div class="badge--fill">
                                <span>02</span>
                            </div>
                            <img src="assets/images/02.jpg" alt="">
                        </a>
                    </div>
                </li>
                
                <li class="listItem"  id="03">
                    <div class="listItem__imgArea">
                        <a href="03.php">
                            <div class="badge--fill">
                                <span>03</span>
                            </div>
                            <img src="assets/images/03.jpg" alt="">
                        </a>
                    </div>
                </li>
                
                <li class="listItem"  id="04">
                    <div class="listItem__imgArea">
                        <a href="04.php">
                            <div class="badge--fill">
                                <span>04</span>
                            </div>
                            <img src="assets/images/04.jpg" alt="">
                        </a>
                    </div>
                </li>
                
                <li class="listItem"  id="05">
                    <div class="listItem__imgArea">
                        <a href="05.php">
                            <div class="badge--fill">
                                <span>05</span>
                            </div>
                            <img src="assets/images/05.jpg" alt="">
                        </a>
                    </div>
                </li>
                
                <li class="listItem"  id="06">
                    <div class="listItem__imgArea">
                        <a href="06.php">
                            <div class="badge--fill">
                                <span>06</span>
                            </div>
                            <img src="assets/images/06.jpg" alt="">
                        </a>
                    </div>
                </li>
                
                <li class="listItem"  id="07">
                    <div class="listItem__imgArea">
                        <a href="07.php">
                            <div class="badge--fill">
                                <span>07</span>
                            </div>
                            <img src="assets/images/07.jpg" alt="">
                        </a>
                    </div>
                </li>
                
                <li class="listItem"  id="08">
                    <div class="listItem__imgArea">
                        <a href="08.php">
                            <div class="badge--fill">
                                <span>08</span>
                            </div>
                            <img src="assets/images/08.jpg" alt="">
                        </a>
                    </div>
                </li>
                
                <li class="listItem"  id="09">
                    <div class="listItem__imgArea">
                        <a href="09.php">
                            <div class="badge--fill">
                                <span>09</span>
                            </div>
                            <img src="assets/images/09.jpg" alt="">
                        </a>
                    </div>
                </li>
                
                <li class="listItem"  id="10">
                    <div class="listItem__imgArea">
                        <a href="10.php">
                            <div class="badge--fill">
                                <span>10</span>
                            </div>
                            <img src="assets/images/10.jpg" alt="">
                        </a>
                    </div>
                </li>
                
                <li class="listItem"  id="11">
                    <div class="listItem__imgArea">
                        <a href="11.php">
                            <div class="badge--fill">
                                <span>11</span>
                            </div>
                            <img src="assets/images/11.jpg" alt="">
                        </a>
                    </div>
                </li>
                
                <li class="listItem"  id="12">
                    <div class="listItem__imgArea">
                        <a href="12.php">
                            <div class="badge--fill">
                                <span>12</span>
                            </div>
                            <img src="assets/images/12.jpg" alt="">
                        </a>
                    </div>
                </li>
                
            </ul>
        </div>
    </section>
    