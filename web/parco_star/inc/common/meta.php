<title>パルコのスター｜広島PARCO</title>
<meta name="description" content="接客のスペシャリストが集結！広島パルコで働く個性輝くスタッフです。">
<meta name="keyword" content="広島,イベント,スタッフ,パルコのスター,PARCO,パルコ">

<meta property="og:site_name" content="PARCO">
<meta property="og:type" content="article">
<meta property="og:url" content="http://hiroshima.parco.jp/web/parco_star/">
<meta property="og:title" content="パルコのスター｜広島PARCO">
<meta property="og:description" content="接客のスペシャリストが集結！広島パルコで働く個性輝くスタッフです。">
<meta property="og:image" content="http://hiroshima.parco.jp/web/parco_star/assets/images/ogp.png">
<meta property="og:locale" content="ja_JP">