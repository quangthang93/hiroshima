    <section class="heroArea bg--colored">
        <div class="heroImgArea">
                <img src="assets/images/hero02.jpg" alt="">
        </div>
        <div class="widthBinder">
            <div class="heroInfoArea">
                <div class="badge--star">
                     02                        
                </div>
                <div class="hero__info">
                    <span>
                        本館 / 2F                                                
                    </span>
                    <span>
                    アーバンリサーチアンドロッソ                        
                    </span>
                </div>
                <a href="/page2/9200/" class="btn--line">
                    <svg>
                        <use xmlns:xlink="http://www.w3.org/2000/svg" xlink:href="#char_shopinfo"/>
                    </svg>
                     <span class="arrow"></span>
                </a>
                <p class="hero__copy">
                    おもしろファッションスピーカー
                </p>
                <p class="hero__name">
                    原田<span class="hero__name--small" >さん</span>
                </p>
                <div class="hero__sns">
                    
                                        
                </div>
            </div>
        </div>
    </section>
    <section class="qaListArea">
        <div class="widthBinder">
             <ul class="qaList">
                <li class="qaItem">
                    <p class="qa_question">
                        Q.ニックネームをお教え下さい。   
                    </p>
                    <p class="qa_answer">
                            けーし
                    </p>
                </li>
                <li class="qaItem">
                    <p class="qa_question">
                        Q.こんな接客を心がけています！   
                    </p>
                    <p class="qa_answer">
                            日々の生活の中で、アーバンリサーチをふと思い出すような接客を心がけてます。
                            
                    </p>
                </li>
                <li class="qaItem">
                    <p class="qa_question">
                            Q.特技   
                    </p>
                    <p class="qa_answer">
                            頭の回転が早い、人を笑わせる
                    </p>
                </li>
                <li class="qaItem">
                    <p class="qa_question">
                            Q.趣味・休日の過ごし方   
                    </p>
                    <p class="qa_answer">
                            ショップ巡り 洋服作り    
                    </p>
                </li>
                <li class="qaItem">
                    <p class="qa_question">
                            Q.目標・野望   
                    </p>
                    <p class="qa_answer">
                            様々なお客様の脳裏にアーバンリサーチを焼き付ける                                
                    </p>
                </li>
                <li class="qaItem">
                    <p class="qa_question">
                            Q.あなたにとってのスターとは？
                    </p>
                    <p class="qa_answer">
                            誰もが知っている人のこと、知らない人はいない人のこと                                
                            
                    </p>
                </li>
                <li class="qaItem">
                    <p class="qa_question">
                            Q.自店以外で、好きな(お気に入りの)パルコのショップは？  
                    </p>
                    <p class="qa_answer">
                            アーバンリサーチ以外にないです！                                
                            
                    </p>
                </li>
                <li class="qaItem">
                    <p class="qa_question">
                            Q.広島のおススメスポットは？  
                    </p>
                    <p class="qa_answer">
                            縮景園                                
                            
                    </p>
                </li>
                <li class="qaItem">
                    <p class="qa_question">
                            Q.好きな音楽・歌手は？   
                    </p>
                    <p class="qa_answer">
                            Sly &amp; The Family Stone                                
                            
                    </p>
                </li>
                <li class="qaItem">
                    <p class="qa_question">
                            Q.お客様への一言メッセージ  
                    </p>
                    <p class="qa_answer">
                            洋服がお好きでしたらあなたに洋服に関してあなたに何かできるかもしれません。
 
 皆様でアパレル業界を盛り上げていきましょう！                                
                            
                    </p>
                </li>
                
                <li class="qaItem">
                    <p class="qa_question">
                           <img class="ico_comment" src="assets/images/ico_comment.png" alt=""> 同僚スタッフからのコメント  
                    </p>
                    <p class="qa_answer">
                            楽しいPARCOで楽しく働きましょう。                                
                            
                    </p>
                </li>
                
            </ul>
        </div>
    </div>
    </section>
    <section class="staffListSlider">
        <div class="widthBinder">
            <p class="contentsList__ttl"><img src="assets/images/char_stflist_w.svg" alt="STAFF LIST"></p>
            <ul class="contentsLists">
                
                <li class="listItem"  id="01">
                    <div class="listItem__imgArea">
                        <a href="01.php">
                            <div class="badge--fill">
                                <span>01</span>
                            </div>
                            <img src="assets/images/01.jpg" alt="">
                        </a>
                    </div>
                </li>
                
                <li class="listItem"  id="02">
                    <div class="listItem__imgArea">
                        <a href="02.php">
                            <div class="badge--fill">
                                <span>02</span>
                            </div>
                            <img src="assets/images/02.jpg" alt="">
                        </a>
                    </div>
                </li>
                
                <li class="listItem"  id="03">
                    <div class="listItem__imgArea">
                        <a href="03.php">
                            <div class="badge--fill">
                                <span>03</span>
                            </div>
                            <img src="assets/images/03.jpg" alt="">
                        </a>
                    </div>
                </li>
                
                <li class="listItem"  id="04">
                    <div class="listItem__imgArea">
                        <a href="04.php">
                            <div class="badge--fill">
                                <span>04</span>
                            </div>
                            <img src="assets/images/04.jpg" alt="">
                        </a>
                    </div>
                </li>
                
                <li class="listItem"  id="05">
                    <div class="listItem__imgArea">
                        <a href="05.php">
                            <div class="badge--fill">
                                <span>05</span>
                            </div>
                            <img src="assets/images/05.jpg" alt="">
                        </a>
                    </div>
                </li>
                
                <li class="listItem"  id="06">
                    <div class="listItem__imgArea">
                        <a href="06.php">
                            <div class="badge--fill">
                                <span>06</span>
                            </div>
                            <img src="assets/images/06.jpg" alt="">
                        </a>
                    </div>
                </li>
                
                <li class="listItem"  id="07">
                    <div class="listItem__imgArea">
                        <a href="07.php">
                            <div class="badge--fill">
                                <span>07</span>
                            </div>
                            <img src="assets/images/07.jpg" alt="">
                        </a>
                    </div>
                </li>
                
                <li class="listItem"  id="08">
                    <div class="listItem__imgArea">
                        <a href="08.php">
                            <div class="badge--fill">
                                <span>08</span>
                            </div>
                            <img src="assets/images/08.jpg" alt="">
                        </a>
                    </div>
                </li>
                
                <li class="listItem"  id="09">
                    <div class="listItem__imgArea">
                        <a href="09.php">
                            <div class="badge--fill">
                                <span>09</span>
                            </div>
                            <img src="assets/images/09.jpg" alt="">
                        </a>
                    </div>
                </li>
                
                <li class="listItem"  id="10">
                    <div class="listItem__imgArea">
                        <a href="10.php">
                            <div class="badge--fill">
                                <span>10</span>
                            </div>
                            <img src="assets/images/10.jpg" alt="">
                        </a>
                    </div>
                </li>
                
                <li class="listItem"  id="11">
                    <div class="listItem__imgArea">
                        <a href="11.php">
                            <div class="badge--fill">
                                <span>11</span>
                            </div>
                            <img src="assets/images/11.jpg" alt="">
                        </a>
                    </div>
                </li>
                
                <li class="listItem"  id="12">
                    <div class="listItem__imgArea">
                        <a href="12.php">
                            <div class="badge--fill">
                                <span>12</span>
                            </div>
                            <img src="assets/images/12.jpg" alt="">
                        </a>
                    </div>
                </li>
                
            </ul>
        </div>
    </section>
    