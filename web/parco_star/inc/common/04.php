    <section class="heroArea bg--colored">
        <div class="heroImgArea">
                <img src="assets/images/hero04.jpg" alt="">
        </div>
        <div class="widthBinder">
            <div class="heroInfoArea">
                <div class="badge--star">
                     04                        
                </div>
                <div class="hero__info">
                    <span>
                        本館 / 4F                                                
                    </span>
                    <span>
                    スライ                        
                    </span>
                </div>
                <a href="/page2/994/" class="btn--line">
                    <svg>
                        <use xmlns:xlink="http://www.w3.org/2000/svg" xlink:href="#char_shopinfo"/>
                    </svg>
                     <span class="arrow"></span>
                </a>
                <p class="hero__copy">
                    『似合う』を見つけるカリスマ
                </p>
                <p class="hero__name">
                    吉武<span class="hero__name--small" >さん</span>
                </p>
                <div class="hero__sns">
                    
                    <div class="snsItem">
                        <img src="assets/images/ico_insta.svg" alt="">instagram :  <a href="https://www.instagram.com/hinami_yoshitake">@hinami_yoshitake</a>
                    </div>
                    
                                        
                </div>
            </div>
        </div>
    </section>
    <section class="qaListArea">
        <div class="widthBinder">
             <ul class="qaList">
                <li class="qaItem">
                    <p class="qa_question">
                        Q.ニックネームをお教え下さい。   
                    </p>
                    <p class="qa_answer">
                            ひいちゃん
                    </p>
                </li>
                <li class="qaItem">
                    <p class="qa_question">
                        Q.こんな接客を心がけています！   
                    </p>
                    <p class="qa_answer">
                            「お客様と店員」というよりも「人と人」としてお話すること。
                            
                    </p>
                </li>
                <li class="qaItem">
                    <p class="qa_question">
                            Q.特技   
                    </p>
                    <p class="qa_answer">
                            書道。幼稚園の頃から１０年続けたので自信があります！（笑）
                    </p>
                </li>
                <li class="qaItem">
                    <p class="qa_question">
                            Q.趣味・休日の過ごし方   
                    </p>
                    <p class="qa_answer">
                            お好み焼きが大好きなので、休日は美味しいお好み焼き屋さん探しをよくします。いま特に好きなのは、定番の【ねぎ庵】と、安佐南区にある【kate-kate】です。オススメがあれば教えてください＾＾    
                    </p>
                </li>
                <li class="qaItem">
                    <p class="qa_question">
                            Q.目標・野望   
                    </p>
                    <p class="qa_answer">
                            「ＳＬＹといえばあのお姉さん！」と思ってもらえたら最高です。
 
 あとは私にとってのスターである店長の麻衣子さんを超えたいです！（笑）                                
                    </p>
                </li>
                <li class="qaItem">
                    <p class="qa_question">
                            Q.あなたにとってのスターとは？
                    </p>
                    <p class="qa_answer">
                            店長の麻衣子さん。
 
 たくさん褒めて、たくさん叱って、たくさん笑わせてくださる最高の店長です。
 
 人としても上司としても尊敬するところばかりです。                                
                            
                    </p>
                </li>
                <li class="qaItem">
                    <p class="qa_question">
                            Q.自店以外で、好きな(お気に入りの)パルコのショップは？  
                    </p>
                    <p class="qa_answer">
                            DIANAさん。いつも可愛い靴やカバンが並べてあるのでついつい入ってしまいます。                                
                            
                    </p>
                </li>
                <li class="qaItem">
                    <p class="qa_question">
                            Q.広島のおススメスポットは？  
                    </p>
                    <p class="qa_answer">
                            morning juice stand！！！
 
 フルーツサンドとスムージーが最高に美味しくて大好きです。
 
 見た目も可愛いので、ついつい写真を撮りたくなります。                                
                            
                    </p>
                </li>
                <li class="qaItem">
                    <p class="qa_question">
                            Q.好きな音楽・歌手は？   
                    </p>
                    <p class="qa_answer">
                            宇多田ヒカルさん。母の影響で、幼稚園の頃からずっと聴いています。
 
 活動再開してくれたので、またいつかliveに行きたいです。                                
                            
                    </p>
                </li>
                <li class="qaItem">
                    <p class="qa_question">
                            Q.お客様への一言メッセージ  
                    </p>
                    <p class="qa_answer">
                            お客様の「お気に入りの一着」を見つけるお手伝いができたら嬉しいです！
 
 是非お気軽に遊びにいらっしゃってください＾＾                                
                            
                    </p>
                </li>
                
                <li class="qaItem">
                    <p class="qa_question">
                           <img class="ico_comment" src="assets/images/ico_comment.png" alt=""> 同僚スタッフからのコメント  
                    </p>
                    <p class="qa_answer">
                            誰よりも話のレパートリーが多く、おもしろいので、いつもみんな大爆笑です！（笑）                                
                            
                    </p>
                </li>
                
            </ul>
        </div>
    </div>
    </section>
    <section class="staffListSlider">
        <div class="widthBinder">
            <p class="contentsList__ttl"><img src="assets/images/char_stflist_w.svg" alt="STAFF LIST"></p>
            <ul class="contentsLists">
                
                <li class="listItem"  id="01">
                    <div class="listItem__imgArea">
                        <a href="01.php">
                            <div class="badge--fill">
                                <span>01</span>
                            </div>
                            <img src="assets/images/01.jpg" alt="">
                        </a>
                    </div>
                </li>
                
                <li class="listItem"  id="02">
                    <div class="listItem__imgArea">
                        <a href="02.php">
                            <div class="badge--fill">
                                <span>02</span>
                            </div>
                            <img src="assets/images/02.jpg" alt="">
                        </a>
                    </div>
                </li>
                
                <li class="listItem"  id="03">
                    <div class="listItem__imgArea">
                        <a href="03.php">
                            <div class="badge--fill">
                                <span>03</span>
                            </div>
                            <img src="assets/images/03.jpg" alt="">
                        </a>
                    </div>
                </li>
                
                <li class="listItem"  id="04">
                    <div class="listItem__imgArea">
                        <a href="04.php">
                            <div class="badge--fill">
                                <span>04</span>
                            </div>
                            <img src="assets/images/04.jpg" alt="">
                        </a>
                    </div>
                </li>
                
                <li class="listItem"  id="05">
                    <div class="listItem__imgArea">
                        <a href="05.php">
                            <div class="badge--fill">
                                <span>05</span>
                            </div>
                            <img src="assets/images/05.jpg" alt="">
                        </a>
                    </div>
                </li>
                
                <li class="listItem"  id="06">
                    <div class="listItem__imgArea">
                        <a href="06.php">
                            <div class="badge--fill">
                                <span>06</span>
                            </div>
                            <img src="assets/images/06.jpg" alt="">
                        </a>
                    </div>
                </li>
                
                <li class="listItem"  id="07">
                    <div class="listItem__imgArea">
                        <a href="07.php">
                            <div class="badge--fill">
                                <span>07</span>
                            </div>
                            <img src="assets/images/07.jpg" alt="">
                        </a>
                    </div>
                </li>
                
                <li class="listItem"  id="08">
                    <div class="listItem__imgArea">
                        <a href="08.php">
                            <div class="badge--fill">
                                <span>08</span>
                            </div>
                            <img src="assets/images/08.jpg" alt="">
                        </a>
                    </div>
                </li>
                
                <li class="listItem"  id="09">
                    <div class="listItem__imgArea">
                        <a href="09.php">
                            <div class="badge--fill">
                                <span>09</span>
                            </div>
                            <img src="assets/images/09.jpg" alt="">
                        </a>
                    </div>
                </li>
                
                <li class="listItem"  id="10">
                    <div class="listItem__imgArea">
                        <a href="10.php">
                            <div class="badge--fill">
                                <span>10</span>
                            </div>
                            <img src="assets/images/10.jpg" alt="">
                        </a>
                    </div>
                </li>
                
                <li class="listItem"  id="11">
                    <div class="listItem__imgArea">
                        <a href="11.php">
                            <div class="badge--fill">
                                <span>11</span>
                            </div>
                            <img src="assets/images/11.jpg" alt="">
                        </a>
                    </div>
                </li>
                
                <li class="listItem"  id="12">
                    <div class="listItem__imgArea">
                        <a href="12.php">
                            <div class="badge--fill">
                                <span>12</span>
                            </div>
                            <img src="assets/images/12.jpg" alt="">
                        </a>
                    </div>
                </li>
                
            </ul>
        </div>
    </section>
    