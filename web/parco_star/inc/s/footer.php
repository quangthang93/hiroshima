<div class="footer">
    <p class="btn-pagetop"><a href="#" class="page-scroll"><img src="assets/images/btn_pagetop.png" alt="ページトップ" width="71" height="28"></a></p>
    <div class="footer__in">

        <ul class="snsnlist">
            <li class="snavi02">
                <div class="fb-like" data-href="http://hiroshima.parco.jp/web/parco_star/" data-layout="button" data-action="like" data-size="small" data-show-faces="true" data-share="false"></div>
            </li>
            <li class="snavi01"><a href="https://twitter.com/share" class="twitter-share-button" data-url="http://hiroshima.parco.jp/web/parco_star/" data-text="パルコのスター｜広島PARCO" data-lang="ja" data-count="none">ツイート</a>
                <script>
                    ! function(d, s, id) {
                        var js, fjs = d.getElementsByTagName(s)[0],
                            p = /^http:/.test(d.location) ? 'http' : 'https';
                        if (!d.getElementById(id)) {
                            js = d.createElement(s);
                            js.id = id;
                            js.src = p + '://platform.twitter.com/widgets.js';
                            fjs.parentNode.insertBefore(js, fjs);
                        }
                    }(document, 'script', 'twitter-wjs');
                </script>
            </li>
            <li class="snavi03">

                <!-- +1 ボタン を表示したい位置に次のタグを貼り付けてください。 -->
                <div class="g-plusone" data-size="tall" data-annotation="none" data-href="http://hiroshima.parco.jp/web/parco_star/"></div>

                <!-- 最後の +1 ボタン タグの後に次のタグを貼り付けてください。 -->
                <script type="text/javascript">
                    window.___gcfg = {
                        lang: 'ja'
                    };

                    (function() {
                        var po = document.createElement('script');
                        po.type = 'text/javascript';
                        po.async = true;
                        po.src = 'https://apis.google.com/js/platform.js';
                        var s = document.getElementsByTagName('script')[0];
                        s.parentNode.insertBefore(po, s);
                    })();
                </script>
            </li>
            <li class="snavi04">
                <div class="line-it-button" data-lang="ja" data-type="share-a" data-url="http://hiroshima.parco.jp/web/parco_star/" style="display: none;"></div>
                <script src="https://d.line-scdn.net/r/web/social-plugin/js/thirdparty/loader.min.js" async="async" defer="defer"></script>

            </li>

        </ul>

        <div class="parco"><a href="/"><img src="assets/images/btn_sitetop.png" alt="SITE TOP" width="186" height="10" /></a></div>
        <div class="copyright"><img src="assets/images/txt_copyright.png" alt="COPYRIGHT (C) PARCO CO., LTD. ALL RIGHTS RESERVED." width="198" height="9" /></div>


    </div>
</div>