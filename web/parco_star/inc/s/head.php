<head>

    <meta charset="UTF-8">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">


    <?php include_once($_SERVER['DOCUMENT_ROOT'].'/web/parco_star/inc/common/meta.php'); ?>

    <link rel="canonical" href="http://hiroshima.parco.jp/web/parco_star/" />

    <link rel="stylesheet" href="assets/css/slick.css">
    <link rel="stylesheet" href="assets/css/slick-theme.css">
    <link rel="stylesheet" href="index.css">
</head>