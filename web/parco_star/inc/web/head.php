<head>

    <meta charset="UTF-8">
    <script>
        var ua = navigator.userAgent;
        var redirectPass = '../../s/parco_star/';

        if ((ua.indexOf('iPhone') > 0 && ua.indexOf('iPad') == -1) || ua.indexOf('iPod') > 0 || (ua.indexOf('Android') > 0 && ua.indexOf('Mobile') > 0)) {
            location.href = redirectPass;
        }
    </script>

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=1240, initial-scale=1.0">

    <?php include_once($_SERVER['DOCUMENT_ROOT'].'/web/parco_star/inc/common/meta.php'); ?>


    <link rel="alternate" media="only screen and (max-width: 640px)" href="http://hiroshima.parco.jp/s/parco_star/" />

    <link rel="stylesheet" href="assets/css/slick.css">
    <link rel="stylesheet" href="assets/css/slick-theme.css">
    <link rel="stylesheet" href="index.css">
</head>