<footer class="footer">
    <div class="widthBinder">
        <div class="footerArea">
            <div class="footerItem">
                <a href="/"><img src="assets/images/char_sitetop.svg" alt=""></a>
            </div>
            <div class="footerItem">
                <img src="assets/images/char_cpr.svg" alt="">
            </div>
            <div class="footerItem">
                <img class="follow" src="assets/images/char_follow.svg" alt="">
                <a href="http://www.facebook.com/hiroshimaparco/" target="_blank">
                    <img src="assets/images/ico_fb.svg" alt="">
                </a>
                <a href="https://twitter.com/parco_hiroshima" target="_blank">                
                    <img src="assets/images/ico_tw.svg" alt="">
                </a>
                <a href="https://plus.google.com/114250524026298727909/" target="_blank">                
                    <img src="assets/images/ico_googleplus.svg" alt="">
                </a>
            </div>

        </div>
    </div>
</footer>