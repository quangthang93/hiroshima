;(function(){
    var $sliderTarget =  $('.contentsLists')
    $sliderTarget.slick({
        infinite: true,
        slidesToShow: 3,
        slidesToScroll: 3,
        variableWidth:true,
      })
      
    //   Slider移動
      var id = getFileName().replace( /.php/g , "" );
      var num = parseInt(id , 10)

      $sliderTarget.slick('slickGoTo', num);
      

    //  Add Class
    $('#'+id).addClass('currentarticle')

      function getFileName() {
        return window.location.href.match(".+/(.+?)([\?#;].*)?$")[1];
    }
})()