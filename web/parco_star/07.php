<!DOCTYPE html>
<html lang="ja">

<?php include_once($_SERVER['DOCUMENT_ROOT'].'/web/parco_star/inc/web/head.php'); ?>
        

<body>
<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-5FDDGB" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src='//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);})(window,document,'script','dataLayer','GTM-5FDDGB');</script>
<!-- End Google Tag Manager -->
<svg display="none" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
    <defs>
        <symbol id="char_shopinfo"  viewBox="0 0 57 9">
            <path class="cls-1" d="M3.5,1.35A2.33,2.33,0,0,0,2,.59a1,1,0,0,0-1.09,1c0,.36.07.68,1,1.23l.54.33C3.7,3.94,4,4.3,4,5.09A1.83,1.83,0,0,1,2,7a2.51,2.51,0,0,1-2-1.1l.54-.43A1.81,1.81,0,0,0,2,6.43,1.25,1.25,0,0,0,3.33,5.18c0-.54,0-.73-1.23-1.45l-.61-.37A2,2,0,0,1,.24,1.67,1.66,1.66,0,0,1,2.06,0,2.82,2.82,0,0,1,3.92.84Z"
            />
            <path class="cls-1" d="M6.48,3.09H8.83v-3h.7V6.89h-.7V3.68H6.48V6.89h-.7V.13h.7Z" />
            <path class="cls-1" d="M13.28,0c2,0,2.07,2.25,2.07,3.51,0,.9.07,3.51-2.07,3.51s-2.07-2.55-2.07-3.51C11.21.68,12.14,0,13.28,0Zm1.37,3.51c0-.62.09-2.92-1.37-2.92s-1.37,2.52-1.37,2.92c0,.77,0,2.92,1.37,2.92C14,6.43,14.65,6,14.65,3.51Z"
            />
            <path class="cls-1" d="M17.35.13H19A1.82,1.82,0,0,1,20.9,2.2c0,1.43-.94,2-2.26,2h-.59V6.89h-.7Zm1.48,3.51A1.29,1.29,0,0,0,20.2,2.21c0-1.06-.49-1.48-1.32-1.48h-.83V3.64Z"
            />
            <path class="cls-1" d="M29.92.72H29V6.3h.92v.59H27.39V6.3h.91V.72h-.91V.13h2.53Z" />
            <path class="cls-1" d="M32.55.13h.82l2.06,5h0v-5h.7V7h-.67L33.27,1.5l0,0V6.89h-.7Z" />
            <path class="cls-1" d="M42.14.72H39.22V3.07h2.19v.59H39.22V6.89h-.7V.13h3.62Z" />
            <path class="cls-1" d="M45.54,0c2,0,2.07,2.25,2.07,3.51,0,.9.07,3.51-2.07,3.51s-2.07-2.55-2.07-3.51C43.46.68,44.4,0,45.54,0Zm1.37,3.51c0-.62.09-2.92-1.37-2.92s-1.37,2.52-1.37,2.92c0,.77,0,2.92,1.37,2.92C46.24,6.43,46.91,6,46.91,3.51Z"
            />
        </symbol>
    </defs>
</svg>
<header class="ttlheaderArea" >
    <a href="index.php"><img src="assets/images/logo.svg" alt="パルコのスター"></a>
</header>
<main>

    
<?php include_once($_SERVER['DOCUMENT_ROOT'].'/web/parco_star/inc/common/07.php'); ?>


<?php include_once($_SERVER['DOCUMENT_ROOT'].'/web/parco_star/inc/common/banner.php'); ?>
</main>

<?php include_once($_SERVER['DOCUMENT_ROOT'].'/web/parco_star/inc/web/footer.php'); ?>



    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/js/slick.min.js"></script>
    <script src="index.js"></script>
</body>
</html>