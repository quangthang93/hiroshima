<?php header("Content-Type:text/html;charset=shift_jis");?>
<?php
$userAgent = null;
if(array_key_exists('HTTP_USER_AGENT', $_SERVER)) {
    $userAgent = $_SERVER['HTTP_USER_AGENT'];
}
if(!empty($userAgent)) {
    if ((strpos($userAgent, 'iPhone') !== false && strpos($userAgent, 'iPad') === false) || strpos($userAgent, 'iPod') !== false || strpos($userAgent, 'Android') !== false) {
        $dispMode = $_REQUEST['mode'];
        if($dispMode != "pc"){
            header("Location: /s/fashionweek/");
            exit;
        }
    }
}
?>
<!DOCTYPE html>
<html lang="ja">
<head>
    <meta charset="shift_jis">
    <meta name="viewport" content="width=1200, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>2017 広島ファッションウィーク</title>
    <meta name="description" content="ファッションで広島のマチを盛り上げる！ナイトランウェイをはじめ、ライブや豪華ゲストもぞくぞく登場。ここでしか体感できないイベントが盛りだくさん。">
    <meta name="keyword" content="広島,ファッション,ウィーク,イベント,ショー,ライブ,パルコ">

    <link rel="alternate" media="only screen and (max-width: 640px)" href="http://www.chushinren.jp/s/fashionweek/" />

    <meta property="og:locale" content="ja_JP" />
    <meta property="og:type" content="article" />
    <meta property="og:url" content="http://www.chushinren.jp/fashionweek/" />
    <meta property="og:title" content="2017 広島ファッションウィーク" />
    <meta property="og:description" content="ファッションで広島のマチを盛り上げる！ナイトランウェイをはじめ、ライブや豪華ゲストもぞくぞく登場。ここでしか体感できないイベントが盛りだくさん。" />
    <meta property="og:image" content="http://www.chushinren.jp/fashionweek/images/ogp.png" />

    <link rel="stylesheet" href="index.css">
</head>
<body>
<div id="fb-root"></div>
<script>(function(d, s, id) {
var js, fjs = d.getElementsByTagName(s)[0];
if (d.getElementById(id)) return;
js = d.createElement(s); js.id = id;
js.src = "//connect.facebook.net/ja_JP/sdk.js#xfbml=1&version=v2.7";
fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>

<script src="https://apis.google.com/js/platform.js" async defer>
{lang: 'ja'}
</script>


<div class="contentsWrapper" id="pagetop">
    <nav class="fixedBar">
        <div class="navbar">
            <div class="nav_left">
                <a href="#pagetop"><img src="images/nav_logo.png" alt=""></a>
            </div>
            <div class="nav_right">
                <div class="menu_btns">
                    <img class="imege__close" src="images/nav_close.png" alt="">
                    <img class="image__menu" src="images/nav_menu.png" alt="">
                </div>
                <div class="sns_btns">
                    <a href="https://twitter.com/hiroshima_fw" target="_blank"><img src="images/nav_twitter.png" alt=""></a><a href="https://www.facebook.com/hiroshimafashionweek" target="_blank"><img src="images/nav_fb.png" alt=""></a><a href="https://www.instagram.com/hiroshima_fw/" target="_blank"><img src="images/nav_insta.png" alt=""></a>
                </div>    
            </div>
        </div>
        <div class="navlistArea">
            <ul class="listItems listItems--menu">
                <li class="listItem menuItem">
                    <a href="#fashionwalk"><img src="images/menu01.png" alt=""></a>
                </li> 
                <li class="listItem menuItem">
                    <a href="#barwalk"><img src="images/menu02.png" alt=""></a>
                </li> 
                <li class="listItem menuItem">
                    <a href="#chance"><img src="images/menu03.png" alt=""></a>
                </li> 
                <li class="listItem menuItem comingsoon">
                    <a href="#eventsinfo"><img src="images/menu04.png" alt=""></a>
                </li> 
                <li class="listItem menuItem comingsoon">
                    <a href="#snsinfo"><img src="images/menu05.png" alt=""></a>
                </li> 
            </ul>
        </div>
    </nav>
    <div class="contensWidth contensWidth--mv sctarget">
        <div class="header__imgWrapper">
            <img class="header__image" src="images/mv.png" alt="">
        </div>
    </div>
    <div class="contensWidth">
        <div class="col-1 sctarget">
            <div class="contents" id="fashionwalk">
                <img src="images/event_01.jpg" alt="">
            </div>
        </div>
        <div class="col-2 sctarget">
            <div class="contents" id="barwalk">
                <img src="images/event_02.jpg" alt="">
            </div>
            <div class="contents" id="chance">
                <img src="images/event_03.jpg" alt="">
            </div>
        </div>
        <div class="col-2 sctarget">
            <div class="contents" id="eventsinfo">
                <img src="images/event_04.jpg" alt="">
            </div>
            <div class="contents" id="snsinfo">
                <img src="images/event_05.jpg" alt="">
            </div>
        </div>
    </div>
    <div class="pageWidth sctarget">
        <div class="followusonContents">
            <img class="image__centerFixed" src="images/followuson.png" alt="">
            <div class="icon__container">
                <a href="https://www.facebook.com/hiroshimafashionweek" target="_blank"><img src="images/ico_fb.png" alt=""></a>
                <a href="https://twitter.com/hiroshima_fw" target="_blank"><img src="images/ico_twitter.png" alt=""></a>
                <a href="https://www.instagram.com/hiroshima_fw/" target="_blank"><img src="images/ico_insta.png" alt=""></a>
            </div>
        </div>
    </div>
    <div class="contensWidth contensWidth--credits sctarget">
        <ul class="listItems">
            <li class="listItem">
                <p class="listItems__heading">
                    主 催
                </p>
                <div class="listItems__contents">  
                    <p class="listItem__contetns__ttl">
                         広島ファッションウィーク実行委員会
                    </p>
                    <p class="listItem__contents__body">
                        広島市中央部商店街振興組合連合会、広島市、広島商工会議所、広島ホームテレビ
                    </p>
                </div>
            </li>
        </ul>
    </div>


    <div class="sns-block">
        <ul class="sns_list">
            <li>
                <div class="fb-like" data-href="http://www.chushinren.jp/fashionweek/" data-layout="button" data-action="like" data-size="small"
                    data-show-faces="true" data-share="true"></div>
            </li>
            <li>
                <a href="https://twitter.com/share" class="twitter-share-button">Tweet</a>
            </li>
            <li>
                <div class="g-plusone" data-annotation="none"></div>
            </li>
        </ul>
    </div>
    <p class="copyright">&copy; HIROSHIMA FASHION WEEK ORGANIZATION 2017 , All Rights Reserved.</p>
</div>

<div class="btn_pagetop">
    <a href="#pagetop">
      <img src="images/btn_pagetop.png" width="62" height="62" alt="page top">
    </a>
</div>

<script src="lib/jquery-3.2.1.min.js"></script>
<script src="lib/anime.min.js"></script>
<script src="lib/scrollMonitor.js"></script>
<script src="index.js"></script>
</body>
</html>