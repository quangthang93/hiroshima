;(function(){

    // 
    // 
    // フェードインアニメーションの実装
    // 
    // 

    var targetElms = document.querySelectorAll('.sctarget');
    var targets = Array.prototype.slice.apply(targetElms);
    targets.forEach(makeWatcher);
    
    function addClass() {
        if (this.isInViewport) {
            fadeInUp(this.watchItem)
            this.destroy();
        }
    }

    function makeWatcher( element ) {
        var watcher = scrollMonitor.create( element );
        watcher.stateChange(addClass);
        addClass.call( watcher );
    }

    function fadeInUp(target){
        anime({
            targets: target,
            translateY:[250, 0],
            opacity:[0, 1],
            easing: 'easeOutQuart',
            delay: 350,
            duration:800
        })
    }

    // 
    // 
    // ComingSoonの場合、リンクさせずにする。
    // 
    //
    
    var $comingsoonElms = document.querySelectorAll('.comingsoon a');
    var $comingsoon = Array.prototype.slice.apply($comingsoonElms); 
    $comingsoon.forEach(function(x){
        x.addEventListener('click', function(e){
            e.preventDefault();
        })
    })



    $('.menuItem a').on('click',function(e){
        e.preventDefault();        
        if(!$(this).parent().hasClass('comingsoon')){
            var linkId = $(this).attr('href');
            scrollTo(linkId);
            $('.navlistArea').slideUp()
            $('.imege__close').hide()
        }
    })

    $('.btn_pagetop a').on('click',function(e){
        e.preventDefault();
        scrollTo($(this).attr('href'));
    })

    $('.btn_pagetop a,.nav_left a').on('click',function(e){
        e.preventDefault();
        scrollTo($(this).attr('href'));
    })


   

    function scrollTo(linktarget){
        var position = $(linktarget).offset().top;
        anime({
            targets:'html,body',
            scrollTop:position - 150,
            easing: 'easeInOutQuart'
        })
    }

    // 
    // 
    // Menuの開け閉め
    // 
    //
    $('.image__menu').on('click',function(){
        $('.navlistArea').slideToggle();
        $('.imege__close').toggle();
    })

})()