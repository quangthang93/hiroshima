<!DOCTYPE html>
<!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7 le6"><![endif]-->
<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8 ie7"><![endif]-->
<!--[if IE 8]><html class="no-js lt-ie9 ie8"><![endif]-->
<!--[if gt IE 8]><!--><html class="no-js"><!--<![endif]-->

<head>
<meta charset="Shift_JIS">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width,user-scalable=no">
<title>2016 広島ファッションウィーク</title>
<meta name="description" content="ファッションで広島のマチを盛り上げる！ナイトランウェイをはじめ、ライブや豪華ゲストもぞくぞく登場。ここでしか体感できないイベントが盛りだくさん。">
<meta name="keyword" content="広島,ファッション,ウィーク,イベント,ショー,ライブ,アリスガーデン,パルコ">

<link rel="canonical" href="/fashionweek/" />

<?php /* OGP */ ?>
<meta property="og:locale" content="ja_JP" />
<meta property="og:type" content="website" />
<meta property="og:title" content="2016 HIROSHIMA FASHION WEEK" />
<meta property="og:description" content="ファッションで広島のマチを盛り上げる！ナイトランウェイをはじめ、ライブや豪華ゲストもぞくぞく登場。ここでしか体感できないイベントが盛りだくさん。" />
<meta property="og:image" content="http://www.chushinren.jp/fashionweek/images/common/ogp.jpg" />

<?php /* stylesheet */ ?>
<!-- <link rel="stylesheet" href="css/animate.css"> -->
<link rel="stylesheet" href="css/common.css">
<link rel="stylesheet" href="css/home.css">

<?php /* scripts */ ?>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="js/jquery.js"><\/script>')</script>
<script src="js/_src/modernizr.js"></script>
<script src="js/_src/scrollsmoothly.js"></script>
<!-- <script src="js/_src/wow.min.js"></script> -->
<script src="js/common.js"></script>


<?php /* wow初期化 */ ?>
<script>
new WOW().init();
</script>

<?php /*  GAタグ */ ?>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-83609823-1', 'auto');
  ga('send', 'pageview');

</script>
</head>

<body id="Top" class="home">

<?php /* SNS */ ?>
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/ja_JP/sdk.js#xfbml=1&version=v2.7";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>

<script src="https://apis.google.com/js/platform.js" async defer>
  {lang: 'ja'}
</script>

<div class="wrapper">

  <div class="border_box">
    <i class="border top"></i>
    <i class="border right"></i>
    <i class="border left"></i>
  <!-- border_box --></div>

  <div class="menu menu_open">
    <img src="images/common/menu_open.png" width="48" height="48" alt="menu">
  <!-- menu_open --></div>

  <div class="menu menu_wrapper">
    <div class="menu_close">
      <img src="images/common/menu_close.png" width="23" height="23" alt="menu close">
    </div>
    <ul class="menu_list">
      <li>
        <a href="index.php">
          <img src="images/common/menu_list01.png" width="33" height="21" alt="TOP">
        </a>
      </li>
      <li>
        <a href="runway.php">
          <img src="images/common/menu_list02.png" width="128" height="21" alt="NIGHT RUNWAY">
        </a>
      </li>
      <li>
        <a href="http://www.chushinren.jp/fashionweek/event/">
          <img src="images/common/menu_list03.png" width="49" height="20" alt="EVENT">
        </a>
      </li>
      <li>
        <a href="https://parcophotos.shuttlerock.com/content/hiroshimafashionweek16" target="blank">
          <img src="images/common/menu_list04.png" width="105" height="21" alt="SOCIAL FEED">
        </a>
      </li>

    </ul>
  <!-- menu_wrapper --></div>

  <div class="main-block">
    <div class="main">
      <img src="images/home/main.png" width="100%" alt="HIROSHIMA FASHION WEEK">
    </div>

    <div class="guest">
      <img src="images/home/guest_list.jpg" width="100%" alt="NIGHT RUNWAY SPECIAL GUEST">
    </div>

    <ul class="link_list">
      <li>
        <a href="runway.php">
          <img src="images/home/list01.jpg" width="100%" alt="Night Runway">
        </a>
      </li>
      <li>
        <a href="http://www.chushinren.jp/fashionweek/event/">
          <img src="images/home/list02.jpg" width="100%" alt="イベント情報はコチラ！">
        </a>
      </li>
      <li>
        <a href="https://parcophotos.shuttlerock.com/content/hiroshimafashionweek16" target="blank">
          <img src="images/home/list03.jpg" width="100%" alt="#広島ファッションウィーク">
        </a>
      </li>
    </ul>
  <!-- main-block --></div>

  <div class="txt_area">
    <div class="txt_area__in">
      ドレスコードは<br>
      「HIROSHIMA RED」！！<br>
      期間中テーマカラーである<br>
      「赤」の服や小物などを<br>
      身に着けている方には<br>
      ステキな特典があるかも♪
    </div>
  <!-- txt_area --></div>

  <div class="contents01 mb60"><img src="images/home/contents01.png" width="100%" alt="オシャレdeチャンス"></div>

  <div class="sns_block01">
    <div class="sns_block01__in">
      <div class="txt"><img src="images/home/sec02_txt.png" width="226" height="94" alt="FOLLOW US ON"></div>
      <ul class="sns_list"><!--
        --><li>
            <a href="https://www.facebook.com/hiroshimafashionweek/" target="blank">
              <img src="images/home/sns01.png" width="60" height="60" alt="Facebook">
            </a>
           </li><!--
        --><li>
            <a href="https://twitter.com/hiroshima_fw" target="blank">
              <img src="images/home/sns02.png" width="60" height="60" alt="Twitter">
            </a>
           </li><!--
        --><li>
            <a href="https://www.instagram.com/hiroshima_fw/" target="blank">
              <img src="images/home/sns03.png" width="60" height="60" alt="Instagram">
            </a>
           </li><!--
      --></ul>
    </div>
  <!-- sns_list --></div>

  <div class="txt-block01">
    <div class="list">
      <div class="ttl">主催</div>
      <p class="txt">
        <span> 広島ファッションウィーク実行委員会</span><br>
        <br>
        広島市　広島商工会議所　中国新聞社　 広島市中央部商店街振興組合連合会<br>
        パルコ広島店　福屋　そごう広島店　アクア広島センター街　基町クレド・パセーラテナント会
      </p>
    <!-- list --></div>
    <div class="list">
      <div class="ttl">後援</div>
      <p class="txt">
        NHK広島放送局・中国放送・広島テレビ・テレビ新広島・広島ホームテレビ・広島エフエム放送・FMちゅーピー76.6MHZ
      </p>
    <!-- list --></div>
    <div class="list">
      <div class="ttl">協賛</div>
      <p class="txt">(株)クレディセゾン</p>
    <!-- list --></div>
  <!-- txt-block01 --></div>

  <div class="sns-block02">
    <ul class="sns_list">
      <li>
        <div class="fb-like" data-href="http://www.chushinren.jp/s/fashionweek/" data-layout="button" data-action="like" data-size="small" data-show-faces="true" data-share="true"></div>
      </li>
      <li>
        <a href="https://twitter.com/share" class="twitter-share-button">Tweet</a>
      </li>
      <li>
        <div class="g-plusone" data-annotation="none"></div>
      </li>
    </ul>
  <!-- sns-block --></div>

  <p class="copyright">&copy; HIROSHIMA FASHION WEEK ORGANIZATION 2016 , <br>All Rights Reserved.</p>

<!--   <div class="pagetop">
    <a href="#Top">
      <img src="images/common/btn_pagetop.png" width="42" height="42" alt="page top">
    </a>
  </div> -->
<!-- wrapper --></div>
</body>
</html>
