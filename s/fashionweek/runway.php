<!DOCTYPE html>
<!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7 le6"><![endif]-->
<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8 ie7"><![endif]-->
<!--[if IE 8]><html class="no-js lt-ie9 ie8"><![endif]-->
<!--[if gt IE 8]><!--><html class="no-js"><!--<![endif]-->

<head>
<meta charset="Shift_JIS">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width,user-scalable=no">
<title>ナイトランウェイ｜2016 HIROSHIMA FASHION</title>
<meta name="description" content="10/8（土）開催のナイトランウェイ！最旬ファッションが夜のランウェイを駆け抜ける！">
<meta name="keyword" content="ナイトランウェイ,広島,ファッション,ウィーク,イベント,ショー,ライブ,アリスガーデン,パルコ">

<link rel="canonical" href="/fashionweek/runway.php" />

<?php /* OGP */ ?>
<meta property="og:locale" content="ja_JP" />
<meta property="og:type" content="website" />
<meta property="og:title" content="2016 HIROSHIMA FASHION WEEK" />
<meta property="og:description" content="ファッションで広島のマチを盛り上げる！ナイトランウェイをはじめ、ライブや豪華ゲストもぞくぞく登場。ここでしか体感できないイベントが盛りだくさん。" />
<meta property="og:image" content="http://www.chushinren.jp/fashionweek/images/common/ogp.jpg" />

<?php /* stylesheet */ ?>
<!-- <link rel="stylesheet" href="css/animate.css"> -->
<link rel="stylesheet" href="css/common.css">
<link rel="stylesheet" href="css/runway.css">

<?php /* scripts */ ?>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="js/jquery.js"><\/script>')</script>
<script src="js/_src/modernizr.js"></script>
<script src="js/_src/scrollsmoothly.js"></script>
<script src="js/common.js"></script>


<?php /*  GAタグ */ ?>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-83609823-1', 'auto');
  ga('send', 'pageview');

</script>
</head>

<body id="Top" class="home">

<?php /* SNS */ ?>
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/ja_JP/sdk.js#xfbml=1&version=v2.7";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>

<script src="https://apis.google.com/js/platform.js" async defer>
  {lang: 'ja'}
</script>

<div class="wrapper">

  <div class="border_box">
    <i class="border top"></i>
    <i class="border right"></i>
    <i class="border left"></i>
  <!-- border_box --></div>

  <div class="menu menu_open">
    <img src="images/common/menu_open.png" width="48" height="48" alt="menu">
  <!-- menu_open --></div>

  <div class="menu menu_wrapper">
    <div class="menu_close">
      <img src="images/common/menu_close.png" width="23" height="23" alt="menu close">
    </div>
    <ul class="menu_list">
      <li>
        <a href="index.php">
          <img src="images/common/menu_list01.png" width="33" height="21" alt="TOP">
        </a>
      </li>
      <li>
        <a href="runway.php">
          <img src="images/common/menu_list02.png" width="128" height="21" alt="NIGHT RUNWAY">
        </a>
      </li>
      <li>
        <a href="http://www.chushinren.jp/fashionweek/event/">
          <img src="images/common/menu_list03.png" width="49" height="20" alt="EVENT">
        </a>
      </li>
      <li>
        <a href="https://parcophotos.shuttlerock.com/content/hiroshimafashionweek16" target="blank">
          <img src="images/common/menu_list04.png" width="105" height="21" alt="SOCIAL FEED">
        </a>
      </li>

    </ul>
  <!-- menu_wrapper --></div>

  <div class="main-block">
    <div class="main">
      <img src="images/runway/main.png" width="100%" alt="HIROSHIMA FASHION WEEK">
    <!-- main --></div>
    <div class="ttl_guest">
      <div class="ttl_guest__in">
        <img src="images/runway/ttl_guest.png" width="181" height="66" alt="SPECIAL GUEST">
      </div>
    <!-- ttl_guest --></div>
  <!-- main-block --></div>

  <div class="guest_area">
    <div class="guest">
      <div class="photo"><img src="images/runway/guest01.jpg" width="100%" alt="西山 茉希"></div>
      <div class="ttl">西山 茉希</div>
      <p class="txt">
        1985年11月16日生まれ、新潟県出身。O型。
        2005年より雑誌「CanCam」の専属モデルを務め、主に同世代の女性から支持を集め人気を博す。現在はバラエティ番組にも多く出演し、活動の幅を広げている。
      </p>
    <!-- guest --></div>
    <div class="guest">
      <div class="photo"><img src="images/runway/guest02.jpg" width="100%" alt="DJ DC BRAND'S"></div>
      <div class="ttl">DJ DC BRAND'S</div>
      <p class="txt">
          DC BRAND’S（ディーシー・ブランズ）-SINCE2016-<br>
          DJ SAILORS by 掟ポルシェ ＋ DJ K-FACTORY by ミッツィー申し訳 ＋ DJ Karl Helmut by 藤井隆を中心に、DJ LUNA MATTINO by アキオクマガイ、DJ PERSON’S、DJ IXI:Zなど擁する、80~90年代のELECTRO DANCE MUSIC「Hi-NRG」に特化したDJ TEAM。<br>
          高価で綺羅びやかなDCブランドファッションを身に纏い、ANALOG,CDJ,SAMPLERを駆使したDJ PLAYに加え、DANCE,LIVEなど予想もつかないアンチファストなGIG STYLEはサプールの進化形と評される。<br>
          NO BUBBLE!, NO IMPORT BRAND!, MORE PWL!を基調にEDM界へ殴りこみをかけるFASHION ICON型DJ TEAM。
      </p>
    <!-- guest --></div>
  <!-- guest_area --></div>

  <div class="txt01"><img src="images/runway/ttl_more.png" width="124" height="25" alt="AND MORE!"></div>

  <div class="contents_photo type01"><img src="images/runway/contents01.jpg" width="100%" alt="FASHON SHOW"></div>
  <div class="contents_photo type02"><img src="images/runway/contents02.jpg" width="100%" alt="学生ファッションショー"></div>

  <div class="txt_time"><img src="images/runway/txt_time.png" width="100%" alt="TIME SCHEDULE"></div>

  <div class="sns-block02">
    <ul class="sns_list">
      <li>
        <div class="fb-like" data-href="http://www.chushinren.jp/s/fashionweek/runway.php" data-layout="button" data-action="like" data-size="small" data-show-faces="true" data-share="true"></div>
      </li>
      <li>
        <a href="https://twitter.com/share" class="twitter-share-button">Tweet</a>
      </li>
      <li>
        <div class="g-plusone" data-annotation="none"></div>
      </li>
    </ul>
  <!-- sns-block --></div>

  <p class="copyright">&copy; HIROSHIMA FASHION WEEK ORGANIZATION 2016 , <br>All Rights Reserved.</p>

<!--   <div class="pagetop">
    <a href="#Top">
      <img src="images/common/btn_pagetop.png" width="42" height="42" alt="page top">
    </a>
  </div> -->
<!-- wrapper --></div>
</body>
</html>
