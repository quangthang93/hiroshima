<?php header("Content-Type:text/html;charset=shift_jis");?>
<!DOCTYPE html>
<html lang="js">
<head>
    <meta charset="Shift_JIS">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,user-scalable=no">
    <title>2017 広島ファッションウィーク</title>
    <meta name="description" content="ファッションで広島のマチを盛り上げる！ナイトランウェイをはじめ、ライブや豪華ゲストもぞくぞく登場。ここでしか体感できないイベントが盛りだくさん。">
    <meta name="keyword" content="広島,ファッション,ウィーク,イベント,ショー,ライブ,パルコ">

    <link rel="canonical" href="http://www.chushinren.jp/fashionweek/" />

    <meta property="og:locale" content="ja_JP" />
    <meta property="og:type" content="article" />
    <meta property="og:url" content="http://www.chushinren.jp/fashionweek/" />
    <meta property="og:title" content="2017 広島ファッションウィーク" />
    <meta property="og:description" content="ファッションで広島のマチを盛り上げる！ナイトランウェイをはじめ、ライブや豪華ゲストもぞくぞく登場。ここでしか体感できないイベントが盛りだくさん。" />
    <meta property="og:image" content="http://www.chushinren.jp/fashionweek/images/ogp.png" />



    <link rel="stylesheet" href="index.css">

</head>
<body>
<div id="fb-root"></div>
<script>
    (function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/ja_JP/sdk.js#xfbml=1&version=v2.7";
    fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
</script>
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
<script src="https://apis.google.com/js/platform.js" async defer>{lang: 'ja'}</script>


<div class="contentsWrapper" id='pagetop'>

    <div class="nav_barger">
        <img class="icon_barger" src="images/nav_barger.png" alt="">
    </div>
    <div class="nav_cross">
        <img class="icon_cross" src="images/nav_cross.png" alt="">
    </div>
    <div class="navlistArea">
        <ul class="listItems">
            <li class="listItem menuItem">
                <a href="#fashionwalk"><img width="157" src="images/nav_menu01.png" alt=""></a>
            </li> 
            <li class="listItem menuItem">
                <a href="#barwalk"><img width="158" src="images/nav_menu02.png" alt=""></a>
            </li> 
            <li class="listItem menuItem">
                <a href="#chance"><img width="174" src="images/nav_menu03.png" alt=""></a>
            </li> 
            <li class="listItem comingsoon menuItem">
                <a href="#eventsinfo"><img width="251" src="images/nav_menu04.png" alt=""></a>
            </li> 
            <li class="listItem comingsoon menuItem">
                <a href="#snsinfo"><img width="264" src="images/nav_menu05.png" alt=""></a>
            </li> 
        </ul>
    </div>

    <div class="contensWidth contensWidth--mv sctarget">
        <div class="header__imgWrapper">
            <img class="header__image" src="images/mv.png" alt="">
        </div>
    </div>
    <div class="contensWidth">
        <div class="col-1 sctarget" id="fashionwalk">
            <div class="contents">
                <img src="images/event_01.jpg" alt="">
            </div>
        </div>
        <div class="col-1 sctarget" id="barwalk">
            <div class="contents">
                <img src="images/event_02.jpg" alt="">
            </div>
        </div>
        <div class="col-1 sctarget"  id="chance">
            <div class="contents">
                <img src="images/event_03.jpg" alt="">
            </div>
        </div>
        <div class="col-1 sctarget"  id="eventsinfo">
            <div class="contents">
                <img src="images/event_04.jpg" alt="">
            </div>
        </div>
        <div class="col-1 sctarget" id="snsinfo">
            <div class="contents">
                <img src="images/event_05.jpg" alt="">
            </div>
        </div>
    </div>
    <div class="contensWidth sctarget">
        <div class="followusonContents">
            <img class="image__centerFixed" src="images/followuson.png" alt="">
            <div class="icon__container">
                <a href="https://www.facebook.com/hiroshimafashionweek" target="_blank"><img src="images/ico_fb.png" alt=""></a>
                <a href="https://twitter.com/hiroshima_fw" target="_blank"><img src="images/ico_twitter.png" alt=""></a>
                <a href="https://www.instagram.com/hiroshima_fw/" target="_blank"><img src="images/ico_insta.png" alt=""></a>
            </div>
        </div>
    </div>
    <div class="contensWidth sctarget">
        <img src="images/credits.png" alt="">
    </div>


    <div class="sns-block">
        <ul class="sns_list">
            <li>
                <div class="fb-like" data-href="http://www.chushinren.jp/fashionweek/" data-layout="button" data-action="like" data-size="small"
                    data-show-faces="true" data-share="true"></div>
            </li>
            <li>
                <a href="https://twitter.com/share" class="twitter-share-button">Tweet</a>
            </li>
            <li>
                <div class="g-plusone" data-annotation="none"></div>
            </li>
        </ul>
    </div>
    <p class="copyright">&copy; HIROSHIMA FASHION WEEK ORGANIZATION 2017 , All Rights Reserved.</p>
</div>

<div class="btn_pagetop">
    <a href="#pagetop">
      <img src="images/btn_pagetop.png" width="42" height="42" alt="page top">
    </a>
</div>

<script src="lib/jquery-3.2.1.min.js"></script>
<script src="lib/anime.min.js"></script>
<script src="lib/scrollMonitor.js"></script>
<script src="index.js"></script>
</body>
</html>
    
</body>
</html>



