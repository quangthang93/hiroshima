;(function(){

    // 
    // 
    // フェードインアニメーションの実装
    // 
    // 

    var targetElms = document.querySelectorAll('.sctarget');
    var targets = Array.prototype.slice.apply(targetElms);
    targets.forEach(makeWatcher);
    
    function addClass() {
        if (this.isInViewport) {
            fadeInUp(this.watchItem)
            this.destroy();
        }
    }

    function makeWatcher( element ) {
        var watcher = scrollMonitor.create( element );
        watcher.stateChange(addClass);
        addClass.call( watcher );
    }

    function fadeInUp(target){
        anime({
            targets: target,
            translateY:[250, 0],
            opacity:[0, 1],
            easing: 'easeOutQuart',
            delay: 350,
            duration:800
        })
    }

    // 
    // 
    // Menuの開け閉め
    // 
    //
    $('.nav_barger').on('click', function(){
        openMenu()
    })

    $('.nav_cross').on('click',function(){
        closeMenu()
    })

    // ページ内リンク

    $('.menuItem a').on('click',function(e){
        e.preventDefault();        
        if(!$(this).parent().hasClass('comingsoon')){
            var linkId = $(this).attr('href');
            scrollTo(linkId);
            closeMenu()
        }
    })

    $('.btn_pagetop a').on('click',function(e){
        e.preventDefault();
        scrollTo($(this).attr('href'));
    })

    

    function openMenu(){
        slideIn('.navlistArea');
        $('.nav_barger').hide();
        $('.nav_cross').show();
    }

    function closeMenu(){
        slideOut('.navlistArea');
        $('.nav_cross').hide();
        $('.nav_barger').show();
    }

    function slideIn(target){
        anime({
            targets:target,
            translateX:['100%', 0],
            easing: 'easeInOutQuart'
        })
    }

    function slideOut(target){
        anime({
            targets:target,
            translateX:[0, '100%'],
            easing: 'easeInOutQuart'
        })
    }

    function scrollTo(linktarget){
        var position = $(linktarget).offset().top;
        anime({
            targets:'html,body',
            scrollTop:position,
            easing: 'easeInOutQuart'
        })
    }


})()