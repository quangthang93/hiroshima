<?php
include_once($_SERVER['DOCUMENT_ROOT']. '/web/annex15th/inc/config.php');

$title = TITLE;
$description = DESCRIPTION1;
$keywords = KEYWORDS1;
define('PAGE_NAME', 'top');

?><!DOCTYPE html>
<html lang="ja">
<head>
<?php include_once($_SERVER['DOCUMENT_ROOT'].'/s/'.DIRNAME.'/inc/html-head.php'); ?>
</head>

<body class="top">
<?php include_once($_SERVER['DOCUMENT_ROOT'] . '/load_script.php'); ?>
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/ja_JP/sdk.js#xfbml=1&appId=500009400047380&version=v2.0";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<a name="top" id="top"></a>

<?php include_once($_SERVER['DOCUMENT_ROOT'].'/s/'.DIRNAME.'/inc/header.php'); ?>

<div class="top-contents">
<div class="top-contents__in">

<h1 class="top-title"><img src="images/title2.png" alt="<?=TITLE?>" width="104" height="47" /></h1>
<p class="top-catch"><img src="images/catch.png" alt="毎日に、ドキドキを。広島に、ワクワクを。" width="162" height="47"></p>
<p class="top-date"><img src="images/date.png" alt="9/16（金） オープン" width="276" height="50" /></p>
<p class="top-text01"><img src="images/top-txt01.png" alt="本館・新館  大リニューアル!" width="264" height="20" /></p>

<div class="youtube-wrap mt40"><iframe width="560" height="315" src="https://www.youtube.com/embed/GyqCe0ACuj8?rel=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe></div>

<ul class="top-btns">
<li class="top-btns-item"><a href="newshop/"><img src="images/btn-newrenewal.png" width="100%" alt="NEW AND RENEWAL"></a></li>
<li class="top-btns-item"><a href="news/"><img src="images/btn-newsevent.png" width="100%" alt="NEWS AND EVENT"></a></li>
<li class="top-btns-item"><a href="star/"><img src="images/btn-star.jpg" width="100%" alt="パルコスター"></a></li>
<!-- 
<li class="top-btns-item"><a href="http://www.chushinren.jp/s/fashionweek/" target="_blank"><img src="images/bnr01.jpg" width="100%" alt="HIROSHIMA FASHION WEEK"></a></li>-->
</ul>

<!-- /top-contents__in --></div>
<!-- /top-contents --></div>

<?php include_once($_SERVER['DOCUMENT_ROOT']. '/s/'.DIRNAME.'/inc/footer.php'); ?>
</body>
</html>