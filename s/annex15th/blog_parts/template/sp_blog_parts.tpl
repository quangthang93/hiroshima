{{#data.result.items}}
<div class="thumbnail_block divc">
    <div class="image">
        <div{{^image.type}} class="square_img"{{/image.type}}>
            <a href="{{url_sp}}"{{#image.type}} class="imgLiquidBox"{{/image.type}}{{#view.view_link_target}} target="{{view.view_link_target}}"{{/view.view_link_target}}><img src="{{image.L_url}}" alt="{{title}}" /></a>
        </div>
        {{#view.view_social}}
        <div class="like_number">
            <p>{{clipnum}}</p>
        </div>
        {{/view.view_social}}
        {{#storesjp_product_id}}
            {{^view.view_store_name}}<div class="kaeru_block"></div>{{/view.view_store_name}}
            {{#view.view_store_name}}<div class="kaeru_block_storename"></div>{{/view.view_store_name}}
        {{/storesjp_product_id}}
    </div>
    {{#view.view_store_name}}<p class="post_store">{{store_name}}PARCO</p>{{/view.view_store_name}}
    {{#view.view_shop_name}}<p class="shop_name">{{#view.view_floor_name}}<span class="label">{{floor.type}} {{floor.floor}}</span>{{/view.view_floor_name}}{{shop_name}}</p>{{/view.view_shop_name}}
    <p class="text_link">
    {{#event_date_isopen}}<span class="post_event_date"><img src="/s/common/images/common_icon_033.png" width="32"></span>{{/event_date_isopen}}
    {{#event_date_string}}<span class="post_event_date">{{event_date_string}}</span>{{/event_date_string}}
    <a href="{{url_sp}}"{{#view.view_link_target}} target="{{view.view_link_target}}"{{/view.view_link_target}}>{{title}}</a>
    </p>
</div>
{{/data.result.items}}