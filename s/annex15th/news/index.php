<?php
include_once($_SERVER['DOCUMENT_ROOT']. '/web/annex15th/inc/config.php');

$title = TITLE3.' | '.TITLE;
$description = DESCRIPTION3;
$keywords = KEYWORDS3;
define('PAGE_NAME', 'news');

?><!DOCTYPE html>
<html lang="ja">
<head>
<?php include_once($_SERVER['DOCUMENT_ROOT'].'/s/'.DIRNAME.'/inc/html-head.php'); ?>
</head>

<body class="news">
<?php include_once($_SERVER['DOCUMENT_ROOT'] . '/load_script.php'); ?>
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/ja_JP/sdk.js#xfbml=1&appId=500009400047380&version=v2.0";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<?php include_once($_SERVER['DOCUMENT_ROOT']. '/s/'.DIRNAME.'/inc/header.php'); ?>

<div class="contents">

<h2 class="ta-c"><img src="/s/<?=DIRNAME?>/images/ttl-<?=PAGE_NAME?>.png" height="23" alt="NEWS&amp;EVENT" /></h2>

<div class="line01"></div>

<div class="blog">
<div class="blog_parts_block"
  data-view-type="1"
  data-view-store-name="false"
  data-view-shop-name="true"
  data-ext-response-fields="storesjp_product_id"
  data-init-count="8"
  data-more-post="true"
  data-next-count="8"
  data-blog-type="3"
  data-category-id="<?=CATEGORY_ID1?>"
  data-category-id-or=""
  data-post-sdate="<?=POST_SDATE1?>"
  data-post-edate="<?=POST_EDATE1?>"
></div>
<!-- /blog --></div>

<!-- /contents --></div>

<?php include_once($_SERVER['DOCUMENT_ROOT']. '/s/'.DIRNAME.'/inc/footer.php'); ?>
</body>
</html>