<div class="footer">
<p class="btn-pagetop"><a href="#" class="page-scroll"><img src="/s/annex15th/images/btn_pagetop.png" alt="ページトップ" width="71" height="28"></a></p>
<div class="footer__in">

<ul class="snsnlist">
<li class="snavi02"><div class="fb-like" data-href="http://<?=STORE_NAME?>.parco.jp/s/annex15th/" data-layout="button" data-action="like" data-show-faces="false" data-share="false"></div></li>
<li class="snavi01"><a href="https://twitter.com/share" class="twitter-share-button" data-url="http://<?=STORE_NAME?>.parco.jp/s/annex15th/" data-text="<?=TITLE?>" data-lang="ja" data-count="none">ツイート</a>
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script></li>
<li class="snavi03">

<!-- +1 ボタン を表示したい位置に次のタグを貼り付けてください。 -->
<div class="g-plusone" data-size="tall" data-annotation="none" data-href="http://<?=STORE_NAME?>.parco.jp/s/annex15th/"></div>

<!-- 最後の +1 ボタン タグの後に次のタグを貼り付けてください。 -->
<script type="text/javascript">
  window.___gcfg = {lang: 'ja'};

  (function() {
    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
    po.src = 'https://apis.google.com/js/platform.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
  })();
</script></li>
<li class="snavi04"><script type="text/javascript" src="//media.line.me/js/line-button.js?v=20140411" ></script>
<script type="text/javascript">
new media_line_me.LineButton({"pc":false,"lang":"ja","type":"a","text":"http://<?=STORE_NAME?>.parco.jp/s/annex15th/","withUrl":false});
</script>
</li>

</ul>

<div class="parco"><a href="/"><img src="/s/annex15th/images/btn_sitetop.png" alt="SITE TOP" width="186" height="10" /></a></div>
<div class="copyright"><img src="/s/annex15th/images/txt_copyright.png" alt="COPYRIGHT (C) PARCO CO., LTD. ALL RIGHTS RESERVED." width="198" height="9" /></div>


</div>
</div>

<?php // include_once($_SERVER['DOCUMENT_ROOT']. '/web/cardoff/inc/modal.php'); ?>
