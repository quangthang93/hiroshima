<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title><?php echo $title ?></title>
<meta name="description" content="<?php echo $description ?>" />
<meta name="keywords" content="<?php echo $keywords ?>"/>
<?php include_once($_SERVER['DOCUMENT_ROOT'].'/web/'.DIRNAME.'/inc/ogp.php'); ?>
<link rel="canonical" href="/web/<?=DIRNAME?>" />
<link rel="stylesheet" href="/s/<?=DIRNAME?>/css/default.css" media="screen,print" />
<link rel="stylesheet" href="/s/<?=DIRNAME?>/blog_parts/css/blog_parts.css" media="all"/>
<link rel="stylesheet" href="/s/<?=DIRNAME?>/css/style.css" media="screen,print" />
<link rel="stylesheet" href="/s/<?=DIRNAME?>/css/<?=PAGE_NAME?>.css" />

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<!-- <script>window.jQuery || document.write('<script src="/s/<?=DIRNAME?>/js/jquery.js"><\/script>')</script> -->
<!-- <script src="/common/js/jquery.js"></script> -->
<script src="/s/<?=DIRNAME?>/js/masonary.min.js"></script>
<!--<script src="/s/<?=DIRNAME?>/js/jquery.transit.min.js"></script>-->
<script src="/s/<?=DIRNAME?>/js/common.js"></script>
<script src="/common/js/social.js"></script>
<script src="/common/js/imgLiquid.js"></script>
<script src="/s/<?=DIRNAME?>/blog_parts/js/jquery.tile.js"></script>
<script src="/s/<?=DIRNAME?>/blog_parts/js/hogan-2.0.0.js"></script>
<script src="/s/<?=DIRNAME?>/blog_parts/js/jquery.blog_parts.js"></script>

