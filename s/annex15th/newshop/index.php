<?php
include_once($_SERVER['DOCUMENT_ROOT']. '/web/annex15th/inc/config.php');

$title = TITLE2.' | '.TITLE;
$description = DESCRIPTION2;
$keywords = KEYWORDS2;
define('PAGE_NAME', 'newshop');

?><!DOCTYPE html>
<html lang="ja">
<head>
<?php include_once($_SERVER['DOCUMENT_ROOT'].'/s/'.DIRNAME.'/inc/html-head.php'); ?>

</head>

<body class="<?=PAGE_NAME?> pickup">
<?php include_once($_SERVER['DOCUMENT_ROOT'] . '/load_script.php'); ?>
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/ja_JP/sdk.js#xfbml=1&appId=500009400047380&version=v2.0";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<?php include_once($_SERVER['DOCUMENT_ROOT']. '/s/'.DIRNAME.'/inc/header.php'); ?>

<div class="contents">

<h2 class="ta-c"><img src="/s/<?=DIRNAME?>/images/ttl-<?=PAGE_NAME?>.png" height="25" alt="NEW AND RENEWAL" /></h2>

<div class="line01"></div>

<?php include_once($_SERVER['DOCUMENT_ROOT']. '/s/'.DIRNAME.'/inc/nav-nr.php'); ?>

<!--++++++++++++++++++++ UA ++++++++++++++++++++-->

<section class="pickup-block01">
<div class="pickup-block-in">

<div class="pickup-hd">
<div class="pickup-hd-item01">
<p class="pickup-floor"><img src="../images/newshop/img-floor-3f_02.png" alt="新館1F(レディス)・新館B1F(メンズ)" width="160" height="34"></p>
<h1 class="pickup-logo"><img src="../images/newshop/img-ua-logo.png" alt="ユナイテッド・アローズ" width="185" height="83"></h1>
<!-- pickup-hd-item01 --></div>
<div class="pickup-hd-item02">
<p class="fw-b fz15 ta-c">9.16 RENEWAL OPEN</p>
<!--
<p class="fw-b fz11 ta-c mt05">～9/13（火）新館３Fにて営業</p>
<div class="pickup-hd-box">
<p>※新館B1Fメンズは通常営業<br>
※新館１Fは改装のため9/15（木）まで休業</p>
</div>
-->
<p class="fz11 mt20">2001年9月の広島パルコ新館のオープンよりお客様にご愛顧いただいてきたユナイテッドアローズは今秋、新館の15周年を期にリオープンいたします。<br>新たなユナイテッドアローズにご期待下さい。</p>

<!-- pickup-hd-item02 --></div>
<!-- pickup-hd --></div>

<div class="pickup-facade"><img src="../images/newshop/img-ua-facade.jpg" alt="" ></div>

<div class="pickup-point01">
<div class="pickup-point01-item01">
<h2 class="ta-c"><img src="../images/newshop/txt-point.png" alt="Ponit" width="285" height="19"></h2>
<p class="fz11 lh15 mt15">1Fレディスフロア全面改装！<br>
中央通りからの外観も生まれ変わります。広くなった店舗に新しい商品を取りそろえてお待ちしております。</p>

<!-- pickup-point01-item01 --></div>
<div class="pickup-point01-item02 mt20">
<img src="../images/newshop/img-ua-main.jpg" width="100%" alt="">
<!-- pickup-point01-item02 --></div>
<!-- pickup-point01 --></div>
<!--
<h2 class="mt25 ta-c"><img src="../images/newshop/txt-openevent.png" alt="Open event" width="285" height="29"></h2>

<p class="ta-c mt15 fz11">リニューアルを記念して豪華なオープンイベントを開催！</p>

<div class="pickup-oe mt20">
<div class="pickup-oe-pic"><img src="../images/newshop/img-furla-main.jpg" width="100%" alt="FURLA"></div>
<div class="pickup-oe-body">
<h3 class="pickup-oe-title">FURLA<br><span class="pickup-oe-title-ja">フルラ</span></h3>
<p class="mt20 pickup-oe-txt01">9/16(金) - 25(日) <br>
2016 Autumn and Winter METOROPOLIS "CUSTOMIZE"</p>
<p class="mt30 pickup-oe-txt02">およそ90年もの歴史を誇るイタリアンプレミアムブランド　FURLA。<br>
今回は人気のミニバッグ“METROPOLIS（メトロポリス）”のカスタマイズイベントを開催いたします。<br>
4種類のボディとさまざまな色や素材からなる16種類のフラップの組み合わせを選ぶことが出来るスペシャルな販売会です。フラップはスナップボタンで取り付けるため、簡単に交換が可能。その日の気分やコーディネートに合わせて違ったバッグに変身できます。</p>
</div>
</div>

<div class="pickup-oe02">
<div class="pickup-oe02-item01">
FURLA 2016 Autumn and Winter
</div>
<div class="pickup-oe02-item02">
FURLA のアイコニックな存在であるMETROPOLISは、ブランドの多様性やデザイン、クラフトマンシップ、マテリアルの可能性を広げる代表作として展開し、そのミニマルなデザインが人気。<br>
華奢なチェーンと小ぶりなサイズがパーティーシーンから日常使いまで様々なスタイルにフィットするのが魅力で、チェーンをしまうとクラッチバッグとして持つ事も可能。シーズン毎に素材やカラーを変えて進化し続けているバッグのひとつ。</div>
</div>

<div class="pickup-oe mt60">
<div class="pickup-oe-pic"><img src="../images/newshop/img-felisi-main.jpg" width="100%" alt="felisi"></div>
<div class="pickup-oe-body">
<h3 class="pickup-oe-title">felisi <span class="pickup-oe-title-ja">フェリージ</span></h3>
<p class="mt10 pickup-oe-txt01">9/16(金) - 10/2(日)<br>
MORE VARIATION</p>
<p class="mt30 pickup-oe-txt02">バッグ・革小物を中心に展開するイタリアのブランド＜Felisi（フェリージ）＞期間限定販売会を開催いたします。期間中、メンズアイテム中心に、通常お取り扱いのないさまざまなモデルをラインナップし、一同にご覧いただけます。</p>

</div>
</div>

<div class="pickup-oe02">
<div class="pickup-oe02-item01">
Felisi（フェリージ）
</div>
<div class="pickup-oe02-item02">
FURLA 豊富なカラーバリエーションのナイロン素材とナチュラルレザーのコンビ使いが人気の＜フェリージ＞は、シンプルでスタイリッシュでありながらも、しっかりとした造りと軽さで多くのリピーターを抱えるブランドです。レザー部分は使い込むほどに味わいが深まるので、長年愛着をもってお使い頂けます。ビジネスバッグ以外にも豊富なバリエーションが揃っているため、カジュアル志向の方や女性にも幅広くお楽しみいただけます。</div>
</div>

 -->
<!-- pickup-block-in --></div>
<!-- pickup-block01 --></section>

<!--++++++++++++++++++++ //UA ++++++++++++++++++++-->

<!--++++++++++++++++++++ 無印良品 ++++++++++++++++++++-->

<section class="pickup-block02">
<div class="pickup-block-in">

<div class="pickup-hd">
<div class="pickup-hd-item01">
<p class="pickup-floor"><img src="../images/newshop/img-floor-78f.png" alt="7F/8F" width="119" height="34"></p>
<h1 class="pickup-logo"><img src="../images/newshop/img-muji-logo.png" alt="無印良品" width="185" height="94"></h1>
<!-- pickup-hd-item01 --></div>
<div class="pickup-hd-item02">
<p class="fw-b fz15 ta-c">9.16 RENEWAL OPEN</p>
<!--
<p class="fw-b fz11 ta-c mt05">～8/28（日）新館8Fにて営業</p>
<div class="pickup-hd-box type02">
<p>※8/29（月）～9/15（木）新館７F・８F休業</p>
</div>
-->
<p class="fz11 mt20">選びぬいた「MUJI BOOKS」。暮らしのパーツを販売する「INFILL+」。<br>
お客様とのコミュニケーションスペース「OPEN MUJI」。<br>
ちょっと長居したくなる中四国エリア初、をたくさん揃えてあたらしい無印良品がオープンします。</p>

<p class="fz11 mt15">■7F <br>
衣料品・化粧品・キッチン用品・食品・書籍・カスタマイズ工房<br>

<p class="fz11 mt15">■８F<br>
インテリア・家電・生活雑貨・ステーショナリー・自転車・書籍・インテリア相談カウンター</p>

<!-- pickup-hd-item02 --></div>
<!-- pickup-hd --></div>

<div class="pickup-facade"><img src="../images/newshop/img-muji-renewal.jpg" alt="" ></div>


<div class="pickup-point02">
<div class="pickup-point02-item">
<h2 class="ta-c"><img src="../images/newshop/txt-point1.png" alt="Point.1" width="285" height="19"></h2>
<p class="pickup-point02-pic"><img src="../images/newshop/img-mujibooks-logo.png" alt="MUJI BOOKS" width="100%" ></p>
<p class="pickup-point02-txt01">MUJI BOOKS</p>
<p class="pickup-point02-txt02">約１２，０００冊の書籍を無印良品の商品群とともに編集した複合売場が登場。<br>古今東西から長く読み継がれてきた本をあつめて、「ずっといい言葉」とともに本の
あるくらしを提案します。</p>
<!-- pickup-point02-item --></div>
<div class="pickup-point02-item">
<h2 class="ta-c"><img src="../images/newshop/txt-point2.png" alt="Point.2" width="285" height="19"></h2>
<p class="pickup-point02-pic"><img src="../images/newshop/img-mujiinfill-logo.png" alt="MUJI BOOKS" width="100%" ></p>
<p class="pickup-point02-txt01">MUJI  INFILL＋</p>
<p class="pickup-point02-txt02">住まいをもっと自由に、自分好みに編集するための新サービスを開始しました。</p>
<!-- pickup-point02-item --></div>
<div class="pickup-point02-item">
<h2 class="ta-c"><img src="../images/newshop/txt-point3.png" alt="Point.3" width="285" height="19"></h2>
<p class="pickup-point02-pic"><img src="../images/newshop/img-openmuji-logo.png" alt="MUJI BOOKS" width="100%" ></p>
<p class="pickup-point02-txt01">OPEN MUJI</p>
<p class="pickup-point02-txt02">
無印良品が考えているコトを提案し、お客様と共に考え、会話し、活動する空間です。
</p>
<!-- pickup-point02-item --></div>
<div class="mt20" style="text-align: center;"><img src="../images/newshop/txt-service.png" height="40" width="285" alt="中四国地方の新サービスがたくさん"></div>
<div class="pickup-point02-item">
<p class="pickup-point02-txt01">読庫 どくこ</p>
<p class="pickup-point02-txt02">
優れた編集力と美しい造本力で世界から絶大な人気を誇る中国のインディペンデント出版レーベル「読庫」。MUJI BOOKSは、「読庫」の出版シリーズのなかからNOTEBOOKをセレクトし、日本で初めて店頭販売します。
</p>
<!-- pickup-point02-item --></div>

<div class="pickup-point02-item">
<p class="pickup-point02-txt01">Found MUJI</p>
<p class="pickup-point02-txt02">
世界各地から、永く活かされてきた日用品を見つけてきました。今の生活の品質基準に合わせて、作者と対話しながら改良し、無印良品のものとして仕立て直しています。
</p>
<!-- pickup-point02-item --></div>

<div class="pickup-point02-item">
<p class="pickup-point02-txt01">カスタマイズ工房</p>
<p class="pickup-point02-txt02">
刺繍やプリントを無印良品の商品に施して、世界に一つのアイテムがつくれます。 ギフトにもおすすめです。
</p>
<!-- pickup-point02-item --></div>

<div class="pickup-point02-item">
<p class="pickup-point02-txt01">香り工房</p>
<p class="pickup-point02-txt02">
お好きな香りのタイプとイメージに合わせて、60種類からお客様が選んだ香りをその場でブレンドして提供いたします。お気に入りの香り探しは、スタッフがお手伝いいたします。
</p>
<!-- pickup-point02-item --></div>

<div class="pickup-point02-item">
<p class="pickup-point02-txt01">メイクカウンター</p>
<p class="pickup-point02-txt02">
ヘルス＆ビューティー売場に、商品を気軽に試せるメイクカウンターができました。
</p>
<!-- pickup-point02-item --></div>

<div class="pickup-point02-item">
<p class="pickup-point02-txt01">スタイリング相談カウンター</p>
<p class="pickup-point02-txt02">
これからの季節にぴったりの着こなしはもちろん、お客様のお気に入りの秋のウェアを持参いただければ、お好みにふさわしいコーディネートをご提案いたします。
</p>
<!-- pickup-point02-item --></div>

<!-- pickup-point01 --></div>
<!--
<h2 class="mt20 ta-c"><img src="../images/newshop/txt-openevent.png" alt="Open event" width="285" height="29"></h2>

<p class="ta-c mt15 fz11">お得なイベントが盛り沢山！</p>

<p class="fw-b mt20 fz11">
①9/16(金) ～25(日) 〈PARCOカード〉ご利用もしくは新規ご入会で<span class="red">全商品5％OFF</span><br><br>
②9/16(金) ～　MUJI　passport　<span class="red">ご提示でMUJIマイル3倍</span><br><br>
③9/16(金) ～無くなリ次第終了 お買上げの方に先着で <span class="red">「藍染マイバッグ」進呈</span><br><br>
and more...</p>


<div class="pickup-bdrbox01 mt65">
<div class="pickup-bdrbox01-in">
<h2 class="pickup-bdrbox01-ttl"><img src="../images/newshop/txt-talkshow.png" alt="トークショーのお知らせ" width="233" height="19"></h2>
<div class="pickup-talkshow-block">
<div class="pickup-talkshow-pic"><img src="../images/newshop/img-talkshow.jpg" width="100%" alt="谷尻誠さん"></div>
<div class="pickup-talkshow-body fz11">
<h3 class="">【OPENING EVENT】 9/17(土) 14:00~15:00
<br>広島出身建築家 谷尻誠トークショー</h3>
<div class="pickup-talkshow-graybox">
<p class="fw-b">谷尻誠（たにじり まこと） 建築家<br>
SUPPOSE DESIGN OFFICE Co.,Ltd. 代表取締役 </p>

<table class="prof">
<tr><td>1974年</td><td>広島生まれ</td></tr>
<tr><td>2000年</td><td>建築設計事務所Suppose design office 設立</td></tr>
<tr><td>2014年</td><td>SUPPOSE DESIGN OFFICECo.,Ltd 設立</td></tr>
</table>
</div>
<p class="fw-b mt15">
<span style="color: #f71f43;">定員に達しましたので受付を締め切らせて頂きました。</span>
  <br>
  <a href="https://www.muji.com/jp/events/3535/" target="blank" style="text-decoration: underline;">https://www.muji.com/jp/events/3535/</a>
</p>

</div>
</div>
</div>
</div>
-->
<!-- pickup-block-in --></div>
<!-- pickup-block01 --></section>

<!--++++++++++++++++++++ //無印良品 ++++++++++++++++++++-->


<!-- /contents --></div>

<?php include_once($_SERVER['DOCUMENT_ROOT']. '/s/'.DIRNAME.'/inc/footer.php'); ?>
</body>
</html>