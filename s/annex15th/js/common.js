/*================================================================

共通スクリプト

================================================================*/
$(function(){

  $menuBtn = $('.js-menu-btn');
  $menuCloseBtn = $('.js-menu-close-btn');
  $menu = $('.gnav-wrap');

  $menuBtn.on('click', function(event) {
    event.preventDefault();
    $menu.fadeIn();
  });
  $menuCloseBtn.on('click', function(event) {
    event.preventDefault();
    $menu.hide();
  });
  function menuOpen(){
    $menu.hide();
  };
  function menuClose(){
    $menu.fadeIn();
  };


	// クラスの追加
	$('body :first-child').addClass('firstChild'); //最後の要素
	$('body :last-child').addClass('lastChild'); //最初の要素


	// back to pagetop
	$('a[href^=#]').click(function () {
		$(this).blur();
			$('html,body').animate({ scrollTop: 0 }, 'slow');
		return false;
	});


  $('.photo-block-in').each(function() {
    //var flg = 'default';
    var allPhotohover = $('.photo-hover');
    var allVisual = $('.visual');
      $(this).click(function(){
        //if(flg == 'default'){
          allPhotohover.hide();
          allVisual.removeClass('is-active');
          //flg = 'default';
          //event.preventDefault();
          $(this).find('.visual').addClass('is-active');
          $(this).find('.photo-hover').fadeIn(200);
          //flg = "changed";
        //}else{
        //}
      });
  });
});
