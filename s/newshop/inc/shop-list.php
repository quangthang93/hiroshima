<h2 class="floor-ttl mt00"></h2>
<article class="contentsBlock" id="item9">
    <header>
        <h1><img src="/web/newshop/images/item/img_8.jpg" width="100%" alt="エクストララージ"></h1></header>
    <div class="boxTop">
        <div class="date">
          <p class="day">2.16</p>
          <p class="week">FRI</p>
          <p class="new">RENEWAL</p>
        </div>
        <div class="logo">
            <p class="logo-img">
                <a href="javascript:void(0)"><img src="/web/newshop/images/logo/logo_8.png" width="185" alt="エクストララージ"></a>
            </p>
        </div>
        <p class="shopname"></p>
        <!-- /.boxTop -->
    </div>
    <dl class="boxAcd">
        <dd class="boxAcd_contents">
            <p class="oe-title">本館6F&nbsp;エクストララージ</p>
            <p class="oe-category">メンズ</p>
            <div class="oe-text">1991年にLAにて設立。メンズストリートウェアブランドのパイオニア。<br>
              ブランドロゴの「OGゴリラ」は、アイコン的存在として世界中に認知されている。</div>
            <p class="oe-plan">￥15,000(税込)以上お買い上げで、アウトドアミニチェアをプレゼント【2/16～】</p>
        </dd>
        <dt class="boxAcd_btn"></dt>
    </dl>
</article>
<article class="contentsBlock" id="item12">
    <header>
        <h1><img src="/web/newshop/images/item/img_10.jpg" width="100%" alt="エックスガール"></h1></header>
    <div class="boxTop">
        <div class="date">
          <p class="day">2.16</p>
          <p class="week">FRI</p>
          <p class="new">RENEWAL</p>
        </div>
        <div class="logo">
            <p class="logo-img">
                <a href="javascript:void(0)"><img src="/web/newshop/images/logo/logo_10.png" width="185" alt="エックスガール"></a>
            </p>
        </div>
        <p class="shopname"></p>
        <!-- /.boxTop -->
    </div>
    <dl class="boxAcd">
        <dd class="boxAcd_contents">
            <p class="oe-title">本館8F&nbsp;エックスガール</p>
            <p class="oe-category">レディス</p>
            <div class="oe-text">音楽・カルチャー・スポーツ等のストリートシーンから、様々な要素を取り込み、時代の変化に合わせて生み出されていく“REAL GIRL’S CLOTHING”をテーマに掲げ、リアルな女の子のストリートスタイルを提案し続けている。</div>
            <p class="oe-plan">￥5,000(税抜)以上お買い上げで、ポーチセットを￥15,000(税抜)以上お買い上げで、アウトドアミニチェアをプレゼント【2/16～】</p>
        </dd>
        <dt class="boxAcd_btn"></dt>
    </dl>
</article>
<article class="contentsBlock" id="item8">
  <p class="ribbon"><img src="images/ribbon_first.png" alt="中四国初" width="66" height="90"></p>
    <header>
        <h1><img src="/web/newshop/images/item/img_7.jpg" width="100%" alt="トップオブザヒル"></h1></header>
    <div class="boxTop">
        <div class="date">
          <p class="day">3.16</p>
          <p class="week">FRI</p>
          <p class="new">NEW</p>
        </div>
        <div class="logo">
            <p class="logo-img">
                <a href="javascript:void(0)"><img src="/web/newshop/images/logo/logo_7.png" width="185" alt="トップオブザヒル"></a>
            </p>
        </div>
        <p class="shopname"></p>
        <!-- /.boxTop -->
    </div>
    <dl class="boxAcd">
        <dd class="boxAcd_contents">
            <p class="oe-title">本館6F&nbsp;トップオブザヒル</p>
            <p class="oe-category">メンズ・レディス</p>
            <div class="oe-text">メンズ・レディースの60年代～80年代のEURO・USA古着・オリジナルウェアが１ランク上のセレクトで並びます。<br>
              着回しの効く定番アイテム、トレンドアイテム、珍しいアイテムなどバラエティに富んだ物が充実。<br>
              インポートウェア、バッグ、帽子、靴、アクセサリーなども豊富で古着×新品のMIXスタイルも提案しています。<br>
              プレゼントに最適なアクセサリーや時計などの小物やユニセックスアイテムも数多く取り扱っています。</div>
            <p class="oe-plan">3,000円以上お買い上げのお客様にオリジナルトートバッグプレゼント（先着400枚限定）</p>
        </dd>
        <dt class="boxAcd_btn"></dt>
    </dl>
</article>
<article class="contentsBlock" id="item16">
    <header>
        <h1><img src="/web/newshop/images/item/img_14.jpg" width="100%" alt="パルコファクトリー"></h1></header>
    <div class="boxTop">
        <div class="date">
          <p class="day">3.16</p>
          <p class="week">FRI</p>
          <p class="new">NEW</p>
        </div>
        <div class="logo">
            <p class="logo-img">
                <a href="javascript:void(0)"><img src="/web/newshop/images/logo/logo_14.png" width="185" alt="パルコファクトリー"></a>
            </p>
        </div>
        <p class="shopname"></p>
        <!-- /.boxTop -->
    </div>
    <dl class="boxAcd">
        <dd class="boxAcd_contents">
            <p class="oe-title">本館6F&nbsp;パルコファクトリー</p>
            <p class="oe-category"></p>
            <div class="oe-text">情報発信の新拠点誕生！<br>
              カルチャー・ファッションからアート・デザインまで多彩なイベントを展開するスペース「PARCO FACTORY」がオープン！</div>
        </dd>
        <dt class="boxAcd_btn"></dt>
    </dl>
</article>
<article class="contentsBlock" id="item2">
    <header>
        <h1><img src="/web/newshop/images/item/img_2.jpg" width="100%" alt="マーキュリーデュオ"></h1></header>
    <div class="boxTop">
        <div class="date">
          <p class="day">3.16</p>
          <p class="week">FRI</p>
          <p class="new">RENEWAL</p>
        </div>
        <div class="logo">
            <p class="logo-img">
                <a href="javascript:void(0)"><img src="/web/newshop/images/logo/logo_2.png" alt="マーキュリーデュオ" width="185"></a>
            </p>
        </div>
        <p class="shopname"></p>
        <!-- /.boxTop -->
    </div>
    <dl class="boxAcd">
        <dd class="boxAcd_contents">
             <p class="oe-title">本館3F&nbsp;マーキュリーデュオ</p>
             <p class="oe-category">レディス</p>
            <div class="oe-text">華やかに咲き誇る薔薇のような可憐で気品のある彼女<br>
              どこにいても、なにをしてても、華やかで印象的<br>
              気品と知性を兼ね備えた女性になれる色気のあるスタイルを提案</div>
              <p class="oe-plan">カラー限定トップススカート販売<br>
                限定ワンピース販売<br>
                ノベルティフェア</p>
        </dd>
        <dt class="boxAcd_btn"></dt>
    </dl>
</article>
<article class="contentsBlock" id="item3">
  <p class="ribbon"><img src="images/ribbon_first.png" alt="中四国初" width="66" height="90"></p>
    <header>
        <h1><img src="/web/newshop/images/item/img_3.jpg" width="100%" alt="ロイヤルパーティー"></h1></header>
    <div class="boxTop">
        <div class="date">
          <p class="day">3.30</p>
          <p class="week">FRI</p>
          <p class="new">NEW</p>
        </div>
        <div class="logo">
            <p class="logo-img">
               <a href="javascript:void(0)"><img src="/web/newshop/images/logo/logo_3.png" width="185" alt="ロイヤルパーティー"></a>
            </p>
        </div>
        <p class="shopname"></p>
        <!-- /.boxTop -->
    </div>
    <dl class="boxAcd">
        <dd class="boxAcd_contents">
            <p class="oe-title">本館3F&nbsp;ロイヤルパーティー</p>
            <p class="oe-category">レディス</p>
            <div class="oe-text">シンプルでグラマラス、ベーシックでグロッシー あらゆるシーンにおいて いつもの服をドラマティックに演出する ワンランク上のSEXYをリアルに表現</div>
            <p class="oe-plan">税込15,000円以上お買上げでオリジナルミニウォレットプレゼント【3/30～】</p>
        </dd>
        <dt class="boxAcd_btn"></dt>
    </dl>
</article>
<article class="contentsBlock" id="item14">
    <header>
        <h1><img src="/web/newshop/images/item/img_12.jpg" width="100%" alt="スマホ修理工房"></h1></header>
    <div class="boxTop">
        <div class="date">
          <p class="day">2.28</p>
          <p class="week">WED</p>
          <p class="new">RENEWAL</p>
        </div>
        <div class="logo">
            <p class="logo-img">
                <a href="javascript:void(0)"><img src="/web/newshop/images/logo/logo_12.png" width="185" alt="スマホ修理工房"></a>
            </p>
        </div>
        <p class="shopname"></p>
        <!-- /.boxTop -->
    </div>
    <dl class="boxAcd">
        <dd class="boxAcd_contents">
            <p class="oe-title">新館3F&nbsp;スマホ修理工房</p>
            <p class="oe-category">スマートフォン修理</p>
            <div class="oe-text">スマートフォン・タブレットでお困りの事は何でもご相談下さい。ガラス割れ、液晶不良、バッテリー交換、水没修理など即日対応致します。</div>
            <p class="oe-plan">スマホ修理された方に強化ガラス（2,000円相当）プレゼント【2/28～3/11】※機種に限定があります。</p>
        </dd>
        <dt class="boxAcd_btn"></dt>
    </dl>
</article>
<article class="contentsBlock" id="item1">
    <header>
        <h1><img src="/web/newshop/images/item/img_1.jpg" width="100%" alt="オズモーシス"></h1></header>
    <div class="boxTop">
        <div class="date">
          <p class="day">3.16</p>
          <p class="week">FRI</p>
          <p class="new">RENEWAL</p>
            <!-- /.date -->
        </div>
        <div class="logo">
            <p class="logo-img x-logo">
                <a href="javascript:void(0)"><img src="/web/newshop/images/logo/logo_1.png" alt="オズモーシス" width="185"></a>
            </p>
        </div>
        <p class="shopname"></p>
        <!-- /.boxTop -->
    </div>
    <dl class="boxAcd">
        <dd class="boxAcd_contents">
            <p class="oe-title">本館2F&nbsp;オズモーシス</p>
            <p class="oe-category">レディス</p>
            <div class="oe-text">時代に左右されないタイムレスなデザインベースにトレンド性を盛り込み常に新鮮なスタイルを提案<br>
              タイムレスなデザインベースにトレンド感をプラス<br>
              新鮮なスタイルを楽しめるデイリーカジュアル</div>
            <p class="oe-plan">PARCOカードご利用または新規ご入会で10％OFF<br>【3/16～25】</p>
        </dd>
        <dt class="boxAcd_btn"></dt>
    </dl>
</article>
<article class="contentsBlock" id="item7">
    <header>
        <h1><img src="/web/newshop/images/item/img_6.jpg" width="100%" alt="ユニケース"></h1></header>
    <div class="boxTop">
        <div class="date">
          <p class="day">3.7</p>
          <p class="week">WED</p>
          <p class="new">RENEWAL</p>
        </div>
        <div class="logo">
            <p class="logo-img">
                <a href="javascript:void(0)"><img src="/web/newshop/images/logo/logo_6.png" width="185" alt="ユニケース"></a>
            </p>
        </div>
        <p class="shopname"></p>
        <!-- /.boxTop -->
    </div>
    <dl class="boxAcd">
        <dd class="boxAcd_contents">
            <p class="oe-title">本館6F&nbsp;ユニケース</p>
            <p class="oe-category">スマートフォンアクセサリー</p>
            <div class="oe-text">ユニークで厳選されたアイテムを豊富に取り揃え、「面白い」「オリジナル」「可愛い」「トレンド」「便利」をコンセプトとした人気のアイテムを発信し続けるスマートフォンアクセサリー専門店です。</div>
            <p class="oe-plan">一部商品最大60％OFF【～3/31】</p>
        </dd>
        <dt class="boxAcd_btn"></dt>
    </dl>
</article>
<article class="contentsBlock" id="item6">
    <header>
        <h1><img src="/web/newshop/images/item/img_23.jpg" width="100%" alt="エドマッチョ"></h1></header>
    <div class="boxTop">
        <div class="date">
          <p class="day">3.3</p>
          <p class="week">SAT</p>
          <p class="new">RENEWAL</p>
        </div>
        <div class="logo">
            <p class="logo-img">
                <a href="javascript:void(0)"><img src="/web/newshop/images/logo/logo_23.png" width="185" alt="エドマッチョ"></a>
            </p>
        </div>
        <p class="shopname"></p>
        <!-- /.boxTop -->
    </div>
    <dl class="boxAcd">
        <dd class="boxAcd_contents">
            <p class="oe-title">本館4F&nbsp;エドマッチョ</p>
            <p class="oe-category">アート作品・アクセサリー</p>
            <div class="oe-text">ARTで世界中の人々をHAPPYに!<br>
              あなたの心を豊かにする世界一かわいいエドマッチョをお届け♪<br>
              エドマッチョはARTを通して世界中の人々をハッピーにするお店です。九州で活躍するアーティストのできたてほっかほかの作品を販売しています。一緒に盛り上げてくれる広島のアーティストも大募集中です!</div>
        </dd>
        <dt class="boxAcd_btn"></dt>
    </dl>
</article>
<article class="contentsBlock" id="item11">
    <header>
        <h1><img src="/web/newshop/images/item/img_9.jpg" width="100%" alt="ザッカイーキュー（コレット跡）"></h1></header>
    <div class="boxTop">
        <div class="date">
          <p class="day">3.9</p>
          <p class="week">FRI</p>
          <p class="new">RENEWAL</p>
        </div>
        <div class="logo">
            <p class="logo-img">
                <a href="javascript:void(0)"><img src="/web/newshop/images/logo/logo_9.png" width="185" alt="ザッカイーキュー（コレット跡）"></a>
            </p>
        </div>
        <p class="shopname"></p>
        <!-- /.boxTop -->
    </div>
    <dl class="boxAcd">
        <dd class="boxAcd_contents">
            <p class="oe-title">本館7F&nbsp;ザッカイーキュー（コレット跡）</p>
            <p class="oe-category"></p>
            <div class="oe-text">「心の豊かさ」をテーマにワクワクドキドキ楽しい雑貨で埋め尽くします。<br>
              プレゼントに、日用使いに、自分へのご褒美に、ワクワクしたいときに使える雑貨・お店・スタッフと楽しんでください♪</div>
        </dd>
        <dt class="boxAcd_btn"></dt>
    </dl>
</article>
<article class="contentsBlock" id="item13">
    <header>
        <h1><img src="/web/newshop/images/item/img_11.jpg" width="100%" alt="リーカ"></h1></header>
    <div class="boxTop">
        <div class="date">
          <p class="day">3.1</p>
          <p class="week">THU</p>
          <p class="new">RENEWAL</p>
        </div>
        <div class="logo">
            <p class="logo-img">
                <a href="javascript:void(0)"><img src="/web/newshop/images/logo/logo_11.png" width="185" alt="リーカ"></a>
            </p>
        </div>
        <p class="shopname"></p>
        <!-- /.boxTop -->
    </div>
    <dl class="boxAcd">
        <dd class="boxAcd_contents">
            <p class="oe-title">新館3F&nbsp;リーカ</p>
            <p class="oe-category">レディスシューズ</p>
            <div class="oe-text">“ 毎日服を変えるように、 毎日靴も変えてオシャレしたい。”<br>
              トレンドを取り入れながら履き心地にもこだわった、どこか遊び心のある大人カジュアルな靴を取り揃えています。<br>
              2018SSシーズンコンセプト『Curiosity in a chill-out moment』好奇心とリラックス<br>
              リラックスアイテムがベース。でもそれだけじゃ物足りない女子のための、意表をついたアイテムたち。思わず心がときめくような、色使いやディティールをお楽しみください。</div>
        </dd>
        <dt class="boxAcd_btn"></dt>
    </dl>
</article>
<article class="contentsBlock" id="item5">
    <header>
        <h1><img src="/web/newshop/images/item/img_4.jpg" width="100%" alt="G＆H"></h1></header>
    <div class="boxTop">
        <div class="date">
          <p class="day">3.16</p>
          <p class="week">FRI</p>
          <p class="new new-small">LIMITED</p>
        </div>
        <div class="logo">
            <p class="logo-img">
                <a href="javascript:void(0)"><img src="/web/newshop/images/logo/logo_4.png" width="185" alt="G＆H"></a>
            </p>
        </div>
        <p class="shopname"></p>
        <!-- /.boxTop -->
    </div>
    <dl class="boxAcd">
        <dd class="boxAcd_contents">
            <p class="oe-title">本館4F&nbsp;G＆H</p>
            <p class="oe-category">アクセサリー</p>
            <div class="oe-text">2016年から日本で本格的に展開をはじめたアクセサリーブランド G&amp;H ( ジーアンドエイチ ) は、東南アジア主要都市に点在する工場(こうば)を拠点とするアトリエ G-JAK による繊細でありながらも芯の強い世界観を豊かに表現したミスレニアスコレクションです。</div>
        </dd>
        <dt class="boxAcd_btn"></dt>
    </dl>
</article>
<article class="contentsBlock" id="item15">
    <header>
        <h1><img src="/web/newshop/images/item/img_13.jpg" width="100%" alt="三愛水着楽園"></h1></header>
    <div class="boxTop">
        <div class="date">
          <p class="day">2.7</p>
          <p class="week">WED</p>
          <p class="new">RENEWAL</p>
        </div>
        <div class="logo">
            <p class="logo-img">
                <a href="javascript:void(0)"><img src="/web/newshop/images/logo/logo_13.png" width="185" alt="三愛水着楽園"></a>
            </p>
        </div>
        <p class="shopname"></p>
        <!-- /.boxTop -->
    </div>
    <dl class="boxAcd">
        <dd class="boxAcd_contents">
            <p class="oe-title">新館6F&nbsp;三愛水着楽園</p>
            <p class="oe-category">水着</p>
            <div class="oe-text">2018年最新のファッション水着をはじめ、旅行にも最適なリゾートウェアやグッズ、インスタ映えする小物等、ビーチを楽しむアイテムを豊富に取り揃えています。今年のトレンドは「リバーシブルブラ」と「ノンワイヤーブラ」。ぜひお店で着心地をお試しください。</div>
        </dd>
        <dt class="boxAcd_btn"></dt>
    </dl>
</article>
</div>

<h2 class="sec-ttl">第二弾</h2>
<div class="sec-in">
<h2 class="floor-ttl mt00"></h2>
<article class="contentsBlock" id="item17">
  <p class="ribbon"><img src="images/ribbon_first.png" alt="中四国初" width="66" height="90"></p>
    <header>
        <h1><img src="/web/newshop/images/item/img_15.jpg" width="100%" alt="ベーシックアンドアクセント"></h1></header>
    <div class="boxTop">
        <div class="date">
          <p class="day">4月</p>
          <p class="week">下旬</p>
          <p class="new">NEW</p>
        </div>
        <div class="logo">
            <p class="logo-img">
                <a href="javascript:void(0)"><img src="/web/newshop/images/logo/logo_15.png" width="185" alt="ベーシックアンドアクセント"></a>
            </p>
        </div>
        <p class="shopname"></p>
        <!-- /.boxTop -->
    </div>
    <dl class="boxAcd">
        <dd class="boxAcd_contents">
            <p class="oe-title">新館2F&nbsp;ベーシックアンドアクセント</p>
            <p class="oe-category">生活雑貨</p>
            <div class="oe-text">クオリティの高いベーシックなものを愛用し、ユーモアやニュアンスのあるものを日常生活のスパイスに取り入れる。そんな媚びない大人のかっこよさの中に、繊細さを持つ人たちへ。心地よく、ほんの少し刺激的な、いとおしい暮らしを贈るブランド。</div>
        </dd>
        <dt class="boxAcd_btn"></dt>
    </dl>
</article>
<article class="contentsBlock" id="item18">
  <p class="ribbon"><img src="images/ribbon_first.png" alt="中四国初" width="66" height="90"></p>
    <header>
        <h1><img src="/web/newshop/images/item/img_16.jpg" width="100%" alt="ラヴィジュール"></h1></header>
    <div class="boxTop">
        <div class="date">
          <p class="day">4.20</p>
          <p class="week">FRI</p>
          <p class="new">NEW</p>
        </div>
        <div class="logo">
            <p class="logo-img">
                <a href="javascript:void(0)"><img src="/web/newshop/images/logo/logo_16.png" width="185" alt="ラヴィジュール"></a>
            </p>
        </div>
        <p class="shopname"></p>
        <!-- /.boxTop -->
    </div>
    <dl class="boxAcd">
        <dd class="boxAcd_contents">
            <p class="oe-title">本館3F&nbsp;ラヴィジュール</p>
            <p class="oe-category">下着</p>
            <div class="oe-text">「セクシーに生きる-Just be your self-」をコンセプトに、女性の新しいセクシーさを追求し、ワンランク上のアイテムを展開すランジェリーブランド。</div>
        </dd>
        <dt class="boxAcd_btn"></dt>
    </dl>
</article>
<article class="contentsBlock" id="item19">
    <header>
        <h1><img src="/web/newshop/images/item/img_17.jpg" width="100%" alt="ミッシュマッシュ"></h1></header>
    <div class="boxTop">
        <div class="date">
          <p class="day">4.20</p>
          <p class="week">FRI</p>
          <p class="new">RENEWAL</p>
        </div>
        <div class="logo">
            <p class="logo-img">
                <a href="javascript:void(0)"><img src="/web/newshop/images/logo/logo_17.png" width="185" alt="ミッシュマッシュ"></a>
            </p>
        </div>
        <p class="shopname"></p>
        <!-- /.boxTop -->
    </div>
    <dl class="boxAcd">
        <dd class="boxAcd_contents">
            <p class="oe-title">本館3F&nbsp;ミッシュマッシュ</p>
            <p class="oe-category">レディス</p>
            <div class="oe-text">大人っぽくて可愛いレディスアイテムをたっぷりとｍｉｘ！<br>
              女の子の大好きが詰まった、わくわく感あるＳＨＯＰをイメージ。<br>
              大人可愛いエレガンスカジュアル　ライフスタイルをそれそれの価値観でエンジョイする思考へと変化している今日、ＭＩＳＣＨ　ＭＡＳＣＨとすごす特別な時間を配信していく。<br>
              美人度120％！恋するＯＮ/ＯＦＦ服！</div>
        </dd>
        <dt class="boxAcd_btn"></dt>
    </dl>
</article>
<article class="contentsBlock" id="item20">
    <header>
        <h1><img src="/web/newshop/images/item/img_18.jpg" width="100%" alt="アンレリッシュ"></h1></header>
    <div class="boxTop">
        <div class="date">
          <p class="day">5.11</p>
          <p class="week">FRI</p>
          <p class="new">RENEWAL</p>
        </div>
        <div class="logo">
            <p class="logo-img">
                <a href="javascript:void(0)"><img src="/web/newshop/images/logo/logo_18.png" width="185" alt="アンレリッシュ"></a>
            </p>
        </div>
        <p class="shopname"></p>
        <!-- /.boxTop -->
    </div>
    <dl class="boxAcd">
        <dd class="boxAcd_contents">
            <p class="oe-title">本館3F&nbsp;アンレリッシュ</p>
            <p class="oe-category">レディス</p>
            <div class="oe-text">”Enjoy life every day"<br>
              「TREND」「BASIC」「CASUAL」をキーワードに”いま”を生きる女性に向けた大人のワードローブを提案します。いま買うべきトレンドも自分らしさを後押しする要素として楽しんでいる。スタイルある凛とした女性をイメージ。</div>
            <p class="oe-plan">10,000円以上お買い上げでOPEN限定ノベルティ（ミラー）をプレゼント【5/11～】</p>
        </dd>
        <dt class="boxAcd_btn"></dt>
    </dl>
</article>
</div>

<h2 class="sec-ttl">第三弾</h2>
<div class="sec-in">
<h2 class="floor-ttl mt00"></h2>
<article class="contentsBlock" id="item21">
  <p class="ribbon"><img src="images/ribbon_first.png" alt="中四国初" width="66" height="90"></p>
    <header>
        <h1><img src="/web/newshop/images/item/img_20.jpg" width="100%" alt="エチュードハウス"></h1></header>
    <div class="boxTop">
        <div class="date">
          <p class="day">6.8</p>
          <p class="week">FRI</p>
          <p class="new">NEW</p>
        </div>
        <div class="logo">
            <p class="logo-img">
                <a href="javascript:void(0)"><img src="/web/newshop/images/logo/logo_20.png" width="185" alt="エチュードハウス"></a>
            </p>
        </div>
        <p class="shopname"></p>
        <!-- /.boxTop -->
    </div>
    <dl class="boxAcd">
        <dd class="boxAcd_contents">
            <p class="oe-title">本館3F&nbsp;エチュードハウス</p>
            <p class="oe-category">化粧品</p>
            <div class="oe-text">メイクアップは決まりきった習慣ではなく、ワクワクするPLAY（遊び）として楽しむもの“Makeup Play!”をコンセプトに、スキンケアからメイクアップアイテムまでユニークでスイートな商品ラインナップをご提供いたします。<br>
              エチュードハウスなら、あなただけの特別な「かわいい」が叶います。<br>
              今日をもっとスイートに！<br>
              Life is Sweet</div>
        </dd>
        <dt class="boxAcd_btn"></dt>
    </dl>
</article>
<article class="contentsBlock" id="item22">
    <header>
        <h1><img src="/web/newshop/images/item/img_21.jpg" width="100%" alt="アトレナ"></h1></header>
    <div class="boxTop">
        <div class="date">
          <p class="day">6.8</p>
          <p class="week">FRI</p>
          <p class="new">NEW</p>
        </div>
        <div class="logo">
            <p class="logo-img">
                <a href="javascript:void(0)"><img src="/web/newshop/images/logo/logo_21.png" width="185" alt="アトレナ"></a>
            </p>
        </div>
        <p class="shopname"></p>
        <!-- /.boxTop -->
    </div>
    <dl class="boxAcd">
        <dd class="boxAcd_contents">
            <p class="oe-title">新館3F&nbsp;アトレナ</p>
            <p class="oe-category">帽子</p>
            <div class="oe-text">コンセプトは"新しい自分と出会える"トレンド+機能性+自分らしさを演出できる、カスタマイズ機能を盛り込んだ商品の展開。丁寧な接客と確かな商品知識、また永らくご愛用いただける堅実な品質の商品を提供いたします。</div>
        </dd>
        <dt class="boxAcd_btn"></dt>
    </dl>
</article>
<article class="contentsBlock" id="item23">
    <header>
        <h1><img src="/web/newshop/images/item/img_22.jpg" width="100%" alt="マリークヮント"></h1></header>
    <div class="boxTop">
        <div class="date">
          <p class="day">5.25</p>
          <p class="week">FRI</p>
          <p class="new">RENEWAL</p>
        </div>
        <div class="logo">
            <p class="logo-img">
                <a href="javascript:void(0)"><img src="/web/newshop/images/logo/logo_22.png" width="185" alt="マリークヮント"></a>
            </p>
        </div>
        <p class="shopname"></p>
        <!-- /.boxTop -->
    </div>
    <dl class="boxAcd">
        <dd class="boxAcd_contents">
            <p class="oe-title">本館3F&nbsp;マリークヮント</p>
            <p class="oe-category">化粧品・雑貨</p>
            <div class="oe-text">年齢問わず自分を持っているお客様に、コスメ、ファッションの幅広いラインナップで自分らしくなるお手伝いをさせて頂きます。<br>
              「自由に、自分らしく」。<br>
              全ての商品は、「ルック」を完成させるパーツを考え、自由な組み合わせで女性の美しさを彩ります。</div>
        </dd>
        <dt class="boxAcd_btn"></dt>
    </dl>
</article>
