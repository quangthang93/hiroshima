<div class="follow_block_01">
<p class="footer_cap">Social Media</p>
<ul id="sns" class="clear">
<li class="twitter"><a href="https://twitter.com/parco_hiroshima" target="_blank"><img src="/s/common/images/common_footer_sns_twitter.png" width="66" alt="広島PARCO公式Twitter" /></a></li>
<li class="line"><a href="http://accountpage.line.me/hiroshimaparco" target="_blank"><img src="/s/common/images/common_footer_sns_line.png" width="66" alt="広島PARCO公式LINE" /></a></li>
<li class="facebook"><a href="http://www.facebook.com/hiroshimaparco" target="_blank"><img src="/s/common/images/common_footer_sns_fb.png" width="66" alt="広島PARCO公式Facebook" /></a></li>
<li class="instagram"><a href="https://www.instagram.com/parco_hiroshima_official/" target="_blank"><img src="/s/common/images/common_footer_sns_instagram.png" width="66" alt="広島PARCO公式Instagram" /></a></li>
</ul>
<ul class="clear">
<li class="footer_btn_01 width100"><a href="http://parco.jp/s/sns/" target="_blank">PARCO公式SNS一覧</a></li>
</ul>
<!--/follow_block_01--></div>
