<section class="star-footer">

<div class="staff-nav-block">
<div class="staff-nav-block-in">
<div class="staff-nav">
  <div class="slider">
      <div class="item01"><a href="staff01.php"><img src="/web/<?=DIRNAME?>/images/star/staff01-thumb.jpg" alt="玉井さん"></a></div>
      <div class="item02"><a href="staff02.php"><img src="/web/<?=DIRNAME?>/images/star/staff02-thumb.jpg" alt="宮本さん"></a></div>
      <div class="item03"><a href="staff03.php"><img src="/web/<?=DIRNAME?>/images/star/staff03-thumb.jpg" alt="角戸さん"></a></div>
      <div class="item04"><a href="staff04.php"><img src="/web/<?=DIRNAME?>/images/star/staff04-thumb.jpg" alt="内藤さん"></a></div>
      <div class="item05"><a href="staff05.php"><img src="/web/<?=DIRNAME?>/images/star/staff05-thumb.jpg" alt="村岡さん"></a></div>
      <div class="item06"><a href="staff06.php"><img src="/web/<?=DIRNAME?>/images/star/staff06-thumb.jpg" alt="渡邉さん"></a></div>
      <div class="item07"><a href="staff07.php"><img src="/web/<?=DIRNAME?>/images/star/staff07-thumb.jpg" alt="佐藤さん"></a></div>
      <div class="item08"><a href="staff08.php"><img src="/web/<?=DIRNAME?>/images/star/staff08-thumb.jpg" alt="辻さん"></a></div>
      <div class="item09"><a href="staff09.php"><img src="/web/<?=DIRNAME?>/images/star/staff09-thumb.jpg" alt="越智さん"></a></div>
      <div class="item10"><a href="staff10.php"><img src="/web/<?=DIRNAME?>/images/star/staff10-thumb.jpg" alt="山本さん"></a></div>
      <div class="item11"><a href="staff11.php"><img src="/web/<?=DIRNAME?>/images/star/staff11-thumb.jpg" alt="淡路さん"></a></div>
      <div class="item12"><a href="staff12.php"><img src="/web/<?=DIRNAME?>/images/star/staff12-thumb.jpg" alt="関田さん"></a></div>
      <div class="item13"><a href="staff13.php"><img src="/web/<?=DIRNAME?>/images/star/staff13-thumb.jpg" alt="岡さん"></a></div>
      <div class="item14"><a href="staff14.php"><img src="/web/<?=DIRNAME?>/images/star/staff14-thumb.jpg" alt="小林さん"></a></div>
      <div class="item15"><a href="staff15.php"><img src="/web/<?=DIRNAME?>/images/star/staff15-thumb.jpg" alt="橋本さん"></a></div>
      <div class="item16"><a href="staff16.php"><img src="/web/<?=DIRNAME?>/images/star/staff16-thumb.jpg" alt="嘉村さん"></a></div>
  </div>
</div>
</div>
<!-- /.staff-nav-block --></div>

<div class="star-footer-logo">
  <a href="/s/<?=DIRNAME?>/star/"><img src="/s/annex15th/images/ttl-star.png" alt="パルコのスター" width="100%"></a>
</div>

</section>

