<?php
define('STORE_NAME', 'hiroshima');
define('STORE_NAME_JA', '広島PARCO');
// define('CHECK_NUM', '2254');
define('DIRNAME', 'annex15th_17ss');
define('TITLE', 'SPRING NEW＆RENEWAL｜広島PARCO');
define('TITLE2', 'NEW AND RENEWAL');
define('TITLE3', 'NEWS AND EVENT');
define('DESCRIPTION1', '2016年秋、広島パルコ新館15周年！広島パルコの新館15周年を記念して本館、新館が大リニューアル！');
define('KEYWORDS1', '広島,15周年,イベント,リニューアル,新館,本館,PARCO,パルコ');
define('DESCRIPTION2', '広島PARCOのNEW SHOP＆RENEWAL SHOPのブランドコンセプトや取扱いアイテムなどをご紹介！2017年春、広島PARCOに注目のショップが続々オープン！');
define('KEYWORDS2', 'NEW,RENEWAL,リニューアル,広島パルコ,PARCO');
define('DESCRIPTION3', '2016年秋、広島パルコ新館15周年！広島パルコの新館15周年を記念して様々なイベント開催します！');
define('KEYWORDS3', '広島,15周年,イベント,リニューアル,新館,本館,PARCO,パルコ');
define('POST_SDATE1', '');
define('POST_EDATE1', '20170810000000');
//define('POSTS_SDATE2', '20160401000000'); //ブログごとに違う場合
//define('POSTS_EDATE2', '20170401000000'); //ブログごとに違う場合
define('CATEGORY_ID1', '');
// define('CATEGORY_ID2', '32');
$sns_url = 'http://hiroshima.parco.jp/web/annex15th/';
$sns_url_sp = 'http://hiroshima.parco.jp/s/annex15th/';
$sns_title_encode = urlencode(TITLE);
?>
