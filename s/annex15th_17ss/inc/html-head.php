<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title><?php echo $title ?></title>
<meta name="description" content="<?php echo $description ?>" />
<meta name="keywords" content="<?php echo $keywords ?>"/>
<?php include_once'inc/ogp.php'; ?>
<link rel="canonical" href="/web/<?=DIRNAME?>/main.php" />
<link rel="stylesheet" href="css/default.css" media="screen,print" />

<link rel="stylesheet" href="css/style.css" media="screen,print" />
<link rel="stylesheet" href="css/<?=PAGE_NAME?>.css" />

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<!-- <script>window.jQuery || document.write('<script src="js/jquery.js"><\/script>')</script> -->
<!-- <script src="/common/js/jquery.js"></script> -->
<script src="js/masonary.min.js"></script>
<!--<script src="js/jquery.transit.min.js"></script>-->
<script src="js/common.js"></script>
<script src="../common/js/social.js"></script>
<script src="../common/js/imgLiquid.js"></script>
