// SPサイト　ソーシャルアクション用処理
var social = {
    /**
     * AJAX の結果に含まれる status_code のパターン一覧
     */
    status: {
        /**
         * 入力 OK
         */
        OK        : 0,
        
        /**
         * 必須パラメータが空欄であることを示すエラーコード
         */
        EMPTY     : 1,
        
        /**
         * 存在しない店舗コード・ブログID・記事IDが指定されたことを示すエラーコード
         */
        INVALID   : 2,
        
        /**
         * ログインしていないことを示すエラーコード
         */
        NOT_LOGIN : 3,
        
        /**
         * サーバーエラー
         */
        ERROR     : 99
    },
    
    /**
     * このユーザーがログイン済の場合のみ true
     */
    isLogin:      false,
    
    /**
     * このユーザーがログイン済で、かつ twitter 連携している場合のみ true
     */
    linksToTwitter:      false,
    
    /**
     * このユーザーがログイン済で、かつ facebook 連携している場合のみ true
     */
    linksToFacebook:     false,
    
    /**
     * このユーザーがログイン済で、かつ LIKE と facebook いいねを連携している場合のみ true
     */
    acceptsFacebookLike: false,
    
    // 店舗コード
    store_code: "",
    
    // ブログID
    blog_id: -1,
    
    // ブログパス名
    blog_path: "",
    
    // 特設ブログパス名変換用配列
    special_blog_paths: {},
    
    // 特設ブログID変換用配列
    special_blog_ids: {},
    
    // 記事ID
    post_id: -1,
    
    // お気に入り登録ボタンjQueryパス
    follow_btn_jquery_path: "",
    
    // お気に入り登録ボタン画像jQueryパス
    follow_btn_img_jquery_path: "",
    
    // お気に入り登録件数jQueryパス
    follow_number_jquery_path: "",
    
    // お気に入り登録ボタン画像パス
    follow_btn_img_path: "/s/common/images/common_btn_003",
    
    // ログインしているかどうかを確認します。
    // もしも未ログインの場合はログインページに移動します
    checkLogin: function() {
        if (social.isLogin) {
            return true;
        }
        else {
            location.href = "https://" + common.portalDomain + "/spage2/member/login?store_code=" + common.storeCode + "&next=" + location.pathname;
            return false;
        }
    },
    // お気に入り登録ボタンクリック時
    onClickShopFollow: function() {
        social.checkLogin();
        
        // ロック
        
        // お気に入り状態変更呼び出し
        $.ajax({
            type: "post",
            url: "/social/follow/switch/",
            data: {
                "store_code": common.storeCode, 
                "blog_id": social.blog_id
            },
            dateType: "json",
            success: function(data){
                if (data.error_code == 0) {
                    // 現在のお気に入り件数の取得
                    var fav_number_text = $(social.follow_number_jquery_path).text().replace(/人/,'');
                    var fav_number = parseInt(fav_number_text);
                    var img_path = social.follow_btn_img_path;
                    // 変更後のお気に入りが登録状態であれば画像は登録状態、件数は+1
                    if (data.follow_id) {
                        fav_number++;
                        img_path += "_follow";
                    } else {
                        // img_url = ****
                        fav_number--;
                    }
                    
                    // 件数の変更
                    $(social.follow_number_jquery_path).text(fav_number + "人");
                    // 画像の変更
                    $(social.follow_btn_img_jquery_path).attr('src', img_path+".png");
                } else if (data.error_code == 3) {
                    // ログインしていない場合は、その場でログインモーダル表示
                    if (location.protocol == "https:"){
                        location.href = "/spage2/member/login";
                    } else{
                        window.open("https://"+ common.portalDomain + "/spage2/member/login", "secure");
                    }
                }
            } ,
            error: function(){ }
        });
    },
    
    changeDisplay: function(button, parentClass, enabled, shift) {
        if (button[0] != null) {
            for (var i = 0; i < button.length; i ++) {
                social.changeDisplay(button[i], parentClass, enabled, shift);
            }
            return;
        }
        
        shift  = (shift == null) ? 0 : parseInt(shift, 10);
        var div       = $(button).closest("." + parentClass);
        var number    = $(".like_number p", div);
        if (number.length==0) {
            var number = $(".like_number", div);
        }
        var value     = parseInt(number.text(), 10);
        var newValue  = isNaN(value) ? shift : value + shift;
        number.text(Math.max(0, newValue));
        
        var imgAttr = enabled ?
        {
            src:  "/s/common/images/common_icon_003_act.png",
            psrc: "/s/common/images/common_icon_003_act.png",
            osrc: "/s/common/images/common_icon_001_act_on.png"
        }
        :
        {
            src:  "/s/common/images/common_icon_003.png",
            psrc: "/s/common/images/common_icon_003.png",
            osrc: "/s/common/images/common_icon_003_on.png"
        };
        $("img", button).attr(imgAttr);
    },
    
    // LIKEボタンクリック時
    onClickLike: function(blog_id, post_id, className, button, another) {
        if (!social.checkLogin()) {
            return;
        }
        
        $.ajax({
            type : "POST",
            url  : "/social/favorite/switch",
            data : {
                store_code: common.storeCode,
                blog_id:    blog_id,
                post_id:    post_id
            },
            complete : function(xhr) {
                var status = social.status;
                var json   = common.getJsonByXHR(xhr, {error_code: status.ERROR, error_status: "システムエラーが発生しました。"});
                switch (json.error_code) {
                    case status.OK:
                        // LIKE 解除の場合はお気に入り総数を -1, LIKE した場合はお気に入り総数を +1 します。
                        // count プロパティが null の場合は解除とみなします。
                        var enabled   = (json.favorite_id);
                        var increment = enabled ? 1 : -1;
                        
                        // FB連携
                        var facebook_like_id = "";
                        if (social.acceptsFacebookLike) {
                            // 記事のURL取得
                            var blog_path = blog_id;
                            if (social.special_blog_ids[blog_id] != undefined) {
                                blog_path = social.special_blog_ids[blog_id];
                            }
                            var url = "http://"+ location.hostname+ "/spage2/"+ blog_path+ "/"+ post_id+ "/";
                            // FBいいね登録（解除はサーバ側で）
                            if (enabled) {
                                $.ajax({
                                    type: "POST",
                                    url: "https://graph.facebook.com/me/og.likes",
                                    data: {
                                        "object": url,
                                        "access_token": social.accessTokenFacebook
                                    },
                                    dataType: "json",
                                    success: function(data) {
                                        // 登録した記事お気に入り情報にLike IDを追加する
                                        $.ajax({
                                            type: "POST",
                                            url: "/social/favorite/setfblike/",
                                            data: {
                                                "favorite_id": json.favorite_id,
                                                "facebook_like_id": data.id
                                            },
                                            dataType: "json",
                                            success: function(data){
                                            }
                                        });
                                    }
                                });
                            }
                        }
                        social.changeDisplay(button, className, enabled, increment);
                        if (another != null) {
                            social.changeDisplay(another, className, enabled, increment);
                        }
                        
                        if (enabled && $.cookie("no_more_help") == null) {
                            $.fancybox.open({
                                href: "/spage2/social/favorite/done", 
                                type:"iframe"
                            });
                        }
                        return false;
                    case status.NOT_LOGIN:
                        // ログインしていない場合は、その場でログインモーダル表示
                        if(location.protocol == "https:"){
                            location.href = "/spage2/member/login";
                        }else{
                            window.open("https://"+ common.portalDomain + "/spage2/member/login", "secure");
                        }
                        return false;
                    default:
                        alert(json.error_status);
                        return false;
                }
            }
        });
    },
    
    // シェアボタンクリック時
    onClickShare: function(shop_id, post_id) {
        social.checkLogin();       
        if (social.isLogin) {
            // ポータルサイトの場合は store_code をパラメータに付与
            var q = (location.hostname == common.storeDomain) ? "" : "?store_code=" + common.storeCode;
            $.fancybox.open({
                href: "/spage2/social/share/" + shop_id + "/" + post_id + q, 
                type:"iframe"
            });
        }
    },
    
    // 友達に送るボタンクリック時
    onClickSend: function(shop_id, post_id) {
        social.checkLogin();
        if(social.isLogin){
            // ポータルサイトの場合は store_code をパラメータに付与
            var q = (location.hostname == common.storeDomain) ? "" : "?store_code=" + common.storeCode;
            $.fancybox.open({
                href: "/spage2/social/send/" + shop_id + "/" + post_id + q, 
                type:"iframe"
            });
        }
    },
    
    // 記事一覧用の初期表示 /social/favorite/get用のパラメータを指定
    initPostList: function(fav_param_posts) {
        $.ajax({
            type: "post",
            url: "/social/favorite/get/",
            data: {"ids": fav_param_posts},
            dataType: "json",
            success: function(data) {
                if(data.error_code == 0){
                    for(var i=0; i<data.favorites.length; i++){
                        if (data.favorites[i] != null && data.favorites[i].favorite_id != "") {
                            $("div#post_"+ i+ " li.icon_like img").attr("src", "/s/common/images/common_icon_003_act.png");
                            $("div#post_"+ i+ "_disnon li.icon_like img").attr("src", "/s/common/images/common_icon_003_act.png");
                        }
                    }
                }
            }
        });
    },
    
    // 初期化
    init: function(){
        // ブログID取得
        var blogMatch = location.pathname.match(/^\/spage2\/([^\/]+)\/?/);
        if (blogMatch != null) {
            social.blog_id = blogMatch[1];
            // 取得したブログIDが特設ブログのパス名だった場合はIDに変換
            if (social.special_blog_paths[social.blog_id] != undefined) {
                social.blog_path = social.blog_id;
                social.blog_id = social.special_blog_paths[social.blog_id];
            }
        }
        
        // 記事ID取得
        var postMatch = location.pathname.match(/^\/spage2\/[^\/]+\/([0-9]+)\/?/);
        if(postMatch != null) {
            social.post_id = postMatch[1];
        }
        
        // jQueryパスを決定
        social.follow_btn_jquery_path = 'div.item_block_01 div.info_block_02 p.fav_blog';
        social.follow_btn_img_jquery_path = 'div.item_block_01 div.info_block_02 p.fav_blog img';
        social.follow_number_jquery_path = 'div.item_block_01 div.info_block_02 p.fav_number';
    }
};

$(document).ready(function(){
    social.init();
});
