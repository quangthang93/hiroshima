<?php
/**
 * AJAXでJSONファイルを読み込むJavascriptです
 * 
**/
//基本情報の取得
include_once('../../../../approot/api/classes/Config.class.php'); 
?>

//=====記事サーチSTART=====
/**
 * AJAXで記事を表示するJavascriptです
 * 
**/
//ページが読み込まれたら記事を表示
function shop_Onload(){
	$(function(){
		var turn = 0;
		new_like_Search(turn);
		s_shop_Ajax(turn);
		if(0 < $('#parts_class_disnon').size()){
			s_shop_disnon_Ajax(turn);
		}
	});
}

function new_like_Search(turn){
	//新着ボタン、LIKE数順イベント
	$('#main .tab_block_01 .clear .genre li').click(function(){
		var flag = $(this).attr('class');
		if(flag != 'active'){
			if($('a', this).text() == '新着順'){
				turn = 1;
				s_shop_Ajax(turn);
				if(0 < $('#parts_class_disnon').size()){
					s_shop_disnon_Ajax(turn);
				}
			}else{
				turn = 0;
				s_shop_Ajax(turn);
				if(0 < $('#parts_class_disnon').size()){
					s_shop_disnon_Ajax(turn);
				}
			}
		}
	});
}

function squeeze_Search() {
	$('.modal_block_01 .btn_block_09').click(function(){
		//新着順とお気に入り順の取得
		if($('#main .tab_block_01 .clear .genre .active',parent.window.document).text() == '新着順'){
			var turn = 1;
		}else{
			var turn = 0;
		}
		//選択中のアイテムIDを取得
		$('#item_id',parent.window.document).val($('.select_box select:eq(0)').val());
		
		//選択中のイベントIDを取得
		$('#event_id',parent.window.document).val($('.select_box select:eq(1)').val());
		
		//選択中のカレンダーIDを代入
		$('#calender_id',parent.window.document).val($('.select_box select:eq(2)').val());
		s_shop_Ajax_parent(turn);
		if(0 < $('#parts_class_disnon',parent.window.document).size()){
			s_shop_disnon_Ajax_parent(turn);
		}
	});
}

//記事絞り込みに必要なデータをAJAXでPOST
function s_shop_Ajax(turn){
	$.ajax({
		url: "<?php echo $S_PARTS_PATH; ?>" + $('#parts_name').val(),
		type: 'POST',
		dataType:'html',
		async: false,
		data: {
			blog: {'floor_id': 'all',
				   'item_id': $('#item_id').val(),
				   'event_id': $('#event_id').val(),
				   'turn_flag': turn,
				   'event_date': $('#calender_id').val(),
			},
		},
		success: function(data) {
			$($('#parts_class').val()).html(data);
		},
	});
}

// 記事絞り込みに必要なデータをAJAXでPOSTする(非表示)
function s_shop_disnon_Ajax(turn){
	$.ajax({
		url: '<?php echo $S_PARTS_PATH; ?>' + $('#parts_name_disnon').val(),
		type: 'POST',
		dataType:'html',
		async: false,
		data: {
			blog: {'floor_id': 'all',
				   'item_id': $('#item_id').val(),
				   'event_id': $('#event_id').val(),
				   'turn_flag': turn,
				   'event_date': $('#calender_id').val(),
				   },
		},
		success: function(data) {
			$($('#parts_class_disnon').val()).html(data);
		},
	});
}

//記事絞り込みに必要なデータをAJAXでPOST(子画面)
function s_shop_Ajax_parent(turn){
	$.ajax({
		url: "<?php echo $S_PARTS_PATH; ?>" + $('#parts_name',parent.window.document).val(),
		type: 'POST',
		dataType:'html',
		async: false,
		data: {
			blog: {'floor_id': 'all',
				   'item_id': $('#item_id',parent.window.document).val(),
				   'event_id': $('#event_id',parent.window.document).val(),
				   'turn_flag': turn,
				   'event_date': $('#calender_id',parent.window.document).val(),
			},
		},
		success: function(data) {
			$($('#parts_class',parent.window.document).val(),parent.window.document).html(data);
		},
	});
}

//記事絞り込みに必要なデータをAJAXでPOST(子画面, 非表示)
function s_shop_disnon_Ajax_parent(turn){
	$.ajax({
		url: "<?php echo $S_PARTS_PATH; ?>" + $('#parts_name_disnon',parent.window.document).val(),
		type: 'POST',
		dataType:'html',
		async: false,
		data: {
			blog: {'floor_id': 'all',
				   'item_id': $('#item_id',parent.window.document).val(),
				   'event_id': $('#event_id',parent.window.document).val(),
				   'turn_flag': turn,
				   'event_date': $('#calender_id',parent.window.document).val(),
			},
		},
		success: function(data) {
			$($('#parts_class_disnon',parent.window.document).val(),parent.window.document).html(data);
		},
	});
}
