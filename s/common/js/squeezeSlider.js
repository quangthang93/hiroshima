var squeeze = "off";

// 「条件を絞り込む」ボタン押下字のページスクロール量
var lastScrollTop;

/**
 * ページ初期化処理
 */
$(function() {
    // 絞り込み画面の内容が空の場合は、非同期で取得
    if (!$("#squeeze div").length) {
        $.ajax({
            type : 'GET',
            url : '/spage2/parts/' + $('#squeeze_name').val() + '/',
            dataType : 'html',
            success : function(data) {
                $('#squeeze').append(data);
            },
            error : function() {
                alert('Webページの表示に問題が発生しました。');
            }
        });
    }

    // 初期ロード時、画面回転時に絞り込み画面の幅をウィンドウ幅に揃える
    $(window).bind("resize load", function() {
        $("#squeeze").css('width', $(window).width() + 'px');
		});

    // 「条件を絞り込む」ボタン押下で絞り込み画面をオープンする
    $(".openSqueeze").click(function() {
        if (squeeze == "off") {
            openSqueeze();
		}
	});
});

/**
 * 絞り込み画面を開きます。
 */
function openSqueeze() {
    // バナーカルーセルを一時停止する。
    if(0 < $('.flexslider').size()) {
        var slider = $('.flexslider').data('flexslider');
        if(slider) {
            slider.pause();
        }
    }
    squeeze = "sliding";
    
    // ボタン押下時のスクロール位置を記憶
    lastScrollTop = $(window).scrollTop();

    // スムーズにスクロール位置を最上部まで移動してから絞り込み画面を開く
    $($.browser.safari ? 'body' : 'html').animate({scrollTop:0}, 300, 'swing', function(){
        $("#container").css("-webkit-transform", "translate3d(-" + $(window).width() + "px, 0, 0)").one('webkitTransitionEnd', endOpenSqueeze);
        $("#squeeze").addClass('visible');
    });
}

/**
 * 絞り込み画面を開くアニメーション完了時のハンドラ
 */
function endOpenSqueeze() {
    if (squeeze === "sliding") {
        squeeze = "on";
    } else {
		return false;
    }

    // コンテンツの高さを絞り込み画面の高さに揃える（先にメニューをdisplay:noneにしておく）
    $("#leftmenu").css("display", "none");
    $('html, body').css({
        "overflow-x" : "hidden",
        "height" : $('#squeeze').css("height")
    });
    $('body').css("overflow-y", "hidden");

    // メインコンテンツを背面に
    $("#squeeze").css('z-index', '1');
    $("#container").css('z-index', '-1');
}

/**
 * 絞り込み画面を閉じます。
 */
function closeSqueeze() {
    squeeze = "sliding";

    // メインコンテンツを前面に
    $("#container").css('z-index', '1');
    $("#squeeze").css('z-index', '-1');

    $("#container").css("-webkit-transform", "translate3d(0, 0, 0)").one('webkitTransitionEnd', endCloseSqueeze);
    $("#squeeze").removeClass('visible');
}

/**
 * 絞り込み画面を閉じるアニメーション完了時のハンドラ
 */
function endCloseSqueeze() {
    if (squeeze === "sliding") {
        squeeze = "off";
    } else {
			return false;
		}

    // 絞り込み画面の高さに揃えていたコンテンツの高さを元に戻す
    $("#leftmenu").css("display", "");
    $('html, body').css({
        "overflow-x" : "none",
        "height" : "auto"
    });
    $('body').css("overflow-y", "none");

    // スクロール位置を「条件を絞り込む」ボタン押下時のスクロール位置までスムーズに戻す
    $($.browser.safari ? 'body' : 'html').animate({scrollTop:lastScrollTop}, 300, 'swing', function() {
        // バナーカルーセルを再開
        if(0 < $('.flexslider').size()) {
            var slider = $('.flexslider').data('flexslider');
            if(slider) slider.play();
        }
	});
}