
//=====記事サーチSTART=====
/**
 * AJAXで記事を表示するJavascriptです
 *
**/
/* ページが読み込まれたら記事を表示 */
function ajax_Onload(){
    $(function(){
        s_Ajax(common.storeCode);

        // 店舗選択ボタンクリック時
        $('.item_block_01 div.select_box select').change(function(){
            s_Ajax($('#store_list option:selected').attr("id"));
        });
    });
}

//記事絞り込みに必要なデータをAJAXでPOST
function s_Ajax(store){
    $.ajax({
        url: "/page2/ajax/spgetpart/" + $('#parts_name').val(),
        type: 'POST',
        dataType:'html',
        async: false,
        data: {
            blog: {'store_code': store,
            },
        },
        success: function(data) {
            $($('#parts_class').val()).html(data);
            more();
        },
    });
}


// もっと見る用の処理を行います
function more() {
    var nowNum = 4;        // 現在の表示件数(0から数える)
    var addNum = 10;    // 増加量

    // 表示しているブログブロック数を取得します
    var blockNum = $('.commonlist_block_02:eq(1) ul li').size();

    // 表示件数が6件以上の場合処理を行います
    if (blockNum >= 6) {
        $('.tac a').click(function () {

            // 表示件数を増加します
            nowNum += addNum;

            // 表示を切り替えます
            $('.commonlist_block_02 ul li').css('display', '');
            $('.commonlist_block_02 ul li:gt(' + (nowNum) + ')').css('display', 'none');

            // 表示件数が表示件数を超えた場合
            if (nowNum + 1 >= blockNum) {
                // ボタンのクリックイベント削除
                $('.tac').unbind('click');

                //もっと見るボタンを削除
                $('.tac').remove();

                return false;
            }
        });
    }else{
        $('.tac').remove();
    }
}