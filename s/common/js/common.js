var link_enabled = false;
var link_func = function(){
	return link_enabled;
};
$('a').click( function(){ return link_func(); });

var common = {
    /**
     * @type String
     */
    storeCode: "",
    
    /**
     * @type String
     */
    storeDomain: "",
    
    /**
     * @type String
     */
    portalDomain: "",
    
    getJsonByXHR: function(xhr, failedValue) {
        if (failedValue == null) {
            failedValue = { result: "error" };
        }
        
        try {
            var json = $.parseJSON(xhr.responseText);
            return (json != null) ? json : failedValue;
        } catch (e) {
            return failedValue;
        }
    }
};
/* Close Button */
$(document).ready(function() {
    var hasOpener = (window.opener != null);
    if (hasOpener) {
        var backToTop = $("#main div.back_to_top");
        backToTop.removeClass("back_to_top").addClass("close");
        backToTop.html("").append(
            $("<a/>").attr("href", "#").click(function() { window.close(); return false; }).append(
                $("<img/>").attr("src", "/s/common/images/btn_close_01.png").attr("alt", "閉じる")
            )
        );
    }
    else {
        var close  = $("#main div.close");
        var domain = common.storeDomain.length ? common.storeDomain : location.hostname;
        close.removeClass("close").addClass("back_to_top");
        close.html("").append(
            $("<div/>").addClass("btn_block_09").append(
                $("<a/>").attr("href", "http://" + domain + "/spage2/").text("トップページに戻る")
            )
        );
    }
});

/* Open External Link in New Window */
//自身のドメインと、ポータルのドメイン以外へのリンクを別ウィンドウで開く
$(document).ready( function () {
    if (location.hostname !== common.portalDomain) {
        $("a[href^='http']:not({[href*='" + location.hostname + "'],[href*='" + common.portalDomain + "']})").attr('target', '_blank');
	link_enabled = true;
    }
})
/* wear logo click open new window */
$(document).ready(function(){
    $('.entry_block #wear_profile_image').click(function() {
        var user_name = $(this).attr("data-wear-username");
        var snap_id = $(this).attr("data-wear-snapid");
        if(user_name === undefined || snap_id === undefined || user_name == '' || snap_id == '') return false;
        var wear_href = 'http://wear.jp/' + user_name + '/';
        window.open(wear_href, '_blank');
        return false;
    });
    $('.entry_block #wear_logo_image').click(function() {
        var user_name = $(this).attr("data-wear-username");
        var snap_id = $(this).attr("data-wear-snapid");
        if(user_name === undefined || snap_id === undefined || user_name == '' || snap_id == '') return false;
        var wear_href = 'http://wear.jp/' + user_name + '/coordinate/' + snap_id + '/';
        window.open(wear_href, '_blank');
        return false;
    });

    // shop header clip num
    $('#shop_header_clip_num').click(function() {
        $.fancybox.open({
            href: "/s/ppclip/",
            type: "iframe",
            width: "305"
        });
    });
});