//=====記事サーチSTART=====
/**
 * AJAXで記事を表示するJavascriptです
 *
 */
var blogs = {
    index: 0,
    posts: [],
    getWrapper: function() {
        var selector  = $("#parts_class").val();
        var container = $(selector);
        var wrapper   = $("div.thumbnail_block_wrap", container);
        if (wrapper.length) {
            return wrapper;
        }
        
        container
        .append($("<div/>").addClass("thumbnail_block_wrap clear"))
        .append(
            $("<div/>").addClass("btn_block_07")
            .append(
                $("<a/>").attr("href", "javascript:void(0)").text("もっと見る")
            )
        );
        return $("div.thumbnail_block_wrap", container);
    },
    displayReset: function() {
        var selector  = $("#parts_class").val();
        var container = $(selector);
        container.children().remove();
    },
    displayMore: function(count) {
        var wrapper = blogs.getWrapper();
        var next    = Math.min(blogs.index + count, blogs.posts.length);
        for (var i = blogs.index; i < next; i ++) {
            wrapper.append(blogs.posts[i]);
        }
        if(typeof imgLiquid != "undefined"){
            $('.imgLiquidBox').imgLiquid();
        }
        
        $('.thumbnail_block img').imagesLoaded(function () {
            if ($(".masonry-brick", wrapper).length > 0) {
                $(".thumbnail_block_wrap").masonry("destroy");
            }
            $('.thumbnail_block_wrap').masonry({
                itemSelector: '.thumbnail_block'
            });
            thumbnailBlockWrapResize();
        });
        
        blogs.index = next;
    }
};

//ページが読み込まれたら記事を表示
function ajax_Onload(){
    $(function(){
        var turn = 2;
        new_like_Search();
        follow_blog();
        show_flag();
        s_Ajax(turn);
    });
}
//サムネイル、リスト表示イベント
function show_flag(){
  $('.clear .tab li').click(function(){

    var turn = $('#top_blog_sort').val();

    if($('.clear .tab li.select a').text() == 'サムネイル'){
        $("#tab02").children().remove();
      s_Ajax(turn);
    }else{
        $("#tab01").children().remove();
      s_Ajax_disnon(turn);
    }

    });
}

//新着ボタン、LIKE数順イベント
function new_like_Search(){
    $('#top_blog_sort').change(function(){

    var turn = $('#top_blog_sort').val();

    if($('.clear .tab li.select a').text() == 'サムネイル'){
      s_Ajax(turn);
    }else{
      s_Ajax_disnon(turn);
    }

    });
}

//お気に入りイベント
function follow_blog() {
    $('p.fav img').click(function() {
        var fav_input = $("#favorite_flag");
        var next      = fav_input.val() == "0" ? "1" : "0";
        var next_img  = next == "0" ? "/s/common/images/common_btn_010.png" : "/s/common/images/common_btn_010_on.png";
        fav_input.val(next);
        $(this).attr("src", next_img);
        
        var turn = $('#top_blog_sort').val();

        if($('.clear .tab li.select a').text() == 'サムネイル'){
          s_Ajax(turn);
        }else{
          s_Ajax_disnon(turn);
        }
    });
}

function squeeze_Search() {
    $('.modal_block_01 .btn_block_09').click(function(){
        //選択中のアイテムIDを取得
        $('#item_id').val($("#search_select_item").val());

        //選択中のイベントIDを取得
        $('#event_id').val($("#search_select_event").val());

        //選択中のカレンダーIDを代入
        $('#calender_id').val($("#search_select_calendar").val());

        if($("#search_select_item option:selected").text() != 'カテゴリを選択'){
            addSearchLabel('item', $("#search_select_item option:selected").text());
        }else{
            $(".ct_labelBox .label_item").addClass("hide");
        }
        if($("#search_select_event option:selected").text() != 'イベントを選択'){
            addSearchLabel('event', $("#search_select_event option:selected").text());
        }else{
            $(".ct_labelBox .label_event").addClass("hide");
        }
        if($("#search_select_calendar option:selected").text() != '日付を選択'){
            addSearchLabel('date', $("#search_select_calendar option:selected").text());
        }else{
            $(".ct_labelBox .label_date").addClass("hide");
        }
        
        //枠の表示、非表示
        judgeDisplaySearchLabel();

        //新着順とお気に入り順の取得
        var turn = $('#top_blog_sort').val();

        var show_flag = $('.clear .tab li.select a').text();
        if( show_flag == 'サムネイル'){
            s_Ajax(turn);
        }else{
            s_Ajax_disnon(turn);
        }
    });

    // 絞り込み解除
    $(".ct_labelBox .peel").live("click", function(){
        var search_label = $(this).parent().attr("class");
        if(search_label == "label_item"){
            $('#item_id').val('all');
            $('#search_select_item').prop('selectedIndex', 0);
        }else if(search_label == "label_event"){
            $('#event_id').val('all');
            $('#search_select_event').prop('selectedIndex', 0);
        }else if(search_label == "label_date"){
            $('#calender_id').val('all');
            $('#search_select_calendar').prop('selectedIndex', 0);
        }
        //ラベル非表示
        $(this).parent().addClass("hide");
        judgeDisplaySearchLabel();

        //新着順とお気に入り順の取得
        var turn = $('#top_blog_sort').val();

        var show_flag = $('.clear .tab li.select a').text();
        if( show_flag == 'サムネイル'){
            s_Ajax(turn);
        }else{
            s_Ajax_disnon(turn);
        }
    });
}

//絞り込み条件ラベル追加
function addSearchLabel(type, value){
    var label_text = "";
    if(type == "item"){
        label_text = "カテゴリ：";
    }else if(type == "event"){
        label_text = "イベント：";
    }else if(type == "date"){
        label_text = "日付：";
    }
    label_text += value;
    label_name = "label_" + type;
    
    $(".ct_labelBox ." + label_name).children("span").text(label_text);
    $(".ct_labelBox ." + label_name).removeClass("hide");
    
    judgeDisplaySearchLabel();

}

//絞り込み条件ラベル枠全体の表示判定
function judgeDisplaySearchLabel(){
    //表示するラベルがなければ枠ごと非表示
    if($(".ct_labelBox li.hide").length < 3){
        $(".ct_labelBox").show();
    }else{
        $(".ct_labelBox").hide();
    }
}

//記事絞り込みに必要なデータをAJAXでPOST
function s_Ajax(turn) {
    var is_sp_top = $("#is_sp_top").length ? 1 : 0;
    var keep = $("#condition_keep").length ? $("#condition_keep").val() : 1;
    $.ajax({
        url: "/page2/ajax/spgetpart/" + $('#parts_name').val(),
        type: 'POST',
        dataType:'html',
        async: false,
        data: {
            blog: {
                    'floor_id'      : 'all',
                    'item_id'       : $('#item_id').val(),
                    'event_id'      : $('#event_id').val(),
                    'turn_flag'     : turn,
                    'event_date'    : $('#calender_id').val(),
                    'favorite_flag' : $('#favorite_flag').val()
            },
            keep: keep,
            is_sp_top: is_sp_top
        },
        success: function(data) {
            //blogs.posts = data.split("<!--POST_BOUNDARY-->");
            var boundary = "<!--POST_BOUNDARY-->";
            if (-1 < data.indexOf(boundary)) {
                blogs.posts = data.split(boundary);
                blogs.posts.pop();
            } else {
                blogs.posts = [data];
            }
            blogs.index = 0;
            blogs.displayReset();
            blogs.displayMore(8);
            // 「もっと見る」は辞め、強制的に「一覧」ボタンを表示させる
            $('.btn_block_07 a').text('一覧');
            $('.btn_block_07 a').attr('href', '/spage2/hotblog/');
            $('.btn_block_07 a').unbind('click');
        }
    });
}

// 記事絞り込みに必要なデータをAJAXでPOSTする(非表示)
function s_Ajax_disnon(turn) {
    var is_sp_top = $("#is_sp_top").length ? 1 : 0;
    $.ajax({
        url: '/page2/ajax/spgetpart/' + $('#parts_name_disnon').val(),
        type: 'POST',
        dataType:'html',
        async: false,
        data: {
            blog: {'floor_id': 'all',
                   'item_id': $('#item_id').val(),
                   'event_id': $('#event_id').val(),
                   'turn_flag': turn,
                   'event_date': $('#calender_id').val(),
                   'favorite_flag': $('#favorite_flag').val()
            },
            is_sp_top: is_sp_top
        },
        success: function(data) {
            $($('#parts_class_disnon').val()).html(data);

            thumbnailBlockWrapResize();
            more_disnon();
        }
    });
}

// もっと見る用の処理を行います
function more() {
    var addNum = 10;    // 増加量

    // 表示しているブログブロック数を取得します
    var blockNum = blogs.posts.length;

    // 表示件数が5件以上の場合処理を行います
    if (blockNum >= 5) {
        $('.btn_block_07 a').click(function () {
            // 表示件数を増加します
            blogs.displayMore(addNum);
            
            // 表示件数が表示件数以上になった場合
            if (blogs.index >= blockNum) {
                if ($("#no_blog_list").length) {
                    $(".btn_block_07 a").remove();
                } else {
                    // もっと見るを一覧へ変更
                    $('.btn_block_07 a').text('一覧');
                    $('.btn_block_07 a').attr('href', '/spage2/hotblog/');
                    
                    // ボタンのクリックイベント削除
                    $('.btn_block_07 a').unbind('click');
                }
                return false;
            }
        });
    }else{
        $('.btn_block_07').remove();
    }
}

// もっと見る用の処理を行います(disnon)
function more_disnon() {
    var nowNum = 3;     // 現在の表示件数(1から数える)
    var addNum = 10;    // 増加量

    // 表示しているブログブロック数を取得します
    var blockNum = $('#tab02 .list_block').size();

    // 表示件数が5件以上の場合処理を行います
    if (blockNum >= 5) {
        $('.btn_block_07 a').click(function () {

            // 表示件数を増加します
            nowNum += addNum;

            // 表示を切り替えます
            $('#tab02 .list_block').css('display', '');
            $('#tab02 .list_block:gt(' + (nowNum) + ')').css('display', 'none');

            // ブランク画像を本来の画像に差し替えます
            replaceBlankImages("#tab02 .list_block", nowNum);
            
            // 表示件数が表示件数を超えた場合
            if (nowNum + 1 >= blockNum) {
                // もっと見るを一覧へ変更
                $('.btn_block_07 a').text('一覧');
                $('.btn_block_07 a').attr('href', '/spage2/hotblog/');

                // ボタンのクリックイベント削除
                $('.btn_block_07 a').unbind('click');

                return false;
            }
        });
    }else{
        $('.btn_block_07').remove();
    }
}

// ブランク画像を本来の画像に差し替えます
function replaceBlankImages(baseSelector, nowNum) {
    var visibleList = $(baseSelector + ':lt(' + (nowNum + 1) + ')');
    visibleList.each(function() {
        var img = $(".image a img", this);
        if (img.attr("src").match(/\/common\/images\/blank.gif$/)) {
            img.attr("src", $("input", this).val());
        }
    });
}

// thumbnail_block_wrapのサイズ調整
function thumbnailBlockWrapResize() {
    var margin    = 20;
    var maxHeight = 0;
    $('.thumbnail_block_wrap .thumbnail_block').each(function(){
        if ($(this).css('display') == 'none') return false;
        var top    = parseInt($(this).css('top'));
        var height = $(this).height() + top;

        if (maxHeight <= height) {
            maxHeight = height;
        }
    });

    maxHeight += margin;

    $(".thumbnail_block_wrap").css('height', maxHeight);
}