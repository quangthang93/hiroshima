<?php
/**
 * AJAXでJSONファイルを読み込むJavascriptです
 *
**/
//基本情報の取得
include_once($_SERVER['APPROOT_DIR']. '/api/classes/Config.class.php');
?>

//=====記事サーチSTART=====
/**
 * AJAXで記事を表示するJavascriptです
 *
**/
//ページが読み込まれたら記事を表示
function ajax_Onload(){
    $(function(){
        var turn     = $('#turn_flag').val();
        new_like_Search();
        follow_blog();
        show_flag();
        s_Ajax(turn);
    });
}
//サムネイル、リスト表示イベント
function show_flag(){
    $('.clear .tab li').click(function(){
        var turn = ($('#main .tab_block_01 .clear .genre li.active a').text() == '新着順') ? 1 : 0;

        if($('.clear .tab li.select a').text() == 'サムネイル'){
            $("#tab02").children().remove();
            s_Ajax(turn);
        }else{
            $("#tab01").children().remove();
            s_Ajax_disnon(turn);
        }

    });
}

//新着ボタン、LIKE数順イベント
function new_like_Search(){

    $('#main .tab_block_01 .clear .genre li').click(function(){

        var turn = ($('#main .tab_block_01 .clear .genre li.active a').text() != '新着順') ? 1 : 0;

        if($('.clear .tab li.select a').text() == 'サムネイル'){
            s_Ajax(turn);
        }else{
            s_Ajax_disnon(turn);
        }

    });
}

//お気に入りイベント
function follow_blog() {
    $('p.fav img').click(function(){
        var fav_input = $("#favorite_flag");
        var next      = fav_input.val() == "0" ? "1" : "0";
        var next_img  = next == "0" ? "/s/common/images/common_btn_010.png" : "/s/common/images/common_btn_010_on.png";
        fav_input.val(next);
        $(this).attr("src", next_img);

        var turn = ($('#main .tab_block_01 .clear .genre li.active a').text() == '新着順') ? 1 : 0;
        
        if($('.clear .tab li.select a').text() == 'サムネイル'){
          s_Ajax(turn);
        }else{
          s_Ajax_disnon(turn);
        }
    });
}

function squeeze_Search() {
    $('.modal_block_01 .btn_block_09').click(function(){
        //選択中のアイテムIDを取得
        $('#item_id').val($("#search_select_item").val());

        //選択中のイベントIDを取得
        $('#event_id').val($("#search_select_event").val());

        //選択中のカレンダーIDを代入
        $('#calender_id').val($("#search_select_calendar").val());

        //選択中のフロアIDを代入
        $('#floor_id').val($("#search_select_floor").val());

        //新着順とお気に入り順の取得
        if($('#main .tab_block_01 .clear .genre .active').text() == '新着順'){
            var turn = 1;
        }else{
            var turn = 0;
        }

        var show_flag = $('.clear .tab li.select a').text();
        if( show_flag == 'サムネイル'){
            s_Ajax(turn);
        }else{
            s_Ajax_disnon(turn);
        }
    });
}

//記事絞り込みに必要なデータをAJAXでPOST
function s_Ajax(turn) {
    $.ajax({
        url: "<?php echo $S_PARTS_PATH; ?>" + $('#parts_name').val(),
        type: 'POST',
        dataType:'html',
        async: false,
        data: {
            blog: {'item_id': $('#item_id').val(),
                   'event_id': $('#event_id').val(),
                   'event_date': $('#calender_id').val(),
                   'floor_id': $('#floor_id').val(),
                   'turn_flag': turn,
                   'favorite_flag': $("#favorite_flag").val(),
            },
        },
        success: function(data) {
            $($('#parts_class').val()).html(data);

            $('.thumbnail_block img').imagesLoaded(function () {
                $('.thumbnail_block_wrap').masonry({
                    itemSelector: '.thumbnail_block'
                });
            thumbnailBlockWrapResize();
            });

            more();

        }
    });
}

// 記事絞り込みに必要なデータをAJAXでPOSTする(非表示)
function s_Ajax_disnon(turn){
    $.ajax({
        url: '<?php echo $S_PARTS_PATH; ?>' + $('#parts_name_disnon').val(),
        type: 'POST',
        dataType:'html',
        async: false,
        data: {
            blog: {'item_id': $('#item_id').val(),
                   'event_id': $('#event_id').val(),
                   'event_date': $('#calender_id').val(),
                   'floor_id': $('#floor_id').val(),
                   'turn_flag': turn,
                   'favorite_flag': $("#favorite_flag").val()
                   },
        },
        success: function(data) {
            $($('#parts_class_disnon').val()).html(data);

            more_disnon();
        }
    });
}

// もっと見る用の処理を行います
function more() {
    var nowNum = 3;     // 現在の表示件数(0から数える)
    var addNum = 10;    // 増加量

    // 表示しているブログブロック数を取得します
    var blockNum = $('#tab01 .thumbnail_block').size();

        // 表示件数が5件以上の場合処理を行います
        if (blockNum >= 5) {
        $('.btn_block_07 a').click(function () {

            // 表示件数を増加します
            nowNum += addNum;

            // 表示を切り替えます
            $('#tab01 .thumbnail_block').css('display', '');
            $('#tab01 .thumbnail_block:gt(' + (nowNum) + ')').css('display', 'none');

            // ブランク画像を本来の画像に差し替えます
            replaceBlankImages("#tab01 .thumbnail_block", nowNum);
            
            // thumbnail_block_wrapのサイズ調整
            thumbnailBlockWrapResize();

            // 表示件数が表示件数を超えた場合
            if (nowNum + 1 >= blockNum) {
                // ボタンのクリックイベント削除
                $('.btn_block_07').unbind('click');

                //もっと見るボタンを削除
                $('.btn_block_07').remove();

                return false;
            }
        });
    }else{
        $('.btn_block_07').remove();
    }
}

// もっと見る用の処理を行います(disnon)
function more_disnon() {
    var nowNum = 3;     // 現在の表示件数(0から数える)
    var addNum = 10;    // 増加量

    // 表示しているブログブロック数を取得します
    var blockNum = $('#tab02 .list_block').size();

    // 表示件数が5件以上の場合処理を行います
    if (blockNum >= 5) {
        $('.btn_block_07 a').click(function () {

            // 表示件数を増加します
            nowNum += addNum;

            // 表示を切り替えます
            $('#tab02 .list_block').css('display', '');
            $('#tab02 .list_block:gt(' + (nowNum) + ')').css('display', 'none');

            // ブランク画像を本来の画像に差し替えます
            replaceBlankImages("#tab02 .list_block", nowNum);
            
            // 表示件数が表示件数を超えた場合
            if (nowNum + 1 >= blockNum) {
                // ボタンのクリックイベント削除
                $('.btn_block_07').unbind('click');

                //もっと見るボタンを削除
                $('.btn_block_07').remove();

                return false;
            }
        });
    }else{
        $('.btn_block_07').remove();
    }
}

// ブランク画像を本来の画像に差し替えます
function replaceBlankImages(baseSelector, nowNum) {
    var visibleList = $(baseSelector + ':lt(' + (nowNum + 1) + ')');
    visibleList.each(function() {
        var img = $(".image a img", this);
        if (img.attr("src").match(/\/common\/images\/blank.gif$/)) {
            img.attr("src", $("input", this).val());
        }
    });
}

// thumbnail_block_wrapのサイズ調整
function thumbnailBlockWrapResize() {
    var margin    = 20;
    var maxHeight = 0;
    $('.thumbnail_block_wrap .thumbnail_block').each(function(){
        if ($(this).css('display') == 'none') return false;
        var top    = parseInt($(this).css('top'));
        var height = $(this).height() + top;

        if (maxHeight <= height) {
            maxHeight = height;
        }
    });

    maxHeight += margin;

    $(".thumbnail_block_wrap").css('height', maxHeight);
}