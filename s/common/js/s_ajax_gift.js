//=====記事サーチSTART=====
/**
 * AJAXで記事を表示するJavascriptです
 *
 */
var blogs = {
    index: 0,
    posts: [],
    getWrapper: function() {
        var selector         = $("#parts_class").val();
        var container = $(selector);
        var wrapper_class    = $("#wrapper_class").val();
        var wrapper_selector = wrapper_class.replace(/\s/, ".");
        var wrapper   = $("div." + wrapper_selector, container);
        if (wrapper.length) {
            return wrapper;
        }
        
        container
        .append($("<div/>").addClass(wrapper_class))
        .append(
            $("<div/>").addClass("btn_block_07")
            .append(
            $("<a/>").attr("href", "javascript:void(0)").text("もっと見る")
            )
        );
        return $("div." + wrapper_selector, container);
    },
    displayReset: function() {
        var selector         = $("#parts_class").val();
        var container = $(selector);
        container.children().remove();
    },
    displayMore: function(count) {
        var wrapper = blogs.getWrapper();
        var next    = Math.min(blogs.index + count, blogs.posts.length);
        for (var i = blogs.index; i < next; i ++) {
            wrapper.append(blogs.posts[i]);
        }
        //記事の高さ調整 giftだけ
        $('.thumbnail_block img').imagesLoaded(function () {
            if ($(".masonry-brick", wrapper).length > 0) {
                $(".thumbnail_block_wrap").masonry("destroy");
            }
            $('.thumbnail_block_wrap').masonry({
                itemSelector: '.thumbnail_block'
            });
            thumbnailBlockWrapResize();
        });
        blogs.index = next;
    }
};

//ページが読み込まれたら記事を表示
function ajax_Onload(){
    $(function(){
        s_Ajax();
        $("#select_store").change(function(){
            $('#store_code').val($("#select_store option:selected").val());
            s_Ajax();
        });
        $("#select_special").change(function(){
            $('#special_id').val($("#select_special option:selected").val());
            s_Ajax();
        });
        $("#select_item").change(function(){
            $('#item_id').val($("#select_item option:selected").val());
            s_Ajax();
        });
    });
}

//記事絞り込みに必要なデータをAJAXでPOST
// @TODO COOKIEからblogの値を取得する
function s_Ajax() {

    $.ajax({
        url: "/page2/ajax/spgetpart/" + $('#parts_name').val(),
        type: 'POST',
        dataType:'html',
        async: false,
        data: {
            blog: {
                   'item_id': $('#item_id').val(),
                   'type': $('#type').val(),
                   'store_code': $('#store_code').val(),
                   'special_id': $("#special_id").val(),
                   'start_at'  : $("#start_at").val()
            }
        },
        success: function(data) {
            blogs.posts = data.split("<!--POST_BOUNDARY-->");
            blogs.index = 0;
            blogs.displayReset();
            var firstCount = 10;
            var increment = 10;
            blogs.displayMore(firstCount);
            more(firstCount, increment);
        }
    });
}

// もっと見る用の処理を行います
function more(firstCount, increment) {
    // 表示できる総数を取得します(data.splitで最後の配列が空になるため、-1しています)
    var blockNum = blogs.posts.length-1;
    // 表示件数がfirstCount件以上の場合処理を行います
    if (blockNum > firstCount) {
        $('.btn_block_07 a').click(function () {
            // 表示件数を増加します
            blogs.displayMore(increment);

            // 表示件数が表示件数以上になった場合
            if (blogs.index >= blockNum) {
                // ボタンのクリックイベント削除
                $('.btn_block_07').unbind('click');
                //もっと見るボタンを削除
                $('.btn_block_07').remove();
                
                return false;
            }
        });
    }else{
        $('.btn_block_07').remove();
    }
}

// ブランク画像を本来の画像に差し替えます
function replaceBlankImages(baseSelector, nowNum) {
    var visibleList = $(baseSelector + ':lt(' + (nowNum + 1) + ')');
    visibleList.each(function() {
        var img = $(".image a img", this);
        if (img.attr("src").match(/\/common\/images\/blank.gif$/)) {
            img.attr("src", $("input", this).val());
        }
    });
}
// thumbnail_block_wrapのサイズ調整
function thumbnailBlockWrapResize() {
    var margin    = 20;
    var maxHeight = 0;
    $('.thumbnail_block_wrap .thumbnail_block').each(function(){
        if ($(this).css('display') == 'none') return false;
        var top    = parseInt($(this).css('top'));
        var height = $(this).height() + top;

        if (maxHeight <= height) {
            maxHeight = height;
        }
    });

    maxHeight += margin;

    $(".thumbnail_block_wrap").css('height', maxHeight);
}
