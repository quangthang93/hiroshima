/*
 * jQuery squeeze
 * Version 2.0
 * http://srobbin.com/jquery-squeeze/
 *
 * jQuery Javascript plugin which slides a webpage over to reveal an additional interaction pane.
 *
 * Copyright (c) 2011 Scott Robbin (srobbin.com)
 * Dual licensed under the MIT and GPL licenses.
*/

;(function($){
    // Convenience vars for accessing elements
	var $body, $squeeze;

    var _sliding = false,   // Mutex to assist closing only once
        _lastCaller;        // Used to keep track of last element to trigger squeeze

    
    /*
     * Private methods 
     */
    function _load( url, useIframe ) {
        // Are we loading an element from the page or a URL?
        if ( url.indexOf("#") === 0 ) {                
            // Load a page element                
            $(url).clone(true).appendTo( $squeeze.empty() ).show();
        } else {
            // Load a URL. Into an iframe?
            if( useIframe ) {
                var iframe = $("<iframe />").attr({
                                                src: url,
                                                frameborder: 0,
                                                hspace: 0
                                            })
                                            .css({
                                                width: "100%",
                                                height: "100%"
                                            });
                
                $squeeze.html( iframe );
            } else {
                $squeeze.load( url );
            }
            
            $squeeze.data( 'localEl', false );
            
        }
    }

    // Function that controls opening of the squeeze
    function _start( direction, speed ) {
		var scrolltop=$(window).scrollTop();
//        var slideWidth = $squeeze.outerWidth( true ),
        var slideWidth = $(window).width(),
            bodyAnimateIn = {},
            slideAnimateIn = {};

        // If the slide is open or opening, just ignore the call
        if( $squeeze.is(':visible') || _sliding ) return;
        _sliding = true;



//		$squeeze.css({ right: '-' + slideWidth + 'px', left: 'auto' , top: scrolltop, height: $(window).height()+'px'});
		$squeeze.css({ right: '-' + slideWidth + 'px', left: 'auto', position:'fixed'});
		bodyAnimateIn['margin-right'] = '+=' + slideWidth;
		bodyAnimateIn['margin-left'] = '-=' + slideWidth;
		slideAnimateIn['right'] = '+='+slideWidth;

        // Animate the slide, and attach this slide's settings to the element
        $squeeze.show()
                  .animate(slideAnimateIn, speed, function() {
/*
						var agent = navigator.userAgent;
						if(agent.search(/iPhone/) != -1|| agent.search(/iPad/) != -1|| agent.search(/iPod/) != -1){
							$body.css({position:"fixed"});
						} else
							$('html, body').css("overflow", "hidden");
*/
						_sliding = false;
                  });
		$body.animate(bodyAnimateIn, speed);
    }
      
    /*
     * Declaration 
     */
    $.fn.squeeze = function(options) {
        var $elements = this;
        
        // On click
        $elements.click( function(e) {
            var $self = $(this),
                settings = $.extend({ href: "#squeezebody" }, options);
//                settings = $.extend({ href: $self.attr('href') }, options);
            
            // Prevent the default behavior and stop propagation
            e.preventDefault();
            e.stopPropagation();
            
            if ( $squeeze.is(':visible') && $self[0] == _lastCaller ) {
                // If we clicked the same element twice, toggle closed
                $.squeeze.close();
            } else {
                // Open
                $.squeeze( settings );

                // Record the last element to trigger squeeze
                _lastCaller = $self[0];
            }
        });
	};
	
	/*
     * Default settings 
     */
    $.fn.squeeze.defaults = {
        speed:      500,        // Accepts standard jQuery effects speeds (i.e. fast, normal or milliseconds)
        direction:  'right',    // Accepts 'left' or 'right'
        modal:      false,      // If set to true, you must explicitly close squeeze using $.squeeze.close();
        iframe:     true,       // By default, linked pages are loaded into an iframe. Set this to false if you don't want an iframe.
        href:       null        // Override the source of the content. Optional in most cases, but required when opening squeeze programmatically.
    };
	
	/*
     * Public methods 
     */
	
	// Open the squeeze
	$.squeeze = function( options ) {	    
	    // Extend the settings with those the user has provided
        var settings = $.extend({}, $.fn.squeeze.defaults, options);
	    
	    // Are we trying to open in different direction?
        if( $squeeze.is(':visible') && $squeeze.data( 'direction' ) != settings.direction) {
            $.squeeze.close(function(){
                _load( settings.href, settings.iframe );
                _start( settings.direction, settings.speed );
            });
        } else {                
            _load( settings.href, settings.iframe );
            if( $squeeze.is(':hidden') ) {
                _start( settings.direction, settings.speed );
            }
        }
        
        $squeeze.data( settings );
	}
	
	// Close the squeeze
	$.squeeze.close = function( callback ) {
/*
		var agent = navigator.userAgent;
		if(agent.search(/iPhone/) != -1|| agent.search(/iPad/) != -1|| agent.search(/iPod/) != -1){
				$body.css({position:"absolute"});
		} else
			$('html, body').css("overflow", "auto");
*/
		$("body").animate({scrollTop: 0}, 1);
        var $squeeze = $('#squeeze'),
//            slideWidth = $squeeze.outerWidth( true ),
        	slideWidth = $(window).width(),
            speed = $squeeze.data( 'speed' ),
            bodyAnimateIn = {},
            slideAnimateIn = {}
            	        
        // If the slide isn't open, just ignore the call
        if( $squeeze.is(':hidden') || _sliding ) return;	        
        _sliding = true;

		bodyAnimateIn['margin-right'] = '-=' + slideWidth;
		bodyAnimateIn['margin-left'] = '+=' + slideWidth;
		slideAnimateIn['right'] = '-=' + slideWidth;
        $squeeze.animate(slideAnimateIn, speed);
        $body.animate(bodyAnimateIn, speed, function() {
            $squeeze.hide();
            _sliding = false;
            if( typeof callback != 'undefined' ) callback();
        });
    }
	


	$(document).ready(function($){
		$body = $('#container');
		$squeeze = $('#squeeze');

		if( $squeeze.length == 0 ) {
			 $squeeze = $('<div />').attr( 'id', 'squeeze' )
									  .css( 'display', 'none' )
									  .appendTo( $('body') );
		}

		// Don't let clicks to the squeeze close the window
		$squeeze.click(function(e) {
			e.stopPropagation();
		});

		$body.click(function(e) {
			if( $squeeze.is( ':visible' ) && !$squeeze.data( 'modal' ) ) {	        
				$.squeeze.close();
			}
		});

		$.ajax({
				type: 'GET',
				url: '/s/squeeze3.html',
				dataType: 'html',
				success: function(data) {
				$('#squeezebody').append(data);
			},
			error:function() {
				alert('問題がありました。');
			}
		});

	});


})(jQuery);