// メニューの開閉状態
var slide = "off";
var sidemenuLoad = "off";
/**
 * ページ初期化処理 
 */
$(function() {
    $("#leftmenu").css("display", "none");
    
    // メニューコンテンツが空の場合は、メニューの内容を非同期で取得
    if ($("#side_menu").length) {
        sidemenuLoad = 'on';
    } else {
        $.ajax({
            type : 'GET',
            url : '/spage2/parts/sidemenu',
            dataType : 'html',
            success : function(data) {
                $('#leftmenu').html("").append(data);
                sidemenuLoad = 'on';
            },
            error : function() {
                alert('Webページの表示に問題が発生しました。');
            }
        });
    }
    
    // メニューボタン押下でメニューをオープンする 
    $(".open").click(function() {
        if (slide == "off" && sidemenuLoad == "on") {
            openSlide();
        }
    });
    
    // メニューが開いているときはメインコンテンツのクリックイベントは無効にし、メニューを閉じる
    $("#container").click(function(e) {
        if (slide == "on") {
            e.preventDefault();
            closeSlide();
        }
    });
});

/**
 * メニューを開きます。 
 */
function openSlide() {
    // バナーカルーセルを一時停止する。
    if (0 < $('.flexslider').size()) {
        var slider = $('.flexslider').data('flexslider');
        if(slider) {
            slider.pause();
        }
    }
    slide = "sliding";
    
    $("#leftmenu").css("display", "");
    
    // メニューを開く
    $("#container").addClass('left_visible').one('webkitTransitionEnd', endOpenSlide);
    $("#leftmenu").addClass('visible');
}

/**
 * メニューを開くアニメーション完了時のハンドラ 
 */
function endOpenSlide() {
    if (slide === "sliding") {
        slide = "on";
    } else {
        return false;
    }
    
    // コンテンツの高さをメニューの高さに揃える
    $('html, body, #container').css({
        "overflow-x" : "hidden",
        "height" : $('#leftmenu').css("height")
    });
    $('body').css("overflow-y", "hidden");
    
    // メインコンテンツを背面に
    $("#leftmenu").css('z-index', '1');
    $("#container").css('z-index', '-1');
}

/**
 * メニューを閉じます。 
 */
function closeSlide() {
    slide = "sliding";

    // メインコンテンツを前面に
    $("#container").css('z-index', '1');
    $("#leftmenu").css('z-index', '-1');

    $("#container").removeClass('left_visible').one('webkitTransitionEnd', endCloseSlide);
    $("#leftmenu").removeClass('visible');
}

/**
 * メニューを閉じるアニメーション完了時のハンドラ 
 */
function endCloseSlide() {
    if (slide === "sliding") {
        slide = "off";
    } else {
        return false;
    }

    // メニューの高さに揃えていたコンテンツの高さを元に戻す
    $('html, body, #container').css({
        "overflow-x" : "none",
        "height" : "auto"
    });
    $('body').css("overflow-y", "none");
    
    $("#leftmenu").css("display", "none");
    
    // バナーカルーセルを再開
    if(0 < $('.flexslider').size()) {
        var slider = $('.flexslider').data('flexslider');
        if(slider) slider.play();
    }
}
