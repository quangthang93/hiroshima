$(function(){
    // ランキングカテゴリ読み込み
    var url = '/blog/common/get_category/';

    $.ajax({
        type: 'GET',
            url: url,
            dataType: 'json',
            timeout: 10000,
            success: function(ret, status, xhr){
                $.each(ret, function(i, val){
                    $('.select_box select').append('<option data-id="' + i + '">' + val + '</option>');
                });

                $(".select_box select").change(function () {
                    if(typeof $(':selected', this).attr('data-id') !== "undefined"){
                        getRankingData($(':selected', this).attr('data-id'));
                        return false;
                    }
                });

            },
            error: function() {

            }
    });

    // ランキング取得
    getRankingData(0);

    $("#tab li").click(function() {
        $('.select_box select').val('');
        $("#tab li").removeClass('active');
        $(this).addClass('active')
        getRankingData(0);
    });

});


function getRankingData(category_id) {
    var type = 1;
    $('.item_block_01 ul li').each(function(index, element){
        if ($(this).hasClass('active')) {
            type = index + 1;
        }
    });

    var url = '/blog/ranking/get_ranking_blog/?&type=' + type + '&category_id=' + category_id;

    $.ajax({
        type: 'GET',
            url: url,
            dataType: 'json',
            timeout: 10000,
            success: function(ret, status, xhr){
                $('.ranking_category li.sub').removeClass('click');
                $('.ranking_block_01').remove();
                $('.ranking_block_02').remove();

                var html = '';
                $.each(ret, function(i, val){
                    if (i <= 3) {
                        html += '<div class="ranking_block_01">';
                        html += '<p class="no' + i + '"><a href="/spage2/' + val['id'] + '">' + val['name'] + '</a></p>';
                        html += '<div class="image">';
                        html += '<a href="/spage2/' + val['id'] + '"><img src="' + val['image'] + '" alt="' + val['name']+ '" width="87" height="65" /></a>';
                        html += '<!--/image--></div>';
                        html += '<!--/ranking_block_01--></div>'
                    } else {
                        if (i == 4) {
                            html += '<div class="ranking_block_02">';
                            html += '<ul>';
                        }
                        if (i == 10) {
                            html += '<li class="no' + i + ' mb00"><a href="/spage2/' + val['id'] + '">' + val['name'] + '</a></li>';
                            html += '</ul>';
                            html += '<!--/ranking_block_02--></div>';
                            html += '<!--/content_wrap_02--></div>';
                        } else {
                            html += '<li class="no' + i + '"><a href="/spage2/' + val['id'] + '">' + val['name'] + '</a></li>';
                        }
                    }
                });

                if(html.length == 0) {
                    html += '<div class="ranking_block_01">';
                    html += 'ランキングがありません';
                    html += '<!--/ranking_block_01--></div>';
                }

                $('.tab_block_01 .item_block_02 .content_wrap').not('disnon').html(html);

                $('#category_overlay').remove();
            },
            error: function() {
                $('.ranking_category li.sub').removeClass('click');
                $('.ranking_block_01').remove();
                $('.ranking_block_02').remove();

            }
    });
}