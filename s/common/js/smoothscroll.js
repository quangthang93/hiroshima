$(function(){
    $('a[href^=#]').click(function() {

        var speed = 500;
        var href= $(this).attr("name");
	if (!href)
		return false;
        var target = $(href == "#" || href == "" ? 'html' : href);

        var position = target.offset().top;
        $($.browser.safari ? 'body' : 'html').animate({scrollTop:position}, speed, 'swing');
        return false;

    });
});