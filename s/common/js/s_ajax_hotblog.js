//=====記事サーチSTART=====
/**
 * AJAXで記事を表示するJavascriptです
 *
 */
var blogs = {
    index: 0,
    posts: [],
    getWrapper: function() {
        var selector  = $("#parts_class").val();
        var container = $(selector);
        var wrapper   = $("div.thumbnail_block_wrap", container);
        if (wrapper.length) {
            return wrapper;
        }
        
        container
        .append($("<div/>").addClass("thumbnail_block_wrap clear"))
        .append(
            $("<div/>").addClass("btn_block_07")
            .append(
            $("<a/>").attr("href", "javascript:void(0)").text("もっと見る")
            )
        );
        return $("div.thumbnail_block_wrap", container);
    },
    displayReset: function() {
        var selector  = $("#parts_class").val();
        var container = $(selector);
        container.children().remove();
    },
    displayCriteria: function() {
        var wrapper = blogs.getWrapper();
        var condition = getCondition();
        if(condition) {
            wrapper.before('<p class="search_criteria">絞り込み条件（' + condition + '）</p>');
        }
    },
    displayMore: function(count) {
        var wrapper = blogs.getWrapper();
        var next    = Math.min(blogs.index + count, blogs.posts.length);
        for (var i = blogs.index; i < next; i ++) {
            wrapper.append(blogs.posts[i]);
        }
        if(typeof imgLiquid != "undefined"){
            $('.imgLiquidBox').imgLiquid();
        }
        //記事の高さ調整
        wrapper.children(".fixHeight").fixHeight();
        blogs.index = next;
    }
};

//ページが読み込まれたら記事を表示
function ajax_Onload(){
    $(function(){
        var turn     = $('#turn_flag').val();
        new_like_Search();
        follow_blog();
        show_flag();
        s_Ajax(turn);
    });
}
//サムネイル、リスト表示イベント
function show_flag(){
    $('.clear .tab li').click(function(){
        var turn = ($('#main .tab_block_01 .clear .genre li.active a').text() == '新着順') ? 1 : 0;
        s_Ajax(turn);
    });
}

//新着ボタン、LIKE数順イベント
function new_like_Search(){

    $('#main .tab_block_01 .clear .genre li').click(function(){

        var turn = ($('#main .tab_block_01 .clear .genre li.active a').text() != '新着順') ? 1 : 0;

        s_Ajax(turn);
    });
}

//お気に入りイベント
function follow_blog() {
    $('p.fav img').click(function(){
        var fav_input = $("#favorite_flag");
        var next      = fav_input.val() == "0" ? "1" : "0";
        var next_img  = next == "0" ? "/s/common/images/common_btn_010.png" : "/s/common/images/common_btn_010_on.png";
        fav_input.val(next);
        $(this).attr("src", next_img);

        var turn = ($('#main .tab_block_01 .clear .genre li.active a').text() == '新着順') ? 1 : 0;
        s_Ajax(turn);
    });
}

function squeeze_Search() {
    $('.modal_block_01 .btn_block_09').click(function(){
        //選択中のアイテムIDを取得
        $('#item_id').val($("#search_select_item").val());

        //選択中のイベントIDを取得
        $('#event_id').val($("#search_select_event").val());

        //選択中のカレンダーIDを代入
        $('#calender_id').val($("#search_select_calendar").val());

        //選択中のフロアIDを代入
        $('#floor_id').val($("#search_select_floor").val());

        //新着順とお気に入り順の取得
        if($('#main .tab_block_01 .clear .genre .active').text() == '新着順'){
            var turn = 1;
        }else{
            var turn = 0;
        }

        s_Ajax(turn);
    });
}

//記事絞り込みに必要なデータをAJAXでPOST
function s_Ajax(turn) {
    $.ajax({
        url: "/page2/ajax/spgetpart/" + $('#parts_name').val(),
        type: 'POST',
        dataType:'html',
        async: false,
        data: {
            blog: {'item_id': $('#item_id').val(),
                   'event_id': $('#event_id').val(),
                   'event_date': $('#calender_id').val(),
                   'floor_id': $('#floor_id').val(),
                   'turn_flag': turn,
                   'favorite_flag': $("#favorite_flag").val()
            }
        },
        success: function(data) {
            blogs.posts = data.split("<!--POST_BOUNDARY-->");
            blogs.index = 0;
            blogs.displayReset();
            blogs.displayCriteria();
            // 初期表示行数 3×134 = 402
            var firstCount = 134;
            // 増加行数 3×3
            var increment = 3;
            blogs.displayMore(firstCount);
            more(firstCount, increment);
        }
    });
}

// もっと見る用の処理を行います
function more(firstCount, increment) {
    // 表示できる総数を取得します(data.splitで最後の配列が空になるため、-1しています)
    var blockNum = blogs.posts.length-1;
    // 表示件数がfirstCount件以上の場合処理を行います
    if (blockNum > firstCount) {
        $('.btn_block_07 a').click(function () {
            // 表示件数を増加します
            blogs.displayMore(increment);

            // 表示件数が表示件数以上になった場合
            if (blogs.index >= blockNum) {
                // ボタンのクリックイベント削除
                $('.btn_block_07').unbind('click');
                //もっと見るボタンを削除
                $('.btn_block_07').remove();
                
                return false;
            }
        });
    }else{
        $('.btn_block_07').remove();
    }
}

// ブランク画像を本来の画像に差し替えます
function replaceBlankImages(baseSelector, nowNum) {
    var visibleList = $(baseSelector + ':lt(' + (nowNum + 1) + ')');
    visibleList.each(function() {
        var img = $(".image a img", this);
        if (img.attr("src").match(/\/common\/images\/blank.gif$/)) {
            img.attr("src", $("input", this).val());
        }
    });
}

//絞り込み条件取得
function getCondition() {
    var item_id = $('#item_id').val();
    var event_id =  $('#event_id').val();
    var event_date = $('#calender_id').val();
    var floor_id = $('#floor_id').val();
    condition = new Array();
    condition_count = 0;
    if (item_id != "all") {
        $('#search_select_item').children('"option[value = ' + item_id + ']"').attr("selected","selected");
        condition[condition_count] = "アイテムカテゴリ：" + $('#search_select_item > option:selected').text();
        condition_count++;
    }
    if (event_id != "all") {
        $('#search_select_event').children('"option[value = ' + event_id + ']"').attr("selected","selected");
        condition[condition_count] = "イベント・セール：" + $('#search_select_event > option:selected').text();
        condition_count++;
    }
    if (event_date != "all") {
        $('#search_select_calendar').children('"option[value = ' + event_date + ']"').attr("selected","selected");
        condition[condition_count] = "カレンダー：" + $('#search_select_calendar > option:selected').text();
        condition_count++;
    }
    if (floor_id != "all") {
        $('#search_select_floor').children('"option[value = ' + floor_id + ']"').attr("selected","selected");
        condition[condition_count] = "フロア：" + $('#search_select_floor > option:selected').text();
        condition_count++;
    }
    return condition.join("、");
}