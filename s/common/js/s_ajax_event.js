//=====記事サーチSTART=====
/**
 * AJAXで記事を表示するJavascriptです
 *
 */
var blogs = {
    index: 0,
    posts: [],
    getWrapper: function() {
        var selector  = $("#parts_class").val();
        var container = $(selector);
        var wrapper   = $("div.thumbnail_block_wrap", container);
        if (wrapper.length) {
            return wrapper;
        }
        
        container
        .append($("<div/>").addClass("thumbnail_block_wrap clear"))
        .append(
            $("<div/>").addClass("btn_block_07")
            .append(
                $("<a/>").attr("href", "javascript:void(0)").text("もっと見る")
            )
        );
        return $("div.thumbnail_block_wrap", container);
    },
    displayReset: function() {
        var selector  = $("#parts_class").val();
        var container = $(selector);
        container.children().remove();
    },
    displayMore: function(count) {
        var wrapper = blogs.getWrapper();
        var next    = Math.min(blogs.index + count, blogs.posts.length);
        for (var i = blogs.index; i < next; i ++) {
            wrapper.append(blogs.posts[i]);
        }
        if(typeof imgLiquid != "undefined"){
            $('.imgLiquidBox').imgLiquid();
        }
        
        $('.thumbnail_block img').imagesLoaded(function () {
            if ($(".masonry-brick", wrapper).length > 0) {
                $(".thumbnail_block_wrap").masonry("destroy");
            }
            $('.thumbnail_block_wrap').masonry({
                itemSelector: '.thumbnail_block'
            });
            thumbnailBlockWrapResize();
        });
        
        blogs.index = next;
    }
};

//ページが読み込まれたら記事を表示
function ajax_Onload(){
    $(function(){
        var turn = 1;
        new_like_Search();
        show_flag();
        s_Ajax(turn);
    });
}

//サムネイル、リスト表示イベント
function show_flag(){
    $('.clear .tab li').click(function(){
    var turn = 0;
    if($('#main .tab_block_01 .clear .genre li.active a').text() == '新着順'){
      turn = 1;
    }

    if($('.clear .tab li.select a').text() == 'サムネイル'){
        $("#tab02").children().remove();
      s_Ajax(turn);
    }else{
         $("#tab01").children().remove();
      s_Ajax_disnon(turn);
    }
    });
}

//新着ボタン、LIKE数順イベント
function new_like_Search(){
    $('#main .tab_block_01 .clear .genre li').click(function(){
    var turn = 0;
    if($('#main .tab_block_01 .clear .genre li.active a').text() != '新着順'){
      turn = 1;
    }

    if($('.clear .tab li.select a').text() == 'サムネイル'){
      s_Ajax(turn);
    }else{
      s_Ajax_disnon(turn);
    }
    });
}

function squeeze_Search() {
    $('.modal_block_01 .btn_block_09').click(function(){
        //選択中のカレンダーIDを代入
        $('#calender_id').val($('.select_right form .select_box select').val());

        //新着順とお気に入り順の取得
        if($('#main .tab_block_01 .clear .genre .active').text() == '新着順'){
            var turn = 1;
        }else{
            var turn = 0;
        }
        var show_flag = $('.clear .tab li.select a').text();
        if( show_flag == 'サムネイル'){
            s_Ajax(turn);
        }else{
            s_Ajax_disnon(turn);
        }
    });
}

//記事絞り込みに必要なデータをAJAXでPOST
function s_Ajax(turn){
    $.ajax({
        url: "/page2/ajax/spgetpart/" + $('#parts_name').val(),
        type: 'POST',
        dataType:'html',
        async: false,
        data: {
            blog: {'floor_id': 'all',
                   'item_id': 'all',
                   'event_id': 'all',
                   'turn_flag': turn,
                   'event_date': $('#calender_id').val()
            }
        },
        success: function(data) {
            blogs.posts = data.split("<!--POST_BOUNDARY-->");
            blogs.index = 0;
            blogs.displayReset();
            blogs.displayMore(10);
            more();
        }
    });
}

// 記事絞り込みに必要なデータをAJAXでPOSTする(非表示)
function s_Ajax_disnon(turn){
    $.ajax({
        url: '/page2/ajax/spgetpart/' + $('#parts_name_disnon').val(),
        type: 'POST',
        dataType:'html',
        async: false,
        data: {
            blog: {'floor_id': 'all',
                   'item_id': 'all',
                   'event_id': 'all',
                   'turn_flag': turn,
                   'event_date': $('#calender_id').val()
                   }
        },
        success: function(data) {
            $($('#parts_class_disnon').val()).html(data);
            more_disnon();
        }
    });
}

// もっと見る用の処理を行います
function more() {
    var addNum = 10;    // 増加量

    // 表示しているブログブロック数を取得します
    var blockNum = blogs.posts.length;

    // 表示件数が初期表示件数+１件以上の場合処理を行います
    if (blockNum >= 11) {
        $('.btn_block_07 a').click(function () {
            // 表示件数を増加します
            blogs.displayMore(addNum);
            
            // 表示件数が表示件数以上になった場合
            if (blogs.index >= blockNum) {
                // ボタンのクリックイベント削除
                $('.btn_block_07').unbind('click');

                //もっと見るボタンを削除
                $('.btn_block_07').remove();

                return false;
            }
        });
    }else{
        $('.btn_block_07').remove();
    }
}

// もっと見る用の処理を行います(disnon)
function more_disnon() {
    var nowNum = 9;     // 現在の表示件数(0から数える)
    var addNum = 10;    // 増加量

    // 表示しているブログブロック数を取得します
    var blockNum = $('#tab02 .list_block').size();

    // 表示件数が1件以上の場合処理を行います
    if (blockNum >= 11) {
        $('.btn_block_07 a').click(function () {

            // 表示件数を増加します
            nowNum += addNum;

            // 表示を切り替えます
            $('#tab02 .list_block').css('display', '');
            $('#tab02 .list_block:gt(' + (nowNum) + ')').css('display', 'none');

            // ブランク画像を本来の画像に差し替えます
            replaceBlankImages("#tab02 .list_block", nowNum);
            
            // 表示件数が表示件数を超えた場合
            if (nowNum + 1 >= blockNum) {
                // ボタンのクリックイベント削除
                $('.btn_block_07').unbind('click');

                //もっと見るボタンを削除
                $('.btn_block_07').remove();

                return false;
            }
        });
    }else{
        $('.btn_block_07').remove();
    }
}

// ブランク画像を本来の画像に差し替えます
function replaceBlankImages(baseSelector, nowNum) {
    var visibleList = $(baseSelector + ':lt(' + (nowNum + 1) + ')');
    visibleList.each(function() {
        var img = $(".image a img", this);
        if (img.attr("src").match(/\/common\/images\/blank.gif$/)) {
            img.attr("src", $("input", this).val());
        }
    });
}

// thumbnail_block_wrapのサイズ調整
function thumbnailBlockWrapResize() {
    var margin    = 20;
    var maxHeight = 0;
    $('.thumbnail_block_wrap .thumbnail_block').each(function(){
        if ($(this).css('display') == 'none') return false;
        var top    = parseInt($(this).css('top'));
        var height = $(this).height() + top;

        if (maxHeight <= height) {
            maxHeight = height;
        }
    });

    maxHeight += margin;

    $(".thumbnail_block_wrap").css('height', maxHeight);
}