<?php
//基本情報の取得
include('../../../../wordpress/php/classes/Config.class.php');
?>

//日付を取得する関数です
function getDate(){
	var date = new Date();
	var year = date.getYear();
	var year4 = (year < 2000) ? year+1900 : year;
	var month = date.getMonth() + 1;
	var date = date.getDate();
	if (month < 10) {
		month = '0' + month;
	}
	if (date < 10) {
		date = '0' + date;
	}
	var strDate = year4 + '-' + month + '-' + date;
	return strDate;
}

//=====記事サーチSTART=====
function s_top_show_Post(){
	//ページが読み込まれたら記事を表示
	alert("ページ");
	$(function(){
		var floor = 'all';
		var item = 'all';
		var event = 'all';
		var turn = 1;
		var date = getDate();
		s_top_postSearch(floor, item, event, turn, date);
		s_top_Ajax(floor, item, event, turn, date);
		s_top_disnon_Ajax(floor, item, event, turn, date);
	});

// 記事を絞り込む
	function s_top_postSearch(floor, item, event, turn, date) {
// アイテムカテゴリーで記事を絞り込む
		$('div.blog_search_window .search_words:first ul.clear li').click(function(){
		alert("アイテム");
			var item_name = $('a', this).text();
			item = $(this).attr('id');
			s_top_Ajax(floor, item, event, turn, date);
			s_top_disnon_Ajax(floor, item, event, turn, date);
		});
// イベント・セールで記事を絞り込む
		$('div.blog_search_window .search_words:last ul.clear li').click(function(){
		alert("イベント");
			var event_name = $('a', this).text();
			event = $(this).attr('id');
			s_top_Ajax(floor, item, event, turn, date);
			s_top_disnon_Ajax(floor, item, event, turn, date);
		});
// 新着順・LIKE数順で絞り込む
		$('ul.genre li').click(function(){
			var flag = $('a', this).text();
			if(flag == '新着順'){
			alert("新着");
				turn = 1;
			}else if(flag == 'LIKE数順'){
			alert("LIKE数");
				turn = 0;
			}
			s_top_Ajax(floor, item, event, turn, date);
			s_top_disnon_Ajax(floor, item, event, turn, date);
		});
// カレンダーの日付で絞り込む
		$('#mycarousel li').click(function(){
		alert("カレンダー");
			var tmp_date = $('a', this).text();
			var year = $(this).attr('id');
			tmp_date = tmp_date.replace('/', '');
			date = tmp_date.substr(0,2) + '-' + tmp_date.substr(2,4);
			date = year + '-' + date;
			s_top_Ajax(floor, item, event, turn, date);
			s_top_disnon_Ajax(floor, item, event, turn, date);
		});
	}

// 記事絞り込みに必要なデータをAJAXでPOSTする
	function s_top_Ajax(floor, item, event, turn, date){
		$.ajax({
			url: '<?php echo $S_PARTS_PATH; ?>sp_parco_top_shopblog_main.php',
			type: 'POST',
			dataType:'html',
			data: {
				blog: {'floor_id': floor,
					   'item_id': item,
					   'event_id': event,
					   'turn_flag': turn,
					   'event_date': date,
					   },
			},
			success: function(data) {
				$('#tab01').html(data);
			},
		});
	}
// 記事絞り込みに必要なデータをAJAXでPOSTする(非表示)
	function s_top_disnon_Ajax(floor, item, event, turn, date){
		$.ajax({
			url: '<?php echo $S_PARTS_PATH; ?>sp_parco_top_shopblog_main_disnon.php',
			type: 'POST',
			dataType:'html',
			data: {
				blog: {'floor_id': floor,
					   'item_id': item,
					   'event_id': event,
					   'turn_flag': turn,
					   'event_date': date,
					   },
			},
			success: function(data) {
				$('#tab02').html(data);
			},
		});
	}
}
