<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title><?php echo $title ?></title>
<meta name="description" content="<?php echo $description ?>" />
<meta name="keywords" content="<?php echo $keywords ?>"/>
<?php include_once($_SERVER['DOCUMENT_ROOT'].'/web/'.DIRNAME.'/inc/ogp.php'); ?>
<link rel="canonical" href="http://hiroshima.parco.jp/web/autumun_newshop/" />
<link rel="stylesheet" href="/s/<?=DIRNAME?>/css/default.css" media="screen,print" />
<link rel="stylesheet" href="/s/<?=DIRNAME?>/css/style.css" media="screen,print" />
<link rel="stylesheet" href="/s/<?=DIRNAME?>/css/<?=PAGE_NAME?>.css" />

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script src="/s/<?=DIRNAME?>/js/masonary.min.js"></script>
<script src="/s/<?=DIRNAME?>/js/common.js"></script>
<script src="/common/js/social.js"></script>
<script src="/common/js/imgLiquid.js"></script>

