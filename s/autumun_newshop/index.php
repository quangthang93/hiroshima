<?php
include_once( $_SERVER[ 'DOCUMENT_ROOT' ] . '/web/autumun_newshop/inc/config.php' );

$title = TITLE;
$description = DESCRIPTION1;
$keywords = KEYWORDS1;
define( 'PAGE_NAME', 'newshop' );

?>
<!DOCTYPE html>
<html lang="ja">

<head>
	<?php include_once($_SERVER['DOCUMENT_ROOT'].'/s/'.DIRNAME.'/inc/html-head.php'); ?>
</head>

<body>
	<?php include_once($_SERVER['DOCUMENT_ROOT'] . '/load_script.php'); ?>
	<div id="fb-root"></div>
	<script>
		( function ( d, s, id ) {
			var js, fjs = d.getElementsByTagName( s )[ 0 ];
			if ( d.getElementById( id ) ) return;
			js = d.createElement( s );
			js.id = id;
			js.src = "//connect.facebook.net/ja_JP/sdk.js#xfbml=1&appId=500009400047380&version=v2.0";
			fjs.parentNode.insertBefore( js, fjs );
		}( document, 'script', 'facebook-jssdk' ) );
	</script>

	<h2 class="ttl"><img src="/s/<?=DIRNAME?>/images/ttl-<?=PAGE_NAME?>.png" width="100%" alt="NEW AND RENEWAL" /></h2>
	<div class="contents">
		<div class="pickup-block03 mt20">


			<div class="photo-wrap">
				<?php
				/**
				 * PHOTO-START
				 */
				?>
				<div class="photo-block">
					<div class="photo-block-in">
						<div class="top-visual"><img class="visual" src="../../web/autumun_newshop/images/newshop/shop/01.jpg" width="100%" alt="キャスキッドソン">
							<div class="visual-bottom">
								<p class="top-floor">新館3F</p>
								<p class="top-shop">キャスキッドソン&nbsp;&nbsp;<span class="top-category">レディス・雑貨</span>
								</p>
								<p class="top-date">9/22(金)&nbsp;NEW OPEN!</p>
							</div>
						</div>
						<div class="photo-hover"><span class="icon new"></span><span class="badge"></span>
							<div class="fadeInLeft animated">
								<h1 class="hover-ttl">新館3F<br>9/22(金)NEW OPEN!<br><span>レディス・雑貨</span></h1>
								<p class="hover-img pt10"><img src="../../web/autumun_newshop/images/newshop/logo/lo-01.png" height="30" alt="キャスキッドソン">
								</p>
								<p class="hover-text">「モダンヴィンテージ」をテーマに、多彩なプリントデザインで世界中の女性に愛される英国発のライフスタイルブランド。</p>
								<div class="hover-box clearfix">
									<!-- hover-box -->
								</div>
								<!-- fadeInLeft animated -->
							</div>
							<!-- photo-hover -->
						</div>
					</div>
				</div>
				<div class="photo-block">
					<div class="photo-block-in">
						<div class="top-visual"><img class="visual" src="../../web/autumun_newshop/images/newshop/shop/02.jpg" width="100%" alt="ジェイダ">
							<div class="visual-bottom">
								<p class="top-floor">本館4F</p>
								<p class="top-shop">ジェイダ&nbsp;&nbsp;<span class="top-category">レディス</span>
								</p>
								<p class="top-date">9/2(土)&nbsp;NEW OPEN!</p>
							</div>
						</div>
						<div class="photo-hover"><span class="icon new"></span><span class="badge-ch"></span>
							<div class="fadeInLeft animated">
								<h1 class="hover-ttl">本館4F<br>9/2(土)NEW OPEN!<br><span>レディス</span></h1>
								<p class="hover-img pt10"><img src="../../web/autumun_newshop/images/newshop/logo/lo-02.png" height="30" alt="ジェイダ">
								</p>
								<p class="hover-text">西海岸LAの空気感をイメージした、カジュアルで色気のあるオンナの子のスタイルを提案。</p>
								<div class="hover-box clearfix">
									<p class="hover-plan">〈PARCOカード〉新規入会またはご利用で10％OFF
										<br>★プレミアムデイ＜PARCOカード＞新規入会またはご利用で15％OFF</p>
									<!-- hover-box -->
								</div>
								<!-- fadeInLeft animated -->
							</div>
							<!-- photo-hover -->
						</div>
					</div>
				</div>
				<div class="photo-block">
					<div class="photo-block-in">
						<div class="top-visual"><img class="visual" src="../../web/autumun_newshop/images/newshop/shop/03.jpg" width="100%" alt="アンビー＆エクラン">
							<div class="visual-bottom">
								<p class="top-floor">本館3F</p>
								<p class="top-shop">アンビー＆エクラン&nbsp;&nbsp;<span class="top-category">レディス</span>
								</p>
								<p class="top-date">8/26(土)&nbsp;NEW OPEN!</p>
							</div>
						</div>
						<div class="photo-hover"><span class="icon new"></span><span class="badge-ch"></span>
							<div class="fadeInLeft animated">
								<h1 class="hover-ttl">本館3F<br>8/26(土)NEW OPEN!<br><span>レディス</span></h1>
								<p class="hover-img"><img src="../../web/autumun_newshop/images/newshop/logo/lo-03.png" height="30" alt="アンビー＆エクラン">
								</p>
								<p class="hover-text">（アンビー）シンプルなCOORDINATEの中に、エッジのあるITEMを取り入れた現代的な女性のSTYLE。<br> （エクラン）フェミニティとクリアなセンシュアルモードで女性の心をくすぐるリアルクローズを提案。</p>
								<div class="hover-box clearfix">
									<p class="hover-plan">〈PARCOカード〉新規入会またはご利用で10％OFF</p>
									<!-- hover-box -->
								</div>
								<!-- fadeInLeft animated -->
							</div>
							<!-- photo-hover -->
						</div>
					</div>
				</div>
				<div class="photo-block">
					<div class="photo-block-in">
						<div class="top-visual"><img class="visual" src="../../web/autumun_newshop/images/newshop/shop/04.jpg" width="100%" alt="オズモーシス">
							<div class="visual-bottom">
								<p class="top-floor">本館3F</p>
								<p class="top-shop">オズモーシス&nbsp;&nbsp;<span class="top-category">レディス</span>
								</p>
								<p class="top-date">8/5(土)&nbsp;NEW OPEN!</p>
							</div>
						</div>
						<div class="photo-hover"><span class="icon new"></span>
							<div class="fadeInLeft animated">
								<h1 class="hover-ttl">本館3F<br>8/5(土)NEW OPEN!<br><span>レディス</span></h1>
								<p class="hover-img pt10"><img src="../../web/autumun_newshop/images/newshop/logo/lo-04.png" height="30" alt="オズモーシス">
								</p>
								<p class="hover-text">時代に左右されないタイムレスなデザインベースにトレンド性を盛り込み、常に新鮮なスタイルを提案</p>
								<div class="hover-box clearfix">
									<p class="hover-plan">〈PARCOカード〉新規入会またはご利用で10％OFF【9/15～18】</p>
									<!-- hover-box -->
								</div>
								<!-- fadeInLeft animated -->
							</div>
							<!-- photo-hover -->
						</div>
					</div>
				</div>
				<div class="photo-block">
					<div class="photo-block-in">
						<div class="top-visual"><img class="visual" src="../../web/autumun_newshop/images/newshop/shop/05.jpg" width="100%" alt="トランジション">
							<div class="visual-bottom">
								<p class="top-floor">本館5F</p>
								<p class="top-shop">トランジション&nbsp;&nbsp;<span class="top-category">メンズ</span>
								</p>
								<p class="top-date">9/1(金)&nbsp;RENEWAL OPEN!</p>
							</div>
						</div>
						<div class="photo-hover"><span class="icon renewal"></span><span class="badge-ch"></span>
							<div class="fadeInLeft animated">
								<h1 class="hover-ttl">本館5F<br>9/1(金)RENEWAL OPEN!<br><span>メンズ</span></h1>
								<p class="hover-img"><img src="../../web/autumun_newshop/images/newshop/logo/lo-05.png" height="30" alt="トランジション">
								</p>
								<p class="hover-text">「本物・リアルクローズ」。最旬のドメスティックブランド、IMPORTラインや雑貨を取り入れ、洗練されたコーディネートを提案。</p>
								<div class="hover-box clearfix">
									<p class="hover-plan">〈PARCOカード〉新規入会またはご利用で10％OFF</p>
									<!-- hover-box -->
								</div>
								<!-- fadeInLeft animated -->
							</div>
							<!-- photo-hover -->
						</div>
					</div>
				</div>
				<div class="photo-block">
					<div class="photo-block-in">
						<div class="top-visual"><img class="visual" src="../../web/autumun_newshop/images/newshop/shop/06.jpg" width="100%" alt="ポルコロッソ">
							<div class="visual-bottom">
								<p class="top-floor">新館3F</p>
								<p class="top-shop">ポルコロッソ&nbsp;&nbsp;<span class="top-category">バッグ・雑貨</span>
								</p>
								<p class="top-date">9/7(木)&nbsp;NEW OPEN!</p>
							</div>
						</div>
						<div class="photo-hover"><span class="icon new"></span>
							<div class="fadeInLeft animated">
								<h1 class="hover-ttl">新館3F<br>9/7(木)NEW OPEN!<br><span>バッグ・雑貨</span></h1>
								<p class="hover-img"><img src="../../web/autumun_newshop/images/newshop/logo/lo-06.png" height="30" alt="ポルコロッソ">
								</p>
								<p class="hover-text">愛着を持って長く使えるモノをコンセプトに、手のぬくもりが感じられる革のカバンやハンドメイド小物が多数。</p>
								<div class="hover-box clearfix">
									<p class="hover-plan">〈PARCOカード〉新規入会またはご利用でバッグ15％ＯＦＦ</p>
									<!-- hover-box -->
								</div>
								<!-- fadeInLeft animated -->
							</div>
							<!-- photo-hover -->
						</div>
					</div>
				</div>
				<div class="photo-block">
					<div class="photo-block-in">
						<div class="top-visual"><img class="visual" src="../../web/autumun_newshop/images/newshop/shop/07.jpg" width="100%" alt="グランビュフル">
							<div class="visual-bottom">
								<p class="top-floor">本館2F</p>
								<p class="top-shop">グランビュフル&nbsp;&nbsp;<span class="top-category">レディス</span>
								</p>
								<p class="top-date">8/25(金)&nbsp;NEW OPEN!</p>
							</div>
						</div>
						<div class="photo-hover"><span class="icon new"></span>
							<div class="fadeInLeft animated">
								<h1 class="hover-ttl">本館2F<br>8/25(金)NEW OPEN!<br><span>レディス</span></h1>
								<p class="hover-img"><img src="../../web/autumun_newshop/images/newshop/logo/lo-07.png" height="30" alt="グランビュフル">
								</p>
								<p class="hover-text"></p>
								<div class="hover-box clearfix">
									<!-- hover-box -->
								</div>
								<!-- fadeInLeft animated -->
							</div>
							<!-- photo-hover -->
						</div>
					</div>
				</div>
				<div class="photo-block">
					<div class="photo-block-in">
						<div class="top-visual"><img class="visual" src="../../web/autumun_newshop/images/newshop/shop/08.jpg" width="100%" alt="アルファショップ">
							<div class="visual-bottom">
								<p class="top-floor">本館6F</p>
								<p class="top-shop">アルファショップ&nbsp;&nbsp;<span class="top-category">メンズ・レディス</span>
								</p>
								<p class="top-date">8/25(金)&nbsp;NEW OPEN!</p>
							</div>
						</div>
						<div class="photo-hover"><span class="icon new"></span>
							<div class="fadeInLeft animated">
								<h1 class="hover-ttl">本館6F<br>8/25(金)NEW OPEN!<br><span>メンズ・レディス</span></h1>
								<p class="hover-img"><img src="../../web/autumun_newshop/images/newshop/logo/lo-08.png" height="30" alt="アルファショップ">
								</p>
								<p class="hover-text">1959年から現在まで全世界で愛され続けてきたミリタリーウェアーの代表的ブランド。</p>
								<div class="hover-box clearfix">
									<!-- hover-box -->
								</div>
								<!-- fadeInLeft animated -->
							</div>
							<!-- photo-hover -->
						</div>
					</div>
				</div>
				<div class="photo-block">
					<div class="photo-block-in">
						<div class="top-visual"><img class="visual" src="../../web/autumun_newshop/images/newshop/shop/09.jpg" width="100%" alt="コレットアンドバイイング">
							<div class="visual-bottom">
								<p class="top-floor">本館7F</p>
								<p class="top-shop">コレットアンドバイイング&nbsp;&nbsp;<span class="top-category">アクセサリー</span>
								</p>
								<p class="top-date">9/15(金)&nbsp;NEW OPEN!!</p>
							</div>
						</div>
						<div class="photo-hover"><span class="icon new"></span>
							<div class="fadeInLeft animated">
								<h1 class="hover-ttl">本館7F<br>9/15(金)NEW OPEN!<br><span>アクセサリー</span></h1>
								<p class="hover-img"><img src="../../web/autumun_newshop/images/newshop/logo/lo-09.png" height="30" alt="コレットアンドバイイング">
								</p>
								<p class="hover-text">"手に取りたくなるモノがいっぱい""大量買いしたくなる店"をコンセプトとしたアクセサリーショップ</p>
								<div class="hover-box clearfix">
									<p class="hover-plan">2,160円以上お買い上げでノベルティプレゼント【9/15～24】</p>
									<!-- hover-box -->
								</div>
								<!-- fadeInLeft animated -->
							</div>
							<!-- photo-hover -->
						</div>
					</div>
				</div>
				<div class="photo-block">
					<div class="photo-block-in">
						<div class="top-visual"><img class="visual" src="../../web/autumun_newshop/images/newshop/shop/10.jpg" width="100%" alt="ハナアフバイバルコスデザイン">
							<div class="visual-bottom">
								<p class="top-floor">新館3F</p>
								<p class="top-shop">ハナアフバイバルコスデザイン&nbsp;&nbsp;<span class="top-category">バッグ</span>
								</p>
								<p class="top-date">9/1(金)&nbsp;NEW OPEN!</p>
							</div>
						</div>
						<div class="photo-hover"><span class="icon new"></span></span><span class="badge-li"></span>
							<div class="fadeInLeft animated">
								<h1 class="hover-ttl">新館3F<br>9/1(金)NEW OPEN!<br><span>バッグ</span></h1>
								<p class="hover-img"><img src="../../web/autumun_newshop/images/newshop/logo/lo-10.png" height="30" alt="ハナアフバイバルコスデザイン">
								</p>
								<p class="hover-text">東洋らしいフォルムと西洋のエレガンスが融合するスタイルが新しいジャパニーズ・モダンを表現。</p>
								<div class="hover-box clearfix">
									<p></p>
									<!-- hover-box -->
								</div>
								<!-- fadeInLeft animated -->
							</div>
							<!-- photo-hover -->
						</div>
					</div>
				</div>
				<div class="photo-block">
					<div class="photo-block-in">
						<div class="top-visual"><img class="visual" src="../../web/autumun_newshop/images/newshop/shop/11.jpg" width="100%" alt="soa">
							<div class="visual-bottom">
								<p class="top-floor">新館4F</p>
								<p class="top-shop">soa&nbsp;&nbsp;<span class="top-category">アクセサリー</span>
								</p>
								<p class="top-date">9/15(金)&nbsp;NEW OPEN!</p>
							</div>
						</div>
						<div class="photo-hover"><span class="icon new"></span><span class="badge-li"></span>
							<div class="fadeInLeft animated">
								<h1 class="hover-ttl">新館4F<br>9/15(金)NEW OPEN!<br><span>アクセサリー</span></h1>
								<p class="hover-img"><img src="../../web/autumun_newshop/images/newshop/logo/lo-11.png" height="30" alt="soa">
								</p>
								<p class="hover-text">足を運んでいただく度に違った装いをみせるアクセサリーのラインナップは一期一会を表す世界。自分へのご褒美をもっと気軽に。</p>
								<div class="hover-box clearfix">
									<p class="hover-plan">3,780円以上ご購入で3ピースポーチプレゼント（先着100名様）</p>
									<!-- hover-box -->
								</div>
								<!-- fadeInLeft animated -->
							</div>
							<!-- photo-hover -->
						</div>
					</div>
				</div>

				<?php
				/**
				 * PHOTO-END
				 */
				?>
			</div>

		</div>


		<!-- /contents -->
	</div>
	<div class="footer">
		<p class="btn-pagetop"><a href="#" class="page-scroll"><img src="images/btn_pagetop.png" width="50px" alt="ページトップ"></a>
		</p>
		<div class="footer__in">
			<ul class="snsnlist">
				<li class="snavi02">
					<div class="fb-like" data-href="http://<?=STORE_NAME?>.parco.jp/web/autumun_newshop/" data-layout="button" data-action="like" data-show-faces="false" data-share="false"></div>
				</li>
				<li class="snavi01"><a href="https://twitter.com/share" class="twitter-share-button" data-url="http://<?=STORE_NAME?>.parco.jp/web/autumun_newshop/" data-text="<?=TITLE?>" data-lang="ja" data-count="none">ツイート</a>
					<script>
						! function ( d, s, id ) {
							var js, fjs = d.getElementsByTagName( s )[ 0 ],
								p = /^http:/.test( d.location ) ? 'http' : 'https';
							if ( !d.getElementById( id ) ) {
								js = d.createElement( s );
								js.id = id;
								js.src = p + '://platform.twitter.com/widgets.js';
								fjs.parentNode.insertBefore( js, fjs );
							}
						}( document, 'script', 'twitter-wjs' );
					</script>
				</li>
				<li class="snavi03">

					<!-- +1 ボタン を表示したい位置に次のタグを貼り付けてください。 -->
					<div class="g-plusone" data-size="tall" data-annotation="none" data-href="http://<?=STORE_NAME?>.parco.jp/s/autumun_newshop/"></div>

					<!-- 最後の +1 ボタン タグの後に次のタグを貼り付けてください。 -->
					<script type="text/javascript">
						window.___gcfg = {
							lang: 'ja'
						};

						( function () {
							var po = document.createElement( 'script' );
							po.type = 'text/javascript';
							po.async = true;
							po.src = 'https://apis.google.com/js/platform.js';
							var s = document.getElementsByTagName( 'script' )[ 0 ];
							s.parentNode.insertBefore( po, s );
						} )();
					</script>
				</li>
				<li class="snavi04">
					<script type="text/javascript" src="//media.line.me/js/line-button.js?v=20140411"></script>
					<script type="text/javascript">
						new media_line_me.LineButton( {
							"pc": false,
							"lang": "ja",
							"type": "a",
							"text": "http://<?=STORE_NAME?>.parco.jp/s/autumun_newshop/",
							"withUrl": false
						} );
					</script>
				</li>

			</ul>

			<div class="parco"><a href="/"><img src="/s/autumun_newshop/images/btn_sitetop.png" alt="SITE TOP" width="230" height="12" /></a>
			</div>
			<div class="copyright"><img src="/s/autumun_newshop/images/txt_copyright.png" alt="COPYRIGHT (C) PARCO CO., LTD. ALL RIGHTS RESERVED." width="198" height="9"/>
			</div>


		</div>
</body>

</html>