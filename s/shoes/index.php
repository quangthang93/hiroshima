<?php
include_once($_SERVER['DOCUMENT_ROOT']."/static/shoes/inc/config.php");

$title = 'Shoes Shops｜'. STORE_NAME_JA .'PARCO';
$description = STORE_NAME_JA .'パルコのShoes Shops。'. STORE_NAME_JA .'パルコおすすめシューズが勢ぞろい！注目のヒールやスニーカーをご紹介いたします。';
$keywords = 'シューズ,スニーカー,ヒール';
$shareurl = 'http://'. STORE_NAME.'.parco.jp/web/'.DIRNAME.'/';
$otherdevice = '/web/'.DIRNAME.'/';

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="ja" xml:lang="ja">
<head>
<?php include_once($_SERVER['DOCUMENT_ROOT'].'/static/s/'.DIRNAME.'/inc/html-head.php'); ?>
</head>
<body style="overflow-x:hidden" class="blog">
<?php include_once($_SERVER['DOCUMENT_ROOT'] . '/load_script.php'); ?>

<a name="top" id="top"></a>

<div id="leftmenu">
</div>

<div id="container">
<script type="text/javascript" src="/spage2/parts/headermenu.js?sidebar=1"></script>

<script type="text/javascript">
$(function(){
    var trigger = $('.floor_list .shop_list_trigger');
    trigger.on('click', function(e){
        e.preventDefault();
        var box = $(this).next();
        $(this).toggleClass('box_open')
        box.slideToggle(300);
    });
});
</script>

<div id="main">

<?php include_once($_SERVER['DOCUMENT_ROOT'].'/static/s/'.DIRNAME.'/inc/header.php'); ?>

<div class="list clearfix">

<h2 class="ttlShops ttl-border" id="shops"><img src="/static/s/<?=DIRNAME?>/images/txt_shops.png" width="52" height="10" alt="SHOPS" /></h2>
<ul class="list-shops">
  <li class="item-list-shops">
    <a href="http://hiroshima.parco.jp/spage2/968/">
      <div class="floor">本館<br>1F</div>
      <div class="image"><img src="/static/s/<?=DIRNAME?>/images/img_shops01.jpg" alt=""></div>
      <div class="brand">
        <div class="logo"><img src="/static/s/<?=DIRNAME?>/images/logo_shops01.png" alt=""></div>
        <p class="name">ダイアナ</p>
      </div>
    </a>
  </li>
  <li class="item-list-shops">
    <a href="http://hiroshima.parco.jp/spage2/976/">
      <div class="floor">本館<br>1F</div>
      <div class="image"><img src="/static/s/<?=DIRNAME?>/images/img_shops02.jpg" alt=""></div>
      <div class="brand">
        <div class="logo"><img src="/static/s/<?=DIRNAME?>/images/logo_shops02.png" alt=""></div>
        <p class="name">オゥバニスター</p>
      </div>
    </a>
  </li>
  <li class="item-list-shops">
    <a href="http://hiroshima.parco.jp/spage2/1100/">
      <div class="floor">本館<br>4F</div>
      <div class="image"><img src="/static/s/<?=DIRNAME?>/images/img_shops03.jpg" alt=""></div>
      <div class="brand">
        <div class="logo"><img src="/static/s/<?=DIRNAME?>/images/logo_shops03.png" alt=""></div>
        <p class="name">エスペランサ</p>
      </div>
    </a>
  </li>
  <li class="item-list-shops">
    <a href="http://hiroshima.parco.jp/spage2/10823/">
      <div class="floor">本館<br>6F</div>
      <div class="image"><img src="/static/s/<?=DIRNAME?>/images/img_shops04.jpg" alt=""></div>
      <div class="brand">
        <div class="logo"><img src="/static/s/<?=DIRNAME?>/images/logo_shops04.png" alt=""></div>
        <p class="name">エースシューズ</p>
      </div>
    </a>
  </li>
  <li class="item-list-shops">
    <a href="http://hiroshima.parco.jp/spage2/1242/">
      <div class="floor">本館<br>7F</div>
      <div class="image"><img src="/static/s/<?=DIRNAME?>/images/img_shops05.jpg" alt=""></div>
      <div class="brand">
        <div class="logo"><img src="/static/s/<?=DIRNAME?>/images/logo_shops05.png" alt=""></div>
        <p class="name">オリエンタル<br>トラフィック</p>
      </div>
    </a>
  </li>
  <li class="item-list-shops">
    <a href="http://hiroshima.parco.jp/spage2/10854/">
      <div class="floor">新館<br>3F</div>
      <div class="image"><img src="/static/s/<?=DIRNAME?>/images/img_shops06.jpg" alt=""></div>
      <div class="brand">
        <div class="logo"><img src="/static/s/<?=DIRNAME?>/images/logo_shops06.png" alt=""></div>
        <p class="name">シューズラウンジ</p>
      </div>
    </a>
  </li>
  <li class="item-list-shops">
    <a href="http://hiroshima.parco.jp/spage2/1073/">
      <div class="floor">新館<br>6F</div>
      <div class="image"><img src="/static/s/<?=DIRNAME?>/images/img_shops07.jpg" alt=""></div>
      <div class="brand">
        <div class="logo"><img src="/static/s/<?=DIRNAME?>/images/logo_shops07.png" alt=""></div>
        <p class="name">ABCマート</p>
      </div>
    </a>
  </li>
</ul>

<?php /*<h2 class="ttlBlog ttlBlog--type02">NEWS&amp;EVENT</h2>*/ ?>
<?php /*<div class="blog_parts_block"
	data-blog-type="3"
	data-view-type="3"
	data-category-id="170"
	data-init-count="4"
	data-more-post="true"
	data-next-count="10"
	data-post-sdate="20161212000000"
	data-post-edate="20170118000000"
	data-ext-response-fields="kaeru2_product_id"
></div>*/ ?>

<h2 class="ttlBlog ttlBlog--type02 ttl-border" id="blog"><img src="/static/s/<?=DIRNAME?>/images/txt_shop_blog.png" width="44" height="10" alt="BLOG" /></h2>
<div class="blog_parts_block"
	data-blog-type="1"
	data-view-type="3"
	data-category-id="20"
	data-init-count="6"
	data-more-post="true"
	data-next-count="10"
	data-post-sdate="20170508000000"
	data-post-edate=""
	data-ext-response-fields="kaeru2_product_id"
></div>


</div>

<div class="snavi pb10">
<ul>
<?php
  $urlencode = urlencode($shareurl);
  $titleencode = urlencode($title);
  $urlhost =str_replace('http%3A%2F%2F','',$urlencode);
?>
<li class="snavi02">
<a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo $urlencode; ?>" target="_blank"><img src="/s/images/check/snsbtn-facebook.png" height="20" alt="facebookでシェアする"></a>
</li>
<li class="snavi01">
<a href="https://twitter.com/share?url=<?php echo $urlencode; ?>&text=<?php echo $titleencode; ?>" target="_blank"><img src="/s/images/check/snsbtn-twitter.png" height="20" alt="ツイートする"></a>
</li>
<li class="snavi03">
<a href="https://plus.google.com/share?url=<?php echo $urlhost; ?>" target="_blank"><img src="/s/images/check/snsbtn-googleplus.png" height="20" alt="Google +1"></a>
</li>
<li class="snavi04"><a href="http://line.me/R/msg/text/?<?php echo $titleencode; ?>%0D%0A<?php echo $urlencode; ?>" target="_blank"><img src="/s/images/check/snsbtn-line.png" height="20" alt="LINEで送る"></a>
</li>
</ul>
<!-- /snavi --></div>

</div><!--/main-->

<?php include_once($_SERVER['DOCUMENT_ROOT'].'/static/s/'.DIRNAME.'/inc/footer.php'); ?>

<script>
$(document).ready( function () {
	link_enabled = true;
})
</script>
</body>
</html>
