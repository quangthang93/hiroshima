<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta http-equiv="content-style-type" content="text/css" />
<meta http-equiv="content-script-type" content="text/javascript" />
<meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=no" />
<meta name="robots" content="index,follow" />
<title><?php echo $title; ?></title>
<meta name="description" content="<?php echo $description; ?>" />
<meta name="keywords" content="<?php echo $keywords; ?>"/>

<link rel="canonical" href="<?php echo $otherdevice; ?>" />
<!-- <link rel="stylesheet" href="/s/common/css/containerSlider.css" /> -->
<link rel="shortcut icon" href="/favicon.ico" />
<link rel="stylesheet" href="/s/common/css/import_check.css" />
<link rel="stylesheet" href="/web/blog_parts/css/blog_parts.css" />
<link rel="stylesheet" href="/static/s/<?=DIRNAME?>/css/styles.css" />

<script src="/s/common/js/jquery.js" type="text/javascript"></script>
<script src="/s/common/js/jquery.cookie.js" type="text/javascript"></script>
<script src="/s/common/js/jquery.flexslider.js" type="text/javascript"></script>
<script src="/s/common/js/jquery.fancybox.js?v=2.1.3" type="text/javascript"></script>
<script src="/s/common/js/common.js" type="text/javascript"></script>
<script src="/s/common/js/menuSlider.js" type="text/javascript"></script>
<script src="/static/s/<?=DIRNAME?>/js/page.js" type="text/javascript"></script>

<script src="/s/common/js/imgLiquid.js" type="text/javascript"></script>
<script type="text/javascript">
  $(function(){
    $('.imgLiquidBox').imgLiquid();
  });
</script>
<script type="text/javascript" src="/s/common/js/social.js"></script>
<script type="text/javascript" src="/common/js/imgLiquid.js"></script>
<script type="text/javascript" src="/web/blog_parts/js/jquery.tile.js"></script>
<script type="text/javascript" src="/web/blog_parts/js/hogan-2.0.0.js"></script>
<script type="text/javascript" src="/web/blog_parts/js/jquery.blog_parts.js"></script>

<!-- OGP -->
<meta property="fb:admins" content="100005423256030">
<meta property="og:locale" content="ja_JP" />
<meta property="og:type" content="website" />
<meta property="og:title" content="<?php echo $title; ?>" />
<meta property="og:url" content="<?php echo $shareurl; ?>" />
<meta property="og:site_name" content="<?=STORE_NAME_JA?>PARCO" />
<meta property="og:description" content="<?php echo $description; ?>" />
<meta property="og:image" content="http://<?=STORE_NAME?>.parco.jp/static/<?=DIRNAME?>/images/ogp.jpg" />
<!-- /OGP -->
