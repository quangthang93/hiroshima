$(function(){
  // navFixed();
});

$(window).on('load', function() {
  // navFixed();
});

/* !nav fixed -------------------------------------------------------------- */
var navFixed = (function(){

var nav = $('.js-fixed'),
    navH = nav.height(),
    navWrap = $('.category_nav_wrap');
    offset = nav.offset();
    // console.log(offset);
    navWrap.height(navH);
    $(window).scroll(function () {
      if($(window).scrollTop() > offset.top) {
        nav.addClass('fixed');
      } else {
        nav.removeClass('fixed');
      }
    });
});