<!DOCTYPE html>
<html lang="ja">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
  <meta name="description" content="Hiroshima PARCO is a shopping hot spot packed with 170 stores that feature fashion, interior, sundries, cosmetics, and more.">
  <title>Welcome to PARCO　－歡迎光臨－What's HIROSHIMA PARCO?</title>
  <link rel="canonical" href="../../web/language/index.html">

  <meta property="og:site_name" content="広島PARCO">
  <meta property="og:type" content="article">
  <meta property="og:url" content="https://hiroshima.parco.jp/s/language/">
  <meta property="og:title" content="Welcome to PARCO　－歡迎光臨－What's HIROSHIMA PARCO?">
  <meta property="og:description" content="Hiroshima PARCO is a shopping hot spot packed with 170 stores that feature fashion, interior, sundries, cosmetics, and more.">
  <meta property="og:image" content="http://hiroshima.parco.jp/s/language/images/ogp.png">
  <meta property="og:locale" content="ja_JP">
  <meta property="fb:admins" content="100005423256030">

  <link rel="stylesheet" href="css/reset.css">
  <link rel="stylesheet" href="css/layout.css">
  <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
  <script src="js/common.js"></script>
</head>

<body>

  <!-- Google Tag Manager -->
  <script>
    (function (w, d, s, l, i) {
      w[l] = w[l] || [];
      w[l].push({
        'gtm.start': new Date().getTime(),
        event: 'gtm.js'
      });
      var f = d.getElementsByTagName(s)[0],
        j = d.createElement(s),
        dl = l != 'dataLayer' ? '&l=' + l : '';
      j.async = true;
      j.src = '//www.googletagmanager.com/gtm.js?id=' + i + dl;
      f.parentNode.insertBefore(j, f);
    })(window, document, 'script', 'dataLayer', 'GTM-5FDDGB');
  </script>
  <!-- End Google Tag Manager -->

  <div class="wrapper">
    <div class="head">
      <div class="page-header">
      <h1 class="main_ttl">
        <img src="images/ttl_main.png" class="w100" alt="Welcome to PARCO　－歡迎光臨－">
      </h1>
    </div>
  </div>

    <section class="main-contents">
      <div class="explain_area">
        <div class="explain_txt">
          <p class="explain_en">Hiroshima PARCO is a shopping hot spot packed with 170 stores that feature fashion, interior, sundries, cosmetics, and more. <br>
          It's located right in central Hiroshima, so feel free to stop by during trips to Miyajima or the Seto-uchi Area.</p>
          <p class="explain_ch">廣島PARCO是家匯聚了流行服飾、生活傢飾、雜貨以及美妝等多達170家店舖的購物景點。<br>
          其位於廣島中心地帶，當您到宮島或瀨戶內觀光時，不妨可以順道前來逛逛喔！</p>
          <p class="explain_note">※Photos are for illustration purposes only.
            <br>※Not all items featured in the photos may be in stock. Thank you for your understanding.
            <br>※照片僅供參考。
            <br>※店內可能會沒有照片中的商品。敬請諒解。
          </p>
        </div>
      </div>
      <h2 class="sale_ttl">
        <img class="w100" src="images/bnr_sale.png" alt="">
      </h2>
      <div class="recommend_area">
        <div class="recommend_ttl_en">
          <img src="images/ttl_recommend.png" alt="" width="260">
        </div>
        <div class="recommend_ttl_ch">推薦的商店和餐廳</div>
        <div class="item_area">
          <ul class="shop_area">
            <li class="shop_box">
              <figure>
                <img src="images/img_shop_01.png" alt="SUZU CAFE" width="">
              </figure>
              <div class="shop_detail">
                <div class="shop_floor">Main Bldg. B1F</div>
                <div class="shop_name">SUZU CAFE</div>
                <div class="shop_category">&lt;Cafe&gt;</div>
              </div>
            </li>
            <li class="shop_box">
              <figure>
                <img src="images/img_shop_02.png" alt="ATTENIR" width="">
              </figure>
              <div class="shop_detail">
                <div class="shop_floor">Main Bldg. B1F</div>
                <div class="shop_name">ATTENIR</div>
                <div class="shop_category">&lt;Cosmetics&gt;</div>
              </div>
            </li>
            <li class="shop_box">
              <figure>
                <img src="images/img_shop_03.png" alt="FRED PERRY" width="">
              </figure>
              <div class="shop_detail">
                <div class="shop_floor">New Bldg. 4F</div>
                <div class="shop_name">FRED PERRY</div>
                <div class="shop_category">&lt;Men's&Ladies' Fashion&gt;</div>
              </div>
            </li>
            <li class="shop_box">
              <figure>
                <img src="images/img_shop_04.png" alt="ABC MART" width="">
              </figure>
              <div class="shop_detail">
                <div class="shop_floor">New Bldg. 6F</div>
                <div class="shop_name">ABC MART</div>
                <div class="shop_category">&lt;Shoes&gt;</div>
              </div>
            </li>
            <li class="shop_box">
              <figure>
                <img src="images/img_shop_05.png" alt="MUJI" width="">
              </figure>
              <div class="shop_detail last">
                <div class="shop_floor">New Bldg. 7・8F</div>
                <div class="shop_name">MUJI</div>
                <div class="shop_category">&lt;Lifestyle Goods&gt;</div>
              </div>
            </li>
          </ul>
        </div>
        <div class="more_btn">
          <a href="https://hiroshima.parco.jp/s/storeinformation/">
            <img src="images/btn_more.png" width="110" alt="AND MORE!!">
          </a>
        </div>
      </div>
    </section>
    <footer class="page_footer">
      <div class="top_shop">
        <a href="/">
          <img src="images/link_top.png" class="w100" alt="広島PARCO TOP">
        </a>
      </div>
      <div class="">
        <img src="images/txt_copyright.png" width="260" alt="COPYRIGHT (C) PARCO CO.,LTD ALL RIGHTS RESERVED.">
      </div>
    </footer>
    <!-- /.wrapper -->
  </div>
  <div class="btn_top">
      <img src="images/btn_top.png">
  </div>
</body>

</html>
