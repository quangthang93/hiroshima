
//  page top btn
///////////////////////////////////////////////////////////////
$(function() {
  $(".btn_top").click(function(event) {
      $('body,html').animate({
          scrollTop: 0
      }, 500);
  });
});

$(window).on("scroll", function(){
  var sc = $(this).scrollTop();
  scrollShowPagetopBtn(sc);
});
function scrollShowPagetopBtn(scrollTop) {
  var trigger = $(".head").outerHeight(),
      $btn = $(".btn_top");

  if(scrollTop >= trigger) {
      $btn.addClass("show");
  } else {
      $btn.removeClass("show");
  }
}
