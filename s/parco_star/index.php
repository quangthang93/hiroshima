<!DOCTYPE html>
<html lang="ja">

<?php include_once($_SERVER['DOCUMENT_ROOT'].'/web/parco_star/inc/s/head.php'); ?>



<body>
    <!-- Google Tag Manager -->
    <noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-5FDDGB" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <script>
        (function(w, d, s, l, i) {
            w[l] = w[l] || [];
            w[l].push({
                'gtm.start': new Date().getTime(),
                event: 'gtm.js'
            });
            var f = d.getElementsByTagName(s)[0],
                j = d.createElement(s),
                dl = l != 'dataLayer' ? '&l=' + l : '';
            j.async = true;
            j.src = '//www.googletagmanager.com/gtm.js?id=' + i + dl;
            f.parentNode.insertBefore(j, f);
        })(window, document, 'script', 'dataLayer', 'GTM-5FDDGB');
    </script>
    <!-- End Google Tag Manager -->
    <div id="fb-root"></div>
    <script>
        (function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s);
            js.id = id;
            js.src = "//connect.facebook.net/ja_JP/sdk.js#xfbml=1&version=v2.10";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    </script>
    <header class="bg--colored">
        <div class="widthBinder">
            <div class="headerArea">
                <p class="subttl">
                    <img src="assets/images/char_spcntnt.svg" alt="SPECIAL CONTENT">
                </p>
                <h1 class="logo">
                    <img src="assets/images/logo.svg" alt="パルコのスター">
                </h1>
                <p class="subscript">
                    広島パルコの個性輝く12名のショップスタッフが集結！<br> 　各ショップにてご来店お待ちしております！
                </p>
            </div>
        </div>
    </header>
    <main class="bg contentsList">
        <div class="widthBinder">
            <p class="contentsList__ttl"><img src="assets/images/char_stflist.svg" alt="STAFF LIST"></p>
            <ul class="contentsLists">

                <li class="listItem">
                    <div class="listItem__imgArea">
                        <a href="01.php">
                            <div class="badge--fill">
                                <span>01</span>
                            </div>
                            <img src="assets/images/01.jpg" alt="">
                        </a>
                    </div>
                    <p class="listItem__info">
                        <span>本館 / B1F</span>
                        <span>アテニア</span><br><br>
                        アテニアの天使
                    </p>
                    <p class="listItem__name">
                        太田さん
                    </p>
                </li>

                <li class="listItem">
                    <div class="listItem__imgArea">
                        <a href="02.php">
                            <div class="badge--fill">
                                <span>02</span>
                            </div>
                            <img src="assets/images/02.jpg" alt="">
                        </a>
                    </div>
                    <p class="listItem__info">
                        <span>本館 / 2F</span>
                        <span>アーバンリサーチアンドロッソ</span><br><br>
                        おもしろファッションスピーカー
                    </p>
                    <p class="listItem__name">
                        原田さん
                    </p>
                </li>

                <li class="listItem">
                    <div class="listItem__imgArea">
                        <a href="03.php">
                            <div class="badge--fill">
                                <span>03</span>
                            </div>
                            <img src="assets/images/03.jpg" alt="">
                        </a>
                    </div>
                    <p class="listItem__info">
                        <span>本館 / 3F</span>
                        <span>ムルーア</span><br><br>
                        『魅せ方』のプロ・インスタ女子
                    </p>
                    <p class="listItem__name">
                        中野さん
                    </p>
                </li>

                <li class="listItem">
                    <div class="listItem__imgArea">
                        <a href="04.php">
                            <div class="badge--fill">
                                <span>04</span>
                            </div>
                            <img src="assets/images/04.jpg" alt="">
                        </a>
                    </div>
                    <p class="listItem__info">
                        <span>本館 / 4F</span>
                        <span>スライ</span><br><br>
                        お姉さんコーデのカリスマ
                    </p>
                    <p class="listItem__name">
                        吉武さん
                    </p>
                </li>

                <li class="listItem">
                    <div class="listItem__imgArea">
                        <a href="05.php">
                            <div class="badge--fill">
                                <span>05</span>
                            </div>
                            <img src="assets/images/05.jpg" alt="">
                        </a>
                    </div>
                    <p class="listItem__info">
                        <span>本館 / 6F</span>
                        <span>ハレ</span><br><br>
                        オシャレ宣教師
                    </p>
                    <p class="listItem__name">
                        小林さん
                    </p>
                </li>

                <li class="listItem">
                    <div class="listItem__imgArea">
                        <a href="06.php">
                            <div class="badge--fill">
                                <span>06</span>
                            </div>
                            <img src="assets/images/06.jpg" alt="">
                        </a>
                    </div>
                    <p class="listItem__info">
                        <span>本館 / 7F</span>
                        <span>ヘザー</span><br><br>
                        カワイイ女子力モンスター
                    </p>
                    <p class="listItem__name">
                        中島さん
                    </p>
                </li>

                <li class="listItem">
                    <div class="listItem__imgArea">
                        <a href="07.php">
                            <div class="badge--fill">
                                <span>07</span>
                            </div>
                            <img src="assets/images/07.jpg" alt="">
                        </a>
                    </div>
                    <p class="listItem__info">
                        <span>新館 / B1F</span>
                        <span>ユナイテッドアローズ</span><br><br>
                        Mr.UA
                    </p>
                    <p class="listItem__name">
                        村上さん
                    </p>
                </li>

                <li class="listItem">
                    <div class="listItem__imgArea">
                        <a href="08.php">
                            <div class="badge--fill">
                                <span>08</span>
                            </div>
                            <img src="assets/images/08.jpg" alt="">
                        </a>
                    </div>
                    <p class="listItem__info">
                        <span>新館 / 1F</span>
                        <span>ジャーナルスタンダード</span><br><br>
                        笑う門にはフクきたる
                    </p>
                    <p class="listItem__name">
                        菊川さん
                    </p>
                </li>

                <li class="listItem">
                    <div class="listItem__imgArea">
                        <a href="09.php">
                            <div class="badge--fill">
                                <span>09</span>
                            </div>
                            <img src="assets/images/09.jpg" alt="">
                        </a>
                    </div>
                    <p class="listItem__info">
                        <span>新館 / 2F</span>
                        <span>ガリャルダガランテ</span><br><br>
                        アジアンビューティー・癒し接客
                    </p>
                    <p class="listItem__name">
                        野稲さん
                    </p>
                </li>

                <li class="listItem">
                    <div class="listItem__imgArea">
                        <a href="10.php">
                            <div class="badge--fill">
                                <span>10</span>
                            </div>
                            <img src="assets/images/10.jpg" alt="">
                        </a>
                    </div>
                    <p class="listItem__info">
                        <span>新館 / 4F</span>
                        <span>ユナイテッドアローズ<br>グリーンレーベルリラクシング</span><br><br>
                        働く女性の強力サポーター！
                    </p>
                    <p class="listItem__name">
                        畑中さん
                    </p>
                </li>

                <li class="listItem">
                    <div class="listItem__imgArea">
                        <a href="11.php">
                            <div class="badge--fill">
                                <span>11</span>
                            </div>
                            <img src="assets/images/11.jpg" alt="">
                        </a>
                    </div>
                    <p class="listItem__info">
                        <span>新館 / 7F・8F</span>
                        <span>無印良品</span><br><br>
                        やさしさ100%！インテリアの匠
                    </p>
                    <p class="listItem__name">
                        吉原さん
                    </p>
                </li>

                <li class="listItem">
                    <div class="listItem__imgArea">
                        <a href="12.php">
                            <div class="badge--fill">
                                <span>12</span>
                            </div>
                            <img src="assets/images/12.jpg" alt="">
                        </a>
                    </div>
                    <p class="listItem__info">
                        <span>新館 / 9F</span>
                        <span>タワーレコード</span><br><br>
                        邦楽ペディア
                    </p>
                    <p class="listItem__name">
                        山田さん
                    </p>
                </li>

            </ul>
        </div>
        <?php include_once($_SERVER['DOCUMENT_ROOT'].'/web/parco_star/inc/common/banner.php'); ?>
    </main>

    <?php include_once($_SERVER['DOCUMENT_ROOT'].'/web/parco_star/inc/s/footer.php'); ?>




</body>

</html>