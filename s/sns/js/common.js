/*---------

!common.js

------------ */

/* !smoothscroll --------------------------------------------------- */
$(function() {
  $('a[href^=#]').click(function(){
    var speed = 500;
    var href= $(this).attr('href');
    var target = $(href == '#' || href == "" ? 'html' : href);
    var position = target.offset().top;
    $('html, body').animate({scrollTop:position}, speed, 'swing');
    return false;
  });

  $('.photo-link').each(function() {
    //var flg = 'default';
    var allPhotohover = $('.photo-hover');
    var allVisual = $('.visual');
      $(this).click(function(){
        //if(flg == 'default'){
          allPhotohover.hide();
          allVisual.removeClass('is-active');
          //flg = 'default';
          //event.preventDefault();
          $(this).find('.visual').addClass('is-active');
          $(this).find('.photo-hover').fadeIn(200);
          //flg = "changed";
        //}else{
        //}
      });
  });
/* !categoryclass --------------------------------------------------- */
  $('.nav-in li a').each(function() {
    $(this).click(function(event){
      $this = $(this);
      $body = $('body');
      var selected = $(this).attr('class');
      event.preventDefault();
      $body.removeClass(function(index, className) {
      return (className.match(/\bcategory-\S+/g) || []).join(' ');
      });
      $body.addClass('category-' + selected);
    });
  });
});

