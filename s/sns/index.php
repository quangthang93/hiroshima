<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width,initial-scale=1">
<meta name="description" content="広島PARCOのSNSアカウントをご紹介！最新トレンドはSNSで見つける！">
<meta name="keyword" content="広島,sns,#広島パルコ,トレンド、ショップアカウント">
<title>SNS LIST｜広島PARCO</title>
<?php /* OGP */ ?>
<meta property="og:site_name" content="PARCO">
<meta property="og:type" content="article">
<meta property="og:url" content="http://hiroshima.parco.jp/web/sns/">
<meta property="og:title" content="SNS LIST｜広島PARCO">
<meta property="og:description" content="広島PARCOのSNSアカウントをご紹介！最新トレンドはSNSで見つける！">
<meta property="og:image" content="http://hiroshima.parco.jp/web/sns/images/ogp.png">
<meta property="og:locale" content="ja_JP">
<meta property="fb:admins" content="100005423256030">
<link rel="stylesheet" href="css/style.css">
<link rel="canonical" href="http://hiroshima.parco.jp/web/sns/" />
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
</head>

<body id="Top">
  <?php include_once('../../load_script.php'); ?>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.0";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<header class="header">
  <div id="header" class="clear">
<p class="header_logo"><a href="/spage2/"><img width="107" src="../common/images/header_logo.png" alt="広島PARCO"></a></p>
<!--/header--></div>
   <h1 class="header-ttl"><img src="images/main.jpg" alt="最新トレンドはSNSで見つける! #広島PARCO"></h1>
   <div class="header_sns">
     <img src="images/left.png" width="15" height="33" alt="">
     <a href="#"><img src="images/btn_instagram.png" width="30" height="30" alt=""></a>
     <a href="#"><img src="images/btn_twitter.png" width="30" height="30" alt=""></a>
     <img src="images/right.png" width="23" height="33" alt="">
   </div>
</header>

  <div class="days">
  <p class="ribbon">広島パルコ　ショップアカウント</p>
    <img src="images/days.png" width="80" alt="日々更新中">
  </div>

<div class="box">
  <ul class="shopList">
      <li class="shop">
          <p class="floor">本館B1F</p>
          <div class="shop_img"><img src="../../web/sns/images/logo/logo_1.jpg" alt="スズカフェ"></div>
          <div class="sns">
              <div class="instagram"><a href="https://instagram.com/suzucafe_hiroshimaparco" target="_blank"><img src="images/icon_insta.png" width="17" height="17" alt="Instagram"></a></div>
              <div class="twitter"><a href="https://twitter.com/suzucafe_parco" target="_blank"><img src="images/icon_tw.png" width="21" height="17" alt="Twitter"></a></div>
          </div>
      </li>
      <li class="shop">
          <p class="floor">本館B1F</p>
          <div class="shop_img"><img src="../../web/sns/images/logo/logo_2.jpg" alt="ザッカイーキュー"></div>
          <div class="sns">
              <div class="instagram"><a href="https://instagram.com/zakkaeq_hiroshima" target="_blank"><img src="images/icon_insta.png" width="17" height="17" alt="Instagram"></a></div>
              <div class="twitter"><img src="images/noicon_tw.png" width="21" height="17" alt="Twitter"></div>
          </div>
      </li>
      <li class="shop">
          <p class="floor">本館1F</p>
          <div class="shop_img"><img src="../../web/sns/images/logo/logo_3.jpg" alt="イアパピヨネ"></div>
          <div class="sns">
              <div class="instagram"><a href="https://instagram.com/hi_earhappy" target="_blank"><img src="images/icon_insta.png" width="17" height="17" alt="Instagram"></a></div>
              <div class="twitter"><img src="images/noicon_tw.png" width="21" height="17" alt="Twitter"></div>
          </div>
      </li>
      <li class="shop">
          <p class="floor">本館1F</p>
          <div class="shop_img"><img src="../../web/sns/images/logo/logo_4.jpg" alt="シエナ"></div>
          <div class="sns">
              <div class="instagram"><a href="https://instagram.com/siena_hiroshima" target="_blank"><img src="images/icon_insta.png" width="17" height="17" alt="Instagram"></a></div>
              <div class="twitter"><img src="images/noicon_tw.png" width="21" height="17" alt="Twitter"></div>
          </div>
      </li>
      <li class="shop">
          <p class="floor">本館2F</p>
          <div class="shop_img"><img src="../../web/sns/images/logo/logo_5.jpg" alt="アングリッド"></div>
          <div class="sns">
              <div class="instagram"><img src="images/noicon_insta.png" width="17" height="17" alt="Instagram"></div>
              <div class="twitter"><a href="https://twitter.com/Ungridhiroshima" target="_blank"><img src="images/icon_tw.png" width="21" height="17" alt="Twitter"></a></div>
          </div>
      </li>
      <li class="shop">
          <p class="floor">本館2F</p>
          <div class="shop_img"><img src="../../web/sns/images/logo/logo_6.jpg" alt="アトリエドゥサボン"></div>
          <div class="sns">
              <div class="instagram"><img src="images/noicon_insta.png" width="17" height="17" alt="Instagram"></div>
              <div class="twitter"><a href="https://twitter.com/hiroshima_ats_" target="_blank"><img src="images/icon_tw.png" width="21" height="17" alt="Twitter"></a></div>
          </div>
      </li>
      <li class="shop">
          <p class="floor">本館3F</p>
          <div class="shop_img"><img src="../../web/sns/images/logo/logo_7.jpg" alt="アンレリッシュ"></div>
          <div class="sns">
              <div class="instagram"><a href="https://instagram.com/unrelish_hiroshima" target="_blank"><img src="images/icon_insta.png" width="17" height="17" alt="Instagram"></a></div>
              <div class="twitter"><a href="https://twitter.com/UNRELISH_082" target="_blank"><img src="images/icon_tw.png" width="21" height="17" alt="Twitter"></a></div>
          </div>
      </li>
      <li class="shop">
          <p class="floor">本館3F</p>
          <div class="shop_img"><img src="../../web/sns/images/logo/logo_8.jpg" alt="ピーチジョン"></div>
          <div class="sns">
              <div class="instagram"><a href="https://instagram.com/pjs_hiroshima" target="_blank"><img src="images/icon_insta.png" width="17" height="17" alt="Instagram"></a></div>
              <div class="twitter"><img src="images/noicon_tw.png" width="21" height="17" alt="Twitter"></div>
          </div>
      </li>
      <li class="shop">
          <p class="floor">本館3F</p>
          <div class="shop_img"><img src="../../web/sns/images/logo/logo_9.jpg" alt="マーキュリーデュオ"></div>
          <div class="sns">
              <div class="instagram"><img src="images/noicon_insta.png" width="17" height="17" alt="Instagram"></div>
              <div class="twitter"><a href="https://twitter.com/mercuryhirosima" target="_blank"><img src="images/icon_tw.png" width="21" height="17" alt="Twitter"></a></div>
          </div>
      </li>
      <li class="shop">
          <p class="floor">本館4F</p>
          <div class="shop_img"><img src="../../web/sns/images/logo/logo_10.jpg" alt="デイライルノアール"></div>
          <div class="sns">
              <div class="instagram"><img src="images/noicon_insta.png" width="17" height="17" alt="Instagram"></div>
              <div class="twitter"><a href="https://twitter.com/Delylehiroshima" target="_blank"><img src="images/icon_tw.png" width="21" height="17" alt="Twitter"></a></div>
          </div>
      </li>
      <li class="shop">
          <p class="floor">本館4F</p>
          <div class="shop_img"><img src="../../web/sns/images/logo/logo_11.jpg" alt="ダズリン"></div>
          <div class="sns">
              <div class="instagram"><img src="images/noicon_insta.png" width="17" height="17" alt="Instagram"></div>
              <div class="twitter"><a href="https://twitter.com/dazzlin_hiro" target="_blank"><img src="images/icon_tw.png" width="21" height="17" alt="Twitter"></a></div>
          </div>
      </li>
      <li class="shop">
          <p class="floor">本館4F</p>
          <div class="shop_img"><img src="../../web/sns/images/logo/logo_12.jpg" alt="ロデオクラウンズ"></div>
          <div class="sns">
              <div class="instagram"><a href="https://instagram.com/rcs_hiroshima" target="_blank"><img src="images/icon_insta.png" width="17" height="17" alt="Instagram"></a></div>
              <div class="twitter"><img src="images/noicon_tw.png" width="21" height="17" alt="Twitter"></div>
          </div>
      </li>
      <li class="shop">
          <p class="floor">本館4F</p>
          <div class="shop_img"><img src="../../web/sns/images/logo/logo_13.jpg" alt="フルリールアネモネ"></div>
          <div class="sns">
              <div class="instagram"><a href="https://instagram.com/fluriranemone_hiroshima" target="_blank"><img src="images/icon_insta.png" width="17" height="17" alt="Instagram"></a></div>
              <div class="twitter"><img src="images/noicon_tw.png" width="21" height="17" alt="Twitter"></div>
          </div>
      </li>
      <li class="shop">
          <p class="floor">本館4F</p>
          <div class="shop_img"><img src="../../web/sns/images/logo/logo_14.jpg" alt="エスペランサ"></div>
          <div class="sns">
              <div class="instagram"><a href="https://www.instagram.com/esperanza_hiroshimaparco/" target="_blank"><img src="images/icon_insta.png" width="17" height="17" alt="Instagram"></a></div>
              <div class="twitter"><img src="images/noicon_tw.png" width="21" height="17" alt="Twitter"></div>
          </div>
      </li>
      <li class="shop">
          <p class="floor">本館5F</p>
          <div class="shop_img"><img src="../../web/sns/images/logo/logo_15.jpg" alt="メンズビギ"></div>
          <div class="sns">
              <div class="instagram"><a href="https://www.instagram.com/mensbigihiroshima/" target="_blank"><img src="images/icon_insta.png" width="17" height="17" alt="Instagram"></a></div>
              <div class="twitter"><img src="images/noicon_tw.png" width="21" height="17" alt="Twitter"></div>
          </div>
      </li>
      <li class="shop">
          <p class="floor">本館5F</p>
          <div class="shop_img"><img src="../../web/sns/images/logo/logo_16.jpg" alt="291295オム"></div>
          <div class="sns">
              <div class="instagram"><a href="https://instagram.com/hiroshima_parco29" target="_blank"><img src="images/icon_insta.png" width="17" height="17" alt="Instagram"></a></div>
              <div class="twitter"><img src="images/noicon_tw.png" width="21" height="17" alt="Twitter"></div>
          </div>
      </li>
      <li class="shop">
          <p class="floor">本館5F</p>
          <div class="shop_img"><img src="../../web/sns/images/logo/logo_17.jpg" alt="ストララッジョ"></div>
          <div class="sns">
              <div class="instagram"><a href="https://instagram.com/straraggio_hiroshima" target="_blank"><img src="images/icon_insta.png" width="17" height="17" alt="Instagram"></a></div>
              <div class="twitter"><img src="images/noicon_tw.png" width="21" height="17" alt="Twitter"></div>
          </div>
      </li>
      <li class="shop">
          <p class="floor">本館5F</p>
          <div class="shop_img"><img src="../../web/sns/images/logo/logo_18.jpg" alt="シュリセル"></div>
          <div class="sns">
              <div class="instagram"><a href="https://instagram.com/schlussel.hiroshima" target="_blank"><img src="images/icon_insta.png" width="17" height="17" alt="Instagram"></a></div>
              <div class="twitter"><img src="images/noicon_tw.png" width="21" height="17" alt="Twitter"></div>
          </div>
      </li>
      <li class="shop">
          <p class="floor">本館6F</p>
          <div class="shop_img"><img src="../../web/sns/images/logo/logo_19.jpg" alt="マンハッタンポーテージ"></div>
          <div class="sns">
              <div class="instagram"><a href="https://instagram.com/mp_hiroshima" target="_blank"><img src="images/icon_insta.png" width="17" height="17" alt="Instagram"></a></div>
              <div class="twitter"><a href="https://twitter.com/mp_hiroshima" target="_blank"><img src="images/icon_tw.png" width="21" height="17" alt="Twitter"></a></div>
          </div>
      </li>
      <li class="shop">
          <p class="floor">本館6F</p>
          <div class="shop_img"><img src="../../web/sns/images/logo/logo_20.jpg" alt="シャッポ"></div>
          <div class="sns">
              <div class="instagram"><a href="https://instagram.com/shappo_parco_hiroshima" target="_blank"><img src="images/icon_insta.png" width="17" height="17" alt="Instagram"></a></div>
              <div class="twitter"><img src="images/noicon_tw.png" width="21" height="17" alt="Twitter"></div>
          </div>
      </li>
      <li class="shop">
          <p class="floor">本館7F</p>
          <div class="shop_img"><img src="../../web/sns/images/logo/logo_21.jpg" alt="ナイスクラップ"></div>
          <div class="sns">
              <div class="instagram"><img src="images/noicon_insta.png" width="17" height="17" alt="Instagram"></div>
              <div class="twitter"><a href="https://twitter.com/Nchiroshima" target="_blank"><img src="images/icon_tw.png" width="21" height="17" alt="Twitter"></a></div>
          </div>
      </li>
      <li class="shop">
          <p class="floor">本館8F</p>
          <div class="shop_img"><img src="../../web/sns/images/logo/logo_22.jpg" alt="セポ"></div>
          <div class="sns">
              <div class="instagram"><img src="images/noicon_insta.png" width="17" height="17" alt="Instagram"></div>
              <div class="twitter"><a href="https://twitter.com/cepo133parco" target="_blank"><img src="images/icon_tw.png" width="21" height="17" alt="Twitter"></a></div>
          </div>
      </li>
      <li class="shop">
          <p class="floor">本館8F</p>
          <div class="shop_img"><img src="../../web/sns/images/logo/logo_23.jpg" alt="メディストア"></div>
          <div class="sns">
              <div class="instagram"><a href="https://instagram.com/medistore_hiroshima" target="_blank"><img src="images/icon_insta.png" width="17" height="17" alt="Instagram"></a></div>
              <div class="twitter"><a href="https://twitter.com/ms＿hiroshima" target="_blank"><img src="images/icon_tw.png" width="21" height="17" alt="Twitter"></a></div>
          </div>
      </li>
      <li class="shop">
          <p class="floor">本館8F</p>
          <div class="shop_img"><img src="../../web/sns/images/logo/logo_24.jpg" alt="プードゥドゥ"></div>
          <div class="sns">
              <div class="instagram"><a href="https://instagram.com/pdd_hiroshima" target="_blank"><img src="images/icon_insta.png" width="17" height="17" alt="Instagram"></a></div>
              <div class="twitter"><img src="images/noicon_tw.png" width="21" height="17" alt="Twitter"></div>
          </div>
      </li>
      <li class="shop">
          <p class="floor">本館8F</p>
          <div class="shop_img"><img src="../../web/sns/images/logo/logo_25.jpg" alt="ダブルクローゼット"></div>
          <div class="sns">
              <div class="instagram"><a href="https://instagram.com/wclosethiroshima" target="_blank"><img src="images/icon_insta.png" width="17" height="17" alt="Instagram"></a></div>
              <div class="twitter"><img src="images/noicon_tw.png" width="21" height="17" alt="Twitter"></div>
          </div>
      </li>
      <li class="shop">
          <p class="floor floor_new">新館B1F</p>
          <div class="shop_img"><img src="../../web/sns/images/logo/logo_26.jpg" alt="ユナイテッドアローズ"></div>
          <div class="sns">
              <div class="instagram"><a href="https://instagram.com/UA_HIROSHIMA" target="_blank"><img src="images/icon_insta.png" width="17" height="17" alt="Instagram"></a></div>
              <div class="twitter"><a href="https://twitter.com/UA_HIROSHIMA" target="_blank"><img src="images/icon_tw.png" width="21" height="17" alt="Twitter"></a></div>
          </div>
      </li>
      <li class="shop">
          <p class="floor floor_new">新館B1F</p>
          <div class="shop_img"><img src="../../web/sns/images/logo/logo_27.jpg" alt="MSPCプロダクトソート"></div>
          <div class="sns">
              <div class="instagram"><img src="images/noicon_insta.png" width="17" height="17" alt="Instagram"></div>
              <div class="twitter"><a href="https://twitter.com/MSPC_HIROSHIMA" target="_blank"><img src="images/icon_tw.png" width="21" height="17" alt="Twitter"></a></div>
          </div>
      </li>
      <li class="shop">
          <p class="floor floor_new">新館B1F</p>
          <div class="shop_img"><img src="../../web/sns/images/logo/logo_28.jpg" alt="アバハウス・ラストワード"></div>
          <div class="sns">
              <div class="instagram"><a href="https://instagram.com/abahouse_hiroshima" target="_blank"><img src="images/icon_insta.png" width="17" height="17" alt="Instagram"></a></div>
              <div class="twitter"><img src="images/noicon_tw.png" width="21" height="17" alt="Twitter"></div>
          </div>
      </li>
      <li class="shop">
          <p class="floor floor_new">新館B1F</p>
          <div class="shop_img"><img src="../../web/sns/images/logo/logo_29.jpg" alt="チューズライフバイキャサリンハムネット"></div>
          <div class="sns">
              <div class="instagram"><a href="https://instagram.com/katharine_hamnett_hiroshima" target="_blank"><img src="images/icon_insta.png" width="17" height="17" alt="Instagram"></a></div>
              <div class="twitter"><img src="images/noicon_tw.png" width="21" height="17" alt="Twitter"></div>
          </div>
      </li>
      <li class="shop">
          <p class="floor floor_new">新館3F</p>
          <div class="shop_img"><img src="../../web/sns/images/logo/logo_30.jpg" alt="ネストローブ"></div>
          <div class="sns">
              <div class="instagram"><img src="images/noicon_insta.png" width="17" height="17" alt="Instagram"></div>
              <div class="twitter"><a href="https://twitter.com/nR_hiroshima" target="_blank"><img src="images/icon_tw.png" width="21" height="17" alt="Twitter"></a></div>
          </div>
      </li>
      <li class="shop">
          <p class="floor floor_new">新館4F</p>
          <div class="shop_img"><img src="../../web/sns/images/logo/logo_31.jpg" alt="ネストローブコンフェクト"></div>
          <div class="sns">
              <div class="instagram"><img src="images/noicon_insta.png" width="17" height="17" alt="Instagram"></div>
              <div class="twitter"><a href="https://twitter.com/Nconfect" target="_blank"><img src="images/icon_tw.png" width="21" height="17" alt="Twitter"></a></div>
          </div>
      </li>
      <li class="shop">
          <p class="floor floor_new">新館6F</p>
          <div class="shop_img"><img src="../../web/sns/images/logo/logo_32.jpg" alt="サンキューマート"></div>
          <div class="sns">
              <div class="instagram"><img src="images/noicon_insta.png" width="17" height="17" alt="Instagram"></div>
              <div class="twitter"><a href="https://twitter.com/390hiroshimaP" target="_blank"><img src="images/icon_tw.png" width="21" height="17" alt="Twitter"></a></div>
          </div>
      </li>
      <li class="shop">
          <p class="floor floor_new">新館9F</p>
          <div class="shop_img"><img src="../../web/sns/images/logo/logo_33.jpg" alt="タワーレコード"></div>
          <div class="sns">
              <div class="instagram"><img src="images/noicon_insta.png" width="17" height="17" alt="Instagram"></div>
              <div class="twitter"><a href="https://twitter.com/TOWER_Hiroshima" target="_blank"><img src="images/icon_tw.png" width="21" height="17" alt="Twitter"></a></div>
          </div>
      </li>
      <li class="shop ishop"></li>
  </ul>
</div>

<!-- <footer class="page-footer">
<div class="footer_bottom">
  <p class="btn_siteTop"><a href="http://hiroshima.parco.jp/page/storeinformation/index?type=recommend&lang=eng" target="blank"><img src="images/hiroshima_top.png" width="242" alt="HIROSHIMA PARCO TOP"></a></p>
  <p class="copyright"><img src="images/copyright.png" width="175" alt="CopyRight"></p>
</div>

<div class="pageTop">
  <a href="#Top"><img src="images/pagetop.png" width="45" alt="ページTOP"></a>
</div>

</footer> -->

<footer class="page-ft">
    <div class="btn"><a href="#Top"><img src="images/page_top.png" alt="" width="75" height="29"></a></div>

  <!-- <div class="pageTop">
    <a href="#Top"><img src="images/pagetop.png" width="45" alt="ページTOP"></a>
  </div> -->

<div class="snavi">
  <ul class="clearfix">
  <li class="snavi02"><div class="fb-like" data-href="http%3a%2f%2fsendai%2eparco%2ejp%2fweb%2fsns%2f" data-layout="button" data-action="like" data-show-faces="false" data-share="false"></div></li>
  <li class="snavi01"><a href="https://twitter.com/share" class="twitter-share-button" data-url="http://hiroshima.parco.jp/web/sns/" data-text="SNS LIST｜広島PARCO" data-lang="ja" data-count="none">ツイート</a>
  </li>
  <li class="snavi03">

  <!-- +1 ボタン を表示したい位置に次のタグを貼り付けてください。 -->
  <div class="g-plusone" data-size="tall" data-annotation="none" data-href="http://hiroshima.parco.jp/web/sns/"></div>

  <!-- 最後の +1 ボタン タグの後に次のタグを貼り付けてください。 -->
  <script type="text/javascript">
    window.___gcfg = {lang: 'ja'};

    (function() {
      var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
      po.src = 'https://apis.google.com/js/platform.js';
      var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
    })();
  </script></li>
  <li class="snavi04"><script type="text/javascript" src="//media.line.me/js/line-button.js?v=20140411" ></script>
  <script type="text/javascript">
  new media_line_me.LineButton({"pc":true,"lang":"ja","type":"a","text":"http://hiroshima.parco.jp/s/sns/","withUrl":false});
  </script>
  </li>
  </ul>
  <!-- /snavi --></div>

<p class="parco-top"><a href="/spage2/" target="_blank"><img src="images/hiroshima_top.png" width="242" alt="PARCO TOP"></a></p>
<p class="copyright"><img src="images/copyright.png" alt="copyright©PARCO.co.,ltd. all rights reserved." width="237" height="7"></p>
<!-- /.page-ft --></footer>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
<script>
  $(function(){
    $('a[href^="#"]').click(function() {
       var speed = 300;
       var href = $(this).attr("href");
       var target = $(href == "#" || href == "" ? 'html' : href);
       var position = target.offset().top;
       $('body,html').animate({scrollTop:position}, speed, 'swing');
       return false;
    });
  });
</script>

</body>
</html>
