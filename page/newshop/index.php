<?php

include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/global-config.php';
//下記のパス「demo」を変更
include_once $_SERVER['DOCUMENT_ROOT'].'/page/newshop/assets/inc/config.php';

$nowURL = $_SERVER['HTTP_HOST'];
$pageURL = STORE_NAME.'.parco.jp';
$devURL = 'dev-'.STORE_NAME.'-parco.sc-concierge.jp';

//ドメインの判定
if ($nowURL === $pageURL || $devURL === $pageURL) {
  $productionFlag = true; //本番とdev
} else {
  $productionFlag = false; //上記以外
}

if ($productionFlag === true) {
  //basic include
  include_once $_SERVER['DOCUMENT_ROOT'] . '/inc/page_include.php';
}

//ページ用の変数
$pege_title = '2021 AUTUMN NEW ＆ RENEWAL ｜広島PARCO';
$pege_description = '2021 AUTUMN NEW ＆ RENEWAL 広島PARCO。この秋新たにOPENするショップ情報をご紹介します。';
$pege_keywords = '';
$page_shareurl = 'https://'.STORE_NAME.'.parco.jp/page/newshop/'; //必ずディレクトリ名を変更する

?>
<!DOCTYPE html>
<html lang="ja">

<head>
  <?php include_once $_SERVER['DOCUMENT_ROOT'].'/page/assets/inc/meta.php';?>
  <?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/tagmanager1.php';?>
  <?php include_once $_SERVER['DOCUMENT_ROOT'].'/'.DIRNAME.'/assets/inc/css.php';?>
</head>

<body class="<?php echo STORE_NAME; ?> page-tag" id="top">
  <?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/tagmanager2.php';?>
  <?php include_once $_SERVER['DOCUMENT_ROOT'].'/page/assets/inc/svgs.php';?>
  <div class="wrapper">
    <?php
/**
 * Header
 */
?>
    <?php include_once $_SERVER['DOCUMENT_ROOT'].'/'.DIRNAME.'/assets/inc/header.php'; ?>
    <?php
/**
 * Main contents
 */
?>
    <main class="main-contents">
      <div class="newrenew-block" id="shoplist" v-cloak>　
        <div class="list-block">
          <h2 class="list-ttl">PICK UP</h2>
          <ul class="item-list01 js-tabarea" v-cloak>
            <li class="item" v-for="(listPickup, index) in listPickup" v-bind:key="index">
              <?php /* 表で表示 */ ?>
              <div class="front-cont">
                <div class="badge-l">
                  <p class="date" v-if="listPickup.date != ''">{{listPickup.date}}</p>
                  <p class="weeks" v-if="listPickup.weeks != ''">{{listPickup.weeks}}</p>
                  <p class="new-renew">{{listPickup.new_renew}}</p>
                </div>
                <div class="img-wrap">
                  <div v-if="listPickup.badge != ''" class="badge-r">
                    <p class="txt">{{listPickup.badge}}</p>
                  </div>
                  <div class="img"><img :src="listPickup.image01" :alt="listPickup.name">
                  </div>
                  <!-- /.img-wrap -->
                </div>
                <div class="detail">
                  <div class="logo"><img :src="listPickup.logo" :alt="listPickup.name"></div>
                  <p class="shop">{{listPickup.name}}</p>
                  <p class="floor">{{listPickup.yakata}} {{listPickup.floor}}</p>
                  <p class="ctg">{{listPickup.category_name}}</p>
                  <div class="more"><button class="js-modalopen" :data-modal="'modalPickup-'+index">+ MORE</button></div>
                  <!-- /.detail -->
                </div>
                <!-- /.front-cont -->
              </div>
              <?php /* モーダル  */ ?>
              <div :class="'modal-block js-modal newrenew-modal modalPickup-' + index">
                <div class="modal-bg js-modalclose"></div>
                <div class="modal-wrap">
                  <button class="modal-close js-modalclose"></button>
                  <div class="modal-cont">
                    <div class="badge-l">
                      <p class="date" v-if="listPickup.date != ''">{{listPickup.date}}</p>
                      <p class="weeks" v-if="listPickup.weeks != ''">{{listPickup.weeks}}</p>
                      <p class="new-renew">{{listPickup.new_renew}}</p>
                    </div>
                    <p v-if="listPickup.badge != ''" class="badge-r"><span class="txt">{{listPickup.badge}}</span></p>
                    <div class="cont-wrap">
                      <div class="img-wrap">
                        <div class="img-slider js-imgslider">
                          <div class="img"><img :src="listPickup.image01" :alt="listPickup.name"></div>
                          <div class="img" v-if="listPickup.image02 != ''"><img :src="listPickup.image02" :alt="listPickup.name"></div>
                          <div class="img" v-if="listPickup.image03 != ''"><img :src="listPickup.image03" :alt="listPickup.name"></div>
                          <div class="img" v-if="listPickup.image04 != ''"><img :src="listPickup.image04" :alt="listPickup.name"></div>
                          <div class="img" v-if="listPickup.image05 != ''"><img :src="listPickup.image05" :alt="listPickup.name"></div>
                        </div>
                        <!-- /.img-wrap -->
                      </div>
                      <div class="detail">
                        <div class="logo"><img :src="listPickup.logo" :alt="listPickup.name"></div>
                        <p class="shop">{{listPickup.name}}</p>
                        <p class="floor">{{listPickup.yakata}} {{listPickup.floor}}</p>
                        <p class="ctg">{{listPickup.category_name}}</p>
                        <p class="introduction" v-html="brTxt(listPickup.text)"></p>
                        <div class="open-event" v-if="listPickup.opening != ''">
                          <p class="ttl">オープニング企画</p>
                          <p class="txt" v-html="brTxt(listPickup.opening)">企画内容</p>
                        </div>
                        <!-- /.detail -->
                      </div>
                    </div>
                    <ul class="link-list">
                      <li v-if="listPickup.link_page != ''" class="link-item home"><a :href="listPickup.link_page" target="_blank"><img src="/<?php echo DIRNAME; ?>/assets/images/ico_home.png" alt="HOME"></a></li>

                      <li v-if="listPickup.link_recruit != ''" class="link-item recruit"><a :href="listPickup.link_recruit" target="_blank"><img src="/<?php echo DIRNAME; ?>/assets/images/ico_recruit.png" alt="STAFF RECRUIT"></a></li>

                      <li v-if="listPickup.link_twitter != ''" class="link-item tw"><a :href="listPickup.link_twitter" target="_blank"><img src="/<?php echo DIRNAME; ?>/assets/images/ico_tw.png" alt="Twitter"></a></li>

                      <li v-if="listPickup.link_facebook != ''" class="link-item fb"><a :href="listPickup.link_facebook" target="_blank"><img src="/<?php echo DIRNAME; ?>/assets/images/ico_fb.png" alt="Facebook"></a></li>
                      
                      <li v-if="listPickup.link_instagram != ''" class="link-item ig"><a :href="listPickup.link_instagram" target="_blank"><img src="/<?php echo DIRNAME; ?>/assets/images/ico_ig.png" alt="Instagram"></a></li>
                      <!-- /.link-list -->
                    </ul>
                  </div>
                  <div class="modal-arrow prev" v-if=modalPrev(index)><button class="js-modalopen" :data-modal="'modalPickup-'+ (index - 1)">前のアイテム</button></div>
                  <div class="modal-arrow next" v-if=modalNext(index,'listPickup')><button class="js-modalopen" :data-modal="'modalPickup-'+ (index + 1)">次のアイテム</button></div>
                  <div class="modal-arrow-close"><button class="js-modalclose">× CLOSE</button></div>
                  <!-- /.modal-block -->
                </div>
              </div>
            </li>
          </ul>
        </div>
        <div class="list-block" id="ctg01">
          <ul class="item-list02 js-tabarea" v-cloak>
            <li class="item" v-for="(list01, index) in list01" v-bind:key="index">
              <div class="item-inner">
                <?php /* 表で表示 */ ?>
                <div class="front-cont">
                  <div class="img-wrap">
                    <div class="badge-l">
                      <p class="date" v-if="list01.date != ''">{{list01.date}}</p>
                      <p class="weeks" v-if="list01.weeks != ''">{{list01.weeks}}</p>
                      <p class="new-renew">{{list01.new_renew}}</p>
                    </div>
                    <div v-if="list01.badge != ''" class="badge-r">
                      <p class="txt">{{list01.badge}}</p>
                    </div>
                    <div class="img"><img :src="list01.image01" :alt="list01.name"></div>
                    <!-- /.img-wrap -->
                  </div>
                  <div class="detail">
                    <div class="logo"><img :src="list01.logo" :alt="list01.name"></div>
                    <p class="shop js-shop">{{list01.name}}</p>
                    <p class="floor">{{list01.yakata}} {{list01.floor}}</p>
                    <p class="ctg">{{list01.category_name}}</p>
                    <div class="more"><button class="js-modalopen" :data-modal="'modal01-'+index">+ MORE</button></div>
                    <!-- /.detail -->
                  </div>
                  <!-- /.front-cont -->
                </div>
                <?php /* モーダル  */ ?>
                <div :class="'modal-block js-modal newrenew-modal modal01-' + index">
                  <div class="modal-bg js-modalclose"></div>
                  <div class="modal-wrap">
                    <button class="modal-close js-modalclose"></button>
                    <div class="modal-cont">
                      <div class="badge-l">
                        <p class="date" v-if="list01.date != ''">{{list01.date}}</p>
                        <p class="weeks" v-if="list01.weeks != ''">{{list01.weeks}}</p>
                        <p class="new-renew">{{list01.new_renew}}</p>
                      </div>
                      <p v-if="list01.badge != ''" class="badge-r"><span class="txt">{{list01.badge}}</span></p>
                      <div class="cont-wrap">
                        <div class="img-wrap">
                          <div class="img-slider js-imgslider">
                            <div class="img"><img :src="list01.image01" :alt="list01.name"></div>
                            <div class="img" v-if="list01.image02 != ''"><img :src="list01.image02" :alt="list01.name"></div>
                            <div class="img" v-if="list01.image03 != ''"><img :src="list01.image03" :alt="list01.name"></div>
                            <div class="img" v-if="list01.image04 != ''"><img :src="list01.image04" :alt="list01.name"></div>
                            <div class="img" v-if="list01.image05 != ''"><img :src="list01.image05" :alt="list01.name"></div>
                          </div>
                          <!-- /.img-wrap -->
                        </div>
                        <div class="detail">
                          <div class="logo"><img :src="list01.logo" :alt="list01.name"></div>
                          <p class="shop">{{list01.name}}</p>
                          <p class="floor">{{list01.yakata}} {{list01.floor}}</p>
                          <p class="ctg">{{list01.category_name}}</p>
                          <p class="introduction" v-html="brTxt(list01.text)"></p>
                          <div class="open-event" v-if="list01.opening != ''">
                            <p class="ttl">オープニング企画</p>
                            <p class="txt" v-html="brTxt(list01.opening)">企画内容</p>
                          </div>
                          <!-- /.detail -->
                        </div>
                      </div>
                      <ul class="link-list">
                        <li v-if="list01.link_page != ''" class="link-item home"><a :href="list01.link_page" target="_blank"><img src="/<?php echo DIRNAME; ?>/assets/images/ico_home.png" alt="HOME"></a></li>
                        <li v-if="list01.link_recruit != ''" class="link-item recruit"><a :href="list01.link_recruit" target="_blank"><img src="/<?php echo DIRNAME; ?>/assets/images/ico_recruit.png" alt="STAFF RECRUIT"></a></li>
                        <li v-if="list01.link_twitter != ''" class="link-item tw"><a :href="list01.link_twitter" target="_blank"><img src="/<?php echo DIRNAME; ?>/assets/images/ico_tw.png" alt="Twitter"></a></li>
                        <li v-if="list01.link_facebook != ''" class="link-item fb"><a :href="list01.link_facebook" target="_blank"><img src="/<?php echo DIRNAME; ?>/assets/images/ico_fb.png" alt="Facebook"></a></li>
                        <li v-if="list01.link_instagram != ''" class="link-item ig"><a :href="list01.link_instagram" target="_blank"><img src="/<?php echo DIRNAME; ?>/assets/images/ico_ig.png" alt="Instagram"></a></li>
                        <!-- /.link-list -->
                      </ul>
                    </div>
                    <div class="modal-arrow prev" v-if=modalPrev(index)><button class="js-modalopen" :data-modal="'modal01-'+ (index - 1)">前のアイテム</button></div>
                    <div class="modal-arrow next" v-if=modalNext(index,'list01')><button class="js-modalopen" :data-modal="'modal01-'+ (index + 1)">次のアイテム</button></div>
                    <div class="modal-arrow-close"><button class="js-modalclose">× CLOSE</button></div>
                    <!-- /.modal-block -->
                  </div>
                </div>
              </div>
            </li>
          </ul>
        </div><!-- /.list-block -->
      </div>
    </main>
    <?php
/**
 * Footer
 */
?>
    <?php include_once $_SERVER['DOCUMENT_ROOT'].'/page/assets/inc/footer.php'; ?>
  </div>
  <!-- Javascript -->
  <script>
  const list = '/<?php echo DIRNAME; ?>/assets/data/shoplist.json';
  </script>
  <?php include_once $_SERVER['DOCUMENT_ROOT'].'/'.DIRNAME.'/assets/inc/js.php';?>
  <script src="https://cdn.jsdelivr.net/npm/vue@2.6.10/dist/vue.js"></script>
  <script src="https://www.promisejs.org/polyfills/promise-7.0.4.min.js"></script>
  <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
  <script src="/<?php echo DIRNAME; ?>/assets/js/lib/slick.min.js"></script>
  <script src="/<?php echo DIRNAME; ?>/assets/js/newrenew.js"></script>
  <!-- /Javascript -->
</body>

</html>